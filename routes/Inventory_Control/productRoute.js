const express = require('express');
const router = express.Router();

const productController = require('../../controllers/Inventory_Control/productController');

router.get('/product',productController.list);
router.get('/product/add',productController.add);
router.post('/product/save',productController.save);
router.get('/product/detail/:pdid',productController.detail);
router.get('/product/disable/:id',productController.disable);
router.get('/product/enable/:id',productController.enable);
router.get('/product/edit/:id',productController.edit);
router.post('/product/update/:id',productController.update);


router.get('/product/transactionhistory/:pdid',productController.transactionhistory);



router.get('/api/product/deletephoto/:pdid/:photoid',productController.deletephoto);
router.get('/api/product/changephotomain/:pdid/:photoid',productController.changephotomain);
router.get('/api/getgroup',productController.getgroup);
router.post('/api/addgroup/add',productController.addgroup);
router.get('/api/updategroup/:id/:va',productController.updategroup);
router.get('/api/delgroup/:id/',productController.delgroup);

module.exports = router;
