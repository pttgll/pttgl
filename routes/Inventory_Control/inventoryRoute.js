const express = require('express');
const router = express.Router();

const inventoryController = require('../../controllers/Inventory_Control/inventoryController');

router.get('/inventory',inventoryController.list);
router.get('/inventory/add',inventoryController.add);
router.post('/inventory/save',inventoryController.save);

router.get('/inventory/detail/:id',inventoryController.detail);
router.get('/inventory/productdetail/:pdid/:itid',inventoryController.productdetail);

router.get('/inventory/disableinventory/:itid',inventoryController.disableinventory);
router.get('/inventory/enableinventory/:itid',inventoryController.enableinventory);
router.get('/inventory/disable/:pdid/:itid',inventoryController.disable);
router.get('/inventory/enable/:pdid/:itid',inventoryController.enable);
router.get('/inventory/edit/:id',inventoryController.edit);
router.post('/inventory/update/:id',inventoryController.update);
// router.get('/product/transactionhistory/:pdid',productController.transactionhistory);

router.get('/inventory/transfer',inventoryController.transfer);
router.get('/inventory/transferadd',inventoryController.transferadd);
router.post('/inventory/transfersave',inventoryController.transfersave);
router.get('/inventory/transferdelete/:tfid',inventoryController.transferdelete);
router.get('/inventory/transferedit/:tfid',inventoryController.transferedit);
router.post('/inventory/transferupdate/:tfid',inventoryController.transferupdate);
router.get('/inventory/transferallow/:tfid',inventoryController.transferallow);
router.get('/inventory/transferallowsave/:tfid',inventoryController.transferallowsave);
router.get('/inventory/transferallowcancel/:tfid',inventoryController.transferallowcancel);


router.get('/inventory/transaction_inventory/:itid',inventoryController.transaction_inventory);
router.get('/inventory/transaction_inventory_product/:itid/:pdid',inventoryController.transaction_inventory_product);


router.get('/inventory/addproduct/:itid',inventoryController.inventoryadd_product);
router.post('/inventory/addproduct_save/:itid',inventoryController.inventoryadd_product_save);

router.get('/inventory/editstock/:itid',inventoryController.editstock);
router.post('/inventory/updatestock/:itid',inventoryController.updatestock);



router.get('/api/getproductlist/:id',inventoryController.getproductlist);
router.get('/api/getproductunit/:id/:Aid',inventoryController.getproductunit);
router.get('/api/getproductchacknumber_product/:id/:Aid/:Uid',inventoryController.getproductchacknumber_product);
router.get('/api/getaddinventoryproduct/:itid/:pdid',inventoryController.getaddinventoryproduct);





module.exports = router;
