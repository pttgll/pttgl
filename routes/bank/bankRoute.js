const express = require('express');
const router = express.Router();
const bankController = require('../../controllers/bank/bankController');

router.get('/bank/list',bankController.bank_list);
router.get('/bank/add',bankController.bank_add);
router.post('/bank/save',bankController.bank_save);
router.get('/bank/edit/:modulebank',bankController.bank_edit);
router.post('/bank/update/:modulebank',bankController.bank_update);
router.get('/bank/delete/:modulebank',bankController.bank_delete);



module.exports = router;
