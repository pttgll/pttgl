const express = require('express');
const router = express.Router();

const homeController = require('../../controllers/General_Ledger_System/homeController');

router.get('/',homeController.List);
router.get('/homeInventoryList',homeController.homeInventoryList);

module.exports = router;
