const express = require('express');
const router = express.Router();

const companyController = require('../../controllers/General_Ledger_System/companyController');

router.get('/company', companyController.list);
router.get('/company/add',companyController.add);
router.post('/company/save',companyController.save);
router.get('/company/edit/:comid',companyController.edit);
router.post('/company/update/:comid',companyController.update);
router.get('/company/delete/:comid',companyController.delete);


module.exports = router;
