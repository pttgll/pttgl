const express = require('express');
const router = express.Router();

const glController = require('../../controllers/General_Ledger_System/glController');

router.get('/gl', glController.list);
router.get('/gl/detail/:comid/:glhid', glController.detail);
router.get('/gl/add', glController.add);
router.post('/gl/save', glController.save);
router.post('/gl/update/:comid/:glhid', glController.update);
router.get('/gl/delete/:glhid', glController.delete);

module.exports = router;
