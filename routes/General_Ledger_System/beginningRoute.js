const express = require('express');
const router = express.Router();
const beginningController = require('../../controllers/General_Ledger_System/beginningController');

router.get('/beginning',beginningController.list);
router.get('/beginning/add',beginningController.add);
router.post('/beginning/save',beginningController.save);
router.get('/beginning/edit/:year',beginningController.edit);
router.post('/beginning/update/:year',beginningController.update);
router.post('/beginning/delete/:year',beginningController.delete);
module.exports = router;
