const express = require('express');
const router = express.Router();

const projectglController = require('../../controllers/General_Ledger_System/projectglController');

router.get('/projectgl', projectglController.list);
router.get('/projectgl/add', projectglController.add);
router.post('/projectgl/save', projectglController.save);
router.get('/projectgl/edit/:proid', projectglController.edit);
router.post('/projectgl/update/:proid', projectglController.update);
router.post('/projectgl/delete/:proid',projectglController.delete);
module.exports = router;
