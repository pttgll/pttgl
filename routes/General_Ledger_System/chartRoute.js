const express = require('express');
const router = express.Router();
const chartController = require('../../controllers/General_Ledger_System/chartController');

router.get('/selectchart',chartController.selectchart);
router.post('/addchart',chartController.addchart);
router.post('/chart/save',chartController.save);

router.post('/chart/update',chartController.update);
router.post('/chart/del',chartController.delete);
router.get('/chart',chartController.chart);

router.get('/beginning_balance/edit',chartController.edit_balance);
router.get('/beginning_balance/edit/:category',chartController.edit_balance);
router.post('/beginning_balance/save',chartController.save_balance);

module.exports = router;
