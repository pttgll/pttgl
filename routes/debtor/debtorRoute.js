const express = require('express');
const router = express.Router();
const debtorController = require('../../controllers/debtor/debtorController');

router.get('/debtor',debtorController.list);
router.get('/debtor/add',debtorController.add);
router.post('/debtor/save',debtorController.save);
router.get('/debtor/edit/:dtid',debtorController.edit);
router.post('/debtor/update/:id',debtorController.update);
router.get('/debtor/delete/:id',debtorController.delete);

// ajax route
router.get('/api/debtor/edit/:dtid',debtorController.apiEdit);

module.exports = router;
