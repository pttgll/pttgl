const express = require('express');
const router = express.Router();
const salary_managementController = require('../../controllers/salary_management/salary_managementController');
const salary_Controller = require('../../controllers/salary_management/salaryController');

const department_Controller = require('../../controllers/salary_management/departmentController');//department

const up_list_Controller = require('../../controllers/salary_management/up_listController');//up_list

const down_list_Controller = require('../../controllers/salary_management/down_listController');//down_list

const payment_method_Controller = require('../../controllers/salary_management/payment_methodController');//payment_method

const salary_setting_Controller = require('../../controllers/salary_management/salary_setting_Controller');

const social_Controller = require('../../controllers/salary_management/social_Controller');

const payment_topic_Controller = require('../../controllers/salary_management/payment_topicController');//payment_topic

const employee_type_Controller = require('../../controllers/salary_management/employee_typeController');//employee_type

const employee_Controller = require('../../controllers/salary_management/employeeController');

const pay_save_Controller = require('../../controllers/salary_management/pay_saveController');


router.get('/salary',salary_Controller.list);
router.post('/salaryadd',salary_Controller.add);
router.get('/salaryajax/:id',salary_Controller.ajax);
router.get('/salarydelcon/:id',salary_Controller.delcon);
router.post('/salaryeditcon/:id',salary_Controller.edit);

router.get('/salarysetting',salary_setting_Controller.list);
router.post('/salarysettingadd',salary_setting_Controller.add);
router.get('/salarysettingajax/:id',salary_setting_Controller.ajax);
router.get('/salarysettingdelcon/:id',salary_setting_Controller.delcon);
router.post('/salarysettingeditcon/:id',salary_setting_Controller.edit);

router.get('/social',social_Controller.list);
router.post('/socialadd',social_Controller.add);
router.get('/socialajax/:id',social_Controller.ajax);
router.get('/socialdelcon/:id',social_Controller.delcon);
router.post('/socialeditcon/:id',social_Controller.edit);
router.get('/salary_management',salary_managementController.list);


router.get('/department',department_Controller.list);
router.post('/departmentadd',department_Controller.add);
router.get('/departmentajax/:id',department_Controller.ajax);
router.get('/departmentdelcon/:id',department_Controller.delcon);
router.post('/departmenteditcon/:id',department_Controller.edit);


router.get('/up_list',up_list_Controller.list);
router.post('/up_listadd',up_list_Controller.add);
router.get('/up_listajax/:id',up_list_Controller.ajax);
router.get('/up_listdelcon/:id',up_list_Controller.delcon);
router.post('/up_listeditcon/:id',up_list_Controller.edit);


router.get('/down_list',down_list_Controller.list);
router.post('/down_listadd',down_list_Controller.add);
router.get('/down_listajax/:id',down_list_Controller.ajax);
router.get('/down_listdelcon/:id',down_list_Controller.delcon);
router.post('/down_listeditcon/:id',down_list_Controller.edit);


router.get('/payment_method',payment_method_Controller.list);
router.post('/payment_methodadd',payment_method_Controller.add);
router.get('/payment_methodajax/:id',payment_method_Controller.ajax);
router.get('/payment_methoddelcon/:id',payment_method_Controller.delcon);
router.post('/payment_methodeditcon/:id',payment_method_Controller.edit);



router.get('/employee_type',employee_type_Controller.list);
router.post('/employee_typeadd',employee_type_Controller.add);
router.get('/employee_typeajax/:id',employee_type_Controller.ajax);
router.get('/employee_typedelcon/:id',employee_type_Controller.delcon);
router.post('/employee_typeeditcon/:id',employee_type_Controller.edit);

router.get('/employee',employee_Controller.list);
router.post('/employeeadd',employee_Controller.add);
router.get('/employeeajax/:id',employee_Controller.ajax);
router.get('/employeedelcon/:id',employee_Controller.delcon);
router.post('/employeeeditcon/:id',employee_Controller.edit);


router.get('/paymenttopic',payment_topic_Controller.list);
router.post('/paymenttopicadd',payment_topic_Controller.add);
router.post('/paymentaddform',payment_topic_Controller.addform);
router.post('/paymenttopicaddlist/:id',payment_topic_Controller.addpaylist);
router.post('/paymenttopicupdate/:id',payment_topic_Controller.update);
router.get('/paymentedit/:id',payment_topic_Controller.editform);
router.get('/paymenttopicdel/:id',payment_topic_Controller.delpaytop);



router.get('/pay_save',pay_save_Controller.list);
router.post('/pay_saveadd',pay_save_Controller.add);
router.get('/pay_saveajax/:id',pay_save_Controller.ajax);
router.get('/pay_savedelcon/:id',pay_save_Controller.delcon);
router.post('/pay_saveeditcon/:id',pay_save_Controller.edit);

module.exports = router;
