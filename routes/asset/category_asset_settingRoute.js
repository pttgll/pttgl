const express = require('express');
const router = express.Router();
const category_asset_settingController = require('../../controllers/asset/category_asset_settingController');

router.get('/category_asset_setting',category_asset_settingController.list);
router.get('/category_asset_setting/add',category_asset_settingController.add);
router.post('/category_asset_setting/save',category_asset_settingController.save);

module.exports = router;
