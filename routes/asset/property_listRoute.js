const express = require('express');
const router = express.Router();
const property_listController = require('../../controllers/asset/property_listController');

router.get('/property_list',property_listController.list);
router.get('/property_list/new',property_listController.new);
router.get('/property_list/add',property_listController.add);
router.post('/property_list/save',property_listController.save);

module.exports = router;
