const express = require('express');
const router = express.Router();
const record_depreciationController = require('../../controllers/asset/record_depreciationController');

router.get('/record_depreciation',record_depreciationController.list);
router.get('/record_depreciation/add',record_depreciationController.add);
router.post('/record_depreciation/save',record_depreciationController.save);

module.exports = router;
