const express = require('express');
const router = express.Router();
const quotationController = require('../../controllers/sell/quotationController');

router.get('/quotation',quotationController.quotation_list);
router.get('/quotation/add',quotationController.quotation_add);
router.post('/quotation/save',quotationController.quotation_save);
router.get('/quotation/edit/:quotation',quotationController.quotation_edit);
router.post('/quotation/update/:quotation',quotationController.quotation_update);
router.get('/quotation/delete/:quotation',quotationController.quotation_delete);
router.get('/quotation/detail/:quotation',quotationController.quotation_detail);
router.get('/quotation/readd/:quotation',quotationController.quotation_readd);
router.get('/quotation/purchase/:quotation',quotationController.quotation_purchase);
router.get('/quotation/bill/:quotation',quotationController.quotation_bill);
router.get('/quotation/taxinvoice/:quotation',quotationController.quotation_taxinvoice);
router.get('/quotation/cashsale/:quotation',quotationController.quotation_cashsale);
module.exports = router;
