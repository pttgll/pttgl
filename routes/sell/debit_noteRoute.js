const express = require('express');
const router = express.Router();
const debit_noteController = require('../../controllers/sell/debit_noteController');

router.get('/debit_note',debit_noteController.debitnote_list);
router.get('/debit_note/add',debit_noteController.debitnote_add);
router.post('/debit_note/save',debit_noteController.debitnote_save);
router.get('/debit_note/edit/:debitnote',debit_noteController.debitnote_edit);
router.post('/debit_note/update/:debitnote',debit_noteController.debitnote_update);
router.get('/debit_note/delete/:debitnote',debit_noteController.debitnote_delete);
router.get('/debit_note/detail/:debitnote',debit_noteController.debitnote_detail);
module.exports = router;
