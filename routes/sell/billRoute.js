const express = require('express');
const router = express.Router();
const billController = require('../../controllers/sell/billController');

router.get('/bill',billController.bill_list);
router.get('/bill/add',billController.bill_add);
router.post('/bill/save',billController.bill_save);
router.get('/bill/edit/:bill',billController.bill_edit);
router.post('/bill/update/:bill',billController.bill_update);
router.get('/bill/delete/:bill',billController.bill_delete);
router.get('/bill/detail/:bill',billController.bill_detail);
router.get('/bill/readd/:bill',billController.bill_readd);
router.get('/bill/taxinvoice/:bill',billController.bill_taxinvoice);
module.exports = router;
