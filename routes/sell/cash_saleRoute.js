const express = require('express');
const router = express.Router();
const cash_saleController = require('../../controllers/sell/cash_saleController');

router.get('/cash_sale',cash_saleController.cashsale_list);
router.get('/cash_sale/add',cash_saleController.cashsale_add);
router.post('/cash_sale/save',cash_saleController.cashsale_save);
router.get('/cash_sale/edit/:cashsale',cash_saleController.cashsale_edit);
router.post('/cash_sale/update/:cashsale',cash_saleController.cashsale_update);
router.get('/cash_sale/delete/:cashsale',cash_saleController.cashsale_delete);
router.get('/cash_sale/detail/:cashsale',cash_saleController.cashsale_detail);
router.get('/cash_sale/readd/:cashsale',cash_saleController.cashsale_readd);

module.exports = router;
