const express = require('express');
const router = express.Router();
const receiptController = require('../../controllers/sell/receiptController');

router.get('/billreceipt',receiptController.billreceipt_list);
router.get('/billreceipt/add',receiptController.billreceipt_add);
router.post('/billreceipt/save',receiptController.billreceipt_save);
router.get('/billreceipt/edit/:billreceipt',receiptController.billreceipt_edit);
router.post('/billreceipt/update/:billreceipt',receiptController.billreceipt_update);
router.get('/billreceipt/delete/:billreceipt',receiptController.billreceipt_delete);
router.get('/billreceipt/detail/:billreceipt',receiptController.billreceipt_detail);
module.exports = router;
