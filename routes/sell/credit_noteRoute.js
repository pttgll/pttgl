const express = require('express');
const router = express.Router();
const credit_noteController = require('../../controllers/sell/credit_noteController');

router.get('/credit_note',credit_noteController.creditnote_list);
router.get('/credit_note/add',credit_noteController.creditnote_add);
router.post('/credit_note/save',credit_noteController.creditnote_save);
router.get('/credit_note/edit/:creditnote',credit_noteController.creditnote_edit);
router.post('/credit_note/update/:creditnote',credit_noteController.creditnote_update);
router.get('/credit_note/delete/:creditnote',credit_noteController.creditnote_delete);
router.get('/credit_note/detail/:creditnote',credit_noteController.creditnote_detail);
module.exports = router;
