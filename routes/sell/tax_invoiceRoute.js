const express = require('express');
const router = express.Router();
const tax_invoiceController = require('../../controllers/sell/tax_invoiceController');

router.get('/tax_invoice',tax_invoiceController.taxinvoice_list);
router.get('/tax_invoice/add',tax_invoiceController.taxinvoice_add);
router.post('/tax_invoice/save',tax_invoiceController.taxinvoice_save);
router.get('/tax_invoice/edit/:taxinvoice',tax_invoiceController.taxinvoice_edit);
router.post('/tax_invoice/update/:taxinvoice',tax_invoiceController.taxinvoice_update);
router.get('/tax_invoice/delete/:taxinvoice',tax_invoiceController.taxinvoice_delete);
router.get('/tax_invoice/detail/:taxinvoice',tax_invoiceController.taxinvoice_detail);
router.get('/tax_invoice/readd/:taxinvoice',tax_invoiceController.taxinvoice_readd);
module.exports = router;
