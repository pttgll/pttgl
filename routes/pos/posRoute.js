const express = require('express');
const router = express.Router();
const posController = require('../../controllers/pos/posController');

router.get('/pos',posController.list);
router.get('/pos/add',posController.add);
router.post('/pos/save',posController.save);

module.exports = router;
