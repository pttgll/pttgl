const express = require('express');
const router = express.Router();
const creditorController = require('../../controllers/creditor/creditorController');

router.get('/creditor',creditorController.list);
router.get('/creditor/add',creditorController.add);
router.post('/creditor/save',creditorController.save);
router.get('/creditor/edit/:ctid',creditorController.edit);
router.post('/creditor/update/:id',creditorController.update);
router.get('/creditor/delete/:id',creditorController.delete);

router.get('/creditor/history/:creditor',creditorController.history);
// ajax route aaa
router.get('/api/creditor/edit/:ctid',creditorController.apiEdit);
router.get('/api/seachreceipt/:dateA/:dateB/:id',creditorController.seachreceipt);
router.get('/api/seachexpenses/:dateA/:dateB/:id',creditorController.seachexpenses);
router.get('/api/seachpayment/:dateA/:dateB/:id',creditorController.seachpayment);
module.exports = router;
