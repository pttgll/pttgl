const express = require('express');
const router = express.Router();
const settingController = require('../../controllers/settingController/settingController');


router.get('/setting/expenses_head',settingController.setting_expenses_head);
router.post('/setting/expenses_head/add',settingController.setting_expenses_headsave);
router.post('/setting/expenses_head/edit/:expenseshead',settingController.setting_expenses_headupdate);
router.get('/setting/expenses_head/delete/:expenseshead',settingController.setting_expenses_headdelete);

router.get('/setting/expenses_detail/:expensesdetail',settingController.setting_expenses_detail);
router.post('/setting/expenses_detail/add/:expensesdetail',settingController.setting_expenses_save);
router.post('/setting/expenses_detail/edit/:expensesdetail',settingController.setting_expenses_update);
router.get('/setting/expenses_detail/delete/:expensesdetail/:id',settingController.setting_expenses_delete);


module.exports = router;