const express = require('express');
const router = express.Router();
const purchase_orderController = require('../../controllers/buy/purchase_orderController');

router.get('/purchase_order',purchase_orderController.purchase_list);
router.get('/purchase_order/add',purchase_orderController.purchase_add);
router.post('/purchase_order/save',purchase_orderController.purchase_save);
router.get('/purchase_order/edit/:purchase',purchase_orderController.purchase_edit);
router.post('/purchase_order/update/:purchase',purchase_orderController.purchase_update);
router.get('/purchase_order/delete/:purchase',purchase_orderController.purchase_delete);
router.get('/purchase_order/detail/:purchase',purchase_orderController.purchase_detail);
router.get('/purchase_order/readd/:purchase',purchase_orderController.purchase_readd);
router.get('/purchase_order/purchase_to_receipt/add/:purchase',purchase_orderController.purchase_to_receipt);
router.get('/purchase_order/purchase_to_expenses/add/:purchase',purchase_orderController.purchase_to_expenses);

router.get('/api/optionunit/:pid',purchase_orderController.getoptionunit);


module.exports = router;
