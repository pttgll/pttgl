const express = require('express');
const router = express.Router();
const expensesController = require('../../controllers/buy/expensesController');
const product_receiptController = require('../../controllers/buy/product_receiptController');

router.get('/expenses',expensesController.expenses_list);
router.get('/expenses/add',expensesController.expenses_add);
router.post('/expenses/save',expensesController.expenses_save);
router.get('/expenses/edit/:expenses',expensesController.expenses_edit);
router.post('/expenses/update/:expenses',expensesController.expenses_update);
router.get('/expenses/detail/:expenses',expensesController.expenses_detail);
router.get('/expenses/delete/:expenses',expensesController.expenses_delete);
router.get('/expenses/readd/:expenses',expensesController.expenses_readd);

router.get('/expenses/pay/:expenses',product_receiptController.payment);
router.post('/expenses/paymentsave/:expenses',product_receiptController.payment_save);
router.get('/expenses/paymentdelete/delete/:expenses',product_receiptController.payment_delete);
router.get('/expenses/paymentmoney/delete/:expenses',product_receiptController.payment_moneydelete);

router.get('/api/expenses/:expenses',expensesController.api_expenses)
module.exports = router;