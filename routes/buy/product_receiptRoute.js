const express = require('express');
const router = express.Router();
const product_receiptController = require('../../controllers/buy/product_receiptController');

router.get('/product_receipt',product_receiptController.receipt_list);
router.get('/product_receipt/add',product_receiptController.receipt_add);
router.post('/product_receipt/save',product_receiptController.receipt_save);
router.get('/product_receipt/edit/:receipt',product_receiptController.receipt_edit);
router.post('/product_receipt/update/:receipt',product_receiptController.receipt_update);
router.get('/product_receipt/delete/:receipt',product_receiptController.receipt_delete);
router.get('/product_receipt/detail/:receipt',product_receiptController.receipt_detail);
router.get('/product_receipt/readd/:receipt',product_receiptController.receipt_readd);

router.get('/product_receipt/petty_cash/:receipt',product_receiptController.petty_cash);
router.post('/product_receipt/petty_cash/save/:receipt',product_receiptController.petty_cash_save);
router.get('/product_receipt/acc_transfer/:receipt',product_receiptController.acc_transfer);
router.post('/product_receipt/acc_transfer/save/:receipt',product_receiptController.acc_transfer_save);
router.get('/product_receipt/mastercard/:receipt',product_receiptController.mastercard);
router.post('/product_receipt/mastercard/save/:receipt',product_receiptController.mastercard_save);
router.get('/product_receipt/chanal_other/:receipt/:typeid',product_receiptController.chanal_other);
router.post('/product_receipt/chanal_other/save/:receipt',product_receiptController.chanal_other_save);

router.get('/product_receipt/pay/:receipt',product_receiptController.payment);
router.post('/product_receipt/paymentsave/:receipt',product_receiptController.payment_save);
router.get('/product_receipt/paymentdelete/delete/:receipt',product_receiptController.payment_delete);
router.get('/product_receipt/paymentmoney/delete/:receipt',product_receiptController.payment_moneydelete);

//ajex
router.get('/api/inventory/:inventory',product_receiptController.getinventory);
router.get('/api/typemachin/:typemachin',product_receiptController.gettypemachin);
module.exports = router;
