const express = require('express');
const router = express.Router();

const reportController = require('../controllers/reportController');

router.get('/report',reportController.report);
router.post('/general-journal/normal/report',reportController.normal);
router.post('/ledger-account/standartd/report',reportController.standartd);
router.post('/trial-balance/report',reportController.balance);
router.post('/income-statement/report',reportController.income);
router.post('/statement/report',reportController.statement);

router.get('/address/report/:type/:id',reportController.address);

module.exports = router;
