const express = require('express');
const router = express.Router();
const data_managementController = require('../../controllers/data_management/data_managementController');

router.get('/data_management',data_managementController.list);
router.get('/data_management/add',data_managementController.add);
router.post('/data_management/save',data_managementController.save);

module.exports = router;
