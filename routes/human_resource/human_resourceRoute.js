const express = require('express');
const router = express.Router();
const human_resourceController = require('../../controllers/human_resource/human_resourceController');

router.get('/human/list',human_resourceController.human_list);
router.get('/human/add',human_resourceController.human_add);
router.post('/human/save',human_resourceController.human_save);
router.get('/human/edit/:id',human_resourceController.human_edit);
router.post('/human/update/:id',human_resourceController.human_update);
// router.get('/human/delete/:hid',human_resourceController.human_delete);

router.get('/api/checkemail/:email',human_resourceController.checkemail);
router.get('/api/checkusername/:username',human_resourceController.checkusername);


module.exports = router;
