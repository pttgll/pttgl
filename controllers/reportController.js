const controller = {};
const request = require('request');

var d = new Date();
var h = d.getHours();
if (h < 10) {
  h = "0"+h;
}
var n = d.getMinutes();
if (n < 10) {
  n = "0"+n;
}
var s = d.getSeconds();
if (s < 10) {
  s = "0"+s;
}
var time = h+':'+n+':'+s;
var date = d.getDate();
if (date < 10) {
  date = "0"+date;
}
var m = d.getMonth()+1;
if (m < 10) {
  m = "0"+m;
}
var year = d.getFullYear();
var now_date = [{date:date+'/'+(m)+'/'+(year+543),time:time}];

var th_m = ['มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม'];

controller.report = (req, res) => {
  req.getConnection((err,conn)=>{
    conn.query('select * from reportname',(err,reportname)=>{
      conn.query('select * from reportpattern',(err,reportpattern)=>{
        conn.query('select * from company',(err,company)=>{
          conn.query('select * from chart order by code asc',(err,chart)=>{
            conn.query('select * from book',(err,book)=>{
              res.render('report/reportList.ejs',{
                reportname,reportpattern,company,chart,book,session:req.session
              });
            });
          });
        });
      });
    });
  });
};

controller.normal = (req, res) => {
  var data = req.body;
  ///----------first date
  var f_d = new Date(data.f_date);
  var f_date = f_d.getDate();
  var f_m = f_d.getMonth();
  var f_year = f_d.getFullYear();
  ///---------last date
  var l_d = new Date(data.l_date);
  var l_date = l_d.getDate();
  var l_m = l_d.getMonth();
  var l_year = l_d.getFullYear();

  var th_date = [{f_date:f_date+' '+th_m[f_m]+' '+(f_year+543),l_date:l_date+' '+th_m[l_m]+' '+(l_year+543)}];

  req.getConnection((err,conn)=>{
    conn.query('select * from company where id = ?',[41],(err,company)=>{
      conn.query('select glheads.id as glhid,concat(DATE_FORMAT(glheads.date, "%d/%m/"),(DATE_FORMAT(glheads.date, "%Y")+543)) as date,doc_no,detail from glheads join gldetail on gldetail.glheads_id = glheads.id where date >= ? and date <= ? group by glheads.id order by glheads.date asc, doc_no asc',[data.f_date,data.l_date],(err,glheads)=>{
        conn.query('select gldetail.*, chart.code as code, chart.name as chart, typeacc.name as typeacc from gldetail join glheads on gldetail.glheads_id = glheads.id join chart on gldetail.code_id = chart.id join typeacc on chart.typeAcc_id = typeacc.id where glheads.date >= ? and glheads.date <= ? and gldetail.dr > 0 order by glheads.date asc, gldetail.dr desc',[data.f_date,data.l_date],(err,gldetaildr)=>{
          conn.query('select gldetail.*, chart.code as code, chart.name as chart, typeacc.name as typeacc from gldetail join glheads on gldetail.glheads_id = glheads.id join chart on gldetail.code_id = chart.id join typeacc on chart.typeAcc_id = typeacc.id where glheads.date >= ? and glheads.date <= ? and gldetail.cr > 0 order by glheads.date asc, gldetail.dr desc',[data.f_date,data.l_date],(err,gldetailcr)=>{
            conn.query('select * from reportname where id = ?',[1],(err,reportname)=>{
              var data = {
                template:{'shortid':'rkJTnK2ce'},
                data:{
                  company,th_date,now_date,glheads,gldetaildr,gldetailcr,reportname
                }
              }
              // var options = {
              //   url: 'https://ptt.jsreportonline.net/api/report',
              //   method: 'POST',
              //   headers: {
              //     'Authorization': 'Basic bm9ucGhvdG9ob3VzZUBnbWFpbC5jb206RDEwMDAzNTYw',
              //     'Content-Type': 'application/json',
              //   },
              //   json: data
              // }

              var options = {
                url: 'http://10.0.110.40:5588/api/report',
                method: 'POST',
                // headers: {
                //     'Authorization': 'Basic YWRtaW46UGFzc3cwcmQ=',
                //     'Content-Type': 'application/json',
                //   },
                json: data
              }
              request(options).pipe(res);
            });
          });
        });
      });
    });
  });
};

controller.standartd = (req, res) => {
  var data = req.body;
  ///----------first date
  var f_d = new Date(data.f_date);
  var f_date = f_d.getDate();
  if (f_date < 10) {
    f_date = '0'+f_date;
  }
  var f_m = f_d.getMonth();
  var f_year = f_d.getFullYear();
  ///---------last date
  var l_d = new Date(data.l_date);
  var l_date = l_d.getDate();
  if (l_date < 10) {
    l_date = '0'+l_date;
  }
  var l_m = l_d.getMonth();
  var l_year = l_d.getFullYear();
  ///------date for use
  var th_date = [{f_date:f_date+' '+th_m[f_m]+' '+(f_year+543),l_date:l_date+' '+th_m[l_m]+' '+(l_year+543)}];

  var f_code = data.f_code.split(':')[0];
  var l_code = data.l_code.split(':')[0];

  req.getConnection((err,conn)=>{
    conn.query('select * from company where id = ?',[41],(err,company)=>{
      conn.query('select gldetail.* , chart.code as code, chart.name as chart, typeacc.name as typeacc, glheads.id as glhid, concat(DATE_FORMAT(glheads.date,"%d/%m/"),DATE_FORMAT(glheads.date,"%Y")+543) as glhdate, glheads.detail as glhdetail, glheads.doc_no as doc_no, glheads.date as hdate from gldetail join glheads on gldetail.glheads_id = glheads.id join chart on gldetail.code_id = chart.id join typeacc on chart.typeAcc_id = typeacc.id where glheads.date >= ? and glheads.date <= ? order by glheads.date asc, glheads.doc_no asc',[f_year+'-'+'01-01',data.l_date],(err,gldetail)=>{
        conn.query('select * from reportname where id = ?',[3],(err,reportname)=>{
          conn.query('select * from chart where chart.code >= ? and chart.code <= ? order by chart.code asc',[f_code,l_code],(err,chart)=>{
            var glkeep = [];
            var resoul = [];
            var nf_d = new Date(f_d.setHours(f_d.getHours()-7));
            var hdate;
            for (var i = 0; i < chart.length; i++) {
              var sumdr = 0;
              var sumcr = 0;
              var nsumdr = 0;
              var nsumcr = 0;
              var answer = 0;
              var nanswer = 0;
              var totaldr = 0;
              var totalcr = 0;
              nsumdr += chart[i].dr;
              nsumcr += chart[i].cr;
              if (f_m+1 < 10) {
                f_m = '0'+(f_m+1);
              }
              if (l_m+1 < 10) {
                l_m = '0'+(l_m+1);
              }
              for (var j = 0; j < gldetail.length; j++) {
                if (chart[i].id == gldetail[j].code_id) {
                  for (var k = 0; k < gldetail.length; k++) {
                    if (gldetail[j].glheads_id == gldetail[k].glheads_id) {
                      if (chart[i].id != gldetail[k].code_id) {
                        hdate = new Date(gldetail[k].hdate);
                        if (gldetail[j].cr > 0 && gldetail[k].dr > 0) {
                          if (gldetail[k].dr < gldetail[j].cr) {
                            if (hdate < nf_d){
                              nsumdr += gldetail[k].cr;
                              nsumcr += gldetail[k].dr;
                              totaldr += gldetail[k].cr;
                              totalcr += gldetail[k].dr;
                              if (chart[i].code.substring(0,1) == "1" || chart[i].code.substring(0,1) == "5") {
                                nanswer = nsumdr-nsumcr;
                              }else {
                                nanswer = nsumcr-nsumdr;
                              }
                            }else {
                              sumdr += gldetail[k].cr;
                              sumcr += gldetail[k].dr;
                              totaldr += gldetail[k].cr;
                              totalcr += gldetail[k].dr;
                              if (chart[i].code.substring(0,1) == "1" || chart[i].code.substring(0,1) == "5") {
                                answer = totaldr-totalcr+nanswer;
                              }else {
                                answer = totalcr-totaldr-nanswer;
                              }
                              var cr = gldetail[k].dr.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
                              var dr = null;
                              glkeep.push({chid:chart[i].id,glhdate:gldetail[k].glhdate,doc_no:gldetail[k].doc_no,chart:gldetail[k].chart,code:gldetail[k].code,glhdetail:gldetail[k].glhdetail,dr:dr,cr:cr,answer:answer.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')});
                            }
                          }else {
                            if (hdate < nf_d){
                              nsumdr += gldetail[j].dr;
                              nsumcr += gldetail[j].cr;
                              totaldr += gldetail[j].dr;
                              totalcr += gldetail[j].cr;
                              if (chart[i].code.substring(0,1) == "1" || chart[i].code.substring(0,1) == "5") {
                                nanswer = nsumdr-nsumcr;
                              }else {
                                nanswer = nsumcr-nsumdr;
                              }
                            }else {
                              sumdr += gldetail[j].dr;
                              sumcr += gldetail[j].cr;
                              totaldr += gldetail[j].dr;
                              totalcr += gldetail[j].cr;
                              if (chart[i].code.substring(0,1) == "1" || chart[i].code.substring(0,1) == "5") {
                                answer = totaldr-totalcr+nanswer;
                              }else {
                                answer = totalcr-totaldr-nanswer;
                              }
                              var cr = gldetail[j].cr.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
                              var dr = null;
                              glkeep.push({chid:chart[i].id,glhdate:gldetail[k].glhdate,doc_no:gldetail[k].doc_no,chart:gldetail[k].chart,code:gldetail[k].code,glhdetail:gldetail[k].glhdetail,dr:dr,cr:cr,answer:answer.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')});
                            }
                          }
                        }
                        if(gldetail[j].dr > 0 && gldetail[k].cr > 0){
                          if (gldetail[k].cr < gldetail[j].dr) {
                            if (hdate < nf_d){
                              nsumdr += gldetail[k].cr;
                              nsumcr += gldetail[k].dr;
                              totaldr += gldetail[k].cr;
                              totalcr += gldetail[k].dr;
                              if (chart[i].code.substring(0,1) == "1" || chart[i].code.substring(0,1) == "5") {
                                nanswer = nsumdr-nsumcr;
                              }else {
                                nanswer = nsumcr-nsumdr;
                              }
                            }else {
                              sumdr += gldetail[k].cr;
                              sumcr += gldetail[k].dr;
                              totaldr += gldetail[k].cr;
                              totalcr += gldetail[k].dr;
                              if (chart[i].code.substring(0,1) == "1" || chart[i].code.substring(0,1) == "5") {
                                answer = totaldr-totalcr;
                              }else {
                                answer = totalcr-totaldr;
                              }
                              var dr = gldetail[k].cr.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
                              var cr = null;
                              glkeep.push({chid:chart[i].id,glhdate:gldetail[k].glhdate,doc_no:gldetail[k].doc_no,chart:gldetail[k].chart,code:gldetail[k].code,glhdetail:gldetail[k].glhdetail,dr:dr,cr:cr,answer:answer.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')});
                            }
                          }else {
                            if (hdate < nf_d){
                              nsumdr += gldetail[j].dr;
                              nsumcr += gldetail[j].cr;
                              totaldr += gldetail[j].dr;
                              totalcr += gldetail[j].cr;
                              if (chart[i].code.substring(0,1) == "1" || chart[i].code.substring(0,1) == "5") {
                                nanswer = nsumdr-nsumcr;
                              }else {
                                nanswer = nsumcr-nsumdr;
                              }
                            }else {
                              sumdr += gldetail[j].dr;
                              sumcr += gldetail[j].cr;
                              totaldr += gldetail[j].dr;
                              totalcr += gldetail[j].cr;
                              if (chart[i].code.substring(0,1) == "1" || chart[i].code.substring(0,1) == "5") {
                                answer = totaldr-totalcr;
                              }else {
                                answer = totalcr-totaldr;
                              }
                              var dr = gldetail[j].dr.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
                              var cr = null;
                              glkeep.push({chid:chart[i].id,glhdate:gldetail[k].glhdate,doc_no:gldetail[k].doc_no,chart:gldetail[k].chart,code:gldetail[k].code,glhdetail:gldetail[k].glhdetail,dr:dr,cr:cr,answer:answer.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')});
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
              if (chart[i].code.substring(0,1) == "1" || chart[i].code.substring(0,1) == "5") {
                nanswer = nsumdr-nsumcr;
                var getdr = 0,getcr = 0;
                if (nanswer > 0) {
                  getdr = nanswer;
                }else {
                  getcr = Math.abs(nanswer);
                }
              }else {
                var getdr = 0,getcr = 0;
                if (nanswer > 0) {
                  getcr = nanswer;
                }else {
                  getdr = Math.abs(nanswer);
                }
                nanswer = nsumcr-nsumdr;
              }
              if (chart[i].code.substring(0,1) == "1" || chart[i].code.substring(0,1) == "5") {
                answer = (totaldr-totalcr);
              }else {
                answer = (totalcr-totaldr);
              }
              glkeep.unshift({chid:chart[i].id,glhdate:f_date+'/'+f_m+'/'+(f_year+543),doc_no:null,chart:"ยอดยกมา",code:null,glhdetail:null,dr:getdr.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'),cr:getcr.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'),answer:nanswer.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')});
              // glkeep.push({chid:chart[i].id,glhdate:l_date+'/'+l_m+'/'+(l_year+543),doc_no:null,chart:"ยอดยกไป",code:null,glhdetail:null,dr:null,cr:null,answer:answer.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')});
              resoul.push({chid:chart[i].id,dr:sumdr.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'),cr:sumcr.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'),answer:answer.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')});
            }
            var data = {
              template:{'shortid':'9O6HqACPi9'},
              data:{
                company,th_date,now_date,glkeep,reportname,chart,resoul
              }
            }
            // console.log(gldetail);
            // var options = {
            //   url: 'https://ptt.jsreportonline.net/api/report',
            //   method: 'POST',
            //   headers: {
            //     'Authorization': 'Basic bm9ucGhvdG9ob3VzZUBnbWFpbC5jb206RDEwMDAzNTYw',
            //     'Content-Type': 'application/json',
            //   },
            //   json: data
            // }

            var options = {
              url: 'http://10.0.110.40:5590/api/report',
              method: 'post',
              json: data
            }
            request(options).pipe(res);
          });
        });
      });
    });
  });
};

controller.balance = (req, res) => {
  var data = req.body;
  ///---------last date
  var l_d = new Date(data.l_date);
  var l_date = l_d.getDate();
  if (l_date < 10) {
    l_date = '0'+l_date;
  }
  var l_m = l_d.getMonth();
  var l_year = l_d.getFullYear();
  ///----------first date
  var f_d = new Date(l_year+'-'+'01-01');
  var f_date = f_d.getDate();
  if (f_date < 10) {
    f_date = '0'+f_date;
  }
  var f_m = f_d.getMonth();
  var f_year = f_d.getFullYear();
  ///------date for use
  var th_date = [{l_date:l_date+' '+th_m[l_m]+' '+(l_year+543)}];
  var pattern = data.pattern;

  req.getConnection((err,conn)=>{
    conn.query('select * from company where id = ?',[41],(err,company)=>{
      conn.query('select gldetail.* , chart.code as code, chart.name as chart, typeacc.name as typeacc, glheads.id as glhid, concat(DATE_FORMAT(glheads.date,"%d/%m/"),DATE_FORMAT(glheads.date,"%Y")+543) as glhdate, glheads.detail as glhdetail, glheads.doc_no as doc_no, glheads.date as hdate from gldetail join glheads on gldetail.glheads_id = glheads.id join chart on gldetail.code_id = chart.id join typeacc on chart.typeAcc_id = typeacc.id where glheads.date >= ? and glheads.date <= ? order by glheads.date asc, glheads.doc_no asc',[l_year+'-'+'01-01',data.l_date],(err,gldetail)=>{
        conn.query('select * from reportpattern where id = ?',[6],(err,reportpattern)=>{
          conn.query('select * from chart order by code asc',(err,chart)=>{
            var glkeep = [];
            var total = [];
            var totalbegdr = 0;
            var totalbegcr = 0;
            var totalnowdr = 0;
            var totalnowcr = 0;
            var totaldr = 0;
            var totalcr = 0;
            for (var i = 0; i < chart.length; i++) {
              if (!chart[i].dr) {
                chart[i].dr = 0;
              }
              if (!chart[i].cr) {
                chart[i].cr = 0;
              }
              var sumdr = 0;
              var sumcr = 0;
              var nowdr = 0;
              var nowcr = 0;
              var dr = 0;
              var cr = 0;
              for (var j = 0; j < gldetail.length; j++) {
                if (chart[i].id == gldetail[j].code_id) {
                  for (var k = 0; k < gldetail.length; k++) {
                    if (gldetail[j].glheads_id == gldetail[k].glheads_id) {
                      if (chart[i].id != gldetail[k].code_id) {
                        if (gldetail[j].cr > 0 && gldetail[k].dr > 0) {
                          if (gldetail[k].dr < gldetail[j].cr) {
                            sumdr += gldetail[k].cr;
                            sumcr += gldetail[k].dr;
                          }else {
                            sumdr += gldetail[j].dr;
                            sumcr += gldetail[j].cr;
                          }
                        }
                        if(gldetail[j].dr > 0 && gldetail[k].cr > 0){
                          if (gldetail[k].cr < gldetail[j].dr) {
                            sumdr += gldetail[k].cr;
                            sumcr += gldetail[k].dr;
                          }else {
                            sumdr += gldetail[j].dr;
                            sumcr += gldetail[j].cr;
                          }
                        }
                      }
                    }
                  }
                }
              }
              if ((sumdr-sumcr) > 0) {
                nowdr = (sumdr-sumcr);
              }else {
                nowcr = Math.abs(sumdr-sumcr);
              }
              if ((chart[i].dr-chart[i].cr)+(sumdr-sumcr) > 0) {
                dr = (chart[i].dr-chart[i].cr)+(sumdr-sumcr);
              }else {
                cr = Math.abs((chart[i].dr-chart[i].cr)+(sumdr-sumcr));
              }
              if (chart[i].dr != 0 || chart[i].cr != 0 || nowdr != 0 || nowcr != 0 || dr != 0 || cr != 0) {
                glkeep.push({chname:chart[i].name,chcode:chart[i].code,
                  begdr:chart[i].dr.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'),
                  begcr:chart[i].cr.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'),
                  nowdr:nowdr.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'),
                  nowcr:nowcr.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'),
                  dr:dr.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'),
                  cr:cr.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
                });
              }
              totalbegdr += chart[i].dr;
              totalbegcr += chart[i].cr;
              totalnowdr += nowdr;
              totalnowcr += nowcr;
              totaldr += dr;
              totalcr += cr;
            }
            total.push({totalbegdr:totalbegdr.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'),totalbegcr:totalbegcr.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'),totalnowdr:totalnowdr.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'),totalnowcr:totalnowcr.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'),totaldr:totaldr.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'),totalcr:totalcr.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')});
            var data = {
              template:{'shortid':'5TWI42Dcjz'},
              data:{
                company,th_date,now_date,gldetail,reportpattern,pattern,glkeep,total
              }
            }
            // var options = {
            //   url: 'https://ptt.jsreportonline.net/api/report',
            //   method: 'POST',
            //   headers: {
            //     'Authorization': 'Basic bm9ucGhvdG9ob3VzZUBnbWFpbC5jb206RDEwMDAzNTYw',
            //     'Content-Type': 'application/json',
            //   },
            //   json: data
            // }

            var options = {
              url: 'http://10.0.110.40:5593/api/report',
              method: 'post',
              json: data
            }
            request(options).pipe(res);
          });
        });
      });
    });
  });
};

controller.income = (req,res) => {
  var data = req.body;
  ///----------first date
  var f_d = new Date(data.f_date);
  var f_date = f_d.getDate();
  if (f_date < 10) {
    f_date = '0'+f_date;
  }
  var f_m = f_d.getMonth();
  var f_year = f_d.getFullYear();
  ///---------last date
  var l_d = new Date(data.l_date);
  var l_date = l_d.getDate();
  if (l_date < 10) {
    l_date = '0'+l_date;
  }
  var l_m = l_d.getMonth();
  var l_year = l_d.getFullYear();
  ///------date for use
  var th_date = [{f_date:f_date+' '+th_m[f_m]+' '+(f_year+543),l_date:l_date+' '+th_m[l_m]+' '+(l_year+543)}];
  var pattern = data.pattern;
  req.getConnection((err,conn)=>{
    conn.query('select * from company where id = ?',[41],(err,company)=>{
      conn.query('select * from reportpattern where id = ?',[7],(err,reportpattern)=>{
        conn.query('select typeacc.* from typeacc join chart on chart.typeAcc_id = typeacc.id where chart.code like "4%" or chart.code like "5%" group by typeacc.id',(err,typeacc)=>{
          conn.query('select chart.*, case when substring(code,1,1) = 4 then (sum(gldetail.cr)-sum(gldetail.dr)) else sum(gldetail.dr)-sum(gldetail.cr) end as sum from chart join gldetail on code_id = chart.id join glheads on gldetail.glheads_id = glheads.id where (code like "4%" or code like "5%") and glheads.date >= ? and glheads.date <= ? group by chart.id',[data.f_date,data.l_date],(err,chart)=>{
            conn.query('select * from category where id >= 4',(err,category)=>{
              var lastanswer = [];
              var keepchart = [];
              var keeptypeacc = [];
              var keepcategory = [];
              var lai = 0;
              var laj = 0;
              for (var i = 0; i < category.length; i++) {
                var sumcategory = 0;
                for (var j = 0; j < typeacc.length; j++) {
                  var sumtypeacc = 0;
                  if (typeacc[j].category_id == category[i].id) {
                    for (var k = 0; k < chart.length; k++) {
                      var sum = 0;
                      if (chart[k].typeAcc_id == typeacc[j].id) {
                        sumtypeacc += chart[k].sum;
                        if (chart[k].sum > 0) {
                          sum = chart[k].sum.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
                        }else {
                          sum = '('+Math.abs(chart[k].sum).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')+')';
                        }
                        keepchart.push({typeAcc_id:chart[k].typeAcc_id,name:chart[k].name,code:chart[k].code,sum:sum});
                      }
                    }
                    if (sumtypeacc != 0) {
                      sumcategory += sumtypeacc;
                      if (sumtypeacc > 0) {
                        sumtypeacc = sumtypeacc.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
                      }else {
                        sumtypeacc = '('+Math.abs(sumtypeacc).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')+')';
                      }
                      keeptypeacc.push({id:typeacc[j].id,category_id:typeacc[j].category_id,name:typeacc[j].name,sum:sumtypeacc});
                    }
                  }
                }
                if (category[i].id == 4) {
                  ali = sumcategory;
                }else {
                  alj = sumcategory;
                }
                keepcategory .push({id:category[i].id,name:category[i].name,sum:sumcategory.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')});
              }
              var lsum;
              if (ali-alj > 0) {
                lsum = (ali-alj).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
              }else {
                lsum = '('+(Math.abs(ali-alj)).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')+')';
              }
              lastanswer.push({name:'กำไรสุทธิ(ขาดทุนสุทธิ)',sum:lsum});
              var data = {
                template:{'shortid':'OYwPaaWcE2'},
                data:{
                  company,th_date,now_date,reportpattern,pattern,keeptypeacc,keepchart,keepcategory,lastanswer
                }
              }
              // var options = {
              //   url: 'https://ptt.jsreportonline.net/api/report',
              //   method: 'POST',
              //   headers: {
              //     'Authorization': 'Basic bm9ucGhvdG9ob3VzZUBnbWFpbC5jb206RDEwMDAzNTYw',
              //     'Content-Type': 'application/json',
              //   },
              //   json: data
              // }

              var options = {
                url: 'http://10.0.110.40:5591/api/report',
                method: 'post',
                json: data
              }
              request(options).pipe(res);
            });
          });
        });
      });
    });
  });
}

controller.statement = (req,res) => {
  var data = req.body;
  var l_d = new Date(data.l_date);
  var l_date = l_d.getDate();
  if (l_date < 10) {
    l_date = '0'+l_date;
  }
  var l_m = l_d.getMonth();
  var l_year = l_d.getFullYear();
  ///------date for use
  var th_date = [{l_date:l_date+' '+th_m[l_m]+' '+(l_year+543)}];
  var pattern = data.pattern;
  req.getConnection((err,conn)=>{
    conn.query('select * from company where id = ?',[41],(err,company)=>{
      conn.query('select * from reportpattern where id = ?',[8],(err,reportpattern)=>{
        conn.query('select typeacc.* from typeacc join chart on chart.typeAcc_id = typeacc.id where chart.code like "4%" or chart.code like "5%" group by typeacc.id',(err,typeacc1)=>{
          conn.query('select chart.*, case when substring(code,1,1) = 4 then (sum(cr)-sum(dr)) else sum(dr)-sum(cr) end as sum from chart join gldetail on code_id = chart.id join glheads on gldetail.glheads_id = glheads.id where (code like "4%" or code like "5%") and glheads.date >= ? and glheads.date <= ? group by chart.id',[l_year+'-01-01',data.l_date],(err,chart1)=>{
            conn.query('select typeacc.* from typeacc join chart on typeAcc_id = typeacc.id where chart.code like "1%" or chart.code like "2%" or chart.code like "3%" group by typeacc.id',(err,typeacc)=>{
              conn.query('select chart.*, case when substring(code,1,1) = 1 then (sum(gldetail.dr)-sum(gldetail.cr)) else sum(gldetail.cr)-sum(gldetail.dr) end as sum from gldetail join chart on code_id = chart.id join glheads on glheads_id = glheads.id where (chart.code like "1%" or chart.code like "2%" or chart.code like "3%") and glheads.date >= ? and glheads.date <= ? group by chart.id',[l_year+'-01-01',data.l_date],(err,chart)=>{
                console.log(err);
                conn.query('select * from category where id <= 3',(err,category)=>{
                  var lastanswer = [];
                  var keepchart = [];
                  var keeptypeacc = [];
                  var keepcategory = [];
                  var ln = 0;
                  for (var i = 0; i < category.length; i++) {
                    // --------------------------กำไรขาดทุน ----------------------//
                    // for (var j = 0; j < typeacc1.length; j++) {
                    //   var sumtypeacc1 = 0;
                    //   if (typeacc1[j].category_id == category[i].id) {
                    //     for (var k = 0; k < chart1.length; k++) {
                    //       var sum = 0;
                    //       if (chart1[k].typeAcc_id == typeacc1[j].id) {
                    //         sumtypeacc1 += chart1[k].sum;
                    //         }
                    //     }
                    //     if (sumtypeacc1 != 0) {
                    //       sumcategory1 += sumtypeacc1;
                    //     }
                    //   }
                    // }
                    // if (category[i].id == 4) {
                    //   ali = sumcategory;
                    // }else {
                    //   alj = sumcategory;
                    // }
                    // var lsum = (ali-alj);
                    // ------------------- แสดงฐษฯะการเงิน ---------//
                    var sumcategory = 0;
                    var sumt = 0;
                    for (var j = 0; j < typeacc.length; j++) {
                      var sumtypeacc = 0;
                      if (typeacc[j].category_id == category[i].id) {
                        var suml = 0;
                        for (var k = 0; k < chart.length; k++) {
                          if (chart[k].typeAcc_id == typeacc[j].id) {
                            var sums = 0;
                            sumtypeacc += chart[k].sum;
                            if (chart[k].sum < 0) {
                              sums = '('+Math.abs(chart[k].sum).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')+')';
                            }else {
                              sums = chart[k].sum.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
                            }
                            keepchart.push({typeAcc_id:chart[k].typeAcc_id,name:chart[k].name,code:chart[k].code,sum:sums});
                          }
                        }
                        if (sumtypeacc != 0) {
                          sumcategory += sumtypeacc;
                          if (sumtypeacc < 0) {
                            suml = '('+Math.abs(sumtypeacc).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')+')';
                          }else {
                            suml = sumtypeacc.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
                          }
                          keeptypeacc.push({id:typeacc[j].id,category_id:typeacc[j].category_id,name:typeacc[j].name,sum:suml});
                        }
                      }
                    }
                    if (category[i].id != 1) {
                      ln += sumcategory;
                    }
                    if (sumcategory < 0) {
                      sumt = '('+Math.abs(sumcategory).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')+')';
                    }else {
                      sumt = sumcategory.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
                    }
                    keepcategory .push({id:category[i].id,name:category[i].name,sum:sumt});
                  }
                  if (ln < 0) {
                    ln = '('+Math.abs(ln).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')+')';
                  }else {
                    ln = ln.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
                  }
                  lastanswer.push({name:'รวม'+category[1].name+'และ'+category[2].name,sum:ln})
                  var data = {
                    template:{'shortid':'GaZnAVQMBZ'},
                    data:{
                      company,th_date,now_date,reportpattern,pattern,keeptypeacc,keepchart,keepcategory,lastanswer
                    }
                  }
                  // var options = {
                  //   url: 'https://ptt.jsreportonline.net/api/report',
                  //   method: 'POST',
                  //   headers: {
                  //     'Authorization': 'Basic bm9ucGhvdG9ob3VzZUBnbWFpbC5jb206RDEwMDAzNTYw',
                  //     'Content-Type': 'application/json',
                  //   },
                  //   json: data
                  // }

                  var options = {
                    url: 'http://10.0.110.40:5592/api/report',
                    method: 'post',
                    json: data
                  }
                  request(options).pipe(res);
                });
              });
            });
          });
        });
      });
    });
  });
}

controller.address = (req,res) => {
  var url = req.params;
  req.getConnection((err,conn)=>{
    conn.query('select company.*, provinces.name_th as provinces, amphures.name_th as amphures, districts.name_th as districts, zip_code from company join provinces on provinces_id = provinces.id join amphures on amphures_id = amphures.id join districts on districts_id = districts.id where company.id = ?',[41],(err,company)=>{
      var contact, delivery;
      if (url.type == "debtor") {
        contact = "select * from debtors where id = ?";
        delivery = "select provinces.name_th as provinces, amphures.name_th as amphures, districts.name_th as districts, zip_code from debtordelivery join provinces on provinces_id1 = provinces.id join amphures on amphures_id1 = amphures.id join districts on districts_id1 = districts.id where debtors_id = ? and mainadd = 1";
      }else {
        contact = "select * from creditors where id = ?"
        delivery = "select provinces.name_th as provinces, amphures.name_th as amphures, districts.name_th as districts, zip_code from creditordelivery join provinces on provinces_id1 = provinces.id join amphures on amphures_id1 = amphures.id join districts on districts_id1 = districts.id where creditors_id = ? and mainadd = 1";
      }
      conn.query(contact,[url.id],(err,contactname)=>{
        conn.query(delivery,[url.id],(err,addressdelivery)=>{
          console.log(addressdelivery);
          var data = {
            template:{'shortid':'j2Kcg30lCY'},
            data:{
              company,contactname,addressdelivery
            }
          }
          // var options = {
          //   url: 'https://ptt.jsreportonline.net/api/report',
          //   method: 'POST',
          //   headers: {
          //     'Authorization': 'Basic bm9ucGhvdG9ob3VzZUBnbWFpbC5jb206RDEwMDAzNTYw',
          //     'Content-Type': 'application/json',
          //   },
          //   json: data
          // }

          var options = {
            url: 'http://10.0.110.40:5594/api/report',
            method: 'post',
            json: data
          }
          request(options).pipe(res);
        });
      });
    });
  });
}

module.exports = controller;
