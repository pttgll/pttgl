const controller = {};
const session = require("express-session");

controller.bank_list = (req, res) => {
    req.getConnection((err, conn) => {
        conn.query('select module_bank.*,bank.name as bank from module_bank join bank on module_bank.namebank_id = bank.id', (err, module_bank) => {
            res.render('bank/bank_list', {
                session: req.session, module_bank
            });
        });
    });
};

//----------------------------------------------------------------------------------
controller.bank_add = (req, res) => {
    req.getConnection((err, conn) => {
        conn.query('select * from bank', (err, bank) => {
            res.render('bank/bank_add', {
                bank,
                session: req.session
            });
        });
    });
};
//------------------------------------------------------------------------------------
controller.bank_save = (req, res) => {
    var data = req.body;
    req.getConnection((err, conn) => {
        conn.query('INSERT INTO module_bank (namebank_id,name_acc,number_acc,branch,type_acc,namecard_id,type_card,name_card,number_card,expiry_date,number_tail) values(?,?,?,?,?,?,?,?,?,?,?) ',
            [data.namebank_id, data.name_acc, data.number_acc, data.branch, data.type_acc, data.namecard_id, data.type_card, data.name_card, data.number_card, data.expiry_date, data.number_tail], (err, module_bank) => {
                res.redirect('/bank/list');
            });
    });
};
//-------------------------------------------------------------------------------------
controller.bank_edit = (req, res) => {
    var url = req.params;
    req.getConnection((err, conn) => {
        conn.query('select * from module_bank where id =  ?', [url.modulebank], (err, module_bank) => {
            conn.query('select * from bank', (err, bank) => {
                res.render('bank/bank_edit', {
                    session: req.session,module_bank,bank
                });
            });
        });
    });
};
//---------------------------------------------------------------------------------------------------
controller.bank_update = (req, res) => {
    var url = req.params;
    const data = req.body;
    req.getConnection((err, conn) => {
        conn.query('update module_bank set namebank_id = ?,name_acc = ?,number_acc = ?,branch = ?,type_acc = ?,namecard_id = ?,type_card = ?,name_card = ?,number_card = ?,expiry_date = ?,number_tail = ? where id = ?',
            [data.namebank_id, data.name_acc, data.number_acc, data.branch, data.type_acc, data.namecard_id, data.type_card, data.name_card, data.number_card, data.expiry_date, data.number_tail, url.modulebank], (err, module_bank) => {
                res.redirect('/bank/list');
            });
    })
};
//-------------------------------------------------------------------------------------------
controller.bank_delete = (req, res) => {
    var url = req.params;
    req.getConnection((err, conn) => {
        conn.query("Delete from module_bank where id =  ? ", [url.modulebank], (err, module_bank) => {
            res.redirect('/bank/list');
        });
    });
};

module.exports = controller;
