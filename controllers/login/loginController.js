
const controller = {};
const { v4: uuidv4 } = require('uuid');
const fs = require('fs');
var nodemailer = require('nodemailer');
var mail = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'sleepbbk@gmail.com',
        pass: 'alohomora00-'
    }
});

controller.list = (req, res) => {
    req.getConnection((err, conn) => {
        res.render('login/login.ejs', {
            session: req.session
        });

    });
}


controller.login = (req, res) => {
    var data = req.body;
    req.getConnection((err, conn) => {
        var uuu = data.username;
        var ppp = data.password;
        conn.query('select * from human_resource where username = ? ;', [uuu], (err, human_resource) => {
            if (human_resource.length > 0) {
                conn.query('select * from human_resource where username = ? and password = sha1(?) ;', [uuu, ppp + human_resource[0].password2], (err, human_resource2) => {
                    if (human_resource2.length > 0) {
                        req.session.loginWelcome = true;
                        req.session.login = true;
                        req.session.user_id = human_resource2[0].id;
                        req.session.user_permission_id = human_resource2[0].permission_id;
                        req.session.user_firstname = human_resource2[0].firstname;
                        req.session.user_lastname = human_resource2[0].lastname;
                        req.session.user_company_id = human_resource2[0].company_id;

                        req.session.sweetalert2check = 'true';
                        req.session.sweetalert2text = 'เข้าสู่ระบบ สำเร็จ!! /*/ สวัสดี คุณ '+human_resource2[0].firstname+' '+human_resource2[0].lastname;
                        res.redirect('/');
                    } else {
                        req.session.sweetalert2check = 'error';
                        req.session.sweetalert2text = 'ชื่อผู้ใช้ หรือ รหัสผ่าน /*/ ไม่ถูกต้อง';
                        // req.session.login = false;
                        // req.session.loginERROR = true;
                        res.redirect('/login');
                    }

                });

            } else {
                req.session.sweetalert2check = 'error';
                req.session.sweetalert2text = 'ชื่อผู้ใช้ หรือ รหัสผ่าน /*/ ไม่ถูกต้อง';
                // req.session.login = false;
                // req.session.loginERROR = true;
                res.redirect('/login');
            }

        });
    });
}

controller.logout = (req, res) => {
    req.getConnection((err, conn) => {
        req.session.sweetalert2check = 'info';
        req.session.sweetalert2text = 'ออกจากระบบ สำเร็จ!!!';
        // req.session.login = false;
        // req.session.logout = true;
        res.redirect('/login');
    });
}

controller.reset_password_page = (req, res) => {
    req.getConnection((err, conn) => {
        res.render('login/reset_password_page.ejs', {
            session: req.session
        });
    });
}


controller.reset_password_sendEmail = (req, res) => {
    var data = req.body;
    req.getConnection((err, conn) => {
        conn.query('select * from human_resource where email = ? ;', [data.email], (err, human_resource) => {
            // console.log(human_resource);
            if (human_resource.length > 0) {

                var mailOptions = {
                    // from: 'xxx',
                    to: data.email,
                    subject: 'คำขอแก้ไขรหัสผ่าน (Sending Email using Node.js)',
                    html: '<p> สวัดดีคุณ ' + human_resource[0].firstname + ' ' + human_resource[0].lastname + ' ท่านได้ทำการส่งคำขอแก้ไขรหัสผ่าน <a class="" href="http://localhost:8088/login/reset_password_pageedit?token=' + human_resource[0].token + '&email=' + human_resource[0].email + '"> คลิกที่นี่ </a> เพื่อไปที่หน้าแก้ไขรหัสผ่านของท่าน</p>'
                }

                mail.sendMail(mailOptions, function (error, info) {
                    console.log(error);
                    if (error) {
                        console.log(error);
                    } else {
                        console.log('Email sent: ' + info.response);
                    }
                });

                req.session.sweetalert2check = 'success';
                req.session.sweetalert2text = 'ส่งคำขอสำเร็จ!!';
                res.redirect('/login');


            } else {
                req.session.sweetalert2check = 'error';
                req.session.sweetalert2text = 'ไม่มีอีเมลนี้ในข้อมูลระบบ!!';
                // req.session.sendEmailERROR = true;
                res.redirect('/login/reset_password_page');
            }
        });
    });
}



controller.reset_password_pageedit = (req, res) => {
    req.getConnection((err, conn) => {
        conn.query('select * from human_resource where token = ? AND email = ? ;', [req.query.token, req.query.email], (err, human_resource) => {

            if (human_resource.length > 0) {
                res.render('login/reset_password_pageedit.ejs', {
                    session: req.session, token: req.query.token, email: req.query.email
                });
            } else {
                req.session.sweetalert2check = 'error';
                req.session.sweetalert2text = 'คำขอนี้ ไม่สามารถ ทำงานได้อีก /*/ กรุณาส่งคำขอใหม่อีกครั้ง';
                res.redirect('/login/reset_password_page');
            }

        });
    });
}

controller.reset_password_resetpassword = (req, res) => {
    var data = req.body;
    console.log(data);
    req.getConnection((err, conn) => {
        var token = uuidv4();
        var password2 = uuidv4();
        var password = data.password1 + password2;
        console.log(token);
        console.log(password2);
        console.log(password);
        conn.query('UPDATE human_resource set  token = ? ,password = sha1(?) ,password2 = ?  where token = ? AND email = ? ;', [token, password, password2, data.token, data.email], (err, human_resource) => {
            console.log(err);
            console.log(human_resource);
            req.session.sweetalert2check = 'success';
            req.session.sweetalert2text = 'เปลี่ยนแปลงรหัสผ่าน สำเร็จ';
            res.redirect('/login');
        });
    });
}









module.exports = controller;
