const session = require("express-session");
const controller = {};
const { v4: uuidv4 } = require('uuid');
const fs = require('fs');


///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_list
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.list = (req, res) => {
  req.getConnection((err, conn) => {
    conn.query('select * from debtors', (err, debtors) => {
      conn.query('select * from bank', (err, bank) => {
        conn.query('select * from provinces', (err, provinces) => {
          conn.query('select * from human', (err, human) => {
            conn.query('select * from chart where typeAcc_id = 1;', (err, chart) => {
              conn.query('select * from module where id = 3', (err, debtormodule) => {
                conn.query('select max(debtorID) as maxnumber from debtors where debtorID like ?', [debtormodule[0].char + "%"], (err, maxnumber) => {
                  res.render('debtor/debtor/debtorlist.ejs', {
                    debtors, bank, provinces, human, maxnumber, debtormodule, chart, session: req.session
                  });
                });
              });
            })
          });
        });
      });
    });
  });
};
///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_add
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.add = (req, res) => {
  req.getConnection((err, conn) => {
    conn.query('select * from debtors', (err, debtors) => {
      conn.query('select * from bank', (err, bank) => {
        conn.query('select * from human', (err, human) => {
          conn.query('select * from provinces', (err, provinces) => {
            conn.query('select * from chart where typeAcc_id = 1;', (err, chart) => {
              conn.query('select * from module where id = 3', (err, debtormodule) => {
                conn.query('select max(debtorID) as maxnumber from debtors where debtorID like ?', [debtormodule[0].char + "%"], (err, maxnumber) => {
                  res.render('debtor/debtor/debtoradd.ejs', {
                    bank, provinces, human, debtors, module, maxnumber, debtormodule, chart, session: req.session
                  });
                });
              });
            });
          });
        });
      });
    });
  });
};
///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_save
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.save = (req, res) => {
  const data = req.body;
  console.log(data);
  req.getConnection((err, conn) => {
    if (req.files) {
      var file = req.files.file;
      var type = file.name.split(".");
      var filename = uuidv4() + "." + type[type.length - 1];
      data.file = filename;
      file.mv("./public/debtors/" + filename, function (err) {
        if (err) { console.log(err) }
      });
      conn.query('INSERT INTO debtors (chart_id,human_id,crday,debtorID,company,tax,branch,branchID,branchname,address,provinces_id,amphures_id,districts_id,officephone,faxnumber,website,employee,email,mobile,bank_id,bankID,bankbranch,accounttype,file,note,address1,provinces_id1,amphures_id1,districts_id1,balance,crfull,crbalance)  values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)'
        , [data.chart_id, data.human_id, data.crday, data.debtorID, data.company, data.tax, data.branch, data.branchID, data.branchname, data.address, data.provinces_id, data.amphures_id, data.districts_id, data.officephone, data.faxnumber, data.website, data.employee, data.email, data.mobile, data.bank_id, data.bankID, data.bankbranch, data.accounttype, data.file, data.note, data.address1, data.provinces_id1, data.amphures_id1, data.districts_id1, data.balance, data.crfull, data.crfull], (err, debtor) => {
          if (err) {
            res.json(err);
          }
          res.redirect('/debtor');
        });
    } else {
      conn.query('INSERT INTO debtors (chart_id,human_id,crday,debtorID,company,tax,branch,branchID,branchname,address,provinces_id,amphures_id,districts_id,officephone,faxnumber,website,employee,email,mobile,bank_id,bankID,bankbranch,accounttype,file,note,address1,provinces_id1,amphures_id1,districts_id1,balance,crfull,crbalance) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)'
        , [data.chart_id, data.human_id, data.crday, data.debtorID, data.company, data.tax, data.branch, data.branchID, data.branchname, data.address, data.provinces_id, data.amphures_id, data.districts_id, data.officephone, data.faxnumber, data.website, data.employee, data.email, data.mobile, data.bank_id, data.bankID, data.bankbranch, data.accounttype, data.file, data.note, data.address1, data.provinces_id1, data.amphures_id1, data.districts_id1, data.balance, data.crfull, data.crfull], (err, debtor) => {
          if (err) {
            res.json(err);
          }
          res.redirect('/debtor');
        });
    }
  });
};
///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_edit
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.edit = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('select * from debtors where id = ?', [url.dtid], (err, debtor) => {
      conn.query('select * from bank', (err, bank) => {
        conn.query('select * from human', (err, human) => {
          conn.query('select * from module where id = 3', (err, debtormodule) => {
            conn.query('select max(debtorID) as maxnumber from debtors where debtorID like ?', [debtormodule[0].char + "%"], (err, maxnumber) => {
              conn.query('select * from provinces', (err, provinces) => {
                conn.query('select * from chart where typeAcc_id = 1;', (err, chart) => {
                  conn.query('select * from amphures where provinces_id = ?', [debtor[0].provinces_id], (err, amphures) => {
                    conn.query('select * from districts where amphures_id = ?', [debtor[0].amphures_id], (err, districts) => {
                      conn.query('select * from amphures where provinces_id = ?', [debtor[0].provinces_id1], (err, amphures1) => {
                        conn.query('select * from districts where amphures_id = ?', [debtor[0].amphures_id1], (err, districts1) => {
                          res.render('debtor/debtor/debtoredit', {
                            debtor, bank, provinces, amphures, districts, amphures1, districts1, human, debtormodule, maxnumber, chart, session: req.session
                          });
                        });
                      });
                    });
                  });
                });
              });
            });
          });
        });
      });
    });
  });
};
///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_update
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.update = (req, res) => {
  var data = req.body;
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('select * from debtors where id = ?', [url.id], (err, filet) => {
      if (req.files) {
        var file = req.files.file;
        var type = file.name.split(".");
        var filename = uuidv4() + "." + type[type.length - 1];
        data.file = filename;
        file.mv("./public/debtors/" + filename, function (err) {
          if (err) { console.log(err) }
        });
        if (filet[0].file) {
          fs.unlink("public/debtors/" + filet[0].file, function (err) {
            if (err) {
              console.log(err);
            }
          });
        }
        conn.query('UPDATE debtors SET ? WHERE id = ?;', [data, url.id], (err, debtors) => {
          if (err) {
            res.json(err);
          }
          res.redirect('/debtor');
        });
      } else {
        if (filet[0].file == data.file) {
          conn.query('UPDATE debtors SET ? WHERE id = ?;', [data, url.id], (err, debtors) => {
            if (err) {
              res.json(err);
            }
            res.redirect('/debtor');
          });
        } else {
          data.file = '';
          fs.unlink("public/debtors/" + filet[0].file, function (err) {
            if (err) {
              console.log(err);
            }
          });
          conn.query('UPDATE debtors SET ? WHERE id = ?;', [data, url.id], (err, debtors) => {
            if (err) {
              res.json(err);
            }
            res.redirect('/debtor');
          });
        }
      }
    });
  });
}
///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_delete
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.delete = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('select * from debtors where id = ?', [url.id], (err, filet) => {
      fs.unlink("public/debtors/" + filet[0].file, function (err) {
        conn.query('Delete from debtors where id = ?', [url.id], (err, debtor_2) => {
          res.redirect('/debtor');
        });
      });
    });
  });
};
///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_ajax
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.apiEdit = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('select * from debtors where id = ?', [url.dtid], (err, debtor) => {
      conn.query('select * from amphures where provinces_id = ?', [debtor[0].provinces_id], (err, amphures) => {
        conn.query('select * from districts where amphures_id = ?', [debtor[0].amphures_id], (err, districts) => {
          conn.query('select * from amphures where provinces_id = ?', [debtor[0].provinces_id1], (err, amphures1) => {
            conn.query('select * from districts where amphures_id = ?', [debtor[0].amphures_id1], (err, districts1) => {
              res.send({
                debtor, amphures, districts, amphures1, districts1
              });
            });
          });
        });
      });
    });
  });
};

module.exports = controller;
