session = require("express-session");
const controller = {};
const { v4: uuidv4 } = require('uuid');
const fs = require('fs');


///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_list
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.list = (req, res) => {
    req.getConnection((err, conn) => {
        conn.query('select * from creditors', (err, creditor) => {
            conn.query('select * from bank', (err, bank) => {
                conn.query('select * from provinces', (err, provinces) => {
                    conn.query('select * from human', (err, human) => {
                        conn.query('select * from chart where typeAcc_id = 5;', (err, chart) => {
                            conn.query('select * from module where id = 4', (err, creditormodule) => {
                                conn.query('select max(creditorID) as maxnumber from creditors where creditorID like ?', [creditormodule[0].char + "%"], (err, maxnumber) => {
                                    res.render('creditor/creditor/creditorlist.ejs', {
                                        creditor, human, bank, provinces, creditormodule, maxnumber, chart, session: req.session
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    });
};
///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_add
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.add = (req, res) => {
    req.getConnection((err, conn) => {
        conn.query('select * from creditor', (err, creditor) => {
            conn.query('select * from bank', (err, bank) => {
                conn.query('select * from chart where typeAcc_id = 5;', (err, chart) => {
                    conn.query('select * from human', (err, human) => {
                        conn.query('select * from provinces', (err, provinces) => {
                            conn.query('select * from module where id = 4', (err, creditormodule) => {
                                conn.query('select max(creditorID) as maxnumber from creditors where creditorID like ?', [creditormodule[0].char + "%"], (err, maxnumber) => {
                                    res.render('creditor/creditor/creditoradd.ejs', {
                                        creditor, bank, provinces, human, creditormodule, maxnumber, chart, session: req.session
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    });
};
///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_save
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.save = (req, res) => {
    const data = req.body;
    console.log(data);
    req.getConnection((err, conn) => {
        if (req.files) {
            var file = req.files.file;
            var type = file.name.split(".");
            var filename = uuidv4() + "." + type[type.length - 1];
            data.file = filename;
            file.mv("./public/creditors/" + filename, function (err) {
                if (err) { console.log(err) }
            });
            conn.query('INSERT INTO creditors (chart_id,human_id,crday,creditorID,company,tax,branch,branchID,branchname,address,provinces_id,amphures_id,districts_id,officephone,faxnumber,website,employee,email,mobile,bank_id,bankID,bankbranch,accounttype,file,note,address1,provinces_id1,amphures_id1,districts_id1,balance,crfull,crbalance)  values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)'
                , [data.chart_id, data.human_id, data.crday, data.debtorID, data.company, data.tax, data.branch, data.branchID, data.branchname, data.address, data.provinces_id, data.amphures_id, data.districts_id, data.officephone, data.faxnumber, data.website, data.employee, data.email, data.mobile, data.bank_id, data.bankID, data.bankbranch, data.accounttype, data.file, data.note, data.address1, data.provinces_id1, data.amphures_id1, data.districts_id1, data.balance, data.crfull, data.crfull], (err, creditor) => {
                    if (err) {
                        res.json(err);
                    }
                    res.redirect('/creditor');
                });
        } else {
            conn.query('INSERT INTO creditors (chart_id,human_id,crday,creditorID,company,tax,branch,branchID,branchname,address,provinces_id,amphures_id,districts_id,officephone,faxnumber,website,employee,email,mobile,bank_id,bankID,bankbranch,accounttype,file,note,address1,provinces_id1,amphures_id1,districts_id1,balance,crfull,crbalance)  values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)'
                , [data.chart_id, data.human_id, data.crday, data.debtorID, data.company, data.tax, data.branch, data.branchID, data.branchname, data.address, data.provinces_id, data.amphures_id, data.districts_id, data.officephone, data.faxnumber, data.website, data.employee, data.email, data.mobile, data.bank_id, data.bankID, data.bankbranch, data.accounttype, data.file, data.note, data.address1, data.provinces_id1, data.amphures_id1, data.districts_id1, data.balance, data.crfull, data.crfull], (err, creditor) => {
                    if (err) {
                        res.json(err);
                    }
                    res.redirect('/creditor');
                });
        }
    });
}
///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_edit
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.edit = (req, res) => {
    var url = req.params;
    req.getConnection((err, conn) => {
        conn.query('select * from creditors where id = ?', [url.ctid], (err, creditor) => {
            conn.query('select * from bank', (err, bank) => {
                conn.query('select * from human', (err, human) => {
                    conn.query('select * from chart where typeAcc_id = 5;', (err, chart) => {
                        conn.query('select * from module where id = 4', (err, creditormodule) => {
                            conn.query('select max(creditorID) as maxnumber from creditors where creditorID like ?', [creditormodule[0].char + "%"], (err, maxnumber) => {
                                conn.query('select * from provinces', (err, provinces) => {
                                    conn.query('select * from amphures where provinces_id = ?', [creditor[0].provinces_id], (err, amphures) => {
                                        conn.query('select * from districts where amphures_id = ?', [creditor[0].amphures_id], (err, districts) => {
                                            conn.query('select * from amphures where provinces_id = ?', [creditor[0].provinces_id1], (err, amphures1) => {
                                                conn.query('select * from districts where amphures_id = ?', [creditor[0].amphures_id1], (err, districts1) => {
                                                    res.render('creditor/creditor/creditoredit', {
                                                        creditor, bank, provinces, amphures, districts, amphures1, districts1, human, maxnumber, creditormodule, chart, session: req.session
                                                    });
                                                });
                                            });
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    });
};
///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_update
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.update = (req, res) => {
    var data = req.body;
    console.log(data);
    var url = req.params;
    req.getConnection((err, conn) => {
        conn.query('select * from creditors where id = ?', [url.id], (err, filec) => {
            if (req.files) {
                var file = req.files.file;
                var type = file.name.split(".");
                var filename = uuidv4() + "." + type[type.length - 1];
                data.file = filename;
                file.mv("./public/creditors/" + filename, function (err) {
                    if (err) { console.log(err) }
                });
                if (filec[0].file) {
                    fs.unlink("public/creditors/" + filec[0].file, function (err) {
                        if (err) {
                            console.log(err);
                        }
                    });
                }
                conn.query('UPDATE creditors SET ? WHERE id = ?;', [data, url.id], (err, creditor) => {
                    if (err) {
                        res.json(err);
                    }
                    res.redirect('/creditor');
                });
            } else {
                if (filec[0].file == data.file) {
                    conn.query('UPDATE creditors SET ? WHERE id = ?;', [data, url.id], (err, creditor) => {
                        if (err) {
                            res.json(err);
                        }
                        res.redirect('/creditor');
                    });
                } else {
                    data.file = '';
                    if (filec[0].file) {
                        fs.unlink("public/creditors/" + filec[0].file, function (err) {
                            if (err) {
                                console.log(err);
                            }
                        });
                    }
                    conn.query('UPDATE creditors SET ? WHERE id = ?;', [data, url.id], (err, creditor) => {
                        if (err) {
                            res.json(err);
                        }
                        res.redirect('/creditor');
                    });
                }
            }
        });
    });
}
///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_delete
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.delete = (req, res) => {
    var url = req.params;
    req.getConnection((err, conn) => {
        conn.query('select * from creditors where id = ?', [url.id], (err, filec) => {
            fs.unlink("public/creditors/" + filec[0].file, function (err) {
                conn.query('Delete from creditors where id = ?', [url.id], (err, creditor_2) => {
                    res.redirect('/creditor');
                });
            });
        });
    });
};
///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_history
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.history = (req, res) => {
    var url = req.params;
    req.getConnection((err, conn) => {
        conn.query('select receipt.*,status.name as status,DATE_FORMAT(DATE_ADD(receipt.date, INTERVAL 543 YEAR), " %d/%m/%Y" )dateday ,DATE_FORMAT(DATE_ADD(receipt.due, INTERVAL 543 YEAR), " %d/%m/%Y" )dueday from receipt  join status on receipt.status_id = status.id where receipt.creditors_id = ?;', [url.creditor], (err, receipt) => {
            conn.query('select expenses.*,status.name as status,DATE_FORMAT(DATE_ADD(expenses.date, INTERVAL 543 YEAR), " %d/%m/%Y" )dateday ,DATE_FORMAT(DATE_ADD(expenses.due, INTERVAL 543 YEAR), " %d/%m/%Y" )dueday from expenses  join status on expenses.status_id = status.id where expenses.creditors_id = ?;', [url.creditor], (err, expenses) => {
                conn.query('select * from (select   receiptchar as chars , receiptordernumber as number ,DATE_FORMAT(DATE_ADD(receipt.date, INTERVAL 543 YEAR), " %d/%m/%Y" ) as dateday , payment.net_payout as net_payout , receipt.creditors_name as creditorsname, receipt.granttotal as granttotal , status.name as status,receipt.balance as balance,receipt.creditors_id as creditors_id  from receipt join payment on receipt.id = payment.reference_id join status on receipt.status_id = status.id where receipt.creditors_id = ? and payment.module_id = ? and status_id = 5 ) as t union (select  expenseschar as chars , expenses.expensesnumber as number ,DATE_FORMAT(DATE_ADD(expenses.date, INTERVAL 543 YEAR), " %d/%m/%Y" ) as dateday, payment.net_payout as net_payout, expenses.creditors_name as creditorsname, expenses.granttotal as granttotal,status.name as status,expenses.balance as balance,expenses.creditors_id as  creditors_id  from  expenses join payment on expenses.id = payment.reference_id join status on expenses.status_id = status.id where expenses.creditors_id = ? and payment.module_id = ? and status_id = 5);', [url.creditor, 12, url.creditor, 20], (err, payment) => {
                    res.render('creditor/creditor/creditorhistory.ejs', {
                        session: req.session, receipt, expenses, payment
                    });
                });
            })
        });
    });
};
///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_ajax
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.apiEdit = (req, res) => {
    var url = req.params;
    req.getConnection((err, conn) => {
        conn.query('select * from creditors where id = ?', [url.ctid], (err, creditor) => {
            conn.query('select * from amphures where provinces_id = ?', [creditor[0].provinces_id], (err, amphures) => {
                conn.query('select * from districts where amphures_id = ?', [creditor[0].amphures_id], (err, districts) => {
                    conn.query('select * from amphures where provinces_id = ?', [creditor[0].provinces_id1], (err, amphures1) => {
                        conn.query('select * from districts where amphures_id = ?', [creditor[0].amphures_id1], (err, districts1) => {
                            res.send({
                                creditor, amphures, districts, amphures1, districts1
                            });
                        });
                    });
                });
            });
        });
    });
};

controller.seachreceipt = (req, res) => {
    var url = req.params;
    req.getConnection((err, conn) => {
        conn.query('select receipt.*,DATE_FORMAT(DATE_ADD(receipt.date, INTERVAL 543 YEAR), " %d/%m/%Y" )date,DATE_FORMAT(DATE_ADD(receipt.due, INTERVAL 543 YEAR), " %d/%m/%Y" )due,status.name as status from receipt join creditors on receipt.creditors_id = creditors.id join status on receipt.status_id = status.id where receipt.creditors_id = ? and receipt.date BETWEEN ? and  ?;', [url.id, url.dateA, url.dateB], (err, creditors) => {
            res.send({
                creditors
            });
        });
    });
};
controller.seachexpenses = (req, res) => {
    var url = req.params;
    req.getConnection((err, conn) => {
        conn.query('select expenses.*,DATE_FORMAT(DATE_ADD(expenses.date, INTERVAL 543 YEAR), " %d/%m/%Y" )date,DATE_FORMAT(DATE_ADD(expenses.due, INTERVAL 543 YEAR), " %d/%m/%Y" )due,status.name as status from expenses join creditors on expenses.creditors_id = creditors.id join status on expenses.status_id = status.id where expenses.creditors_id = ? and expenses.date BETWEEN ? and  ?;', [url.id, url.dateA, url.dateB], (err, creditors) => {
            res.send({ creditors });
        });
    });
};
controller.seachpayment = (req, res) => {
    var url = req.params;
    req.getConnection((err, conn) => {
        var text = "select * from (select   receiptchar as chars , receiptordernumber as number ,DATE_FORMAT(DATE_ADD(receipt.date, INTERVAL 543 YEAR), ' %d/%m/%Y' ) as dateday , payment.net_payout as net_payout , receipt.creditors_name as creditorsname, receipt.granttotal as granttotal , status.name as status,receipt.balance as balance,receipt.creditors_id as creditors_id  from receipt join payment on receipt.id = payment.reference_id join status on receipt.status_id = status.id where receipt.creditors_id = '" + url.id + "' and receipt.date BETWEEN '" + url.dateA + "' and  '" + url.dateB + "') as t union (select  expenseschar as chars , expenses.expensesnumber as number ,DATE_FORMAT(DATE_ADD(expenses.date, INTERVAL 543 YEAR), ' %d/%m/%Y' ) as dateday, payment.net_payout as net_payout, expenses.creditors_name as creditorsname, expenses.granttotal as granttotal,status.name as status,expenses.balance as balance,expenses.creditors_id as creditors_id from  expenses join payment on expenses.id = payment.reference_id join status on expenses.status_id = status.id where expenses.creditors_id = '" + url.id + "' and expenses.date BETWEEN '" + url.dateA + "' and  '" + url.dateB + "');";
        conn.query(text, (err, payment) => {
            res.send({ payment });
        });
    });
};
module.exports = controller;

