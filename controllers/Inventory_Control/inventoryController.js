
const controller = {};
const { v4: uuidv4 } = require('uuid');
const fs = require('fs');

controller.list = (req, res) => {
  req.getConnection((err, conn) => {
    conn.query('select * from inventory_list where enable = 1;', (err, inventory_list1) => {
      conn.query('select * from inventory_list  where enable = 0;', (err, inventory_list2) => {
        res.render('Inventory_Control/inventory/inventorylist.ejs', {
          session: req.session, inventory_list1, inventory_list2
        });
      });
    });
  });
}

controller.add = (req, res) => {
  req.getConnection((err, conn) => {
    res.render('Inventory_Control/inventory/inventoryadd.ejs', {
      session: req.session
    });
  });
}

controller.save = (req, res) => {
  var data = req.body;
  req.getConnection((err, conn) => {
    conn.query('INSERT INTO  inventory_list set ? ;', [data], (err, inventory_list) => {
      res.redirect('/inventory');
    });
  });
}


controller.detail = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('select * from inventory_list where id = ?; ', [url.id], (err, inventory_list) => {
      conn.query('select inventory.number_product as inventorynumber,inventory.cost_product as inventorycost,inventory.enable as inventoryEnable,inventory.alert as inventoryAlert,product_list.*,receipt_list.priceUnit,product_group.name as product_group,product_type_list.name as typename ,product_tax1.name as product_tax1,product_tax2.name as product_tax2  from product_list join product_type_list on product_list.product_type_list_id = product_type_list.id  join product_type on product_list.product_type_id = product_type.id join product_tax as product_tax1  on product_list.tax_sell_id = product_tax1.id join product_tax as product_tax2 on product_list.tax_buy_id = product_tax2.id join product_group on product_list.product_group_id=product_group.id left join (select product_id,((SUM(quantity*price))/(SUM(quantity))) as priceUnit from receipt_list group by product_id) as receipt_list on receipt_list.product_id = product_list.id join inventory on product_list.id = inventory.product_list_id where product_list.enable = 1 and inventory.inventory_list_id = ? ORDER BY product_list.id desc ; ', [url.id], (err, product_list) => {
        conn.query('select inventory.number_product as inventorynumber,inventory.cost_product as inventorycost,inventory.enable as inventoryEnable,inventory.alert as inventoryAlert,product_list.*,receipt_list.priceUnit,product_group.name as product_group,product_type_list.name as typename ,product_tax1.name as product_tax1,product_tax2.name as product_tax2  from product_list join product_type_list on product_list.product_type_list_id = product_type_list.id  join product_type on product_list.product_type_id = product_type.id join product_tax as product_tax1  on product_list.tax_sell_id = product_tax1.id join product_tax as product_tax2 on product_list.tax_buy_id = product_tax2.id join product_group on product_list.product_group_id=product_group.id left join (select product_id,((SUM(quantity*price))/(SUM(quantity))) as priceUnit from receipt_list group by product_id) as receipt_list on receipt_list.product_id = product_list.id join inventory on product_list.id = inventory.product_list_id where  inventory.inventory_list_id = ?  ORDER BY product_list.id desc ;', [url.id], (err, product_listDel) => {
          conn.query('select inventory.number_product as inventorynumber,inventory.cost_product as inventorycost,inventory.enable as inventoryEnable,inventory.alert as inventoryAlert,product_list.*,receipt_list.priceUnit,product_group.name as product_group,product_type_list.name as typename ,product_tax1.name as product_tax1,product_tax2.name as product_tax2  from product_list join product_type_list on product_list.product_type_list_id = product_type_list.id  join product_type on product_list.product_type_id = product_type.id join product_tax as product_tax1  on product_list.tax_sell_id = product_tax1.id join product_tax as product_tax2 on product_list.tax_buy_id = product_tax2.id join product_group on product_list.product_group_id=product_group.id left join (select product_id,((SUM(quantity*price))/(SUM(quantity))) as priceUnit from receipt_list group by product_id) as receipt_list on receipt_list.product_id = product_list.id join inventory on product_list.id = inventory.product_list_id where product_list.enable = 1 and inventory.enable = 1 and inventory.inventory_list_id = ? and product_list.product_type_list_id = 1 ORDER BY product_list.id desc ; ', [url.id], (err, product_list1) => {
            conn.query('select inventory.number_product as inventorynumber,inventory.cost_product as inventorycost,inventory.enable as inventoryEnable,inventory.alert as inventoryAlert,product_list.*,receipt_list.priceUnit,product_group.name as product_group,product_type_list.name as typename ,product_tax1.name as product_tax1,product_tax2.name as product_tax2  from product_list join product_type_list on product_list.product_type_list_id = product_type_list.id  join product_type on product_list.product_type_id = product_type.id join product_tax as product_tax1  on product_list.tax_sell_id = product_tax1.id join product_tax as product_tax2 on product_list.tax_buy_id = product_tax2.id join product_group on product_list.product_group_id=product_group.id left join (select product_id,((SUM(quantity*price))/(SUM(quantity))) as priceUnit from receipt_list group by product_id) as receipt_list on receipt_list.product_id = product_list.id join inventory on product_list.id = inventory.product_list_id where product_list.enable = 1 and inventory.enable = 1 and inventory.inventory_list_id = ? and product_list.product_type_list_id = 2 ORDER BY product_list.id desc ; ', [url.id], (err, product_list2) => {
              conn.query('select inventory.number_product as inventorynumber,inventory.cost_product as inventorycost,inventory.enable as inventoryEnable,inventory.alert as inventoryAlert,product_list.*,receipt_list.priceUnit,product_group.name as product_group,product_type_list.name as typename ,product_tax1.name as product_tax1,product_tax2.name as product_tax2  from product_list join product_type_list on product_list.product_type_list_id = product_type_list.id  join product_type on product_list.product_type_id = product_type.id join product_tax as product_tax1  on product_list.tax_sell_id = product_tax1.id join product_tax as product_tax2 on product_list.tax_buy_id = product_tax2.id join product_group on product_list.product_group_id=product_group.id left join (select product_id,((SUM(quantity*price))/(SUM(quantity))) as priceUnit from receipt_list group by product_id) as receipt_list on receipt_list.product_id = product_list.id join inventory on product_list.id = inventory.product_list_id where product_list.enable = 1 and inventory.enable = 1 and inventory.inventory_list_id = ? and product_list.product_type_list_id = 3 ORDER BY product_list.id desc ; ', [url.id], (err, product_list3) => {
                conn.query('select product_list .id as id,photo_product .photo as photo from product_list  left join photo_product on photo_product.product_list = product_list.id where photo_product.main =1;', (err, photo) => {
                  res.render('Inventory_Control/inventory/inventorydetail.ejs', {
                    session: req.session, inventory_list, product_list, product_listDel, product_list1, product_list2, product_list3, photo
                  });
                });
              });
            });
          });
        });
      });
    });
  });
}

controller.productdetail = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('select * from inventory_list where id = ? ; ', [url.itid], (err, inventory_list) => {
      conn.query('select * from inventory where product_list_id = ? and inventory_list_id = ? ; ', [url.pdid, url.itid], (err, inventory) => {
        conn.query('select product_type_list.name as product_type_list,product_list.*,receipt_list.priceUnit,product_group.name as product_groupname from product_list join product_group on product_list.product_group_id=product_group.id left join (select product_id,((SUM(quantity*price))/(SUM(quantity))) as priceUnit from receipt_list group by product_id) as receipt_list on receipt_list.product_id = product_list.id join product_type_list on product_type_list.id = product_list.product_type_list_id where product_list.id = ?', [url.pdid], (err, product_list) => {
          conn.query('select * from category_price  where category_price.product_list_id = ? order by category_price.no_unit asc; ', [url.pdid], (err, category_price) => {
            conn.query('select * from photo_product where product_list = ? order by id asc; ', [url.pdid], (err, photo) => {
              res.render('Inventory_Control/inventory/inventoryproductdetail.ejs', {
                session: req.session, product_list, category_price, photo, inventory, inventory_list
              });
            });
          });
        });
      });
    });
  });
}


controller.disableinventory = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('UPDATE  inventory_list set enable = 0 where id = ?;', [url.itid], (err, inventory_list) => {

      res.redirect('/inventory');
    });
  });
}

controller.enableinventory = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('UPDATE  inventory_list set enable = 1 where id = ? ;', [url.itid,], (err, inventory_list) => {
      res.redirect('/inventory');
    });
  });
}
controller.disable = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('UPDATE  inventory set enable = 0 where inventory_list_id = ? and product_list_id = ? ;', [url.itid, url.pdid], (err, inventory_list) => {

      res.redirect('/inventory/detail/' + url.itid);
    });
  });
}

controller.enable = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('UPDATE  inventory set enable = 1 where inventory_list_id = ? and product_list_id = ? ;', [url.itid, url.pdid], (err, inventory_list) => {
      res.redirect('/inventory/detail/' + url.itid);
    });
  });
}




controller.edit = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('select * from inventory_list where id = ?', [url.id], (err, inventory_list) => {
      res.render('Inventory_Control/inventory/inventoryedit.ejs', {
        session: req.session, inventory_list
      })
    });
  });
}

controller.update = (req, res) => {
  var data = req.body;
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('UPDATE  inventory_list set ? where id = ? ;', [data, url.id], (err, inventory_list) => {
      res.redirect('/inventory');
    });
  });
}




controller.transfer = (req, res) => {
  req.getConnection((err, conn) => {
    conn.query("select inventory_transfer_head.*,CONCAT(date_format(inventory_transfer_head.date, '%d-%m-'),(date_format(inventory_transfer_head.date, '%Y')+543)) as dates,a_inventory_list.id as a_id ,a_inventory_list.name as a_name ,b_inventory_list.id as b_id ,b_inventory_list.name as b_name from inventory_transfer_head join inventory_list as a_inventory_list on a_inventory_list.id = inventory_transfer_head.a_inventory join inventory_list as b_inventory_list on b_inventory_list.id = inventory_transfer_head.b_inventory order by inventory_transfer_head.id desc;", (err, inventory_transfer_head) => {
      conn.query("select inventory_transfer_head.*,CONCAT(date_format(inventory_transfer_head.date, '%d-%m-'),(date_format(inventory_transfer_head.date, '%Y')+543)) as dates,a_inventory_list.id as a_id ,a_inventory_list.name as a_name ,b_inventory_list.id as b_id ,b_inventory_list.name as b_name from inventory_transfer_head join inventory_list as a_inventory_list on a_inventory_list.id = inventory_transfer_head.a_inventory join inventory_list as b_inventory_list on b_inventory_list.id = inventory_transfer_head.b_inventory where inventory_transfer_head.allow = 1 order by inventory_transfer_head.id desc;", (err, inventory_transfer_head1) => {
        conn.query("select inventory_transfer_head.*,CONCAT(date_format(inventory_transfer_head.date, '%d-%m-'),(date_format(inventory_transfer_head.date, '%Y')+543)) as dates,a_inventory_list.id as a_id ,a_inventory_list.name as a_name ,b_inventory_list.id as b_id ,b_inventory_list.name as b_name from inventory_transfer_head join inventory_list as a_inventory_list on a_inventory_list.id = inventory_transfer_head.a_inventory join inventory_list as b_inventory_list on b_inventory_list.id = inventory_transfer_head.b_inventory where inventory_transfer_head.allow = 0 order by inventory_transfer_head.id desc;", (err, inventory_transfer_head0) => {
          res.render('Inventory_Control/inventory/inventorytransferlist.ejs', {
            session: req.session, inventory_transfer_head, inventory_transfer_head0, inventory_transfer_head1
          });
        });
      });
    });
  });
}

controller.transferadd = (req, res) => {
  req.getConnection((err, conn) => {
    conn.query('select * from inventory_list where enable = 1;', (err, inventory_list) => {
      res.render('Inventory_Control/inventory/inventorytransferadd.ejs', {
        session: req.session, inventory_list
      });
    });
  });
}

controller.transfersave = (req, res) => {
  var data = req.body;
  console.log(data);
  req.getConnection((err, conn) => {
    conn.query('INSERT INTO  inventory_transfer_head set no_transfer = ? ,a_inventory = ? ,b_inventory = ? ,date = ? ,allow = 0;', [data.no_transfer, data.a_inventory, data.b_inventory, data.date], (err, inventory_transfer_head) => {
      for (var i = 0; i < data.product_list_id.length - 1; i++) {
        conn.query('INSERT INTO  inventory_transfer_detail set inventory_transfer_head = ? ,product_list_id = ? ,number_product = ? , unit = ? ,cost_product = ? ;', [inventory_transfer_head.insertId, data.product_list_id[i], data.number_product[i], data.unit[i], data.cost_product[i]], (err, inventory_transfer_head) => {
          console.log(err);
        });
        if (data.add[i]==1) {
          conn.query('INSERT INTO  inventory set inventory_list_id = ? , product_list_id = ? ,number_product = 0 ,cost_product = 0 , alert = 0 ,enable = 1;', [data.b_inventory,data.product_list_id[i]], (err, xx) => {
          console.log(err);
          });
        }
      }
      res.redirect('/inventory/transfer');
    });


  });
}

controller.transferdelete = (req, res) => {
  var data = req.body;
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('DELETE FROM inventory_transfer_detail where inventory_transfer_head = ? ;', [url.tfid], (err, del) => {
      conn.query('DELETE FROM inventory_transfer_head where id = ? ;', [url.tfid], (err, del) => {
        res.redirect('/inventory/transfer');
      });
    });
  });
}


controller.transferedit = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('select * from inventory_list where enable = 1;', (err, inventory_list) => {
      conn.query("select *,date_format(inventory_transfer_head.date, '%Y-%m-%d') as dates from inventory_transfer_head where id = ?;", [url.tfid], (err, inventory_transfer_head) => {
        conn.query('select inventory_transfer_detail.*,product_list.name as productname ,category_price.id as unitid,category_price.name as unitname from inventory_transfer_detail join product_list on product_list.id = inventory_transfer_detail.product_list_id join category_price on category_price.id = inventory_transfer_detail.unit where inventory_transfer_head = ?;', [url.tfid], (err, inventory_transfer_detail) => {
          res.render('Inventory_Control/inventory/inventorytransferedit.ejs', {
            session: req.session, inventory_list, inventory_transfer_head, inventory_transfer_detail
          });
        });
      });
    });
  });
}
controller.transferupdate = (req, res) => {
  var data = req.body;
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('DELETE FROM inventory_transfer_detail where inventory_transfer_head = ? ;', [url.tfid], (err, del) => {
      conn.query('DELETE FROM inventory_transfer_head where id = ? ;', [url.tfid], (err, del) => {
      });
    });
    conn.query('INSERT INTO  inventory_transfer_head set no_transfer = ? ,a_inventory = ? ,b_inventory = ? ,date = ? ,allow = 0;', [data.no_transfer, data.a_inventory, data.b_inventory, data.date], (err, inventory_transfer_head) => {
      for (var i = 0; i < data.product_list_id.length - 1; i++) {
        conn.query('INSERT INTO  inventory_transfer_detail set inventory_transfer_head = ? ,product_list_id = ? ,number_product = ? , unit = ? ,cost_product = ? ;', [inventory_transfer_head.insertId, data.product_list_id[i], data.number_product[i], data.unit[i], data.cost_product[i]], (err, inventory_transfer_head) => {
        });
        if (data.add[i]==1) {
          conn.query('INSERT INTO  inventory set inventory_list_id = ? , product_list_id = ? ,number_product = 0 ,cost_product = 0 , alert = 0 ,enable = 1;', [data.b_inventory,data.product_list_id[i]], (err, xx) => {
          console.log(err);
          });
        }
      }
      res.redirect('/inventory/transfer');
    });
  });
}


controller.transferallow = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('select * from inventory_list where enable = 1;', (err, inventory_list) => {
      conn.query('select * from inventory_transfer_head where id = ?;', [url.tfid], (err, inventory_transfer_head) => {
        conn.query('select inventory_transfer_detail.*,product_list.name as productname ,category_price.name as unitname from inventory_transfer_detail join product_list on product_list.id = inventory_transfer_detail.product_list_id join category_price on category_price.id = inventory_transfer_detail.unit where inventory_transfer_head = ?;', [url.tfid], (err, inventory_transfer_detail) => {
          res.render('Inventory_Control/inventory/inventorytransferallow.ejs', {
            session: req.session, inventory_list, inventory_transfer_head, inventory_transfer_detail
          });
        });
      });
    });
  });
}

controller.transferallowsave = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('UPDATE  inventory_transfer_head set allow = 1 where id = ? ;', [url.tfid], (err, inventory_transfer_head) => {
    });
    conn.query('select  inventory_transfer_detail.product_list_id as product_list_id ,inventory_transfer_detail.number_product*category_price.quantity as number ,inventory_transfer_head.a_inventory as a_inventory ,inventory_transfer_head.b_inventory  as b_inventory , ((inventory_transfer_detail.number_product*category_price.quantity)*inventory_transfer_detail.cost_product) as cost_product   from inventory_transfer_detail  join inventory_transfer_head on inventory_transfer_head.id = inventory_transfer_detail.inventory_transfer_head join category_price on category_price.id = inventory_transfer_detail.unit where inventory_transfer_detail.inventory_transfer_head = ?;', [url.tfid], (err, inventory_transfer_detail) => {
      console.log(err,inventory_transfer_detail);
      for (var i = 0; i < inventory_transfer_detail.length; i++) {
        console.log(inventory_transfer_detail);
        // if (inventory_transfer_detail[i].addoron == 1) {
        //   var inven = inventory_transfer_detail[i].b_inventory;
        //   var product = inventory_transfer_detail[i].product_list_id;
        //   conn.query('select  *  from inventory where inventory_list_id = ? and product_list_id = ?', [inventory_transfer_detail[i].b_inventory, inventory_transfer_detail[i].product_list_id], (err, inventory1) => {
        //     console.log(inventory1);
        //     console.log(inventory1.length);
        //     if (inventory1.length == 0) {
        //       console.log("ADDDDDDDDDDDDDDDDDDDDDDDDDDDDDD");
        //       conn.query('INSERT INTO  inventory set inventory_list_id = ? , product_list_id = ? ,number_product = 0 ,cost_product = 0 , alert = 0 ,enable = 1;', [inven, product], (err, xx) => {
        //         console.log(xx);
        //         console.log(err);
        //       });
        //     }
        //   });
        // }
        conn.query('select  inventory.*, (inventory.number_product - ? ) as newnumber  from inventory where inventory_list_id = ? and product_list_id = ?', [inventory_transfer_detail[i].number, inventory_transfer_detail[i].a_inventory, inventory_transfer_detail[i].product_list_id], (err, inventory) => {
          conn.query('UPDATE  inventory set number_product = ? where inventory_list_id = ? and product_list_id = ?;', [inventory[0].newnumber, inventory[0].inventory_list_id, inventory[0].product_list_id], (err, inventory_transfer_head) => {
          });
        });
        conn.query('select inventory.*, (inventory.number_product + ? ) as newnumber , (((inventory.number_product*inventory.cost_product)+?)/(inventory.number_product + ?)) as cost_product from inventory where inventory_list_id = ? and product_list_id = ?', [inventory_transfer_detail[i].number, inventory_transfer_detail[i].cost_product, inventory_transfer_detail[i].number, inventory_transfer_detail[i].b_inventory, inventory_transfer_detail[i].product_list_id], (err, inventory) => {
          conn.query('UPDATE  inventory set number_product = ? , cost_product = ? where inventory_list_id = ? and product_list_id = ?;', [inventory[0].newnumber, inventory[0].cost_product, inventory[0].inventory_list_id, inventory[0].product_list_id], (err, inventory_transfer_head) => {
          });
        });
      }
    });
    res.redirect('/inventory/transfer');
  });
}

controller.transferallowcancel = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('UPDATE  inventory_transfer_head set allow = 0 where id = ? ;', [url.tfid], (err, inventory_transfer_head) => {
    });
    conn.query('select  inventory_transfer_detail.product_list_id as product_list_id ,inventory_transfer_detail.number_product*category_price.quantity as number ,inventory_transfer_head.a_inventory as a_inventory ,inventory_transfer_head.b_inventory  as b_inventory , ((inventory_transfer_detail.number_product*category_price.quantity)*inventory_transfer_detail.cost_product) as cost_product from inventory_transfer_detail  join inventory_transfer_head on inventory_transfer_head.id = inventory_transfer_detail.inventory_transfer_head join category_price on category_price.id = inventory_transfer_detail.unit where inventory_transfer_detail.inventory_transfer_head = ?;', [url.tfid], (err, inventory_transfer_detail) => {
      console.log(inventory_transfer_detail);
      for (var i = 0; i < inventory_transfer_detail.length; i++) {
        conn.query('select  inventory.*, (inventory.number_product + ? ) as newnumber  from inventory where inventory_list_id = ? and product_list_id = ?', [inventory_transfer_detail[i].number, inventory_transfer_detail[i].a_inventory, inventory_transfer_detail[i].product_list_id], (err, inventory) => {
          console.log("1111", inventory);
          conn.query('UPDATE  inventory set number_product = ? where inventory_list_id = ? and product_list_id = ?;', [inventory[0].newnumber, inventory[0].inventory_list_id, inventory[0].product_list_id], (err, inventory_transfer_head) => {
          });
        });
        conn.query('select inventory.*, (inventory.number_product - ? ) as newnumber , (((inventory.number_product*inventory.cost_product)-?)/(inventory.number_product - ?)) as cost_product from inventory where inventory_list_id = ? and product_list_id = ?', [inventory_transfer_detail[i].number, inventory_transfer_detail[i].cost_product, inventory_transfer_detail[i].number, inventory_transfer_detail[i].b_inventory, inventory_transfer_detail[i].product_list_id], (err, inventory) => {
          console.log("2222", inventory);
          conn.query('UPDATE  inventory set number_product = ? , cost_product = ? where inventory_list_id = ? and product_list_id = ?;', [inventory[0].newnumber, inventory[0].cost_product, inventory[0].inventory_list_id, inventory[0].product_list_id], (err, inventory_transfer_head) => {
          });
        });
      }
    });
    res.redirect('/inventory/transfer');
  });
}

controller.transaction_inventory = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('select * from inventory_list where id = ?;', [url.itid], (err, inventory_list) => {
      conn.query("select receipt.id as id,receipt.receiptchar as code,receipt.receiptordernumber as codename,receipt.creditors_name as creditors_name ,CONCAT(date_format(receipt.date, '%d-%m-'),(date_format(receipt.date, '%Y')+543)) as date ,receipt_list.product_id as product_id ,product_list.name as product_name,receipt_list.quantity as quantity ,category_price.name as category_price ,receipt_list.totallist as totallist from receipt join receipt_list on receipt_list.receipt_id = receipt.id join category_price on category_price.id = receipt_list.categoryprice_id join product_list on product_list.id = receipt_list.product_id  where receipt.inventory_id = ? group by receipt_list.receipt_id;", [url.itid], (err, receipt) => {
        var txt = "select *,CONCAT(date_format(inven_ab.date, '%d-%m-'),(date_format(inven_ab.date, '%Y')+543)) as dates from inventory_list as inven join ( select inven_a.id as idclick,inven_a.a_inventory as idinven,inven_a.date as date,inven_a.no_transfer as no,if(inven_a.a_inventory != " + url.itid + " ,'-','-') as input,if(inven_a.a_inventory = " + url.itid + " ,inventory_list.name,'-') as output,if(inven_a.a_inventory = " + url.itid + " ,'โอนออก','โอนเข้า') as x  from inventory_transfer_head as inven_a join inventory_list on inventory_list.id = inven_a.b_inventory where inven_a.a_inventory = " + url.itid + " and inven_a.allow = 1 UNION select inven_b.id as idclick,inven_b.b_inventory as idinven,inven_b.date as date,inven_b.no_transfer as no,if(inven_b.b_inventory = " + url.itid + " ,inventory_list.name,'-') as input,if(inven_b.b_inventory != " + url.itid + " ,'-','-') as output,if(inven_b.b_inventory = " + url.itid + " ,'โอนเข้า','โอนออก') as x  from inventory_transfer_head as inven_b join inventory_list on inventory_list.id = inven_b.a_inventory where inven_b.b_inventory = " + url.itid + " and inven_b.allow = 1 UNION select receipt.id as idclick,receipt.inventory_id as idinven ,receipt.date as date,CONCAT(receipt.receiptchar,receipt.receiptordernumber) as no ,receipt.creditors_name as input , '-' as output ,'รับสินค้า' as x  from receipt  where receipt.inventory_id = " + url.itid + " ) as inven_ab on inven.id = inven_ab.idinven where inven.id = " + url.itid + " and inven.enable = 1 order by inven_ab.date DESC";
        conn.query(txt, (err, inventory_transaction) => {
          res.render('Inventory_Control/inventory/inventory_transaction.ejs', {
            session: req.session, inventory_list: inventory_list[0], receipt, inventory_transaction
          });
        });
      });
    });
  });
}

controller.transaction_inventory_product = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('select * from inventory_list where id = ?;', [url.itid], (err, inventory_list) => {
      conn.query('select * from product_list where id = ?;', [url.pdid], (err, product_list) => {
        conn.query("select receipt.id as id,receipt.receiptchar as code,receipt.receiptordernumber as codename,receipt.creditors_name as creditors_name ,CONCAT(date_format(receipt.date, '%d-%m-'),(date_format(receipt.date, '%Y')+543)) as date,receipt_list.product_id as product_id ,product_list.name as product_name,receipt_list.quantity as quantity ,category_price.name as category_price ,receipt_list.totallist as totallist from receipt join receipt_list on receipt_list.receipt_id = receipt.id join category_price on category_price.id = receipt_list.categoryprice_id join product_list on product_list.id = receipt_list.product_id  where receipt.inventory_id = ? and product_list.id = ?;", [url.itid, url.pdid], (err, receipt) => {
          var txt = "select *,CONCAT(date_format(inven_ab.date, '%d-%m-'),(date_format(inven_ab.date, '%Y')+543)) as dates from inventory_list as inven join ( select inven_a.id as idclick,inven_a.a_inventory as idinven,inven_a.date as date,inven_a.no_transfer as no,if(inven_a.a_inventory != " + url.itid + " ,'-','-') as input,if(inven_a.a_inventory = " + url.itid + " ,inventory_list.name,'-') as output , inven_de_a.number_product as  quantity , category_price.name as unit ,'-' as price,'-' as priceall, if(inven_a.a_inventory = " + url.itid + " ,'โอนออก','โอนเข้า') as x  from inventory_transfer_head as inven_a join inventory_list on inventory_list.id = inven_a.b_inventory join inventory_transfer_detail as inven_de_a on inven_de_a.inventory_transfer_head = inven_a.id join category_price on category_price.id = inven_de_a.unit where inven_a.a_inventory = " + url.itid + " and inven_a.allow = 1 and inven_de_a.product_list_id = " + url.pdid + " UNION select inven_b.id as idclick,inven_b.b_inventory as idinven,inven_b.date as date,inven_b.no_transfer as no,if(inven_b.b_inventory = " + url.itid + " ,inventory_list.name,'-') as input,if(inven_b.b_inventory != " + url.itid + " ,'-','-') as output , inven_de_b.number_product as  quantity , category_price.name as unit ,'-' as price,'-' as priceall, if(inven_b.b_inventory = " + url.itid + " ,'โอนเข้า','โอนออก') as x  from inventory_transfer_head as inven_b join inventory_list on inventory_list.id = inven_b.a_inventory join inventory_transfer_detail as inven_de_b on inven_de_b.inventory_transfer_head = inven_b.id join category_price on category_price.id = inven_de_b.unit where inven_b.b_inventory = " + url.itid + " and inven_b.allow = 1 and inven_de_b.product_list_id = " + url.pdid + " UNION select receipt.id as idclick,receipt.inventory_id as idinven ,receipt.date as date,CONCAT(receipt.receiptchar,receipt.receiptordernumber) as no ,receipt.creditors_name as input  , '-' as output ,receipt_list.quantity as quantity , category_price.name as unit , receipt_list.price as price , receipt_list.totallist as priceall , 'รับสินค้า' as x  from receipt join receipt_list on receipt.id = receipt_list.receipt_id join category_price on category_price.id = receipt_list.categoryprice_id where receipt.inventory_id = " + url.itid + " and receipt_list.product_id = " + url.pdid + " ) as inven_ab on inven.id = inven_ab.idinven where inven.id = " + url.itid + " and inven.enable = " + url.itid + " order by inven_ab.date DESC"
          conn.query(txt, (err, inventory_transaction) => {
            res.render('Inventory_Control/inventory/inventory_product_transaction.ejs', {
              session: req.session, inventory_list: inventory_list[0], receipt, url, inventory_transaction, product_list
            });
          });
        });
      });
    });
  });
}


controller.inventoryadd_product = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('select * from inventory_list where id = ?;', [url.itid], (err, inventory_list) => {
      conn.query('select product_list.id,product_list.name ,inventory.inventory_list_id,inventory.number_product ,inventory.cost_product from product_list left join (select * from inventory where inventory.inventory_list_id = ? )as inventory on inventory.product_list_id = product_list.id where inventory.inventory_list_id is null;', [url.itid], (err, product) => {

        conn.query('select product_list.*,product_type_list.name as typename ,product_tax1.name as product_tax1,product_tax2.name as product_tax2  from product_list join product_type_list on product_list.product_type_list_id = product_type_list.id  join product_type on product_list.product_type_id = product_type.id join product_tax as product_tax1  on product_list.tax_sell_id = product_tax1.id join product_tax as product_tax2 on product_list.tax_buy_id = product_tax2.id; ', (err, product_list) => {
          conn.query('select * from product_type_list;', (err, product_type_list) => {
            conn.query('select * from product_type;', (err, product_type) => {
              conn.query('select * from product_tax;', (err, product_tax) => {
                conn.query('select * from chart where typeAcc_id = 9 order by code asc;', (err, acc_sell) => {
                  conn.query('select * from chart where typeAcc_id = 11 order by code asc;', (err, acc_buy) => {
                    conn.query('select * from product_group;', (err, product_group) => {
                      res.render('Inventory_Control/inventory/inventoryadd_product.ejs', {
                        session: req.session, inventory_list: inventory_list[0], product, product_list: product_list,
                        product_type_list: product_type_list,
                        product_type: product_type,
                        product_tax: product_tax,
                        product_group: product_group,
                        acc_buy,
                        acc_sell,
                      });
                    });
                  });
                });
              });
            });
          });
        });
      });
    });
  });
}

controller.inventoryadd_product_save = (req, res) => {
  var data = req.body;
  var url = req.params;
  req.getConnection((err, conn) => {
    for (var i = 0; i < data.product_list_id.length - 1; i++) {
      conn.query('INSERT INTO  inventory set inventory_list_id = ? , product_list_id = ? ,number_product = ? ,cost_product = ? , alert = ? ,enable = 1;', [url.itid, data.product_list_id[i], data.number_product[i], data.cost_product[i], data.alert[i]], (err, xx) => {
      });
    }
    res.redirect('/inventory/detail/' + url.itid);
  });
}










controller.editstock = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('select * from inventory_list where id = ?;', [url.itid], (err, inventory_list) => {
      conn.query('select inventory.id as inventory_id,inventory.inventory_list_id as id , inventory.number_product , inventory.cost_product , inventory.alert , product_list.name as product_name , product_list.id as product_id from inventory join product_list on product_list.id = inventory.product_list_id where inventory.inventory_list_id = ? ;', [url.itid], (err, inventory) => {

        res.render('Inventory_Control/inventory/inventory_editstock.ejs', {
          session: req.session, inventory, inventory_list: inventory_list[0],
        })
      });
    });
  });
}


controller.updatestock = (req, res) => {
  var data = req.body;
  console.log(data);
  var url = req.params;
  req.getConnection((err, conn) => {
    if (data.i.length > 1) {
      console.log('if');
      for (var i = 0; i < data.i.length; i++) {
        conn.query('UPDATE  inventory set number_product = ? , cost_product = ? , alert = ? where id = ?;', [data.number_product[i], data.cost_product[i], data.alert[i], data.inventory_id[i]], (err, inventory_list) => {
        });
      }
    } else {
      console.log('else');

      conn.query('UPDATE  inventory set number_product = ? , cost_product = ? , alert = ? where id = ?;', [data.number_product, data.cost_product, data.alert[i], data.inventory_id], (err, inventory_list) => {
      });
    }

    console.log(data.i.length);
    res.redirect('/inventory/detail/' + url.itid);
  });
}





















controller.getproductlist = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('select inventory.*,product_list.name as product_list_name from inventory join product_list on product_list.id = inventory.product_list_id where inventory.inventory_list_id = ? ;', [url.id], (err, productlist) => {
      res.send(productlist);
    });
  });
};


controller.getproductunit = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('select category_price.*,inventory.number_product as number_product,inventory.cost_product as cost_product from category_price join inventory on inventory.product_list_id = category_price.product_list_id  where category_price.product_list_id = ? and  inventory.inventory_list_id = ?', [url.id, url.Aid], (err, getproductunit) => {

      res.send(getproductunit);
    });
  });
};


controller.getproductchacknumber_product = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('select inventory.number_product as number_product,category_price.unit as unit,(inventory.number_product/category_price.unit) as sum from category_price join inventory on inventory.product_list_id = category_price.product_list_id  where category_price.product_list_id = ? and  inventory.inventory_list_id = ? and category_price.id = ?', [url.id, url.Aid, url.Uid], (err, getproductunit) => {
      res.send(getproductunit);
    });
  });
};






controller.getaddinventoryproduct = (req, res) => {
  var url = req.params;
  console.log(url);
  req.getConnection((err, conn) => {
    conn.query('select * from inventory where inventory_list_id = ? and product_list_id = ? ;', [url.itid, url.pdid], (err, add) => {
      console.log(add);
      console.log(add.length);
      if (add.length > 0) {
        res.send("0");

      } else {
        res.send("1");

      }
      // res.send(getproductunit);
    });
  });
};


















module.exports = controller;
