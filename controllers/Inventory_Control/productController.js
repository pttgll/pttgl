
const controller = {};
const { v4: uuidv4 } = require('uuid');
const fs = require('fs');

controller.list = (req, res) => {
  req.getConnection((err, conn) => {
    conn.query('select product_list.*,receipt_list.priceUnit,product_group.name as product_group,product_type_list.name as typename ,product_tax1.name as product_tax1,product_tax2.name as product_tax2  from product_list join product_type_list on product_list.product_type_list_id = product_type_list.id  join product_type on product_list.product_type_id = product_type.id join product_tax as product_tax1  on product_list.tax_sell_id = product_tax1.id join product_tax as product_tax2 on product_list.tax_buy_id = product_tax2.id join product_group on product_list.product_group_id=product_group.id left join (select product_id,((SUM(quantity*price))/(SUM(quantity))) as priceUnit from receipt_list group by product_id) as receipt_list on receipt_list.product_id = product_list.id  where product_list.enable = 1  ORDER BY product_list.id desc ; ', (err, product_list) => {
      conn.query('select product_list.*,receipt_list.priceUnit,product_group.name as product_group,product_type_list.name as typename ,product_tax1.name as product_tax1,product_tax2.name as product_tax2  from product_list join product_type_list on product_list.product_type_list_id = product_type_list.id  join product_type on product_list.product_type_id = product_type.id join product_tax as product_tax1  on product_list.tax_sell_id = product_tax1.id join product_tax as product_tax2 on product_list.tax_buy_id = product_tax2.id join product_group on product_list.product_group_id=product_group.id left join (select product_id,((SUM(quantity*price))/(SUM(quantity))) as priceUnit from receipt_list group by product_id) as receipt_list on receipt_list.product_id = product_list.id  where product_list.enable = 0  ORDER BY product_list.id desc ; ', (err, product_listDel) => {
        conn.query('select product_list.*,receipt_list.priceUnit,product_group.name as product_group,product_type_list.name as typename ,product_tax1.name as product_tax1,product_tax2.name as product_tax2  from product_list join product_type_list on product_list.product_type_list_id = product_type_list.id  join product_type on product_list.product_type_id = product_type.id join product_tax as product_tax1  on product_list.tax_sell_id = product_tax1.id join product_tax as product_tax2 on product_list.tax_buy_id = product_tax2.id join product_group on product_list.product_group_id=product_group.id left join (select product_id,((SUM(quantity*price))/(SUM(quantity))) as priceUnit from receipt_list group by product_id) as receipt_list on receipt_list.product_id = product_list.id  where product_list.enable = 1 and product_list.product_type_list_id = 1 ORDER BY product_list.id desc ; ', (err, product_list1) => {
          conn.query('select product_list.*,receipt_list.priceUnit,product_group.name as product_group,product_type_list.name as typename ,product_tax1.name as product_tax1,product_tax2.name as product_tax2  from product_list join product_type_list on product_list.product_type_list_id = product_type_list.id  join product_type on product_list.product_type_id = product_type.id join product_tax as product_tax1  on product_list.tax_sell_id = product_tax1.id join product_tax as product_tax2 on product_list.tax_buy_id = product_tax2.id join product_group on product_list.product_group_id=product_group.id left join (select product_id,((SUM(quantity*price))/(SUM(quantity))) as priceUnit from receipt_list group by product_id) as receipt_list on receipt_list.product_id = product_list.id  where product_list.enable = 1 and product_list.product_type_list_id = 2 ORDER BY product_list.id desc ; ', (err, product_list2) => {
            conn.query('select product_list.*,receipt_list.priceUnit,product_group.name as product_group,product_type_list.name as typename ,product_tax1.name as product_tax1,product_tax2.name as product_tax2  from product_list join product_type_list on product_list.product_type_list_id = product_type_list.id  join product_type on product_list.product_type_id = product_type.id join product_tax as product_tax1  on product_list.tax_sell_id = product_tax1.id join product_tax as product_tax2 on product_list.tax_buy_id = product_tax2.id join product_group on product_list.product_group_id=product_group.id left join (select product_id,((SUM(quantity*price))/(SUM(quantity))) as priceUnit from receipt_list group by product_id) as receipt_list on receipt_list.product_id = product_list.id  where product_list.enable = 1 and product_list.product_type_list_id = 3 ORDER BY product_list.id desc ; ', (err, product_list3) => {
              conn.query('select * from product_type_list', (err, product_type_list) => {
                conn.query('select * from product_type', (err, product_type) => {
                  conn.query('select * from product_tax', (err, product_tax) => {
                    conn.query('select * from chart where typeAcc_id = 9;', (err, acc_sell) => {
                      conn.query('select * from chart where typeAcc_id = 11;', (err, acc_buy) => {
                        conn.query('select * from product_group;', (err, product_group) => {
                          conn.query('select product_list .id as id,photo_product .photo as photo from product_list  left join photo_product on photo_product.product_list = product_list.id where photo_product.main =1;;', (err, photo) => {
                            res.render('Inventory_Control/product/productlist.ejs', {
                              product_list: product_list,
                              product_listDel: product_listDel,
                              product_list1: product_list1,
                              product_list2: product_list2,
                              product_list3: product_list3,
                              product_type_list: product_type_list,
                              product_type: product_type,
                              product_tax: product_tax,
                              product_group: product_group,
                              photo: photo,
                              acc_sell,
                              acc_buy,
                              session: req.session
                            });
                          });
                        });
                      });
                    });
                  });
                });
              });
            });
          });
        });
      });
    });
  });
}

controller.add = (req, res) => {
  req.getConnection((err, conn) => {
    conn.query('select product_list.*,product_type_list.name as typename ,product_tax1.name as product_tax1,product_tax2.name as product_tax2  from product_list join product_type_list on product_list.product_type_list_id = product_type_list.id  join product_type on product_list.product_type_id = product_type.id join product_tax as product_tax1  on product_list.tax_sell_id = product_tax1.id join product_tax as product_tax2 on product_list.tax_buy_id = product_tax2.id; ', (err, product_list) => {
      conn.query('select * from product_type_list;', (err, product_type_list) => {
        conn.query('select * from product_type;', (err, product_type) => {
          conn.query('select * from product_tax;', (err, product_tax) => {
            conn.query('select * from chart where typeAcc_id = 9 order by code asc;', (err, acc_sell) => {
              conn.query('select * from chart where typeAcc_id = 11 order by code asc;', (err, acc_buy) => {
                conn.query('select * from product_group;', (err, product_group) => {
                  res.render('Inventory_Control/product/productadd.ejs', {
                    product_list: product_list,
                    product_type_list: product_type_list,
                    product_type: product_type,
                    product_tax: product_tax,
                    product_group: product_group,
                    acc_buy,
                    acc_sell,
                    session: req.session
                  });
                });
              });
            });
          });
        });
      });
    });
  });
}

controller.save = (req, res) => {
  var data = req.body;
  console.log(data);
  var nameUnit = data.nameUnit;
  delete data.nameUnit;
  var Unit = data.Unit;
  delete data.Unit;
  var sellAn = data.sellA
  var sellBn = data.sellB
  var sellCn = data.sellC
  delete data.sellA;
  delete data.sellB;
  delete data.sellC;
  var buyAn = data.buyA
  var buyBn = data.buyB
  var buyCn = data.buyC
  delete data.buyA;
  delete data.buyB;
  delete data.buyC;
  var radioPhoto = data.radioPhoto;
  delete data.radioPhoto;
  var quantity = data.quantity;
  delete data.quantity;
  req.getConnection((err, conn) => {

    conn.query('INSERT INTO  product_list set ?;', [data], (err, product) => {
      console.log(err);
    });

    conn.query('select id from product_list order by id desc limit 1;', (err, product_list) => {

      if (req.files) {
        var file = req.files.photo;
        if (file.length) {
          for (var i = 0; i < file.length; i++) {
            var type = file[i].name.split(".");
            var filename = uuidv4() + "." + type[type.length - 1];
            var photo = filename;
            file[i].mv("./public/inventory/" + filename, function (err) {
              if (err) { console.log(err) }
            });
            var main = 0
            if (radioPhoto == i) {
              main = 1
            }
            conn.query('INSERT INTO  photo_product set photo = ? , product_list = ? ,main = ? ;', [photo, product_list[0].id, main], (err, photo) => {
              console.log(err);
            });
          }
        } else {
          var type = file.name.split(".");
          var filename = uuidv4() + "." + type[type.length - 1];
          var photo = filename;
          file.mv("./public/inventory/" + filename, function (err) {
            if (err) { console.log(err) }
          });
          var main = 1;
          conn.query('INSERT INTO  photo_product set photo = ? , product_list = ? ,main = ? ;', [photo, product_list[0].id, main], (err, photo) => {
            console.log(err);
          });
        }

      }

      for (var i = 0; i < Unit.length; i++) {
        var Unitnew = parseInt(Unit[i])
        var nameUnitnew = nameUnit[i];
        if (Unit.length == 1) {
          nameUnitnew = nameUnit;
        }
        var uid

        var sellAnew = sellAn[i];
        if (Unit.length == 1) {
          sellAnew = sellAn;
        }
        var sellBnew = sellBn[i];
        if (Unit.length == 1) {
          sellBnew = sellBn;
        }
        var sellCnew = sellCn[i];
        if (Unit.length == 1) {
          sellCnew = sellCn;
        }
        // var buyAnew = buyAn[i];
        // if (Unit.length == 1) {
        //   buyAnew = buyAn;
        // }
        // var buyBnew = buyBn[i];
        // if (Unit.length == 1) {
        //   buyBnew = buyBn;
        // }
        // var buyCnew = buyCn[i];
        // if (Unit.length == 1) {
        //   buyCnew = buyCn;
        // }
        conn.query('INSERT INTO  category_price set product_list_id=?,name = ? ,unit = ? ,no_unit = ? ,Price_sellA = ? ,Price_sellB = ? ,Price_sellC = ? ,quantity = ?;', [product_list[0].id, nameUnitnew, Unitnew, i + 1, sellAnew, sellBnew, sellCnew, quantity[i]], (err, product) => {
          console.log(err);
        });
      }
    });
    res.redirect('/product');
  });
}

controller.detail = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('select chart_buy.name as chart_buy_name,chart_buy.code as chart_buy_code,chart_sell.name as chart_sell_name,chart_sell.code as chart_sell_code,product_type_list.name as product_type_list,product_list.*,receipt_list.priceUnit,product_group.name as product_groupname from product_list join product_group on product_list.product_group_id=product_group.id left join (select product_id,((SUM(quantity*price))/(SUM(quantity))) as priceUnit from receipt_list group by product_id) as receipt_list on receipt_list.product_id = product_list.id join product_type_list on product_type_list.id = product_list.product_type_list_id left join chart as chart_buy on chart_buy.id = product_list.acc_buy left join chart as chart_sell on chart_sell.id = product_list.acc_sell  where product_list.id = ?', [url.pdid], (err, product_list) => {
      conn.query('select * from category_price  where category_price.product_list_id = ? order by category_price.no_unit asc; ', [url.pdid], (err, category_price) => {
        conn.query('select * from photo_product where product_list = ? order by id asc; ', [url.pdid], (err, photo) => {
          console.log(product_list);
          console.log(category_price);

          res.render('Inventory_Control/product/productdetail.ejs', {
            session: req.session, product_list, category_price, photo

          });
        });
      });
    });
  });
}

controller.disable = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    // conn.query('select * from product_list where id = ?', [url.id], (err, photo) => {
    //   fs.unlink("public/inventory/" + photo[0].photo, function (err) {
    //     conn.query("Delete from product_list where id = ? ;", [url.id], (err, product) => {
    //       res.redirect('/product');
    //     });
    //   });
    // });

    conn.query('UPDATE  product_list set enable = 0 where id =?;', [url.id], (err, product) => {
      res.redirect('/product');
    });
  });
}

controller.enable = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('UPDATE  product_list set enable = 1 where id =?;', [url.id], (err, product) => {
      res.redirect('/product');
    });

  });
}

controller.deletephoto = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('select * from photo_product where id = ?', [url.photoid], (err, photo) => {
      fs.unlink("public/inventory/" + photo[0].photo, function (err) {
        conn.query("Delete from photo_product where id = ? ;", [url.photoid], (err, photos) => {
          res.redirect('/product/edit/' + url.pdid);
        });
      });
    });
  });
}





controller.edit = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('select * from product_list where id = ?', [url.id], (err, product_list) => {
      conn.query('select * from product_type_list', (err, product_type_list) => {
        conn.query('select * from product_type', (err, product_type) => {
          conn.query('select * from product_tax', (err, product_tax) => {
            conn.query('select * from product_group;', (err, product_group) => {
              conn.query('select * from chart where typeAcc_id = 9 order by code asc;', (err, acc_sell) => {
                conn.query('select * from chart where typeAcc_id = 11 order by code asc;', (err, acc_buy) => {
                  conn.query('select * from photo_product where product_list = ? order by id asc; ', [url.id], (err, photo) => {
                    console.log(url.pdid);
                    conn.query('select * from category_price  where category_price.product_list_id = ? order by category_price.no_unit asc; ', [url.id], (err, category_price) => {
                      console.log(category_price);
                      res.render('Inventory_Control/product/productedit.ejs', {
                        product_list: product_list[0],
                        product_type_list: product_type_list,
                        product_type: product_type,
                        product_tax: product_tax,
                        product_group: product_group,
                        category_price: category_price,
                        photo: photo,
                        acc_sell,acc_buy,
                        session: req.session
                      })
                    });
                  });
                });
              });
            });
          });
        });
      });
    });
  });
}

// controller.update = (req, res) => {
//   var data = req.body;
//   var url = req.params;
//   req.getConnection((err, conn) => {
//     conn.query('select * from product_list where id = ?', [url.id], (err, photo) => {
//       if (req.files) {
//         var file = req.files.photo;
//         var filename = uuidv4() + "." + file.name.split(".")[1];
//         data.photo = filename;
//         file.mv("./public/inventory/" + filename, function (err) {
//           if (err) { console.log(err) }
//         });
//         fs.unlink("public/inventory/" + photo[0].photo, function (err) {
//           if (err) {
//             
//           }
//         });
//         conn.query('UPDATE product_list SET ? WHERE id = ?;', [data, url.id], (err, product) => {
//           res.redirect('/product');
//         });
//       } else {
//         if (photo[0].photo == data.photo) {
//           conn.query('UPDATE product_list SET ? WHERE id = ?;', [data, url.id], (err, product) => {
//             res.redirect('/product');
//           });
//         } else {
//           data.photo = '';
//           fs.unlink("public/inventory/" + photo[0].photo, function (err) {
//             if (err) {
//               
//             }
//           });
//           conn.query('UPDATE product_list SET ? WHERE id = ?;', [data, url.id], (err, product) => {
//             res.redirect('/product');
//           });
//         }
//       }
//     });
//   });
// }

controller.update = (req, res) => {
  var url = req.params;
  var data = req.body;
  console.log(data);
  var nameUnit = data.nameUnit;
  delete data.nameUnit;
  var Unit = data.Unit;
  delete data.Unit;
  var sellAn = data.sellA
  var sellBn = data.sellB
  var sellCn = data.sellC
  delete data.sellA;
  delete data.sellB;
  delete data.sellC;
  var buyAn = data.buyA
  var buyBn = data.buyB
  var buyCn = data.buyC
  delete data.buyA;
  delete data.buyB;
  delete data.buyC;
  var radioPhoto = data.radioPhoto;
  delete data.radioPhoto;
  req.getConnection((err, conn) => {

    conn.query('UPDATE  product_list set ? where id =?;', [data, url.id], (err, product) => {

    });
    console.log("XXXXXXXXXXXXXXXXXXXXXX");
    console.log(req.files);
    if (req.files) {
      var file = req.files.photo;
      if (file.length) {
        for (var i = 0; i < file.length; i++) {
          var type = file[i].name.split(".");
          var filename = uuidv4() + "." + type[type.length - 1];
          var photo = filename;
          file[i].mv("./public/inventory/" + filename, function (err) {
            if (err) { console.log(err) }
          });
          var main = 0
          if (radioPhoto == i) {
            main = 1
          }
          conn.query('INSERT INTO  photo_product set photo = ? , product_list = ? ,main = 0 ;', [photo, url.id], (err, photo) => {
            console.log(err);
          });
        }
      } else {
        var type = file.name.split(".");
        var filename = uuidv4() + "." + type[type.length - 1];
        var photo = filename;
        file.mv("./public/inventory/" + filename, function (err) {
          if (err) { console.log(err) }
        });
        var main = 1;
        conn.query('INSERT INTO  photo_product set photo = ? , product_list = ? ,main = 0 ;', [photo, url.id], (err, photo) => {
          console.log(err);
        });
      }

    }
    conn.query('select * from category_price where product_list_id = ?', [url.id], (err, category_price) => {
      console.log(category_price);
      for (var i = 0; i < category_price.length; i++) {
        var Unitnew = parseInt(Unit[i])
        var nameUnitnew = nameUnit[i];
        if (Unit.length == 1) {
          nameUnitnew = nameUnit;
        }
        var uid
        var sellAnew = sellAn[i];
        if (Unit.length == 1) {
          sellAnew = sellAn;
        }
        var sellBnew = sellBn[i];
        if (Unit.length == 1) {
          sellBnew = sellBn;
        }
        var sellCnew = sellCn[i];
        if (Unit.length == 1) {
          sellCnew = sellCn;
        }
        conn.query('UPDATE category_price set product_list_id = ? , name = ? , unit = ? , no_unit = ? , price_sellA = ? , price_sellB = ? , price_sellC = ?  where id = ?;', [url.id, nameUnitnew, Unitnew, i + 1, sellAnew, sellBnew, sellCnew, category_price[i].id], (err, product) => {
          console.log(err);
        });
      }
      for (var i = category_price.length; i < Unit.length; i++) {
        var Unitnew = parseInt(Unit[i])
        var nameUnitnew = nameUnit[i];
        if (Unit.length == 1) {
          nameUnitnew = nameUnit;
        }
        var uid
        var sellAnew = sellAn[i];
        if (Unit.length == 1) {
          sellAnew = sellAn;
        }
        var sellBnew = sellBn[i];
        if (Unit.length == 1) {
          sellBnew = sellBn;
        }
        var sellCnew = sellCn[i];
        if (Unit.length == 1) {
          sellCnew = sellCn;
        }
        conn.query('INSERT INTO  category_price set product_list_id = ? , name = ? , unit = ? , no_unit = ? , price_sellA = ? , price_sellB = ? , price_sellC = ?;', [url.id, nameUnitnew, Unitnew, i + 1, sellAnew, sellBnew, sellCnew,], (err, product) => {
        });
      }
    });
    res.redirect('/product');
  });
}


controller.transactionhistory = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('select * from product_list where id = ?;', [url.pdid], (err, product_list) => {
      conn.query('select concat(date_format(receipt.date,"%d/%m/"),year(receipt.date)+543) as date,receipt_list.id as id,vat.name as vat,receipt_list.listdiscount as listdiscount,discount.id as discount_id,receipt_list.quantity as quantity,category_price.name as unittype,receipt_list.detail as detail,receipt_list.price as price,receipt.receiptordernumber as receiptordernumber,receipt.creditors_name from receipt_list join receipt on receipt_list.receipt_id = receipt.id join category_price on category_price.id = receipt_list.unittype_id left join discount on discount.id = receipt_list.discount_id left join vat on vat.id = receipt_list.vat_id where receipt_list.product_id = ?;', [url.pdid], (err, receipt) => {
        console.log(receipt);
        res.render('Inventory_Control/product/transactionhistory.ejs', {
          session: req.session, product_list: product_list[0], receipt
        });
      });
    });
  });
}












controller.getgroup = (req, res) => {
  req.getConnection((err, conn) => {
    conn.query('select * from product_group ;', (err, product_group) => {
      res.send(product_group);
    });
  });
};

controller.addgroup = (req, res) => {
  var data = req.body;
  req.getConnection((err, conn) => {
    conn.query('INSERT INTO product_group (name) VALUES (?);', [data[0].value], (err, add) => {

      res.send(add);
    });
  });
};

controller.updategroup = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('UPDATE product_group set name = ? where id = ?;', [url.va, url.id], (err, product_group) => {

      res.send(product_group);
    });
  });
};

controller.delgroup = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('DELETE FROM product_group where id = ?;', [url.id], (err, product_group) => {

      res.send('success');
    });
  });
};


controller.changephotomain = (req, res) => {
  var url = req.params;
  console.log(url);
  req.getConnection((err, conn) => {
    conn.query('UPDATE photo_product SET main = 0 WHERE product_list = ? ;', [url.pdid], (err, photo) => {

      conn.query('UPDATE photo_product SET main = "1" WHERE id = ? ;', [url.photoid], (err, photo) => {

        res.send('success');
      });
    });
  });
}
module.exports = controller;
