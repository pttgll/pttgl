const session = require("express-session");

const controller = {};


controller.list = (req, res) => {
  req.getConnection((err, conn) => {
    conn.query('select * from bank', (err, bank) => {
      conn.query('select * from provinces', (err, provinces) => {
        res.render('asset/property_list/property_listlist.ejs', {
          bank,provinces,session: req.session
        });
      });
    });
  });
};

controller.new = (req, res) => {
  req.getConnection((err, conn) => {
    conn.query('select * from bank', (err, bank) => {
      conn.query('select * from provinces', (err, provinces) => {
        res.render('asset/property_list/property_listnew.ejs', {
          bank,provinces,session: req.session
        });
      });
    });
  });
};

controller.add = (req, res) => {
  req.getConnection((err, conn) => {
    conn.query('select * from bank', (err, bank) => {
      conn.query('select * from provinces', (err, provinces) => {
        res.render('asset/property_list/property_listadd.ejs', {
          bank,provinces,session: req.session
        });
      });
    });
  });
};
//----------------------------------------------------------------------------------------
controller.save = (req, res) => {
  const data = req.body;
  req.getConnection((err, conn) => {
    conn.query('INSERT INTO contact (contype,conpass,namebusiness,address,provinces_id,amphures_id,districts_id,numbertax,type,company,crday,conname,email,mobile,bank_id,numberbank,facutybank,typeacc) value (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
    [data.contype,data.conpass,data.namebusiness,data.address,data.provinces_id,data.amphures_id,data.districts_id,data.numbertax,
      data.type,data.company,data.crday,data.conname,data.email,data.mobile,data.bank_id,data.numberbank,data.facutybank,data.typeacc], (err, contact) => {
        res.redirect('/property_list');
      });
    });
  };

  module.exports = controller;
