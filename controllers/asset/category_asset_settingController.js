const session = require("express-session");

const controller = {};


controller.list = (req, res) => {
  req.getConnection((err, conn) => {
    res.render('asset/category_asset_setting/category_asset_settinglist.ejs', {
      session: req.session
    });
  });
};

controller.add = (req, res) => {
  req.getConnection((err, conn) => {
    res.render('asset/category_asset_setting/category_asset_settingadd.ejs', {
      session: req.session
    });
  });
};
//----------------------------------------------------------------------------------------
controller.save = (req, res) => {
  const data = req.body;
  req.getConnection((err, conn) => {
    res.redirect('/category_asset_setting');
  });
};

module.exports = controller;
