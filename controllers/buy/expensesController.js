const session = require("express-session");
const controller = {};
const { v4: uuidv4 } = require('uuid');
const fs = require('fs');
const { deflateRawSync } = require("zlib");


///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_list
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.expenses_list = (req, res) => {
    req.getConnection((err, conn) => {
        conn.query(' select expenses.*,expenses_head.name as expenses_name,status.name as status ,DATE_FORMAT(DATE_ADD(expenses.date, INTERVAL 543 YEAR), " %d/%m/%Y" )dateday  from expenses join status on status_id = status.id left join expenses_head on expenses.expenses_type = expenses_head.id where YEAR(date) = YEAR(CURDATE());', (err, expenses) => {
            conn.query('select * from status', (err, status) => {
                res.render('buy/expenses/expenses_list.ejs', {
                    session: req.session, expenses, status
                });
            })
        });
    });
};
///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_add
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.expenses_add = (req, res) => {
    req.getConnection((err, conn) => {
        // runnumber expenses
        conn.query('select * from module where id = 20', (err, expensesmodule) => {
            conn.query('select date_format(date(now()),"%Y%m") as number', (err, runnumber) => {
                conn.query('select count(expensesnumber) as countnumber,max(expensesnumber) as maxnumber from expenses where date_format(date,"%Y%m") = date_format(date(now()),"%Y%m");', (err, expensesnumber) => {
                    //
                    conn.query('select creditors.*,  provinces.name_th as provinces, amphures.name_th as amphures, districts.name_th as districts, zip_code from creditors join provinces on creditors.provinces_id = provinces.id join amphures on creditors.amphures_id = amphures.id join districts on creditors.districts_id = districts.id', (err, creditors) => {
                        conn.query('select * from expenses_head', (err, expenses_head) => {
                            conn.query('select * from human_resource', (err, human_resource) => {
                                conn.query('select * from indicate', (err, indicate) => {
                                    conn.query('select * from witholding', (err, witholding) => {
                                        conn.query('select * from discount', (err, discount) => {
                                            conn.query('select * from vat', (err, vat) => {
                                                conn.query('select * from company', (err, company) => {
                                                    conn.query('select * from compute', (err, compute) => {
                                                        conn.query('select * from projectgl', (err, projectgl) => {
                                                            //creditors
                                                            conn.query('select * from chart where typeAcc_id = 5;', (err, chart) => {
                                                                conn.query('select * from bank', (err, bank) => {
                                                                    conn.query('select * from provinces', (err, provinces) => {
                                                                        conn.query('select * from human', (err, human) => {
                                                                            //runnumber creditors
                                                                            conn.query('select * from module where id = 4', (err, creditormodule) => {
                                                                                conn.query('select max(creditorID) as maxnumber from creditors where creditorID like ?', [creditormodule[0].char + "%"], (err, maxnumber) => {
                                                                                    res.render('buy/expenses/expenses_add.ejs', {
                                                                                        expensesmodule, runnumber, expensesnumber, creditors, expenses_head, human_resource, indicate, witholding, discount, vat, company, compute, projectgl,
                                                                                        chart, bank, provinces, human, creditormodule, maxnumber, session: req.session
                                                                                    });
                                                                                });
                                                                            });
                                                                        });
                                                                    });
                                                                });
                                                            });
                                                        });
                                                    });
                                                });
                                            });
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    });
};
///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_save
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.expenses_save = (req, res) => {
    const data = req.body;
    console.log(data);
    req.getConnection((err, conn) => {
        if (data.creditors_id && data.status_pay == "") {
            console.log("เพิ่ม เป็นหนี้");
            conn.query('INSERT INTO expenses (expenseschar,expensesnumber,creditors_id,creditors_name,address,post,tax,branch,date,credit,indicate_id,due,employee_saler,employee_delivery,project_id,number,compute_id,note,notein,total,afterdiscount,except,vatcalculator,vatamount,notvatamount,alldiscount,discount_id,vat,granttotal,status_id,itemizedvat,itemizeddiscount,expenses_type,company_id,status_pay)  values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)'
                , [data.expenseschar, data.expensesnumber, data.creditors_id, data.creditors_name, data.address, data.post, data.tax, data.branch, data.date, data.credit, data.indicate_id, data.due, data.employee_saler, data.employee_delivery, data.project_id, data.number, data.compute_id, data.note, data.notein, data.total.replace(/,/g, ''), data.afterdiscount.replace(/,/g, ''), data.except.replace(/,/g, ''), data.vatcalculator.replace(/,/g, ''), data.vatamount.replace(/,/g, ''), data.notvatamount.replace(/,/g, ''), data.alldiscount.replace(/,/g, ''), data.discount_id, data.vat, data.granttotal.replace(/,/g, ''), 2, data.itemizedvat, data.itemizeddiscount, data.expenses_type, data.company_id, data.status_pay], (err, expenses) => {
                    if (Array.isArray(data.detail)) {
                        for (var i = 0; i < data.detail.length; i++) {
                            conn.query('INSERT INTO expenses_list (detail,quantity,categoryprice,price,listdiscount,vate_id,totallist,expenses_detail,acc_buy,expenses_id)  values(?,?,?,?,?,?,?,?,?,?)', [data.detail[i], data.quantity[i], data.categoryprice[i], data.price[i], data.listdiscount[i], data.vate_id[i], data.totallist[i], data.expenses_detail[i], data.acc_buy[i], expenses.insertId], (err, expenses_list) => {
                            });
                        }
                    } else {
                        conn.query('INSERT INTO expenses_list (detail,quantity,categoryprice,price,listdiscount,vate_id,totallist,expenses_detail,acc_buy,expenses_id)  values(?,?,?,?,?,?,?,?,?,?)', [data.detail, data.quantity, data.categoryprice, data.price, data.listdiscount, data.vate_id, data.totallist, data.expenses_detail, data.acc_buy, expenses.insertId], (err, expenses_list) => {
                        });
                    }
                    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    //ส่วนของ creditor เจ้าหนี้
                    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    conn.query('select * from creditors where id = ?', [data.creditors_id], (err, creditors) => {
                        if (creditors[0].balance == 0) {
                            var credits = creditors[0].crfull - data.granttotal.replace(/,/g, '');
                            conn.query('update creditors set balance = ? , crbalance = ? where id = ? ', [data.granttotal.replace(/,/g, ''), credits, creditors[0].id], (er, credit) => {
                            })
                        } else {
                            var total = parseFloat(creditors[0].balance) + parseFloat(data.granttotal.replace(/,/g, ''));
                            var tosum = parseFloat(creditors[0].crfull) - parseFloat(total);
                            conn.query('update creditors set balance = ? , crbalance = ? where id = ? ', [total, tosum, creditors[0].id], (er, credit1) => {
                            })
                        }
                    })
                    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    //ส่วนของ GL
                    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
                    conn.query('select * from creditors where id = ?', [data.creditors_id], (err, creditors) => {
                        console.log("1", err);
                        var expensesnumber = data.expenseschar + data.expensesnumber;
                        var dataH = { book_id: 1, date: data.date, doc_no: expensesnumber, detail: "", company_id: data.company_id, module_id: 20, reference_id: expenses.insertId };
                        var dataD = [];
                        conn.query('INSERT INTO  glheads set ?', [dataH], (err, glheads) => {
                            if (Array.isArray(data.expenses_detail)) {
                                for (var i = 0; i < data.expenses_detail.length; i++) {
                                    dataD.push({ code_id: data.acc_buy[i], cheack: "", date_list: null, dr: data.totallist[i].replace(/,/g, ''), cr: 0, glheads_id: glheads.insertId });
                                }
                                if (data.vat && data.vat != "") {
                                    dataD.push({ code_id: 433, cheack: "", date_list: null, dr: data.vatamount.replace(/,/g, ''), cr: 0, glheads_id: glheads.insertId });
                                }
                                if (data.creditors_id) {
                                    dataD.push({ code_id: creditors[0].chart_id, cheack: "", date_list: null, dr: 0, cr: data.granttotal.replace(/,/g, ''), glheads_id: glheads.insertId });
                                }

                            } else {
                                console.log("GL2222222");
                                if (data.expenses_detail) {
                                    dataD.push({ code_id: data.acc_buy, cheack: "", date_list: null, dr: data.totallist.replace(/,/g, ''), cr: 0, glheads_id: glheads.insertId });
                                }
                                if (data.vat && data.vat != "") {
                                    dataD.push({ code_id: 433, cheack: "", date_list: null, dr: data.vatamount.replace(/,/g, ''), cr: 0, glheads_id: glheads.insertId });
                                }
                                if (data.creditors_id) {
                                    dataD.push({ code_id: creditors[0].chart_id, cheack: "", date_list: null, dr: 0, cr: data.granttotal.replace(/,/g, ''), glheads_id: glheads.insertId });
                                }
                            }
                            for (let i = 0; i < dataD.length; i++) {
                                conn.query('INSERT INTO gldetail set ?', [dataD[i]], (err, gldetail) => {
                                    if (err) {
                                        res.json(err);
                                    }
                                });
                            }
                        });
                    });
                    //
                    if (req.files) {
                        var file = req.files.filename;
                        if (!Array.isArray(file)) {
                            var type = file.name.split(".");
                            var filename = uuidv4() + "." + type[type.length - 1];
                            file.mv("./public/expenses/" + filename, function (err) {
                                if (err) { console.log(err) }
                                data.filename = filename;
                                conn.query('INSERT INTO expenses_file (filename,expenses_id) values(?,?)', [data.filename, expenses.insertId], (err, expenses_file) => {
                                    if (err) {
                                        res.json(err);
                                    }
                                });
                            })
                        } else {
                            for (var i = 0; i < file.length; i++) {
                                var type = file[i].name.split(".");
                                var filename = uuidv4() + "." + type[type.length - 1];
                                file[i].mv("./public/expenses/" + filename, function (err) {
                                    if (err) { console.log(err) }
                                })
                                data.filename = filename;
                                conn.query('INSERT INTO expenses_file (filename,expenses_id) values(?,?)', [data.filename, expenses.insertId], (err, expenses_file) => {
                                    if (err) {
                                        res.json(err);
                                    }
                                });
                            }
                        }
                    }
                    res.redirect('/expenses');
                });
        } else if (data.creditors_id && data.status_pay != "" && data.status_pay == data.granttotal) {
            console.log("เพิ่ม เงิดสด");
            conn.query('INSERT INTO expenses (expenseschar,expensesnumber,creditors_id,creditors_name,address,post,tax,branch,date,credit,indicate_id,due,employee_saler,employee_delivery,project_id,number,compute_id,note,notein,total,afterdiscount,except,vatcalculator,vatamount,notvatamount,alldiscount,discount_id,vat,granttotal,status_id,itemizedvat,itemizeddiscount,expenses_type,company_id,status_pay)  values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)'
                , [data.expenseschar, data.expensesnumber, data.creditors_id, data.creditors_name, data.address, data.post, data.tax, data.branch, data.date, data.credit, data.indicate_id, data.due, data.employee_saler, data.employee_delivery, data.project_id, data.number, data.compute_id, data.note, data.notein, data.total.replace(/,/g, ''), data.afterdiscount.replace(/,/g, ''), data.except.replace(/,/g, ''), data.vatcalculator.replace(/,/g, ''), data.vatamount.replace(/,/g, ''), data.notvatamount.replace(/,/g, ''), data.alldiscount.replace(/,/g, ''), data.discount_id, data.vat, data.granttotal.replace(/,/g, ''), 5, data.itemizedvat, data.itemizeddiscount, data.expenses_type, data.company_id, data.status_pay], (err, expenses) => {
                    if (Array.isArray(data.detail)) {
                        for (var i = 0; i < data.detail.length; i++) {
                            conn.query('INSERT INTO expenses_list (detail,quantity,categoryprice,price,listdiscount,vate_id,totallist,expenses_detail,acc_buy,expenses_id)  values(?,?,?,?,?,?,?,?,?,?)', [data.detail[i], data.quantity[i], data.categoryprice[i], data.price[i], data.listdiscount[i], data.vate_id[i], data.totallist[i], data.expenses_detail[i], data.acc_buy[i], expenses.insertId], (err, expenses_list) => {
                            });
                        }
                    } else {
                        conn.query('INSERT INTO expenses_list (detail,quantity,categoryprice,price,listdiscount,vate_id,totallist,expenses_detail,acc_buy,expenses_id)  values(?,?,?,?,?,?,?,?,?,?)', [data.detail, data.quantity, data.categoryprice, data.price, data.listdiscount, data.vate_id, data.totallist, data.expenses_detail, data.acc_buy, expenses.insertId], (err, expenses_list) => {
                        });
                    }
                    var dataP = { papernumber: data.expenseschar + data.expensesnumber, amount: data.granttotal.replace(/,/g, ''), datepayment: data.date, acc_money: data.granttotal.replace(/,/g, ''), net_payout: data.granttotal.replace(/,/g, ''), withholding: "", withholding_id: "", withholding_vate: "", type_payment_id: 1, Identify: "", petty_cash: "", transfer_account: "", charge: "", check_date: "", check_number: "", check_acc_cut: "", check_charge: "", mastercard: "", typemachin_id: "", chanal_pay: "", note: "", balance: "", overdue: "", module_id: 20, reference_id: expenses.insertId };
                    console.log(dataP);
                    conn.query('INSERT INTO  payment set ?', [dataP], (err, payment) => {
                    });
                    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    //ส่วนของ GL
                    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
                    conn.query('select * from creditors where id = ?', [data.creditors_id], (err, creditors) => {
                        conn.query('select * from expenses_detail', (err, expenses_detail) => {
                            var expensesnumber = data.expenseschar + data.expensesnumber;
                            var dataH = { book_id: 1, date: data.date, doc_no: expensesnumber, detail: "", company_id: data.company_id, module_id: 20, reference_id: expenses.insertId };
                            var dataD = [];
                            conn.query('INSERT INTO  glheads set ?', [dataH], (err, glheads) => {
                                if (Array.isArray(data.expenses_detail)) {
                                    for (var i = 0; i < data.expenses_detail.length; i++) {
                                        dataD.push({ code_id: data.acc_buy[i], cheack: "", date_list: null, dr: data.totallist[i].replace(/,/g, ''), cr: 0, glheads_id: glheads.insertId });
                                    }
                                    if (data.vat && data.vat != "") {
                                        dataD.push({ code_id: 433, cheack: "", date_list: null, dr: data.vatamount.replace(/,/g, ''), cr: 0, glheads_id: glheads.insertId });
                                    }
                                    if (data.creditors_id && data.status_pay != "") {
                                        dataD.push({ code_id: 406, cheack: "", date_list: null, dr: 0, cr: data.granttotal.replace(/,/g, ''), glheads_id: glheads.insertId });
                                    }
                                } else {
                                    console.log("GL2222222");
                                    if (data.expenses_detail) {
                                        dataD.push({ code_id: data.acc_buy, cheack: "", date_list: null, dr: data.totallist.replace(/,/g, ''), cr: 0, glheads_id: glheads.insertId });
                                    }
                                    if (data.vat && data.vat != "") {
                                        dataD.push({ code_id: 433, cheack: "", date_list: null, dr: data.vatamount.replace(/,/g, ''), cr: 0, glheads_id: glheads.insertId });
                                    }
                                    if (data.creditors_id && data.status_pay != "") {
                                        dataD.push({ code_id: 406, cheack: "", date_list: null, dr: 0, cr: data.granttotal.replace(/,/g, ''), glheads_id: glheads.insertId });
                                    }
                                }
                                //
                                for (let i = 0; i < dataD.length; i++) {
                                    conn.query('INSERT INTO gldetail set ?', [dataD[i]], (err, gldetail) => {
                                        if (err) {
                                            res.json(err);
                                        }
                                    });
                                }
                            });
                        });
                    });
                    //
                    if (req.files) {
                        var file = req.files.filename;
                        if (!Array.isArray(file)) {
                            var type = file.name.split(".");
                            var filename = uuidv4() + "." + type[type.length - 1];
                            file.mv("./public/expenses/" + filename, function (err) {
                                if (err) { console.log(err) }
                                data.filename = filename;
                                conn.query('INSERT INTO expenses_file (filename,expenses_id) values(?,?)', [data.filename, expenses.insertId], (err, expenses_file) => {
                                    if (err) {
                                        res.json(err);
                                    }
                                });
                            })
                        } else {
                            for (var i = 0; i < file.length; i++) {
                                var type = file[i].name.split(".");
                                var filename = uuidv4() + "." + type[type.length - 1];
                                file[i].mv("./public/expenses/" + filename, function (err) {
                                    if (err) { console.log(err) }
                                })
                                data.filename = filename;
                                conn.query('INSERT INTO expenses_file (filename,expenses_id) values(?,?)', [data.filename, expenses.insertId], (err, expenses_file) => {
                                    if (err) {
                                        res.json(err);
                                    }
                                });
                            }
                        }
                    }
                    res.redirect('/expenses')
                });
        } else if (data.creditors_id && data.status_pay != "" && data.status_pay != data.granttotal) {
            console.log("เพิ่ม เงินสดและเจ้าหนี้");
            var amount_pay = data.granttotal.replace(/,/g, '') - data.status_pay;
            console.log(amount_pay);
            conn.query('INSERT INTO expenses (expenseschar,expensesnumber,creditors_id,creditors_name,address,post,tax,branch,date,credit,indicate_id,due,employee_saler,employee_delivery,project_id,number,compute_id,note,notein,total,afterdiscount,except,vatcalculator,vatamount,notvatamount,alldiscount,discount_id,vat,granttotal,status_id,itemizedvat,itemizeddiscount,expenses_type,company_id,status_pay,balance)  values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)'
                , [data.expenseschar, data.expensesnumber, data.creditors_id, data.creditors_name, data.address, data.post, data.tax, data.branch, data.date, data.credit, data.indicate_id, data.due, data.employee_saler, data.employee_delivery, data.project_id, data.number, data.compute_id, data.note, data.notein, data.total.replace(/,/g, ''), data.afterdiscount.replace(/,/g, ''), data.except.replace(/,/g, ''), data.vatcalculator.replace(/,/g, ''), data.vatamount.replace(/,/g, ''), data.notvatamount.replace(/,/g, ''), data.alldiscount.replace(/,/g, ''), data.discount_id, data.vat, data.granttotal.replace(/,/g, ''), 6, data.itemizedvat, data.itemizeddiscount, data.expenses_type, data.company_id, data.status_pay, amount_pay], (err, expenses) => {
                    if (Array.isArray(data.detail)) {
                        for (var i = 0; i < data.detail.length; i++) {
                            conn.query('INSERT INTO expenses_list (detail,quantity,categoryprice,price,listdiscount,vate_id,totallist,expenses_detail,acc_buy,expenses_id)  values(?,?,?,?,?,?,?,?,?,?)', [data.detail[i], data.quantity[i], data.categoryprice[i], data.price[i], data.listdiscount[i], data.vate_id[i], data.totallist[i], data.expenses_detail[i], data.acc_buy[i], expenses.insertId], (err, expenses_list) => {
                            });
                        }
                    } else {
                        conn.query('INSERT INTO expenses_list (detail,quantity,categoryprice,price,listdiscount,vate_id,totallist,expenses_detail,acc_buy,expenses_id)  values(?,?,?,?,?,?,?,?,?,?)', [data.detail, data.quantity, data.categoryprice, data.price, data.listdiscount, data.vate_id, data.totallist, data.expenses_detail, data.acc_buy, expenses.insertId], (err, expenses_list) => {
                        });
                    }

                    var dataP = { papernumber: data.expenseschar + data.expensesnumber, amount: amount_pay, datepayment: data.date, acc_money: data.status_pay.replace(/,/g, ''), net_payout: data.status_pay.replace(/,/g, ''), withholding: "", withholding_id: "", withholding_vate: "", type_payment_id: 1, Identify: "", petty_cash: "", transfer_account: "", charge: "", check_date: "", check_number: "", check_acc_cut: "", check_charge: "", mastercard: "", typemachin_id: "", chanal_pay: "", note: "", balance: "", overdue: "", module_id: 20, reference_id: expenses.insertId };
                    conn.query('INSERT INTO  payment set ?', [dataP], (err, payment) => {
                    });
                    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    //ส่วนของ creditor เจ้าหนี้
                    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    conn.query('select * from creditors where id = ?', [data.creditors_id], (err, creditors) => {
                        if (creditors[0].balance == 0) {
                            var credits = creditors[0].crfull - amount_pay;
                            conn.query('update creditors set balance = ? , crbalance = ? where id = ? ', [amount_pay, credits, creditors[0].id], (er, credit) => {
                            })
                        } else {
                            var total = parseFloat(creditors[0].balance) + parseFloat(data.status_pay.replace(/,/g, ''));
                            var tosum = parseFloat(creditors[0].crfull) - parseFloat(total);
                            conn.query('update creditors set balance = ? , crbalance = ? where id = ? ', [total, tosum, creditors[0].id], (er, credit1) => {
                            })
                        }
                    })
                    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    //ส่วนของ GL
                    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
                    conn.query('select * from creditors where id = ?', [data.creditors_id], (err, creditors) => {
                        conn.query('select * from expenses_detail', (err, expenses_detail) => {
                            var expensesnumber = data.expenseschar + data.expensesnumber;
                            var dataH = { book_id: 1, date: data.date, doc_no: expensesnumber, detail: "", company_id: data.company_id, module_id: 20, reference_id: expenses.insertId };
                            var dataD = [];
                            conn.query('INSERT INTO  glheads set ?', [dataH], (err, glheads) => {
                                if (Array.isArray(data.expenses_detail)) {
                                    for (var i = 0; i < data.expenses_detail.length; i++) {
                                        dataD.push({ code_id: data.acc_buy[i], cheack: "", date_list: null, dr: data.totallist[i].replace(/,/g, ''), cr: 0, glheads_id: glheads.insertId });
                                    }
                                    if (data.vat && data.vat != "") {
                                        dataD.push({ code_id: 433, cheack: "", date_list: null, dr: data.vatamount.replace(/,/g, ''), cr: 0, glheads_id: glheads.insertId });
                                    }
                                    if (data.creditors_id && data.status_pay != "" && data.status_pay != data.granttotal) {
                                        dataD.push({ code_id: creditors[0].chart_id, cheack: "", date_list: null, dr: 0, cr: amount_pay, glheads_id: glheads.insertId });
                                        dataD.push({ code_id: 406, cheack: "", date_list: null, dr: 0, cr: data.status_pay.replace(/,/g, ''), glheads_id: glheads.insertId });
                                    }
                                } else {
                                    if (data.expenses_detail) {
                                        dataD.push({ code_id: data.acc_buy, cheack: "", date_list: null, dr: data.totallist.replace(/,/g, ''), cr: 0, glheads_id: glheads.insertId });
                                    }
                                    if (data.vat && data.vat != "") {
                                        dataD.push({ code_id: 433, cheack: "", date_list: null, dr: data.vatamount.replace(/,/g, ''), cr: 0, glheads_id: glheads.insertId });
                                    }
                                    if (data.creditors_id && data.status_pay != "" && data.status_pay != data.granttotal) {
                                        dataD.push({ code_id: creditors[0].chart_id, cheack: "", date_list: null, dr: 0, cr: amount_pay, glheads_id: glheads.insertId });
                                        dataD.push({ code_id: 406, cheack: "", date_list: null, dr: 0, cr: data.status_pay.replace(/,/g, ''), glheads_id: glheads.insertId });
                                    }
                                }
                                //
                                for (let i = 0; i < dataD.length; i++) {
                                    conn.query('INSERT INTO gldetail set ?', [dataD[i]], (err, gldetail) => {
                                        if (err) {
                                            res.json(err);
                                        }
                                    });
                                }
                            });
                        });
                    });
                    //
                    if (req.files) {
                        var file = req.files.filename;
                        if (!Array.isArray(file)) {
                            var type = file.name.split(".");
                            var filename = uuidv4() + "." + type[type.length - 1];
                            file.mv("./public/expenses/" + filename, function (err) {
                                if (err) { console.log(err) }
                                data.filename = filename;
                                conn.query('INSERT INTO expenses_file (filename,expenses_id) values(?,?)', [data.filename, expenses.insertId], (err, expenses_file) => {
                                    if (err) {
                                        res.json(err);
                                    }
                                });
                            })
                        } else {
                            for (var i = 0; i < file.length; i++) {
                                var type = file[i].name.split(".");
                                var filename = uuidv4() + "." + type[type.length - 1];
                                file[i].mv("./public/expenses/" + filename, function (err) {
                                    if (err) { console.log(err) }
                                })
                                data.filename = filename;
                                conn.query('INSERT INTO expenses_file (filename,expenses_id) values(?,?)', [data.filename, expenses.insertId], (err, expenses_file) => {
                                    if (err) {
                                        res.json(err);
                                    }
                                });
                            }
                        }
                    }
                    res.redirect('/expenses')
                });
        }
        //
    });
}
///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_edit
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.expenses_edit = (req, res) => {
    var url = req.params;
    req.getConnection((err, conn) => {
        conn.query('select expenses.*, expenses_list.categoryprice as categoryprice, expenses_detail.name as expenses_detail,expenses_head.name as expensesname ,projectgl.name as projectgl,compute.name as compute,expenses_list.detail as detail,expenses_list.quantity as quantity,expenses_list.price as price ,discount.name as discount,listdiscount,vate_id,totallist,filename,DATE_FORMAT(expenses.date,"%Y-%m-%d") as dateday ,DATE_FORMAT(expenses.due,"%Y-%m-%d") as dueday,expenses_list.acc_buy as acc_buy  from expenses  join expenses_list on expenses_list.expenses_id = expenses.id left join expenses_file on expenses_file.expenses_id = expenses.id left join projectgl on expenses.project_id = projectgl.id join compute on expenses.compute_id  = compute.id  left join discount on expenses.discount_id = discount.id join expenses_head on expenses.expenses_type = expenses_head.id join expenses_detail on expenses_list.expenses_detail = expenses_detail.id   where  expenses_list.expenses_id = ?;', [url.expenses], (err, expenses) => {
            //runnumber expenses
            conn.query('select * from module where id = 20', (err, expensesmodule) => {
                conn.query('select date_format(date(now()),"%Y%m") as number', (err, runnumber) => {
                    conn.query('select count(expensesnumber) as countnumber,max(expensesnumber) as maxnumber from expenses where date_format(date,"%Y%m") = date_format(date(now()),"%Y%m");', (err, expensesnumber) => {
                        //
                        conn.query('select creditors.*,  provinces.name_th as provinces, amphures.name_th as amphures, districts.name_th as districts, zip_code from creditors join provinces on creditors.provinces_id = provinces.id join amphures on creditors.amphures_id = amphures.id join districts on creditors.districts_id = districts.id', (err, creditors) => {
                            conn.query('select * from expenses_head', (err, expenses_head) => {
                                conn.query('select * from expenses_detail', (err, expenses_detail) => {
                                    conn.query('select * from human_resource', (err, human_resource) => {
                                        conn.query('select * from indicate', (err, indicate) => {
                                            conn.query('select * from witholding', (err, witholding) => {
                                                conn.query('select * from discount', (err, discount) => {
                                                    conn.query('select * from vat', (err, vat) => {
                                                        conn.query('select * from company', (err, company) => {
                                                            conn.query('select * from compute', (err, compute) => {
                                                                conn.query('select * from projectgl', (err, projectgl) => {
                                                                    //creditors
                                                                    conn.query('select * from bank', (err, bank) => {
                                                                        conn.query('select * from chart where typeAcc_id = 5;', (err, chart) => {
                                                                            conn.query('select * from provinces', (err, provinces) => {
                                                                                conn.query('select * from human', (err, human) => {
                                                                                    //runnumber creditors
                                                                                    conn.query('select * from module where id = 4', (err, creditormodule) => {
                                                                                        conn.query('select max(creditorID) as maxnumber from creditors where creditorID like ?', [creditormodule[0].char + "%"], (err, maxnumber) => {
                                                                                            res.render('buy/expenses/expenses_edit.ejs', {
                                                                                                expenses, expensesmodule, runnumber, expensesnumber, creditors, expenses_head, expenses_detail, human_resource, indicate, witholding,
                                                                                                discount, vat, company, compute, projectgl, bank, chart, provinces, human, creditormodule, maxnumber, session: req.session
                                                                                            });
                                                                                        });
                                                                                    });
                                                                                });
                                                                            });
                                                                        });
                                                                    });
                                                                });
                                                            });
                                                        });
                                                    });
                                                });
                                            });
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    });
};
///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_update
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.expenses_update = (req, res) => {
    const data = req.body;
    console.log(data);
    var url = req.params;
    req.getConnection((err, conn) => {
        conn.query('select expenses.*, expenses_list.categoryprice as categoryprice, expenses_detail.name as expenses_detail,expenses_head.name as expensesname ,projectgl.name as projectgl,compute.name as compute,expenses_list.detail as detail,expenses_list.quantity as quantity,expenses_list.price as price ,discount.name as discount,listdiscount,vate_id,totallist,filename from expenses  join expenses_list on expenses_list.expenses_id = expenses.id left join expenses_file on expenses_file.expenses_id = expenses.id left join projectgl on expenses.project_id = projectgl.id join compute on expenses.compute_id  = compute.id  left join discount on expenses.discount_id = discount.id join expenses_head on expenses.expenses_type = expenses_head.id join expenses_detail on expenses_list.expenses_detail = expenses_detail.id   where  expenses_list.expenses_id = ?;', [url.expenses], (err, select_expenses) => {
            if (data.creditors_id && data.status_pay == "") {
                console.log("เจ้าหนี้111111111111111");
                conn.query('update expenses set creditors_id = ?,creditors_name = ?,address = ?,post = ?,tax = ?,branch = ?,date = ?,credit = ?,indicate_id = ?,due = ?,employee_saler = ?,employee_delivery = ?,project_id = ?,number =  ?,compute_id = ?,note = ?,notein = ?,total = ?,afterdiscount = ?,except = ?,vatcalculator = ?,vatamount = ?,notvatamount = ?,alldiscount = ?,discount_id = ?,vat = ?,granttotal = ?,itemizedvat = ?,itemizeddiscount = ?,company_id = ?,expenses_type = ?,status_pay = ?,status_id = ? where id = ?'
                    , [data.creditors_id, data.creditors_name, data.address, data.post, data.tax, data.branch, data.date, data.credit, data.indicate_id, data.due, data.employee_saler, data.employee_delivery, data.project_id, data.number, data.compute_id, data.note, data.notein, data.total.replace(/,/g, ''), data.afterdiscount.replace(/,/g, ''), data.except.replace(/,/g, ''), data.vatcalculator.replace(/,/g, ''), data.vatamount.replace(/,/g, ''), data.notvatamount.replace(/,/g, ''), data.alldiscount.replace(/,/g, ''), data.discount_id, data.vat, data.granttotal.replace(/,/g, ''), data.itemizedvat, data.itemizeddiscount, data.company_id, data.expenses_type, data.status_pay, 2, url.expenses], (err, expenses) => {
                        conn.query('delete from expenses_list where expenses_id = ?', [url.expenses], (err, expensesdelete) => {
                            if (Array.isArray(data.detail)) {
                                for (var i = 0; i < data.detail.length; i++) {
                                    conn.query('INSERT INTO expenses_list (detail,quantity,categoryprice,price,listdiscount,vate_id,totallist,expenses_detail,acc_buy,expenses_id)  values(?,?,?,?,?,?,?,?,?,?)', [data.detail[i], data.quantity[i], data.categoryprice[i], data.price[i], data.listdiscount[i], data.vate_id[i], data.totallist[i], data.expenses_detail[i], data.acc_buy[i], url.expenses], (err, expenses_list) => {
                                        console.log(err);
                                    });
                                }
                            } else {
                                conn.query('INSERT INTO expenses_list (detail,quantity,categoryprice,price,listdiscount,vate_id,totallist,expenses_detail,acc_buy,expenses_id)  values(?,?,?,?,?,?,?,?,?,?)', [data.detail, data.quantity, data.categoryprice, data.price, data.listdiscount, data.vate_id, data.totallist, data.expenses_detail, data.acc_buy, url.expenses], (err, expenses_list) => {
                                });
                            }
                        });
                        // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        // //ส่วนของ creditor เจ้าหนี้
                        // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        conn.query('select * from creditors where id = ? ', [select_expenses[0].creditors_id], (err, creditors) => {
                            var balance = creditors[0].balance - select_expenses[0].granttotal + +(data.granttotal.replace(/,/g, ''));
                            console.log(balance);
                            var crbalance = creditors[0].crbalance - balance;
                            conn.query('update creditors set balance = ? , crbalance = ? where id = ? ', [balance, crbalance, creditors[0].id], (er, credit1) => {
                            })
                        });
                        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        //ส่วนของ GL
                        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
                        var expensesbill = select_expenses[0].expenseschar + select_expenses[0].expensesnumber;
                        conn.query('select * from glheads where company_id = ?  and doc_no = ? and module_id = ? and reference_id = ?', [select_expenses[0].company_id, expensesbill, 20, select_expenses[0].id], (err, glheads) => {
                            conn.query('Delete from gldetail  where glheads_id = ?', [glheads[0].id], (err, del_gldetail) => {
                            });
                            conn.query('Delete from glheads  where id = ?', [glheads[0].id], (err, del_glheads) => {
                            });
                        });
                        conn.query('select * from creditors where id = ?', [select_expenses[0].creditors_id], (err, creditors) => {
                            var dataH = { book_id: 1, date: data.date, doc_no: expensesbill, detail: "", company_id: data.company_id, module_id: 20, reference_id: url.expenses };
                            var dataD = [];
                            conn.query('INSERT INTO  glheads set ?', [dataH], (err, glheads) => {
                                if (Array.isArray(data.expenses_detail)) {
                                    for (var i = 0; i < data.expenses_detail.length; i++) {
                                        dataD.push({ code_id: data.acc_buy[i], cheack: "", date_list: null, dr: data.totallist[i].replace(/,/g, ''), cr: 0, glheads_id: glheads.insertId });
                                    }
                                    if (data.vat && data.vat != "") {
                                        dataD.push({ code_id: 433, cheack: "", date_list: null, dr: data.vatamount.replace(/,/g, ''), cr: 0, glheads_id: glheads.insertId });
                                    }
                                    if (data.creditors_id) {
                                        dataD.push({ code_id: creditors[0].chart_id, cheack: "", date_list: null, dr: 0, cr: data.granttotal.replace(/,/g, ''), glheads_id: glheads.insertId });
                                    }
                                } else {
                                    console.log("GL2222222");
                                    if (data.expenses_detail) {
                                        dataD.push({ code_id: data.acc_buy, cheack: "", date_list: null, dr: data.totallist.replace(/,/g, ''), cr: 0, glheads_id: glheads.insertId });
                                    }
                                    if (data.vat && data.vat != "") {
                                        dataD.push({ code_id: 433, cheack: "", date_list: null, dr: data.vatamount.replace(/,/g, ''), cr: 0, glheads_id: glheads.insertId });
                                    }
                                    if (data.creditors_id) {
                                        dataD.push({ code_id: creditors[0].chart_id, cheack: "", date_list: null, dr: 0, cr: data.granttotal.replace(/,/g, ''), glheads_id: glheads.insertId });
                                    }
                                }
                                for (let i = 0; i < dataD.length; i++) {
                                    conn.query('INSERT INTO gldetail set ?', [dataD[i]], (err, gldetail) => {
                                        if (err) {
                                            res.json(err);
                                        }
                                    });
                                }
                            });
                        });
                        if (req.files) {
                            var file = req.files.filename;
                            if (!Array.isArray(file)) {
                                var type = file.name.split(".");
                                var filename = uuidv4() + "." + type[type.length - 1];
                                file.mv("./public/expenses/" + filename, function (err) {
                                    if (err) { console.log(err) }
                                    data.filename = filename;
                                    conn.query('update expenses_file set filename = ? where expenses_id = ?', [data.filename, url.expenses], (err, expenses_file) => {
                                        if (err) {
                                            res.json(err);
                                        }
                                    });
                                })
                            } else {
                                for (var i = 0; i < file.length; i++) {
                                    var type = file[i].name.split(".");
                                    var filename = uuidv4() + "." + type[type.length - 1];
                                    file[i].mv("./public/expenses/" + filename, function (err) {
                                        if (err) { console.log(err) }
                                    })
                                    data.filename = filename;
                                    conn.query('update expenses_file set filename = ? where expenses_id = ?', [data.filename, url.expenses], (err, expenses_file) => {
                                        if (err) {
                                            res.json(err);
                                        }
                                    });
                                }
                            }
                        }
                        res.redirect('/expenses')
                    });
            } else if (data.creditors_id && data.status_pay != "" && data.status_pay == data.granttotal) {
                console.log("เงินสด22222222");
                conn.query('update expenses set creditors_id = ?,creditors_name = ?,address = ?,post = ?,tax = ?,branch = ?,date = ?,credit = ?,indicate_id = ?,due = ?,employee_saler = ?,employee_delivery = ?,project_id = ?,number =  ?,compute_id = ?,note = ?,notein = ?,total = ?,afterdiscount = ?,except = ?,vatcalculator = ?,vatamount = ?,notvatamount = ?,alldiscount = ?,discount_id = ?,vat = ?,granttotal = ?,itemizedvat = ?,itemizeddiscount = ?,company_id = ?,expenses_type = ?,status_pay = ?,status_id = ? where id = ?'
                    , [data.creditors_id, data.creditors_name, data.address, data.post, data.tax, data.branch, data.date, data.credit, data.indicate_id, data.due, data.employee_saler, data.employee_delivery, data.project_id, data.number, data.compute_id, data.note, data.notein, data.total.replace(/,/g, ''), data.afterdiscount.replace(/,/g, ''), data.except.replace(/,/g, ''), data.vatcalculator.replace(/,/g, ''), data.vatamount.replace(/,/g, ''), data.notvatamount.replace(/,/g, ''), data.alldiscount.replace(/,/g, ''), data.discount_id, data.vat, data.granttotal.replace(/,/g, ''), data.itemizedvat, data.itemizeddiscount, data.company_id, data.expenses_type, data.status_pay, 5, url.expenses], (err, expenses) => {
                        conn.query('delete from expenses_list where expenses_id = ?', [url.expenses], (err, expensesdelete) => {
                            if (Array.isArray(data.detail)) {
                                for (var i = 0; i < data.detail.length; i++) {
                                    conn.query('INSERT INTO expenses_list (detail,quantity,categoryprice,price,listdiscount,vate_id,totallist,expenses_detail,acc_buy,expenses_id)  values(?,?,?,?,?,?,?,?,?,?)', [data.detail[i], data.quantity[i], data.categoryprice[i], data.price[i], data.listdiscount[i], data.vate_id[i], data.totallist[i], data.expenses_detail[i], data.acc_buy[i], url.expenses], (err, expenses_list) => {
                                        console.log(err);
                                    });
                                }
                            } else {
                                conn.query('INSERT INTO expenses_list (detail,quantity,categoryprice,price,listdiscount,vate_id,totallist,expenses_detail,acc_buy,expenses_id)  values(?,?,?,?,?,?,?,?,?,?)', [data.detail, data.quantity, data.categoryprice, data.price, data.listdiscount, data.vate_id, data.totallist, data.expenses_detail, data.acc_buy, url.expenses], (err, expenses_list) => {
                                });
                            }
                        });
                        // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        // //ส่วนของ creditor เจ้าหนี้
                        // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        conn.query('select * from creditors where id = ? ', [select_expenses[0].creditors_id], (err, creditors) => {
                            var balance = creditors[0].balance - select_expenses[0].granttotal;
                            var crbalance = creditors[0].crbalance - select_expenses[0].granttotal;
                            conn.query('update creditors set balance = ? , crbalance = ? where id = ? ', [balance, crbalance, creditors[0].id], (er, credit1) => {
                                console.log(err);
                            })
                        });
                        var expensesbill = select_expenses[0].expenseschar + select_expenses[0].expensesnumber;
                        var dataP = { papernumber: expensesbill, amount: data.granttotal.replace(/,/g, ''), datepayment: data.date, acc_money: data.granttotal.replace(/,/g, ''), net_payout: data.granttotal.replace(/,/g, ''), withholding: "", withholding_id: "", withholding_vate: "", type_payment_id: 1, Identify: "", petty_cash: "", transfer_account: "", charge: "", check_date: "", check_number: "", check_acc_cut: "", check_charge: "", mastercard: "", typemachin_id: "", chanal_pay: "", note: "", balance: "", overdue: "", module_id: 20, reference_id: url.expenses };
                        console.log(dataP);
                        conn.query('INSERT INTO  payment set ?', [dataP], (err, payment) => {
                        });
                        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        //ส่วนของ GL
                        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
                        conn.query('select * from creditors where id = ?', [select_expenses[0].creditors_id], (err, creditors) => {
                            var dataH = { book_id: 1, date: data.date, doc_no: expensesbill, detail: "", company_id: data.company_id, module_id: 20, reference_id: url.expenses };
                            var dataD = [];
                            conn.query('INSERT INTO  glheads set ?', [dataH], (err, glheads) => {
                                if (Array.isArray(data.expenses_detail)) {
                                    for (var i = 0; i < data.expenses_detail.length; i++) {
                                        dataD.push({ code_id: data.acc_buy[i], cheack: "", date_list: null, dr: data.totallist[i].replace(/,/g, ''), cr: 0, glheads_id: glheads.insertId });
                                    }
                                    if (data.vat && data.vat != "") {
                                        dataD.push({ code_id: 433, cheack: "", date_list: null, dr: data.vatamount.replace(/,/g, ''), cr: 0, glheads_id: glheads.insertId });
                                    }
                                    if (data.creditors_id && data.status_pay != "") {
                                        dataD.push({ code_id: 406, cheack: "", date_list: null, dr: 0, cr: data.granttotal.replace(/,/g, ''), glheads_id: glheads.insertId });
                                    }
                                } else {
                                    console.log("GL2222222");
                                    if (data.expenses_detail) {
                                        dataD.push({ code_id: data.acc_buy, cheack: "", date_list: null, dr: data.totallist.replace(/,/g, ''), cr: 0, glheads_id: glheads.insertId });
                                    }
                                    if (data.vat && data.vat != "") {
                                        dataD.push({ code_id: 433, cheack: "", date_list: null, dr: data.vatamount.replace(/,/g, ''), cr: 0, glheads_id: glheads.insertId });
                                    }
                                    if (data.creditors_id && data.status_pay != "") {
                                        dataD.push({ code_id: 406, cheack: "", date_list: null, dr: 0, cr: data.granttotal.replace(/,/g, ''), glheads_id: glheads.insertId });
                                    }
                                }
                                for (let i = 0; i < dataD.length; i++) {
                                    conn.query('INSERT INTO gldetail set ?', [dataD[i]], (err, gldetail) => {
                                        if (err) {
                                            res.json(err);
                                        }
                                    });
                                }
                            });
                        });
                        if (req.files) {
                            var file = req.files.filename;
                            if (!Array.isArray(file)) {
                                var type = file.name.split(".");
                                var filename = uuidv4() + "." + type[type.length - 1];
                                file.mv("./public/expenses/" + filename, function (err) {
                                    if (err) { console.log(err) }
                                    data.filename = filename;
                                    conn.query('update expenses_file set filename = ? where expenses_id = ?', [data.filename, url.expenses], (err, expenses_file) => {
                                        if (err) {
                                            res.json(err);
                                        }
                                    });
                                })
                            } else {
                                for (var i = 0; i < file.length; i++) {
                                    var type = file[i].name.split(".");
                                    var filename = uuidv4() + "." + type[type.length - 1];
                                    file[i].mv("./public/expenses/" + filename, function (err) {
                                        if (err) { console.log(err) }
                                    })
                                    data.filename = filename;
                                    conn.query('update expenses_file set filename = ? where expenses_id = ?', [data.filename, url.expenses], (err, expenses_file) => {
                                        if (err) {
                                            res.json(err);
                                        }
                                    });
                                }
                            }
                        }
                        res.redirect('/expenses')
                    });
            } else if (data.creditors_id && data.status_pay != "" && data.status_pay != data.granttotal) {
                console.log("เงินสดและเจ้าหนี้33333333333");
                conn.query('update expenses set creditors_id = ?,creditors_name = ?,address = ?,post = ?,tax = ?,branch = ?,date = ?,credit = ?,indicate_id = ?,due = ?,employee_saler = ?,employee_delivery = ?,project_id = ?,number =  ?,compute_id = ?,note = ?,notein = ?,total = ?,afterdiscount = ?,except = ?,vatcalculator = ?,vatamount = ?,notvatamount = ?,alldiscount = ?,discount_id = ?,vat = ?,granttotal = ?,itemizedvat = ?,itemizeddiscount = ?,company_id = ?,expenses_type = ?,status_pay = ?,status_id = ? where id = ?'
                    , [data.creditors_id, data.creditors_name, data.address, data.post, data.tax, data.branch, data.date, data.credit, data.indicate_id, data.due, data.employee_saler, data.employee_delivery, data.project_id, data.number, data.compute_id, data.note, data.notein, data.total.replace(/,/g, ''), data.afterdiscount.replace(/,/g, ''), data.except.replace(/,/g, ''), data.vatcalculator.replace(/,/g, ''), data.vatamount.replace(/,/g, ''), data.notvatamount.replace(/,/g, ''), data.alldiscount.replace(/,/g, ''), data.discount_id, data.vat, data.granttotal.replace(/,/g, ''), data.itemizedvat, data.itemizeddiscount, data.company_id, data.expenses_type, data.status_pay, 6, url.expenses], (err, expenses) => {
                        conn.query('delete from expenses_list where expenses_id = ?', [url.expenses], (err, expensesdelete) => {
                            if (Array.isArray(data.detail)) {
                                for (var i = 0; i < data.detail.length; i++) {
                                    conn.query('INSERT INTO expenses_list (detail,quantity,categoryprice,price,listdiscount,vate_id,totallist,expenses_detail,acc_buy,expenses_id)  values(?,?,?,?,?,?,?,?,?,?)', [data.detail[i], data.quantity[i], data.categoryprice[i], data.price[i], data.listdiscount[i], data.vate_id[i], data.totallist[i], data.expenses_detail[i], data.acc_buy[i], url.expenses], (err, expenses_list) => {
                                        console.log(err);
                                    });
                                }
                            } else {
                                conn.query('INSERT INTO expenses_list (detail,quantity,categoryprice,price,listdiscount,vate_id,totallist,expenses_detail,acc_buy,expenses_id)  values(?,?,?,?,?,?,?,?,?,?)', [data.detail, data.quantity, data.categoryprice, data.price, data.listdiscount, data.vate_id, data.totallist, data.expenses_detail, data.acc_buy, url.expenses], (err, expenses_list) => {
                                });
                            }
                        });
                        // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        // //ส่วนของ creditor เจ้าหนี้
                        // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        var amount_pay = data.granttotal.replace(/,/g, '') - data.status_pay;
                        console.log(amount_pay);
                        conn.query('select * from creditors where id = ? ', [select_expenses[0].creditors_id], (err, creditors) => {
                            var balance = creditors[0].balance - select_expenses[0].balance + amount_pay;
                            console.log(balance);
                            var crbalance = creditors[0].crbalance - balance;
                            conn.query('update creditors set balance = ? , crbalance = ? where id = ? ', [balance, crbalance, creditors[0].id], (er, credit1) => {
                                console.log(err);
                            })
                        });
                        var expensesbill = select_expenses[0].expenseschar + select_expenses[0].expensesnumber;
                        conn.query('delete from payment where papernumber = ? , reference_id = ?', [expensesbill, url.expenses], (err, payment) => {
                        })
                        var dataP = { papernumber: expensesbill, amount: amount_pay, datepayment: data.date, acc_money: data.status_pay, net_payout: data.status_pay, withholding: "", withholding_id: "", withholding_vate: "", type_payment_id: 1, Identify: "", petty_cash: "", transfer_account: "", charge: "", check_date: "", check_number: "", check_acc_cut: "", check_charge: "", mastercard: "", typemachin_id: "", chanal_pay: "", note: "", balance: "", overdue: "", module_id: 20, reference_id: url.expenses };
                        console.log(dataP);
                        conn.query('INSERT INTO  payment set ?', [dataP], (err, payment) => {
                            conn.query('update expenses set balance = ? where id = ?', [amount_pay, url.expenses], (err, update_expenses) => {
                                console.log(err);
                            })
                        });
                        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        //ส่วนของ GL
                        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
                        conn.query('select * from glheads where company_id = ?  and doc_no = ? and module_id = ? and reference_id = ?', [select_expenses[0].company_id, expensesbill, 20, select_expenses[0].id], (err, glheads) => {
                            conn.query('Delete from gldetail  where glheads_id = ?', [glheads[0].id], (err, del_gldetail) => {
                            });
                            conn.query('Delete from glheads  where id = ?', [glheads[0].id], (err, del_glheads) => {
                            });
                        });
                        conn.query('select * from creditors where id = ?', [select_expenses[0].creditors_id], (err, creditors) => {
                            var dataH = { book_id: 1, date: data.date, doc_no: expensesbill, detail: "", company_id: data.company_id, module_id: 20, reference_id: url.expenses };
                            var dataD = [];
                            conn.query('INSERT INTO  glheads set ?', [dataH], (err, glheads) => {
                                if (Array.isArray(data.expenses_detail)) {
                                    for (var i = 0; i < data.expenses_detail.length; i++) {
                                        dataD.push({ code_id: data.acc_buy[i], cheack: "", date_list: null, dr: data.totallist[i].replace(/,/g, ''), cr: 0, glheads_id: glheads.insertId });
                                    }
                                    if (data.vat && data.vat != "") {
                                        dataD.push({ code_id: 433, cheack: "", date_list: null, dr: data.vatamount.replace(/,/g, ''), cr: 0, glheads_id: glheads.insertId });
                                    }
                                    if (data.creditors_id && data.status_pay != "" && data.status_pay != data.granttotal) {
                                        dataD.push({ code_id: creditors[0].chart_id, cheack: "", date_list: null, dr: 0, cr: amount_pay, glheads_id: glheads.insertId });
                                        dataD.push({ code_id: 406, cheack: "", date_list: null, dr: 0, cr: data.status_pay.replace(/,/g, ''), glheads_id: glheads.insertId });
                                    }
                                } else {
                                    console.log("GL2222222");
                                    if (data.expenses_detail) {
                                        dataD.push({ code_id: data.acc_buy, cheack: "", date_list: null, dr: data.totallist.replace(/,/g, ''), cr: 0, glheads_id: glheads.insertId });
                                    }
                                    if (data.vat && data.vat != "") {
                                        dataD.push({ code_id: 433, cheack: "", date_list: null, dr: data.vatamount.replace(/,/g, ''), cr: 0, glheads_id: glheads.insertId });
                                    }
                                    if (data.creditors_id && data.status_pay != "" && data.status_pay != data.granttotal) {
                                        dataD.push({ code_id: creditors[0].chart_id, cheack: "", date_list: null, dr: 0, cr: amount_pay, glheads_id: glheads.insertId });
                                        dataD.push({ code_id: 406, cheack: "", date_list: null, dr: 0, cr: data.status_pay.replace(/,/g, ''), glheads_id: glheads.insertId });
                                    }
                                }
                                for (let i = 0; i < dataD.length; i++) {
                                    conn.query('INSERT INTO gldetail set ?', [dataD[i]], (err, gldetail) => {
                                        if (err) {
                                            res.json(err);
                                        }
                                    });
                                }
                            });
                        });
                        if (req.files) {
                            var file = req.files.filename;
                            if (!Array.isArray(file)) {
                                var type = file.name.split(".");
                                var filename = uuidv4() + "." + type[type.length - 1];
                                file.mv("./public/expenses/" + filename, function (err) {
                                    if (err) { console.log(err) }
                                    data.filename = filename;
                                    conn.query('update expenses_file set filename = ? where expenses_id = ?', [data.filename, url.expenses], (err, expenses_file) => {
                                        if (err) {
                                            res.json(err);
                                        }
                                    });
                                })
                            } else {
                                for (var i = 0; i < file.length; i++) {
                                    var type = file[i].name.split(".");
                                    var filename = uuidv4() + "." + type[type.length - 1];
                                    file[i].mv("./public/expenses/" + filename, function (err) {
                                        if (err) { console.log(err) }
                                    })
                                    data.filename = filename;
                                    conn.query('update expenses_file set filename = ? where expenses_id = ?', [data.filename, url.expenses], (err, expenses_file) => {
                                        if (err) {
                                            res.json(err);
                                        }
                                    });
                                }
                            }
                        }
                        res.redirect('/expenses')
                    });
            }
            ///
        });
    });
}
///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_detail
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.expenses_detail = (req, res) => {
    var url = req.params;
    req.getConnection((err, conn) => {
        conn.query(' select expenses.*,expenses_detail.name as expenses_detail,company.branchNo as namebranch,expenses_head.name as expenses_name, projectgl.name as projectgl,compute.name as compute,expenses_list.detail as detail,expenses_list.quantity as quantity,expenses_list.price as price ,discount.name as discount,listdiscount,vate_id,totallist,categoryprice,filename,DATE_FORMAT(expenses.date,"%Y-%m-%d") as dateday ,DATE_FORMAT(expenses.due,"%Y-%m-%d") as dueday from expenses  join expenses_list on expenses_list.expenses_id = expenses.id left join expenses_file on expenses_file.expenses_id = expenses.id left join projectgl on expenses.project_id = projectgl.id join compute on expenses.compute_id  = compute.id  left join discount on expenses.discount_id = discount.id join expenses_head on expenses.expenses_type = expenses_head.id join expenses_detail on expenses_list.expenses_detail = expenses_detail.id join company on expenses.company_id = company.id where  expenses_list.expenses_id = ?;', [url.expenses], (err, expenses) => {
            //////////////////////////////////////////////////////////
            var yearen_1 = expenses[0].dateday.split('-')
            var yearth_1 = +(yearen_1[0]) + 543
            var dateth = yearen_1[2] + "/" + yearen_1[1] + "/" + yearth_1;
            //////////////////////////////////////////////////////////
            if (expenses[0].indicate_id == 1) {
                var yearen_2 = expenses[0].dueday.split('-');
                var yearth_2 = +(yearen_2[0]) + 543
                var dueth = yearen_2[2] + "/" + yearen_2[1] + "/" + yearth_2;
            }
            //////////////////////////////////////////////////////////
            conn.query('select * from witholding', (err, witholding) => {
                res.render('buy/expenses/expenses_detail.ejs', {
                    expenses, dateth, dueth, witholding,
                    session: req.session
                });
            });
        });
    });
};
///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_delete
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.expenses_delete = (req, res) => {
    var url = req.params;
    req.getConnection((err, conn) => {
        conn.query('Delete from expenses_file where expenses_id = ?', [url.expenses], (err, expenses_file) => {
            conn.query('select expenses. *,expenses_list.acc_buy as acc_buy,expenses_list.detail as detail,expenses_list.expenses_detail as expenses_detail from expenses join expenses_list on expenses_list.expenses_id = expenses.id where expenses.id = ?;', [url.expenses], (err, expenses) => {
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                //ส่วนของ creditors
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                conn.query('select * from creditors where id = ? ', [expenses[0].creditors_id], (err, creditors) => {
                    if (expenses[0].status_pay < expenses[0].granttotal && expenses[0].balance != "") {
                        console.log("กรณีที่ 3 ");
                        var sum = expenses[0].granttotal - expenses[0].status_pay;
                        if (creditors[0].balance == sum) {
                            var sum1 = creditors[0].crbalance + sum;
                            conn.query('update creditors set crbalance = ? , balance = ? where id = ?', [sum1, 0, creditors[0].id], (err, creditor) => {
                            });
                        } else {
                            var sum1 = creditors[0].balance - expenses[0].balance;
                            var sum2 = creditors[0].crbalance + expenses[0].balance;
                            conn.query('update creditors set   balance = ?, crbalance = ? where id = ?', [sum1, sum2, creditors[0].id], (err, creditor) => {
                            });
                        }
                    }
                    if (expenses[0].status_pay == "" && expenses[0].balance == "" || expenses[0].status_pay != "" && expenses[0].balance == "") {
                        console.log("กรณีที่ 1,2");
                        if (creditors[0].balance == expenses[0].granttotal) {
                            var sum1 = creditors[0].crbalance + expenses[0].granttotal;
                            conn.query('update creditors set crbalance = ? , balance = ? where id = ?', [sum1, 0, creditors[0].id], (err, creditor) => {
                            });
                        } else {
                            var sum1 = creditors[0].balance - expenses[0].granttotal;
                            var sum2 = creditors[0].crbalance + expenses[0].granttotal;
                            conn.query('update creditors set crbalance = ? , balance = ? where id = ?', [sum2, sum1, creditors[0].id], (err, creditor) => {
                            });
                        }
                    }
                });
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                //ส่วนของ GL
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                if (expenses[0].status_pay == "" && expenses[0].balance == "") {
                    console.log("กรณีที่ 11111");
                    var expensesbill = expenses[0].expenseschar + expenses[0].expensesnumber;
                    conn.query('select * from payment where reference_id = ? and papernumber = ?', [url.expenses, expensesbill], (err, select_payment) => {
                        conn.query('select * from glheads where company_id = ?  and doc_no = ? and module_id = ? and reference_id = ?  ', [expenses[0].company_id, expensesbill, 20, url.expenses], (err, glheads) => {
                            conn.query('Delete from gldetail  where glheads_id = ?', [glheads[0].id], (err, gldetail) => {
                                console.log(err);
                                conn.query('Delete from glheads  where id = ?', [glheads[0].id], (err, del_glheads) => {
                                    console.log(err);
                                });
                            });
                            //
                            conn.query('Delete from expenses_list where expenses_id = ?', [url.expenses], (err, expenses_list) => {
                                conn.query('Delete from expenses where id = ?', [url.expenses], (err, expenses) => {
                                    res.redirect('/expenses');
                                });
                            });
                        });
                    });
                }
                if (expenses[0].status_pay.replace(/,/g, '') == expenses[0].granttotal && expenses[0].status_pay != "") {
                    console.log("กรณีที่ 222222");
                    conn.query('Delete from expenses_list where expenses_id = ?', [url.expenses], (err, expenses_list) => {
                        conn.query('Delete from expenses where id = ?', [url.expenses], (err, expenses) => {
                            res.redirect('/expenses');
                        });
                    });
                }
                if (expenses[0].status_pay.replace(/,/g, '') != expenses[0].granttotal && expenses[0].balance != "") {
                    console.log("กรณีที่ 333333");
                    var expensesbill = expenses[0].expenseschar + expenses[0].expensesnumber;
                    conn.query('Delete from payment where reference_id = ? and papernumber = ?', [url.expenses, expensesbill], (err, select_payment) => {
                        conn.query('select * from glheads where company_id = ?  and doc_no = ? and module_id = ? and reference_id = ?  ', [expenses[0].company_id, expensesbill, 20, url.expenses], (err, glheads) => {
                            conn.query('Delete from gldetail  where glheads_id = ?', [glheads[0].id], (err, gldetail) => {
                                conn.query('Delete from glheads  where id = ?', [glheads[0].id], (err, del_glheads) => {
                                });
                            });
                            //
                            conn.query('Delete from expenses_list where expenses_id = ?', [url.expenses], (err, expenses_list) => {
                                conn.query('Delete from expenses where id = ?', [url.expenses], (err, expenses) => {
                                    res.redirect('/expenses');
                                });
                            });
                        });
                    });
                }
                ///
            });
        });
    });
};
///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_readd
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.expenses_readd = (req, res) => {
    var url = req.params;
    req.getConnection((err, conn) => {
        conn.query('select expenses.*, expenses_list.categoryprice as categoryprice, expenses_detail.name as expenses_detail,expenses_head.name as expensesname ,projectgl.name as projectgl,compute.name as compute,expenses_list.detail as detail,expenses_list.quantity as quantity,expenses_list.price as price ,discount.name as discount,listdiscount,vate_id,totallist,filename,DATE_FORMAT(expenses.date,"%Y-%m-%d") as dateday ,DATE_FORMAT(expenses.due,"%Y-%m-%d") as dueday,expenses_list.acc_buy as acc_buy from expenses  join expenses_list on expenses_list.expenses_id = expenses.id left join expenses_file on expenses_file.expenses_id = expenses.id left join projectgl on expenses.project_id = projectgl.id join compute on expenses.compute_id  = compute.id  left join discount on expenses.discount_id = discount.id join expenses_head on expenses.expenses_type = expenses_head.id join expenses_detail on expenses_list.expenses_detail = expenses_detail.id   where  expenses_list.expenses_id = ?;', [url.expenses], (err, expenses) => {
            //runnumber expenses
            conn.query('select * from module where id = 20', (err, expensesmodule) => {
                conn.query('select date_format(date(now()),"%Y%m") as number', (err, runnumber) => {
                    conn.query('select count(expensesnumber) as countnumber,max(expensesnumber) as maxnumber from expenses where date_format(date,"%Y%m") = date_format(date(now()),"%Y%m");', (err, expensesnumber) => {
                        //
                        conn.query('select creditors.*,  provinces.name_th as provinces, amphures.name_th as amphures, districts.name_th as districts, zip_code from creditors join provinces on creditors.provinces_id = provinces.id join amphures on creditors.amphures_id = amphures.id join districts on creditors.districts_id = districts.id', (err, creditors) => {
                            conn.query('select * from expenses_head', (err, expenses_head) => {
                                conn.query('select * from expenses_detail', (err, expenses_detail) => {
                                    conn.query('select * from company', (err, company) => {
                                        conn.query('select * from human_resource', (err, human_resource) => {
                                            conn.query('select * from indicate', (err, indicate) => {
                                                conn.query('select * from witholding', (err, witholding) => {
                                                    conn.query('select * from discount', (err, discount) => {
                                                        conn.query('select * from vat', (err, vat) => {
                                                            conn.query('select * from compute', (err, compute) => {
                                                                conn.query('select * from projectgl', (err, projectgl) => {
                                                                    //creditors
                                                                    conn.query('select * from chart where typeAcc_id = 5;', (err, chart) => {
                                                                        conn.query('select * from bank', (err, bank) => {
                                                                            conn.query('select * from provinces', (err, provinces) => {
                                                                                conn.query('select * from human', (err, human) => {
                                                                                    //runnumber creditors
                                                                                    conn.query('select * from module where id = 4', (err, creditormodule) => {
                                                                                        conn.query('select max(creditorID) as maxnumber from creditors where creditorID like ?', [creditormodule[0].char + "%"], (err, maxnumber) => {
                                                                                            res.render('buy/expenses/expenses_readd.ejs', {
                                                                                                expenses, expensesmodule, runnumber, expensesnumber, creditors, expenses_head, human_resource, indicate, witholding, discount, vat,
                                                                                                compute, projectgl, bank, provinces, human, creditormodule, maxnumber, chart, company, expenses_detail, session: req.session
                                                                                            });
                                                                                        });
                                                                                    });
                                                                                });
                                                                            });
                                                                        });
                                                                    });
                                                                });
                                                            });
                                                        });
                                                    });
                                                });
                                            });
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    });
};
///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_ajax
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.api_expenses = (req, res) => {
    var url = req.params;
    req.getConnection((err, conn) => {
        conn.query('select * from expenses_detail where expenses_id = ?', [url.expenses], (err, expenses) => {
            res.send(expenses);
        });
    });
};
module.exports = controller;