const session = require("express-session");
const controller = {};
const { v4: uuidv4 } = require('uuid');
const fs = require('fs');
const { Cookie } = require("express-session");
const e = require("express");


///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_list
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.receipt_list = (req, res) => {
  req.getConnection((err, conn) => {
    conn.query('select receipt.*, status.name as status ,DATE_FORMAT(DATE_ADD(receipt.date, INTERVAL 543 YEAR), " %d/%m/%Y" )dateday ,DATE_FORMAT(DATE_ADD(receipt.due, INTERVAL 543 YEAR), " %d/%m/%Y" )dueday,DATE_FORMAT(receipt.date,"%Y-%m-%d") as receiptdate from receipt join status on status_id = status.id where YEAR(date) = YEAR(CURDATE())', (err, receipt) => {
      conn.query('select * from status ', (err, status) => {
        conn.query('select * from type_payment;', (err, typepayment) => {
          conn.query('select * from witholding', (err, witholding) => {
            conn.query('select * from petty_cash', (err, petty_cash) => {
              res.render('buy/product_receipt/product_receipt_list.ejs', {
                receipt, status, typepayment, witholding, petty_cash,
                session: req.session
              });
            });
          })
        });
      });
    });
  });
};
///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_add
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.receipt_add = (req, res) => {
  req.getConnection((err, conn) => {
    //runnumber receipt
    conn.query('select * from module where id = 12', (err, receiptmodule) => {
      conn.query('select date_format(date(now()),"%Y%m") as number', (err, runnumber) => {
        conn.query('select count(receiptordernumber) as countnumber,max(receiptordernumber) as maxnumber from receipt where date_format(date,"%Y%m") = date_format(date(now()),"%Y%m");', (err, receiptnumber) => {
          //
          conn.query('select creditors.*,  provinces.name_th as provinces, amphures.name_th as amphures, districts.name_th as districts, zip_code from creditors join provinces on creditors.provinces_id = provinces.id join amphures on creditors.amphures_id = amphures.id join districts on creditors.districts_id = districts.id', (err, creditors) => {
            conn.query('select * from inventory_list;', (err, inventory) => {
              conn.query('select * from human_resource', (err, human_resource) => {
                conn.query('select * from indicate', (err, indicate) => {
                  conn.query('select * from category_price', (err, category_price) => {
                    conn.query('select * from witholding', (err, witholding) => {
                      conn.query('select * from discount', (err, discount) => {
                        conn.query('select * from company', (err, company) => {
                          conn.query('select * from vat', (err, vat) => {
                            conn.query('select * from compute', (err, compute) => {
                              conn.query('select * from projectgl', (err, projectgl) => {
                                //creditors
                                conn.query('select * from chart where typeAcc_id = 5;', (err, chart) => {
                                  conn.query('select * from human', (err, human) => {
                                    conn.query('select * from bank', (err, bank) => {
                                      conn.query('select * from provinces', (err, provinces) => {
                                        //product
                                        conn.query('select * from chart where typeAcc_id = 9;', (err, acc_sell) => {
                                          conn.query('select * from chart where typeAcc_id = 11;', (err, acc_buy) => {
                                            conn.query('select * from product_list where enable = 1', (err, product_list) => {
                                              conn.query('select * from product_type', (err, product_type) => {
                                                conn.query('select * from product_type_list', (err, product_type_list) => {
                                                  conn.query('select * from product_tax', (err, product_tax) => {
                                                    conn.query('select * from product_group', (err, product_group) => {
                                                      //runnumber creditors
                                                      conn.query('select * from module where id = 4', (err, creditormodule) => {
                                                        conn.query('select max(creditorID) as maxnumber from creditors where creditorID like ?', [creditormodule[0].char + "%"], (err, maxnumber) => {
                                                          res.render('buy/product_receipt/product_receipt_add.ejs', {
                                                            creditors, indicate, witholding, discount, vat, compute, projectgl, bank, provinces, product_list, runnumber, human, creditormodule, maxnumber, receiptnumber, inventory, product_group,
                                                            product_type, product_type_list, product_tax, category_price, human_resource, session: req.session, receiptmodule, chart, acc_sell, acc_buy, company
                                                          });
                                                        });
                                                      });
                                                    });
                                                  });
                                                });
                                              });
                                            });
                                          });
                                        });
                                      });
                                    });
                                  });
                                });
                              });
                            });
                          });
                        });
                      });
                    });
                  });
                });
              });
            });
          });
        });
      });
    });
  });
};
///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_save
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.receipt_save = (req, res) => {
  const data = req.body;
  console.log(data);
  req.getConnection((err, conn) => {
    var acc_buy
    if (Array.isArray(data.acc_buy)) {
      acc_buy = data.acc_buy[0];
    } else {
      acc_buy = data.acc_buy;
    }
    if (data.creditors_id && data.status_pay == "") {
      console.log("เป็นหนี้");
      conn.query('INSERT INTO receipt (receiptordernumber,receiptchar,creditors_id,creditors_name,address,post,tax,branch,date,credit,indicate_id,due,employee_saler,project_id,number,compute_id,signature,note,notein,total,alldiscount,afterdiscount,except,vatcalculator,vat,vatamount,notvatamount,granttotal,witholding_id,witholding,payment,status_id,itemizedvat,itemizeddiscount,employee_delivery,balance,inventory_id,company_id,status_pay,acc_buy)  values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)'
        , [data.receiptordernumber, data.receiptchar, data.creditors_id, data.creditors_name, data.address, data.post, data.tax, data.branch, data.date, data.credit, data.indicate_id, data.due, data.employee_saler, data.project_id, data.number, data.compute_id, data.signature, data.note, data.notein, data.total.replace(/,/g, ''), data.alldiscount.replace(/,/g, ''), data.afterdiscount.replace(/,/g, ''), data.except.replace(/,/g, ''), data.vatcalculator.replace(/,/g, ''), data.vat, data.vatamount.replace(/,/g, ''), data.notvatamount.replace(/,/g, ''), data.granttotal.replace(/,/g, ''), data.witholding_id, data.witholding.replace(/,/g, ''), data.payment.replace(/,/g, ''), 2, data.itemizedvat, data.itemizeddiscount, data.employee_delivery, 0, data.inventory_id, data.company_id, data.status_pay, acc_buy], (err, receipt) => {
          if (Array.isArray(data.product_id)) {
            for (var i = 0; i < data.product_id.length; i++) {
              var numberunit = data.quantity[i] * data.unitnumber[i], total = data.totallist[i], productnow = data.product_id[i];
              conn.query('INSERT INTO receipt_list (product_id,detail,quantity,categoryprice_id,price,discount_id,listdiscount,vat_id,totallist,subunits,product_code,receipt_id,status_receipt_list)  values(?,?,?,?,?,?,?,?,?,?,?,?,1)', [data.product_id[i], data.detail[i], data.quantity[i], data.categoryprice_id[i], data.price[i], data.discount_id[i], data.listdiscount[i], data.vat_id[i], data.totallist[i], data.quantity[i] * data.unitnumber[i], data.product_code[i], receipt.insertId], (err, receipt_list) => {
              });
              conn.query('update inventory set cost_product = (((cost_product*number_product) + ?)/(number_product + ?)),number_product = number_product +  ? where product_list_id = ? and inventory_list_id = ?;', [total, numberunit, numberunit, productnow, data.inventory_id], (err, inventory) => {
              });
            }
          } else {
            var numberunit = data.quantity * data.unitnumber;
            conn.query('INSERT INTO receipt_list (product_id,detail,quantity,categoryprice_id,price,discount_id,listdiscount,vat_id,totallist ,subunits,product_code,receipt_id,status_receipt_list)  values(?,?,?,?,?,?,?,?,?,?,?,?,1)', [data.product_id, data.detail, data.quantity, data.categoryprice_id, data.price, data.discount_id, data.listdiscount, data.vat_id, data.totallist, data.quantity * data.unitnumber, data.product_code, receipt.insertId], (err, receipt_list) => {
            });
            conn.query('update inventory set cost_product = (((cost_product*number_product) + ?)/(number_product + ?)),number_product = number_product +  ? where product_list_id = ? and inventory_list_id = ?;', [data.totallist, numberunit, numberunit, data.product_id, data.inventory_id], (err, inventory) => {
            });
          }
          //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
          //ส่วนของ creditor เจ้าหนี้
          //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
          conn.query('select * from creditors where id = ?', [data.creditors_id], (err, creditors) => {
            if (creditors[0].balance == 0) {
              var credits = creditors[0].crfull - data.granttotal.replace(/,/g, '');
              conn.query('update creditors set balance = ? , crbalance = ? where id = ? ', [data.granttotal.replace(/,/g, ''), credits, creditors[0].id], (er, credit) => {
              })
            } else {
              var total = parseFloat(creditors[0].balance) + parseFloat(data.granttotal.replace(/,/g, ''));
              var tosum = parseFloat(creditors[0].crfull) - parseFloat(total);
              conn.query('update creditors set balance = ? , crbalance = ? where id = ? ', [total, tosum, creditors[0].id], (er, credit1) => {
              })
            }
          })
          //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
          //ส่วนของ GL
          //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
          conn.query('select * from creditors where id = ?', [data.creditors_id], (err, creditors) => {
            var numberchar = data.receiptchar + data.receiptordernumber;
            var dataH = { book_id: 1, date: data.date, doc_no: numberchar, detail: data.detail, company_id: data.company_id, module_id: 12, reference_id: receipt.insertId };
            var dataD = [];
            conn.query('INSERT INTO  glheads set ?', [dataH], (err, glheads) => {
              if (err) {
                res.json(err);
              } else {
                var acc_buy;
                if (Array.isArray(data.acc_buy)) {
                  acc_buy = data.acc_buy[0];
                } else {
                  acc_buy = data.acc_buy;
                }
                if (data.total && data.total != "") {
                  dataD.push({ code_id: acc_buy, cheack: "", date_list: null, dr: data.totallist.replace(/,/g, ''), cr: 0, glheads_id: glheads.insertId });
                }
                if (data.vat && data.vat != "") {
                  dataD.push({ code_id: 433, cheack: "", date_list: null, dr: data.vatamount.replace(/,/g, ''), cr: 0, glheads_id: glheads.insertId });
                }
                if (data.withholding && data.withholding != "") {
                  dataD.push({ code_id: 434, cheack: "", date_list: null, dr: data.withholding.replace(/,/g, ''), cr: 0, glheads_id: glheads.insertId });
                }
                if (data.creditors_id && data.status_pay == "") {
                  dataD.push({ code_id: creditors[0].chart_id, cheack: "", date_list: null, dr: 0, cr: data.granttotal.replace(/,/g, ''), glheads_id: glheads.insertId });
                }
                for (let i = 0; i < dataD.length; i++) {
                  conn.query('INSERT INTO gldetail set ?', [dataD[i]], (err, gldetail) => {
                    if (err) {
                      res.json(err);
                    }
                  });
                }
              }
            });
          });
          //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
          if (req.files) {
            var file = req.files.filename;
            if (!Array.isArray(file)) {
              var type = file.name.split(".");
              var filename = uuidv4() + "." + type[type.length - 1];
              file.mv("./public/receipt/" + filename, function (err) {
                if (err) { console.log(err) }
                data.filename = filename;
                conn.query('INSERT INTO receipt_file (filename,receipt_id) values(?,?)', [data.filename, receipt.insertId], (err, receipt_file) => {
                  if (err) {
                    res.json(err);
                  }
                });
              })
            } else {
              for (var i = 0; i < file.length; i++) {
                var type = file[i].name.split(".");
                var filename = uuidv4() + "." + type[type.length - 1];
                file[i].mv("./public/receipt/" + filename, function (err) {
                  if (err) { console.log(err) }
                })
                data.filename = filename;
                conn.query('INSERT INTO receipt_file (filename,receipt_id) values(?,?)', [data.filename, receipt.insertId], (err, receipt_file) => {
                  if (err) {
                    console.log(err);
                    res.json(err);
                  }
                });
              }
            }
          }
          res.redirect('/product_receipt')
        });
    } else if (data.creditors_id && data.status_pay != "" && data.status_pay == data.granttotal) {
      console.log("money");
      conn.query('INSERT INTO receipt (receiptordernumber,receiptchar,creditors_id,creditors_name,address,post,tax,branch,date,credit,indicate_id,due,employee_saler,project_id,number,compute_id,signature,note,notein,total,alldiscount,afterdiscount,except,vatcalculator,vat,vatamount,notvatamount,granttotal,witholding_id,witholding,payment,status_id,itemizedvat,itemizeddiscount,employee_delivery,balance,inventory_id,company_id,status_pay,acc_buy)  values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)'
        , [data.receiptordernumber, data.receiptchar, data.creditors_id, data.creditors_name, data.address, data.post, data.tax, data.branch, data.date, data.credit, data.indicate_id, data.due, data.employee_saler, data.project_id, data.number, data.compute_id, data.signature, data.note, data.notein, data.total.replace(/,/g, ''), data.alldiscount.replace(/,/g, ''), data.afterdiscount.replace(/,/g, ''), data.except.replace(/,/g, ''), data.vatcalculator.replace(/,/g, ''), data.vat, data.vatamount.replace(/,/g, ''), data.notvatamount.replace(/,/g, ''), data.granttotal.replace(/,/g, ''), data.witholding_id, data.witholding.replace(/,/g, ''), data.payment.replace(/,/g, ''), 5, data.itemizedvat, data.itemizeddiscount, data.employee_delivery, data.balance, data.inventory_id, data.company_id, data.status_pay, acc_buy], (err, receipt) => {
          if (Array.isArray(data.product_id)) {
            for (var i = 0; i < data.product_id.length; i++) {
              var numberunit = data.quantity[i] * data.unitnumber[i], total = data.totallist[i], productnow = data.product_id[i];
              conn.query('INSERT INTO receipt_list (product_id,detail,quantity,categoryprice_id,price,discount_id,listdiscount,vat_id,totallist,subunits,product_code,receipt_id,status_receipt_list)  values(?,?,?,?,?,?,?,?,?,?,?,?,1)', [data.product_id[i], data.detail[i], data.quantity[i], data.categoryprice_id[i], data.price[i], data.discount_id[i], data.listdiscount[i], data.vat_id[i], data.totallist[i], data.quantity[i] * data.unitnumber[i], data.product_code[i], receipt.insertId], (err, receipt_list) => {
              });
              conn.query('update inventory set cost_product = (((cost_product*number_product) + ?)/(number_product + ?)),number_product = number_product +  ? where product_list_id = ? and inventory_list_id = ?;', [total, numberunit, numberunit, productnow, data.inventory_id], (err, inventory) => {
              });
            }
          } else {
            var numberunit = data.quantity * data.unitnumber;
            conn.query('INSERT INTO receipt_list (product_id,detail,quantity,categoryprice_id,price,discount_id,listdiscount,vat_id,totallist ,subunits,product_code,receipt_id,status_receipt_list)  values(?,?,?,?,?,?,?,?,?,?,?,?,1)', [data.product_id, data.detail, data.quantity, data.categoryprice_id, data.price, data.discount_id, data.listdiscount, data.vat_id, data.totallist, data.quantity * data.unitnumber, data.product_code, receipt.insertId], (err, receipt_list) => {
            });
            conn.query('update inventory set cost_product = (((cost_product*number_product) + ?)/(number_product + ?)),number_product = number_product +  ? where product_list_id = ? and inventory_list_id = ?;', [data.totallist, numberunit, numberunit, data.product_id, data.inventory_id], (err, inventory) => {
            });
          }
          var dataP = { papernumber: data.receiptchar + data.receiptordernumber, amount: data.granttotal.replace(/,/g, ''), datepayment: data.date, acc_money: data.granttotal.replace(/,/g, ''), net_payout: data.granttotal.replace(/,/g, ''), withholding: "", withholding_id: "", withholding_vate: "", type_payment_id: 1, Identify: "", petty_cash: "", transfer_account: "", charge: "", check_date: "", check_number: "", check_acc_cut: "", check_charge: "", mastercard: "", typemachin_id: "", chanal_pay: "", note: "", balance: "", overdue: "", module_id: 12, reference_id: receipt.insertId };
          console.log(dataP);
          conn.query('INSERT INTO  payment set ?', [dataP], (err, payment) => {
          });
          //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
          //ส่วนของ GL
          //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
          conn.query('select * from creditors where id = ?', [data.creditors_id], (err, creditors) => {
            var numberchar = data.receiptchar + data.receiptordernumber;
            var dataH = { book_id: 1, date: data.date, doc_no: numberchar, detail: data.detail, company_id: data.company_id, module_id: 12, reference_id: receipt.insertId };
            var dataD = [];
            conn.query('INSERT INTO  glheads set ?', [dataH], (err, glheads) => {
              if (err) {
                res.json(err);
              } else {
                var acc_buy;
                if (Array.isArray(data.acc_buy)) {
                  acc_buy = data.acc_buy[0];
                } else {
                  acc_buy = data.acc_buy;
                }
                if (data.total && data.total != "") {
                  dataD.push({ code_id: acc_buy, cheack: "", date_list: null, dr: data.totallist.replace(/,/g, ''), cr: 0, glheads_id: glheads.insertId });
                }
                if (data.vat && data.vat != "") {
                  dataD.push({ code_id: 433, cheack: "", date_list: null, dr: data.vatamount.replace(/,/g, ''), cr: 0, glheads_id: glheads.insertId });
                }
                if (data.withholding && data.withholding != "") {
                  dataD.push({ code_id: 434, cheack: "", date_list: null, dr: data.withholding.replace(/,/g, ''), cr: 0, glheads_id: glheads.insertId });
                }
                if (data.creditors_id && data.status_pay != "") {
                  dataD.push({ code_id: 406, cheack: "", date_list: null, dr: 0, cr: data.granttotal.replace(/,/g, ''), glheads_id: glheads.insertId });
                }
                for (let i = 0; i < dataD.length; i++) {
                  conn.query('INSERT INTO gldetail set ?', [dataD[i]], (err, gldetail) => {
                    if (err) {
                      res.json(err);
                    }
                  });
                }
              }
            });
          });
          //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
          if (req.files) {
            var file = req.files.filename;
            if (!Array.isArray(file)) {
              var type = file.name.split(".");
              var filename = uuidv4() + "." + type[type.length - 1];
              file.mv("./public/receipt/" + filename, function (err) {
                if (err) { console.log(err) }
                data.filename = filename;
                conn.query('INSERT INTO receipt_file (filename,receipt_id) values(?,?)', [data.filename, receipt.insertId], (err, receipt_file) => {
                  if (err) {
                    res.json(err);
                  }
                });
              })
            } else {
              for (var i = 0; i < file.length; i++) {
                var type = file[i].name.split(".");
                var filename = uuidv4() + "." + type[type.length - 1];
                file[i].mv("./public/receipt/" + filename, function (err) {
                  if (err) { console.log(err) }
                })
                data.filename = filename;
                conn.query('INSERT INTO receipt_file (filename,receipt_id) values(?,?)', [data.filename, receipt.insertId], (err, receipt_file) => {
                  if (err) {
                    console.log(err);
                    res.json(err);
                  }
                });
              }
            }
          }
          res.redirect('/product_receipt')
        });
    } else if (data.creditors_id && data.status_pay != "" && data.status_pay != data.granttotal) {
      console.log("creditors and money");
      var amount_pay = data.granttotal.replace(/,/g, '') - data.status_pay;
      conn.query('INSERT INTO receipt (receiptordernumber,receiptchar,creditors_id,creditors_name,address,post,tax,branch,date,credit,indicate_id,due,employee_saler,project_id,number,compute_id,signature,note,notein,total,alldiscount,afterdiscount,except,vatcalculator,vat,vatamount,notvatamount,granttotal,witholding_id,witholding,payment,status_id,itemizedvat,itemizeddiscount,employee_delivery,balance,inventory_id,company_id,status_pay,acc_buy)  values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)'
        , [data.receiptordernumber, data.receiptchar, data.creditors_id, data.creditors_name, data.address, data.post, data.tax, data.branch, data.date, data.credit, data.indicate_id, data.due, data.employee_saler, data.project_id, data.number, data.compute_id, data.signature, data.note, data.notein, data.total.replace(/,/g, ''), data.alldiscount.replace(/,/g, ''), data.afterdiscount.replace(/,/g, ''), data.except.replace(/,/g, ''), data.vatcalculator.replace(/,/g, ''), data.vat, data.vatamount.replace(/,/g, ''), data.notvatamount.replace(/,/g, ''), data.granttotal.replace(/,/g, ''), data.witholding_id, data.witholding.replace(/,/g, ''), data.payment.replace(/,/g, ''), 6, data.itemizedvat, data.itemizeddiscount, data.employee_delivery, amount_pay, data.inventory_id, data.company_id, data.status_pay, acc_buy], (err, receipt) => {
          if (Array.isArray(data.product_id)) {
            for (var i = 0; i < data.product_id.length; i++) {
              var numberunit = data.quantity[i] * data.unitnumber[i], total = data.totallist[i], productnow = data.product_id[i];
              conn.query('INSERT INTO receipt_list (product_id,detail,quantity,categoryprice_id,price,discount_id,listdiscount,vat_id,totallist,subunits,product_code,receipt_id,status_receipt_list)  values(?,?,?,?,?,?,?,?,?,?,?,?,1)', [data.product_id[i], data.detail[i], data.quantity[i], data.categoryprice_id[i], data.price[i], data.discount_id[i], data.listdiscount[i], data.vat_id[i], data.totallist[i], data.quantity[i] * data.unitnumber[i], data.product_code[i], receipt.insertId], (err, receipt_list) => {
              });
              conn.query('update inventory set cost_product = (((cost_product*number_product) + ?)/(number_product + ?)),number_product = number_product +  ? where product_list_id = ? and inventory_list_id = ?;', [total, numberunit, numberunit, productnow, data.inventory_id], (err, inventory) => {
              });
            }
          } else {
            var numberunit = data.quantity * data.unitnumber;
            conn.query('INSERT INTO receipt_list (product_id,detail,quantity,categoryprice_id,price,discount_id,listdiscount,vat_id,totallist ,subunits,product_code,receipt_id,status_receipt_list)  values(?,?,?,?,?,?,?,?,?,?,?,?,1)', [data.product_id, data.detail, data.quantity, data.categoryprice_id, data.price, data.discount_id, data.listdiscount, data.vat_id, data.totallist, data.quantity * data.unitnumber, data.product_code, receipt.insertId], (err, receipt_list) => {
            });
            conn.query('update inventory set cost_product = (((cost_product*number_product) + ?)/(number_product + ?)),number_product = number_product +  ? where product_list_id = ? and inventory_list_id = ?;', [data.totallist, numberunit, numberunit, data.product_id, data.inventory_id], (err, inventory) => {
            });
          }

          var dataP = { papernumber: data.receiptchar + data.receiptordernumber, amount: amount_pay, datepayment: data.date, acc_money: data.status_pay.replace(/,/g, ''), net_payout: data.status_pay.replace(/,/g, ''), withholding: "", withholding_id: "", withholding_vate: "", type_payment_id: 1, Identify: "", petty_cash: "", transfer_account: "", charge: "", check_date: "", check_number: "", check_acc_cut: "", check_charge: "", mastercard: "", typemachin_id: "", chanal_pay: "", note: "", balance: "", overdue: "", module_id: 12, reference_id: receipt.insertId };
          conn.query('INSERT INTO  payment set ?', [dataP], (err, payment) => {
          });
          //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
          //ส่วนของ creditor เจ้าหนี้
          //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
          conn.query('select * from creditors where id = ?', [data.creditors_id], (err, creditors) => {
            if (creditors[0].balance == 0) {
              var credits = creditors[0].crfull - amount_pay;
              conn.query('update creditors set balance = ? , crbalance = ? where id = ? ', [amount_pay, credits, creditors[0].id], (er, credit) => {
              })
            } else {
              var total = parseFloat(creditors[0].balance) + parseFloat(data.status_pay.replace(/,/g, ''));
              var tosum = parseFloat(creditors[0].crfull) - parseFloat(total);
              conn.query('update creditors set balance = ? , crbalance = ? where id = ? ', [total, tosum, creditors[0].id], (er, credit1) => {
              })
            }
          })
          //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
          //ส่วนของ GL
          //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
          conn.query('select * from creditors where id = ?', [data.creditors_id], (err, creditors) => {
            var numberchar = data.receiptchar + data.receiptordernumber;
            var dataH = { book_id: 1, date: data.date, doc_no: numberchar, detail: data.detail, company_id: data.company_id, module_id: 12, reference_id: receipt.insertId };
            var dataD = [];
            conn.query('INSERT INTO  glheads set ?', [dataH], (err, glheads) => {
              if (err) {
                res.json(err);
              } else {
                var acc_buy;
                if (Array.isArray(data.acc_buy)) {
                  acc_buy = data.acc_buy[0];
                } else {
                  acc_buy = data.acc_buy;
                }
                if (data.total && data.total != "") {
                  dataD.push({ code_id: acc_buy, cheack: "", date_list: null, dr: data.totallist.replace(/,/g, ''), cr: 0, glheads_id: glheads.insertId });
                }
                if (data.vat && data.vat != "") {
                  dataD.push({ code_id: 433, cheack: "", date_list: null, dr: data.vatamount.replace(/,/g, ''), cr: 0, glheads_id: glheads.insertId });
                }
                if (data.withholding && data.withholding != "") {
                  dataD.push({ code_id: 434, cheack: "", date_list: null, dr: data.withholding.replace(/,/g, ''), cr: 0, glheads_id: glheads.insertId });
                }
                if (data.creditors_id && data.status_pay != "" && data.status_pay != data.granttotal) {
                  dataD.push({ code_id: creditors[0].chart_id, cheack: "", date_list: null, dr: 0, cr: amount_pay, glheads_id: glheads.insertId });
                  dataD.push({ code_id: 406, cheack: "", date_list: null, dr: 0, cr: data.status_pay.replace(/,/g, ''), glheads_id: glheads.insertId });
                }
                for (let i = 0; i < dataD.length; i++) {
                  conn.query('INSERT INTO gldetail set ?', [dataD[i]], (err, gldetail) => {
                    if (err) {
                      res.json(err);
                    }
                  });
                }
              }
            });
          });
          //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
          if (req.files) {
            var file = req.files.filename;
            if (!Array.isArray(file)) {
              var type = file.name.split(".");
              var filename = uuidv4() + "." + type[type.length - 1];
              file.mv("./public/receipt/" + filename, function (err) {
                if (err) { console.log(err) }
                data.filename = filename;
                conn.query('INSERT INTO receipt_file (filename,receipt_id) values(?,?)', [data.filename, receipt.insertId], (err, receipt_file) => {
                  if (err) {
                    res.json(err);
                  }
                });
              })
            } else {
              for (var i = 0; i < file.length; i++) {
                var type = file[i].name.split(".");
                var filename = uuidv4() + "." + type[type.length - 1];
                file[i].mv("./public/receipt/" + filename, function (err) {
                  if (err) { console.log(err) }
                })
                data.filename = filename;
                conn.query('INSERT INTO receipt_file (filename,receipt_id) values(?,?)', [data.filename, receipt.insertId], (err, receipt_file) => {
                  if (err) {
                    console.log(err);
                    res.json(err);
                  }
                });
              }
            }
          }
          res.redirect('/product_receipt')
        });
    }
  });
}

///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_edit
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.receipt_edit = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('select receipt.* ,product_id,projectgl.name as projectgl,compute.name as compute,product_list.name as name,category_price.name as category,categoryprice_id,receipt_list.detail as detail,receipt_list.quantity as quantity,receipt_list.price as price ,discount.name as discount,listdiscount,vat_id,totallist,product_code,filename,DATE_FORMAT(receipt.date,"%Y-%m-%d") as dateday ,DATE_FORMAT(receipt.due,"%Y-%m-%d") as dueday from receipt  join receipt_list on receipt_list.receipt_id = receipt.id left join receipt_file on receipt_file.receipt_id = receipt.id left join projectgl on receipt.project_id = projectgl.id join compute on receipt.compute_id  = compute.id  join product_list on receipt_list.product_id = product_list.id join category_price  on receipt_list.categoryprice_id = category_price.id left join discount on receipt_list.discount_id = discount.id join inventory_list on receipt.inventory_id = inventory_list.id join company on receipt.company_id = company.id where  receipt_list.receipt_id = ?;', [url.receipt], (err, receipt) => {
      //runnumber receipt
      conn.query('select * from module where id = 12', (err, receiptmodule) => {
        conn.query('select date_format(date(now()),"%Y%m") as number', (err, runnumber) => {
          conn.query('select count(receiptordernumber) as countnumber,max(receiptordernumber) as maxnumber from receipt where date_format(date,"%Y%m") = date_format(date(now()),"%Y%m");', (err, receiptnumber) => {
            //
            conn.query('select creditors.*,  provinces.name_th as provinces, amphures.name_th as amphures, districts.name_th as districts, zip_code from creditors join provinces on creditors.provinces_id = provinces.id join amphures on creditors.amphures_id = amphures.id join districts on creditors.districts_id = districts.id', (err, creditors) => {
              conn.query('select * from human_resource', (err, human_resource) => {
                conn.query('select * from inventory_list;', (err, inventory) => {
                  conn.query('select * from indicate', (err, indicate) => {
                    conn.query('select * from category_price where product_list_id = ?', [receipt[0].product_id], (err, category_price) => {
                      conn.query('select * from witholding', (err, witholding) => {
                        conn.query('select * from discount', (err, discount) => {
                          conn.query('select * from vat', (err, vat) => {
                            conn.query('select * from company', (err, company) => {
                              conn.query('select * from compute', (err, compute) => {
                                conn.query('select * from projectgl', (err, projectgl) => {
                                  //creditors
                                  conn.query('select * from chart where typeAcc_id = 5;', (err, chart) => {
                                    conn.query('select * from bank', (err, bank) => {
                                      conn.query('select * from provinces', (err, provinces) => {
                                        conn.query('select * from human', (err, human) => {
                                          //product
                                          conn.query('select * from chart where typeAcc_id = 9;', (err, acc_sell) => {
                                            conn.query('select * from chart where typeAcc_id = 11;', (err, acc_buy) => {
                                              conn.query('select product_list.* from product_list join inventory on inventory.product_list_id = product_list.id where product_list.enable = 1 and inventory.inventory_list_id = ?', [receipt[0].inventory_id], (err, product_list) => {
                                                conn.query('select * from product_type', (err, product_type) => {
                                                  conn.query('select * from product_type_list', (err, product_type_list) => {
                                                    conn.query('select * from product_tax', (err, product_tax) => {
                                                      conn.query('select * from product_group', (err, product_group) => {
                                                        //runnumber creditors
                                                        conn.query('select * from module where id = 4', (err, creditormodule) => {
                                                          conn.query('select max(creditorID) as maxnumber from creditors where creditorID like ?', [creditormodule[0].char + "%"], (err, maxnumber) => {
                                                            res.render('buy/product_receipt/product_receipt_edit.ejs', {
                                                              receipt, receiptmodule, runnumber, receiptnumber, creditors, human_resource, inventory, indicate, category_price, witholding, company, discount, creditormodule,
                                                              vat, compute, projectgl, chart, bank, provinces, human, acc_sell, acc_buy, product_list, product_type, product_type_list, product_tax, product_group, maxnumber,
                                                              session: req.session
                                                            });
                                                          });
                                                        });
                                                      });
                                                    });
                                                  });
                                                });
                                              });
                                            });
                                          });
                                        });
                                      });
                                    });
                                  });
                                });
                              });
                            });
                          });
                        });
                      });
                    });
                  });
                });
              });
            });
          });
        });
      });
    });
  });
};
///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_update
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.receipt_update = (req, res) => {
  console.log("conupdate");
  var url = req.params;
  const data = req.body;
  console.log(data);
  req.getConnection((err, conn) => {
    conn.query('select receipt.*, receipt_list.quantity as quantity,product_id,inventory_id,subunits,totallist from receipt join receipt_list on receipt_list.receipt_id = receipt.id  where receipt.id = ?;', [url.receipt], (err, select_receipt) => {
      if (data.creditors_id && data.status_pay == "") {
        conn.query('update receipt set creditors_id = ? , creditors_name = ? ,address = ? , post = ? , tax = ? , branch = ? , date = ? , credit = ? , indicate_id = ? , due = ? ,employee_saler = ? ,  project_id = ? ,number = ? , compute_id = ? , signature = ? , note = ? , notein = ? , total = ? ,alldiscount = ? ,  afterdiscount = ? , except = ? , vatcalculator = ? ,vat = ? , vatamount = ? ,notvatamount = ? , granttotal = ?,witholding_id = ? , witholding = ? , payment = ? ,  itemizedvat = ? , itemizeddiscount = ?  , employee_delivery = ? , inventory_id = ? , company_id = ?,status_pay = ? , acc_buy = ?  where id = ? ',
          [data.creditors_id, data.creditors_name, data.address, data.post, data.tax, data.branch, data.date, data.credit, data.indicate_id, data.due, data.employee_saler, data.project_id, data.number, data.compute_id, data.signature, data.note, data.notein, data.total.replace(/,/g, ''), data.alldiscount.replace(/,/g, ''), data.afterdiscount.replace(/,/g, ''), data.except.replace(/,/g, ''), data.vatcalculator.replace(/,/g, ''), data.vat, data.vatamount.replace(/,/g, ''), data.notvatamount.replace(/,/g, ''), data.granttotal.replace(/,/g, ''), data.witholding_id, data.witholding.replace(/,/g, ''), data.payment.replace(/,/g, ''), data.itemizedvat, data.itemizeddiscount, data.employee_delivery, data.inventory_id, data.company_id, data.status_pay, data.acc_buy, url.receipt], (err, receipt) => {
            conn.query('delete from receipt_list where receipt_id = ?', [url.receipt], (err, receipt_product) => {
            });
            if (Array.isArray(data.product_id)) {
              for (var i = 0; i < data.product_id.length; i++) {
                var numberunit = data.quantity[i] * data.unitnumber[i], total = data.totallist[i], productnow = data.product_id[i];
                conn.query('INSERT INTO receipt_list (product_id,detail,quantity,categoryprice_id,price,discount_id,listdiscount,vat_id,totallist,subunits,product_code,receipt_id,status_receipt_list)  values(?,?,?,?,?,?,?,?,?,?,?,?,1)', [data.product_id[i], data.detail[i], data.quantity[i], data.categoryprice_id[i], data.price[i], data.discount_id[i], data.listdiscount[i], data.vat_id[i], data.totallist[i], numberunit, data.product_code[i], url.receipt], (err, receipt_list) => {
                  conn.query('select  * from inventory where inventory_list_id = ? and product_list_id = ? ', [data.inventory_id, productnow], (err, select_inventory) => {
                    var unit = (select_inventory[0].number_product - select_receipt[0].subunits) + numberunit;
                    var average = (+(((select_inventory[0].cost_product * select_inventory[0].number_product) - select_receipt[0].totallist) + +(total)) / +((select_inventory[0].number_product - select_receipt[0].subunits) + numberunit));
                    conn.query('update inventory set number_product =  ?,cost_product = ? where product_list_id = ? and inventory_list_id = ?;', [unit, average, productnow, data.inventory_id], (err, inventory) => {
                    });
                  });
                });
              }
            } else {
              var numberunit = data.quantity * data.unitnumber;
              conn.query('INSERT INTO receipt_list (product_id,detail,quantity,categoryprice_id,price,discount_id,listdiscount,vat_id,totallist ,subunits,product_code,receipt_id,status_receipt_list)  values(?,?,?,?,?,?,?,?,?,?,?,?,1)', [data.product_id, data.detail, data.quantity, data.categoryprice_id, data.price, data.discount_id, data.listdiscount, data.vat_id, data.totallist, numberunit, data.product_code, url.receipt], (err, receipt_list) => {
                conn.query('select  * from inventory where inventory_list_id = ? and product_list_id = ? ', [data.inventory_id, data.product_id], (err, select_inventory) => {
                  var unit = (select_inventory[0].number_product - select_receipt[0].subunits) + numberunit;
                  var average = (+(((select_inventory[0].cost_product * select_inventory[0].number_product) - select_receipt[0].totallist) + +(data.totallist)) / +((select_inventory[0].number_product - select_receipt[0].subunits) + numberunit))
                  conn.query('update inventory set  number_product =  ?,cost_product = ? where product_list_id = ? and inventory_list_id = ?;', [unit, average, data.product_id, data.inventory_id], (err, inventory) => {
                  });
                });
              });
            }
            // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // //ส่วนของ creditor เจ้าหนี้
            // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            conn.query('select * from creditors where id = ? ', [select_receipt[0].creditors_id], (err, creditors) => {
              var balance = creditors[0].balance - select_receipt[0].granttotal + +(data.granttotal.replace(/,/g, ''));
              var crbalance = creditors[0].crbalance - balance;
              conn.query('update creditors set balance = ? , crbalance = ? where id = ? ', [balance, crbalance, creditors[0].id], (er, credit1) => {
                console.log(err);
              })
            });
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //ส่วนของ GL
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            var receiptbill = select_receipt[0].receiptchar + select_receipt[0].receiptordernumber;
            conn.query('select * from glheads where company_id = ?  and doc_no = ? and module_id = ? and reference_id = ?', [select_receipt[0].company_id, receiptbill, 12, select_receipt[0].id], (err, glheads) => {
              conn.query('Delete from gldetail  where glheads_id = ?', [glheads[0].id], (err, del_gldetail) => {
              });
              conn.query('Delete from glheads  where id = ?', [glheads[0].id], (err, del_glheads) => {
              });
            });
            conn.query('select * from creditors where id = ?', [select_receipt[0].creditors_id], (err, creditors) => {
              var numberchar = select_receipt[0].receiptchar + select_receipt[0].receiptordernumber;
              var dataH = { book_id: 1, date: data.date, doc_no: numberchar, detail: data.detail, company_id: data.company_id, module_id: 12, reference_id: url.receipt };
              var dataD = [];
              conn.query('INSERT INTO  glheads set ?', [dataH], (err, glheads) => {
                var acc_buy;
                if (Array.isArray(data.acc_buy)) {
                  acc_buy = data.acc_buy[0];
                } else {
                  acc_buy = data.acc_buy;
                }
                if (data.total && data.total != "") {
                  dataD.push({ code_id: acc_buy, cheack: "", date_list: null, dr: data.totallist.replace(/,/g, ''), cr: 0, glheads_id: glheads.insertId });
                }
                if (data.vat && data.vat != "") {
                  dataD.push({ code_id: 433, cheack: "", date_list: null, dr: data.vatamount.replace(/,/g, ''), cr: 0, glheads_id: glheads.insertId });
                }
                if (data.withholding && data.withholding != "") {
                  dataD.push({ code_id: 434, cheack: "", date_list: null, dr: data.withholding.replace(/,/g, ''), cr: 0, glheads_id: glheads.insertId });
                }
                if (data.creditors_id && data.status_pay == "") {
                  dataD.push({ code_id: creditors[0].chart_id, cheack: "", date_list: null, dr: 0, cr: data.granttotal.replace(/,/g, ''), glheads_id: glheads.insertId });
                }
                for (let i = 0; i < dataD.length; i++) {
                  conn.query('INSERT INTO gldetail set ?', [dataD[i]], (err, gldetail) => {
                  });
                }
              });
            });
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if (req.files) {
              var file = req.files.filename;
              if (!Array.isArray(file)) {
                var type = file.name.split(".");
                var filename = uuidv4() + "." + type[type.length - 1];
                file.mv("./public/receipt/" + filename, function (err) {
                  if (err) { console.log(err) }
                  data.filename = filename;
                  conn.query('update  receipt_file set filename = ? where receipt_id = ?', [data.filename, url.receipt], (err, receipt_file) => {
                    if (err) {
                      res.json(err);
                    }
                  });
                })
              } else {
                for (var i = 0; i < file.length; i++) {
                  var type = file[i].name.split(".");
                  var filename = uuidv4() + "." + type[type.length - 1];
                  file[i].mv("./public/receipt/" + filename, function (err) {
                    if (err) { console.log(err) }
                  })
                  data.filename = filename;
                  conn.query('update  receipt_file set filename = ? where receipt_id = ?', [data.filename, url.receipt], (err, receipt_file) => {
                    if (err) {
                      res.json(err);
                    }
                  });
                }
              }
            }
            res.redirect('/product_receipt')
          });
      } else if (data.creditors_id && data.status_pay != "" && data.status_pay == data.granttotal) {
        console.log("22222222222222");
        conn.query('update receipt set creditors_id = ? , creditors_name = ? ,address = ? , post = ? , tax = ? , branch = ? , date = ? , credit = ? , indicate_id = ? , due = ? ,employee_saler = ? ,  project_id = ? ,number = ? , compute_id = ? , signature = ? , note = ? , notein = ? , total = ? ,alldiscount = ? ,  afterdiscount = ? , except = ? , vatcalculator = ? ,vat = ? , vatamount = ? ,notvatamount = ? , granttotal = ?,witholding_id = ? , witholding = ? , payment = ? ,  itemizedvat = ? , itemizeddiscount = ?  , employee_delivery = ? , inventory_id = ? , company_id = ?,status_pay = ? , acc_buy = ?,status_id = ?  where id = ? ',
          [data.creditors_id, data.creditors_name, data.address, data.post, data.tax, data.branch, data.date, data.credit, data.indicate_id, data.due, data.employee_saler, data.project_id, data.number, data.compute_id, data.signature, data.note, data.notein, data.total.replace(/,/g, ''), data.alldiscount.replace(/,/g, ''), data.afterdiscount.replace(/,/g, ''), data.except.replace(/,/g, ''), data.vatcalculator.replace(/,/g, ''), data.vat, data.vatamount.replace(/,/g, ''), data.notvatamount.replace(/,/g, ''), data.granttotal.replace(/,/g, ''), data.witholding_id, data.witholding.replace(/,/g, ''), data.payment.replace(/,/g, ''), data.itemizedvat, data.itemizeddiscount, data.employee_delivery, data.inventory_id, data.company_id, data.status_pay, data.acc_buy, 5, url.receipt], (err, receipt) => {
            conn.query('delete from receipt_list where receipt_id = ?', [url.receipt], (err, receipt_product) => {
            });
            if (Array.isArray(data.product_id)) {
              for (var i = 0; i < data.product_id.length; i++) {
                var numberunit = data.quantity[i] * data.unitnumber[i], total = data.totallist[i], productnow = data.product_id[i];
                conn.query('INSERT INTO receipt_list (product_id,detail,quantity,categoryprice_id,price,discount_id,listdiscount,vat_id,totallist,subunits,product_code,receipt_id,status_receipt_list)  values(?,?,?,?,?,?,?,?,?,?,?,?,1)', [data.product_id[i], data.detail[i], data.quantity[i], data.categoryprice_id[i], data.price[i], data.discount_id[i], data.listdiscount[i], data.vat_id[i], data.totallist[i], numberunit, data.product_code[i], url.receipt], (err, receipt_list) => {
                  conn.query('select  * from inventory where inventory_list_id = ? and product_list_id = ? ', [data.inventory_id, productnow], (err, select_inventory) => {
                    var unit = (select_inventory[0].number_product - select_receipt[0].subunits) + numberunit;
                    var average = (+(((select_inventory[0].cost_product * select_inventory[0].number_product) - select_receipt[0].totallist) + +(total)) / +((select_inventory[0].number_product - select_receipt[0].subunits) + numberunit));
                    conn.query('update inventory set number_product =  ?,cost_product = ? where product_list_id = ? and inventory_list_id = ?;', [unit, average, productnow, data.inventory_id], (err, inventory) => {
                    });
                  });
                });
              }
            } else {
              var numberunit = data.quantity * data.unitnumber;
              conn.query('INSERT INTO receipt_list (product_id,detail,quantity,categoryprice_id,price,discount_id,listdiscount,vat_id,totallist ,subunits,product_code,receipt_id,status_receipt_list)  values(?,?,?,?,?,?,?,?,?,?,?,?,1)', [data.product_id, data.detail, data.quantity, data.categoryprice_id, data.price, data.discount_id, data.listdiscount, data.vat_id, data.totallist, numberunit, data.product_code, url.receipt], (err, receipt_list) => {
                conn.query('select  * from inventory where inventory_list_id = ? and product_list_id = ? ', [data.inventory_id, data.product_id], (err, select_inventory) => {
                  var unit = (select_inventory[0].number_product - select_receipt[0].subunits) + numberunit;
                  var average = (+(((select_inventory[0].cost_product * select_inventory[0].number_product) - select_receipt[0].totallist) + +(data.totallist)) / +((select_inventory[0].number_product - select_receipt[0].subunits) + numberunit))
                  conn.query('update inventory set  number_product =  ?,cost_product = ? where product_list_id = ? and inventory_list_id = ?;', [unit, average, data.product_id, data.inventory_id], (err, inventory) => {
                  });
                });
              });
            }
            // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // //ส่วนของ creditor เจ้าหนี้
            // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            conn.query('select * from creditors where id = ? ', [select_receipt[0].creditors_id], (err, creditors) => {
              var balance = creditors[0].balance - select_receipt[0].granttotal;
              var crbalance = creditors[0].crbalance - select_receipt[0].granttotal;
              conn.query('update creditors set balance = ? , crbalance = ? where id = ? ', [balance, crbalance, creditors[0].id], (er, credit1) => {
                console.log(err);
              })
            });
            var numberchar = select_receipt[0].receiptchar + select_receipt[0].receiptordernumber;
            var dataP = { papernumber: numberchar, amount: data.granttotal.replace(/,/g, ''), datepayment: data.date, acc_money: data.granttotal.replace(/,/g, ''), net_payout: data.granttotal.replace(/,/g, ''), withholding: "", withholding_id: "", withholding_vate: "", type_payment_id: 1, Identify: "", petty_cash: "", transfer_account: "", charge: "", check_date: "", check_number: "", check_acc_cut: "", check_charge: "", mastercard: "", typemachin_id: "", chanal_pay: "", note: "", balance: "", overdue: "", module_id: 12, reference_id: url.receipt };
            console.log(dataP);
            conn.query('INSERT INTO  payment set ?', [dataP], (err, payment) => {
            });
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //ส่วนของ GL
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            var numberchar = select_receipt[0].receiptchar + select_receipt[0].receiptordernumber;
            conn.query('select * from creditors where id = ?', [data.creditors_id], (err, creditors) => {
              var dataH = { book_id: 1, date: data.date, doc_no: numberchar, detail: data.detail, company_id: data.company_id, module_id: 12, reference_id: url.receipt };
              var dataD = [];
              conn.query('INSERT INTO  glheads set ?', [dataH], (err, glheads) => {
                if (err) {
                  res.json(err);
                } else {
                  var acc_buy;
                  if (Array.isArray(data.acc_buy)) {
                    acc_buy = data.acc_buy[0];
                  } else {
                    acc_buy = data.acc_buy;
                  }
                  if (data.total && data.total != "") {
                    dataD.push({ code_id: acc_buy, cheack: "", date_list: null, dr: data.totallist.replace(/,/g, ''), cr: 0, glheads_id: glheads.insertId });
                  }
                  if (data.vat && data.vat != "") {
                    dataD.push({ code_id: 433, cheack: "", date_list: null, dr: data.vatamount.replace(/,/g, ''), cr: 0, glheads_id: glheads.insertId });
                  }
                  if (data.withholding && data.withholding != "") {
                    dataD.push({ code_id: 434, cheack: "", date_list: null, dr: data.withholding.replace(/,/g, ''), cr: 0, glheads_id: glheads.insertId });
                  }
                  if (data.creditors_id && data.status_pay != "") {
                    dataD.push({ code_id: 406, cheack: "", date_list: null, dr: 0, cr: data.granttotal.replace(/,/g, ''), glheads_id: glheads.insertId });
                  }
                  for (let i = 0; i < dataD.length; i++) {
                    conn.query('INSERT INTO gldetail set ?', [dataD[i]], (err, gldetail) => {
                      if (err) {
                        res.json(err);
                      }
                    });
                  }
                }
              });
            });
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if (req.files) {
              var file = req.files.filename;
              if (!Array.isArray(file)) {
                var type = file.name.split(".");
                var filename = uuidv4() + "." + type[type.length - 1];
                file.mv("./public/receipt/" + filename, function (err) {
                  if (err) { console.log(err) }
                  data.filename = filename;
                  conn.query('update  receipt_file set filename = ? where receipt_id = ?', [data.filename, url.receipt], (err, receipt_file) => {
                    if (err) {
                      res.json(err);
                    }
                  });
                })
              } else {
                for (var i = 0; i < file.length; i++) {
                  var type = file[i].name.split(".");
                  var filename = uuidv4() + "." + type[type.length - 1];
                  file[i].mv("./public/receipt/" + filename, function (err) {
                    if (err) { console.log(err) }
                  })
                  data.filename = filename;
                  conn.query('update  receipt_file set filename = ? where receipt_id = ?', [data.filename, url.receipt], (err, receipt_file) => {
                    if (err) {
                      res.json(err);
                    }
                  });
                }
              }
            }
            res.redirect('/product_receipt')
          });
      } else if (data.creditors_id && data.status_pay != "" && data.status_pay != data.granttotal) {
        console.log("33333333333333");
        conn.query('update receipt set creditors_id = ? , creditors_name = ? ,address = ? , post = ? , tax = ? , branch = ? , date = ? , credit = ? , indicate_id = ? , due = ? ,employee_saler = ? ,  project_id = ? ,number = ? , compute_id = ? , signature = ? , note = ? , notein = ? , total = ? ,alldiscount = ? ,  afterdiscount = ? , except = ? , vatcalculator = ? ,vat = ? , vatamount = ? ,notvatamount = ? , granttotal = ?,witholding_id = ? , witholding = ? , payment = ? ,  itemizedvat = ? , itemizeddiscount = ?  , employee_delivery = ? , inventory_id = ? , company_id = ?,status_pay = ? , acc_buy = ?,status_id = ?  where id = ? ',
          [data.creditors_id, data.creditors_name, data.address, data.post, data.tax, data.branch, data.date, data.credit, data.indicate_id, data.due, data.employee_saler, data.project_id, data.number, data.compute_id, data.signature, data.note, data.notein, data.total.replace(/,/g, ''), data.alldiscount.replace(/,/g, ''), data.afterdiscount.replace(/,/g, ''), data.except.replace(/,/g, ''), data.vatcalculator.replace(/,/g, ''), data.vat, data.vatamount.replace(/,/g, ''), data.notvatamount.replace(/,/g, ''), data.granttotal.replace(/,/g, ''), data.witholding_id, data.witholding.replace(/,/g, ''), data.payment.replace(/,/g, ''), data.itemizedvat, data.itemizeddiscount, data.employee_delivery, data.inventory_id, data.company_id, data.status_pay, data.acc_buy, 6, url.receipt], (err, receipt) => {
            conn.query('delete from receipt_list where receipt_id = ?', [url.receipt], (err, receipt_product) => {
            });
            if (Array.isArray(data.product_id)) {
              for (var i = 0; i < data.product_id.length; i++) {
                var numberunit = data.quantity[i] * data.unitnumber[i], total = data.totallist[i], productnow = data.product_id[i];
                conn.query('INSERT INTO receipt_list (product_id,detail,quantity,categoryprice_id,price,discount_id,listdiscount,vat_id,totallist,subunits,product_code,receipt_id,status_receipt_list)  values(?,?,?,?,?,?,?,?,?,?,?,?,1)', [data.product_id[i], data.detail[i], data.quantity[i], data.categoryprice_id[i], data.price[i], data.discount_id[i], data.listdiscount[i], data.vat_id[i], data.totallist[i], numberunit, data.product_code[i], url.receipt], (err, receipt_list) => {
                  conn.query('select  * from inventory where inventory_list_id = ? and product_list_id = ? ', [data.inventory_id, productnow], (err, select_inventory) => {
                    var unit = (select_inventory[0].number_product - select_receipt[0].subunits) + numberunit;
                    var average = (+(((select_inventory[0].cost_product * select_inventory[0].number_product) - select_receipt[0].totallist) + +(total)) / +((select_inventory[0].number_product - select_receipt[0].subunits) + numberunit));
                    conn.query('update inventory set number_product =  ?,cost_product = ? where product_list_id = ? and inventory_list_id = ?;', [unit, average, productnow, data.inventory_id], (err, inventory) => {
                    });
                  });
                });
              }
            } else {
              var numberunit = data.quantity * data.unitnumber;
              conn.query('INSERT INTO receipt_list (product_id,detail,quantity,categoryprice_id,price,discount_id,listdiscount,vat_id,totallist ,subunits,product_code,receipt_id,status_receipt_list)  values(?,?,?,?,?,?,?,?,?,?,?,?,1)', [data.product_id, data.detail, data.quantity, data.categoryprice_id, data.price, data.discount_id, data.listdiscount, data.vat_id, data.totallist, numberunit, data.product_code, url.receipt], (err, receipt_list) => {
                conn.query('select  * from inventory where inventory_list_id = ? and product_list_id = ? ', [data.inventory_id, data.product_id], (err, select_inventory) => {
                  var unit = (select_inventory[0].number_product - select_receipt[0].subunits) + numberunit;
                  var average = (+(((select_inventory[0].cost_product * select_inventory[0].number_product) - select_receipt[0].totallist) + +(data.totallist)) / +((select_inventory[0].number_product - select_receipt[0].subunits) + numberunit))
                  conn.query('update inventory set  number_product =  ?,cost_product = ? where product_list_id = ? and inventory_list_id = ?;', [unit, average, data.product_id, data.inventory_id], (err, inventory) => {
                  });
                });
              });
            }
            // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // //ส่วนของ creditor เจ้าหนี้
            // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            var amount_pay = data.granttotal.replace(/,/g, '') - data.status_pay;
            conn.query('select * from creditors where id = ? ', [select_receipt[0].creditors_id], (err, creditors) => {
              var balance = creditors[0].balance - select_receipt[0].balance + amount_pay;
              console.log(balance);
              var crbalance = creditors[0].crbalance - balance;
              conn.query('update creditors set balance = ? , crbalance = ? where id = ? ', [balance, crbalance, creditors[0].id], (er, credit1) => {
                console.log(err);
              })
            });
            var numberchar = select_receipt[0].receiptchar + select_receipt[0].receiptordernumber;
            conn.query('delete from payment where papernumber = ? , reference_id = ?', [numberchar, url.receipt], (err, payment) => {
            })
            var dataP = { papernumber: numberchar, amount: amount_pay, datepayment: data.date, acc_money: data.status_pay, net_payout: data.status_pay, withholding: "", withholding_id: "", withholding_vate: "", type_payment_id: 1, Identify: "", petty_cash: "", transfer_account: "", charge: "", check_date: "", check_number: "", check_acc_cut: "", check_charge: "", mastercard: "", typemachin_id: "", chanal_pay: "", note: "", balance: "", overdue: "", module_id: 12, reference_id: url.receipt };
            conn.query('INSERT INTO  payment set ?', [dataP], (err, payment) => {
              console.log(err);
              conn.query('update receipt set balance = ? where id = ?', [amount_pay, url.receipt], (err, update_receipt) => {
                console.log(err);
              })
            });
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //ส่วนของ GL
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            conn.query('select * from glheads where company_id = ?  and doc_no = ? and module_id = ? and reference_id = ?', [select_receipt[0].company_id, numberchar, 12, url.receipt], (err, glheads) => {
              conn.query('Delete from gldetail  where glheads_id = ?', [glheads[0].id], (err, del_gldetail) => {
              });
              conn.query('Delete from glheads  where id = ?', [glheads[0].id], (err, del_glheads) => {
              });
            });
            conn.query('select * from creditors where id = ?', [select_receipt[0].creditors_id], (err, creditors) => {
              var dataH = { book_id: 1, date: data.date, doc_no: numberchar, detail: data.detail, company_id: data.company_id, module_id: 12, reference_id: url.receipt };
              var dataD = [];
              conn.query('INSERT INTO  glheads set ?', [dataH], (err, glheads) => {
                var acc_buy;
                if (Array.isArray(data.acc_buy)) {
                  acc_buy = data.acc_buy[0];
                } else {
                  acc_buy = data.acc_buy;
                }
                if (data.total && data.total != "") {
                  dataD.push({ code_id: acc_buy, cheack: "", date_list: null, dr: data.totallist.replace(/,/g, ''), cr: 0, glheads_id: glheads.insertId });
                }
                if (data.vat && data.vat != "") {
                  dataD.push({ code_id: 433, cheack: "", date_list: null, dr: data.vatamount.replace(/,/g, ''), cr: 0, glheads_id: glheads.insertId });
                }
                if (data.withholding && data.withholding != "") {
                  dataD.push({ code_id: 434, cheack: "", date_list: null, dr: data.withholding.replace(/,/g, ''), cr: 0, glheads_id: glheads.insertId });
                }
                if (data.creditors_id && data.status_pay != "" && data.status_pay != data.granttotal) {
                  dataD.push({ code_id: creditors[0].chart_id, cheack: "", date_list: null, dr: 0, cr: amount_pay, glheads_id: glheads.insertId });
                  dataD.push({ code_id: 406, cheack: "", date_list: null, dr: 0, cr: data.status_pay.replace(/,/g, ''), glheads_id: glheads.insertId });
                }
                for (let i = 0; i < dataD.length; i++) {
                  conn.query('INSERT INTO gldetail set ?', [dataD[i]], (err, gldetail) => {
                  });
                }
              });
            });
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if (req.files) {
              var file = req.files.filename;
              if (!Array.isArray(file)) {
                var type = file.name.split(".");
                var filename = uuidv4() + "." + type[type.length - 1];
                file.mv("./public/receipt/" + filename, function (err) {
                  if (err) { console.log(err) }
                  data.filename = filename;
                  conn.query('update  receipt_file set filename = ? where receipt_id = ?', [data.filename, url.receipt], (err, receipt_file) => {
                    if (err) {
                      res.json(err);
                    }
                  });
                })
              } else {
                for (var i = 0; i < file.length; i++) {
                  var type = file[i].name.split(".");
                  var filename = uuidv4() + "." + type[type.length - 1];
                  file[i].mv("./public/receipt/" + filename, function (err) {
                    if (err) { console.log(err) }
                  })
                  data.filename = filename;
                  conn.query('update  receipt_file set filename = ? where receipt_id = ?', [data.filename, url.receipt], (err, receipt_file) => {
                    if (err) {
                      res.json(err);
                    }
                  });
                }
              }
            }
            res.redirect('/product_receipt')
          });
      }
    });
  });
};
///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_delete
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.receipt_delete = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('Delete from receipt_file where receipt_id  = ?', [url.receipt], (err, receipt_file) => {
      //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
      //สต๊อกสินค้า
      //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
      conn.query('select receipt.*, receipt_list.quantity as quantity,product_id,inventory_id,subunits,totallist from receipt join receipt_list on receipt_list.receipt_id = receipt.id  where receipt.id = ?;', [url.receipt], (err, receipt) => {
        if (receipt.length > 1) {
          for (var i = 0; i < receipt.length; i++) {
            conn.query('update inventory set cost_product = (((cost_product*number_product) - ?)/(number_product - ?)),number_product = number_product - ? where product_list_id = ? and inventory_list_id = ?;', [receipt[i].totallist, receipt[i].subunits, receipt[i].subunits, receipt[i].product_id, receipt[0].inventory_id], (err, inventory) => {
            });
          }
        } else {
          conn.query('update inventory set cost_product = (((cost_product*number_product) - ?)/(number_product - ?)),number_product = number_product - ? where product_list_id = ? and inventory_list_id = ?;', [receipt[0].totallist, receipt[0].subunits, receipt[0].subunits, receipt[0].product_id, receipt[0].inventory_id], (err, inventory) => {
          });
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //ส่วนของ creditors
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        conn.query('select * from creditors where id = ? ', [receipt[0].creditors_id], (err, creditors) => {
          if (receipt[0].status_pay < receipt[0].granttotal) {
            console.log("กรณีที่ 3 ");
            var sum = receipt[0].granttotal - receipt[0].status_pay;
            if (creditors[0].balance == sum) {
              var sum1 = creditors[0].crbalance + sum;
              conn.query('update creditors set crbalance = ? , balance = ? where id = ?', [sum1, 0, creditors[0].id], (err, creditor) => {
              });
            } else {
              var sum1 = creditors[0].balance - receipt[0].balance;
              var sum2 = creditors[0].crbalance + receipt[0].balance;
              conn.query('update creditors set   balance = ?, crbalance = ? where id = ?', [sum1, sum2, creditors[0].id], (err, creditor) => {
              });
            }
          } else {
            console.log("กรณีที่ 1,2");
            if (receipt[0].balance == receipt[0].granttotal) {
              var sum1 = creditors[0].crbalance + receipt[0].granttotal;
              conn.query('update creditors set crbalance = ? , balance = ? where id = ?', [sum1, 0, creditors[0].id], (err, creditor) => {
              });
            } else {
              var sum1 = creditors[0].balance - receipt[0].granttotal;
              var sum2 = creditors[0].crbalance + receipt[0].granttotal;
              conn.query('update creditors set crbalance = ? , balance = ? where id = ?', [sum2, sum1, creditors[0].id], (err, creditor) => {
              });
            }
          }
        });
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //ส่วนของ GL
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        if (receipt[0].status_pay == "" && receipt[0].balance == 0) {
          console.log("กรณีที่ 11111");
          var receiptbill = receipt[0].receiptchar + receipt[0].receiptordernumber;
          conn.query('select * from payment where reference_id = ? and papernumber = ?', [url.receipt, receiptbill], (err, select_payment) => {
            conn.query('select * from glheads where company_id = ?  and doc_no = ? and module_id = ? and reference_id = ?  ', [receipt[0].company_id, receiptbill, 12, url.receipt], (err, glheads) => {
              conn.query('Delete from gldetail  where glheads_id = ?', [glheads[0].id], (err, gldetail) => {
                console.log(err);
                conn.query('Delete from glheads  where id = ?', [glheads[0].id], (err, del_glheads) => {
                  console.log(err);
                });
              });
              //
              conn.query('Delete from receipt_list where receipt_id = ?', [url.receipt], (err, receipt_list) => {
                conn.query('Delete from receipt where id = ?', [url.receipt], (err, receipt) => {
                  res.redirect('/product_receipt');
                });
              });
            });
          });
        }
        if (receipt[0].status_pay.replace(/,/g, '') == receipt[0].granttotal && expenses[0].status_pay != "" ) {
          console.log("กรณีที่ 222222");
          conn.query('Delete from receipt_list where receipt_id = ?', [url.receipt], (err, receipt_list) => {
            conn.query('Delete from receipt where id = ?', [url.receipt], (err, receipt) => {
              res.redirect('/product_receipt');
            });
          });
        }
        if (receipt[0].status_pay.replace(/,/g, '') != receipt[0].granttotal && receipt[0].balance != "" ) {
          console.log("กรณีที่ 333333");
          var receiptbill = receipt[0].receiptchar + receipt[0].receiptordernumber;
          conn.query('Delete from payment where reference_id = ? and papernumber = ?', [url.receipt, receiptbill], (err, select_payment) => {
            conn.query('select * from glheads where company_id = ?  and doc_no = ? and module_id = ? and reference_id = ?  ', [receipt[0].company_id, receiptbill, 12, url.receipt], (err, glheads) => {
              conn.query('Delete from gldetail  where glheads_id = ?', [glheads[0].id], (err, gldetail) => {
                conn.query('Delete from glheads  where id = ?', [glheads[0].id], (err, del_glheads) => {
                });
              });
              //
              conn.query('Delete from receipt_list where receipt_id = ?', [url.receipt], (err, receipt_list) => {
                conn.query('Delete from receipt where id = ?', [url.receipt], (err, receipt) => {
                  res.redirect('/product_receipt');
                });
              });
            });
          });

        } //else
      })
    })
  });
};
///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_detail
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.receipt_detail = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('select receipt.* ,projectgl.name as projectgl,compute.name as compute,product_list.name as name,category_price.name as category,receipt_list.detail as detail,receipt_list.quantity as quantity,receipt_list.price as price ,discount.name as discount,listdiscount,vat_id,totallist,product_code,filename,inventory_list.name as inventory_id,company.branchNo as company_id,DATE_FORMAT(receipt.date,"%Y-%m-%d") as dateday ,DATE_FORMAT(receipt.due,"%Y-%m-%d") as dueday from receipt  join receipt_list on receipt_list.receipt_id = receipt.id left join receipt_file on receipt_file.receipt_id = receipt.id left join projectgl on receipt.project_id = projectgl.id join compute on receipt.compute_id  = compute.id  join product_list on receipt_list.product_id = product_list.id join category_price  on receipt_list.categoryprice_id = category_price.id left join discount on receipt_list.discount_id = discount.id join inventory_list on receipt.inventory_id = inventory_list.id join company on receipt.company_id = company.id where  receipt_list.receipt_id = ?;', [url.receipt], (err, receipt) => {
      //////////////////////////////////////////////////////////
      var yearen_1 = receipt[0].dateday.split('-')
      var yearth_1 = +(yearen_1[0]) + 543
      var dateth = yearen_1[2] + "/" + yearen_1[1] + "/" + yearth_1;
      //////////////////////////////////////////////////////////
      if (receipt[0].indicate_id == 1) {
        var yearen_2 = receipt[0].dueday.split('-');
        var yearth_2 = +(yearen_2[0]) + 543
        var dueth = yearen_2[2] + "/" + yearen_2[1] + "/" + yearth_2;
      }
      //////////////////////////////////////////////////////////
      conn.query('select * from witholding', (err, witholding) => {
        res.render('buy/product_receipt/product_receipt_detail', {
          receipt, dateth, dueth, witholding,
          session: req.session
        });
      });
    });
  });
};
///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_readd
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.receipt_readd = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('select receipt.* ,product_id,projectgl.name as projectgl,compute.name as compute,product_list.name as name,category_price.name as category,categoryprice_id,receipt_list.detail as detail,receipt_list.quantity as quantity,receipt_list.price as price ,discount.name as discount,listdiscount,vat_id,totallist,product_code,filename,DATE_FORMAT(receipt.date,"%Y-%m-%d") as dateday ,DATE_FORMAT(receipt.due,"%Y-%m-%d") as dueday from receipt  join receipt_list on receipt_list.receipt_id = receipt.id left join receipt_file on receipt_file.receipt_id = receipt.id left join projectgl on receipt.project_id = projectgl.id join compute on receipt.compute_id  = compute.id  join product_list on receipt_list.product_id = product_list.id join category_price  on receipt_list.categoryprice_id = category_price.id left join discount on receipt_list.discount_id = discount.id join inventory_list on receipt.inventory_id = inventory_list.id join company on receipt.company_id = company.id where  receipt_list.receipt_id = ?;', [url.receipt], (err, receipt) => {
      //runnumber receipt
      conn.query('select * from module where id = 12', (err, receiptmodule) => {
        conn.query('select date_format(date(now()),"%Y%m") as number', (err, runnumber) => {
          conn.query('select count(receiptordernumber) as countnumber,max(receiptordernumber) as maxnumber from receipt where date_format(date,"%Y%m") = date_format(date(now()),"%Y%m");', (err, receiptnumber) => {
            //
            conn.query('select creditors.*,  provinces.name_th as provinces, amphures.name_th as amphures, districts.name_th as districts, zip_code from creditors join provinces on creditors.provinces_id = provinces.id join amphures on creditors.amphures_id = amphures.id join districts on creditors.districts_id = districts.id', (err, creditors) => {
              conn.query('select * from human_resource', (err, human_resource) => {
                conn.query('select * from inventory_list;', (err, inventory) => {
                  conn.query('select * from indicate', (err, indicate) => {
                    conn.query('select * from category_price where product_list_id = ?', [receipt[0].product_id], (err, category_price) => {
                      conn.query('select * from witholding', (err, witholding) => {
                        conn.query('select * from discount', (err, discount) => {
                          conn.query('select * from vat', (err, vat) => {
                            conn.query('select * from company', (err, company) => {
                              conn.query('select * from compute', (err, compute) => {
                                conn.query('select * from projectgl', (err, projectgl) => {
                                  //creditors
                                  conn.query('select * from chart where typeAcc_id = 5;', (err, chart) => {
                                    conn.query('select * from bank', (err, bank) => {
                                      conn.query('select * from provinces', (err, provinces) => {
                                        conn.query('select * from human', (err, human) => {
                                          //product
                                          conn.query('select * from chart where typeAcc_id = 9;', (err, acc_sell) => {
                                            conn.query('select * from chart where typeAcc_id = 11;', (err, acc_buy) => {
                                              conn.query('select product_list.* from product_list join inventory on inventory.product_list_id = product_list.id where product_list.enable = 1 and inventory.inventory_list_id = ?', [receipt[0].inventory_id], (err, product_list) => {
                                                conn.query('select * from product_type', (err, product_type) => {
                                                  conn.query('select * from product_type_list', (err, product_type_list) => {
                                                    conn.query('select * from product_tax', (err, product_tax) => {
                                                      conn.query('select * from product_group', (err, product_group) => {
                                                        //runnumber creditors
                                                        conn.query('select * from module where id = 4', (err, creditormodule) => {
                                                          conn.query('select max(creditorID) as maxnumber from creditors where creditorID like ?', [creditormodule[0].char + "%"], (err, maxnumber) => {
                                                            res.render('buy/product_receipt/product_receipt_readd.ejs', {
                                                              receipt, receiptmodule, runnumber, receiptnumber, creditors, human_resource, inventory, indicate, category_price, witholding, company, discount, creditormodule,
                                                              vat, compute, projectgl, chart, bank, provinces, human, acc_sell, acc_buy, product_list, product_type, product_type_list, product_tax, product_group, maxnumber,
                                                              session: req.session
                                                            });
                                                          });
                                                        });
                                                      });
                                                    });
                                                  });
                                                });
                                              });
                                            });
                                          });
                                        });
                                      });
                                    });
                                  });
                                });
                              });
                            });
                          });
                        });
                      });
                    });
                  });
                });
              });
            });
          });
        });
      });
    });
  });
};
///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_pay
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.payment = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    if (url.receipt) {
      conn.query('select receipt.*,payment.net_payout as net_payot from receipt left join (select sum(net_payout) as net_payout,reference_id from  payment where reference_id = ?) as payment on payment.reference_id = receipt.id  where receipt.id = ?;', [url.receipt, url.receipt], (err, receipt) => {
        conn.query('select * from type_payment;', (err, typepayment) => {
          conn.query('select * from witholding', (err, witholding) => {
            conn.query('select * from petty_cash', (err, petty_cash) => {
              conn.query('select * from account_transfer', (err, account_transfer) => {
                conn.query('select * from card', (err, mastercasrd) => {
                  conn.query('select * from typepayment_card', (err, typepayment_card) => {
                    conn.query('select * from chanal_other', (err, chanal_other) => {
                      res.render('buy/product_receipt/product_receiptpay.ejs', {
                        session: req.session, typepayment, receipt, petty_cash, url, witholding, account_transfer, mastercasrd, typepayment_card, chanal_other
                      });
                    });
                  });
                })
              });
            });
          });
        });
      });
    } else {
      conn.query('select expenses.* ,payment.net_payout as net_payout from expenses left join (select sum(net_payout) as net_payout,reference_id from payment where reference_id = ?) as payment on payment.reference_id = expenses.id where expenses.id = ?;', [url.expenses, url.expenses], (err, expenses) => {
        conn.query('select * from type_payment;', (err, typepayment) => {
          conn.query('select * from witholding', (err, witholding) => {
            conn.query('select * from petty_cash', (err, petty_cash) => {
              conn.query('select * from account_transfer', (err, account_transfer) => {
                conn.query('select * from card', (err, mastercasrd) => {
                  conn.query('select * from typepayment_card', (err, typepayment_card) => {
                    conn.query('select * from chanal_other', (err, chanal_other) => {
                      res.render('buy/expenses/expenses_pay.ejs', {
                        session: req.session, expenses, typepayment, witholding, petty_cash, account_transfer, mastercasrd, typepayment_card, chanal_other, url
                      });
                    });
                  });
                });
              });
            });
          });
        });
      });
    };
  });
};
///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_savepay
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.payment_save = (req, res) => {
  var url = req.params;
  const data = req.body;
  console.log(data);
  req.getConnection((err, conn) => {
    if (url.receipt) {
      conn.query('select * from receipt where id = ?', [url.receipt], (err, receipt) => {
        if (receipt[0].status_pay == "" && receipt[0].balance == 0 || receipt[0].status_pay == "" && receipt[0].balance != "") {
          console.log("เจ้าหนี้");
          conn.query('INSERT INTO payment (reference_id,papernumber,amount,acc_money,datepayment,net_payout,withholding,withholding_id,withholding_vate,type_payment_id,Identify,petty_cash,transfer_account,charge,check_account,check_date,check_number,check_charge,mastercard,typemachin_id,chanal_pay,note,balance,check_acc_cut,overdue,module_id) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
            [url.receipt, data.papernumber, data.amount, data.acc_money, data.datepayment, data.net_payout, data.withholding, data.withholding_id, data.withholding_vate, data.type_payment_id, data.Identify, data.petty_cash, data.transfer_account, data.charge, data.check_account, data.check_date, data.check_number, data.check_charge, data.mastercard, data.typemachin_id, data.chanal_pay, data.note, data.balance, data.check_acc_cut, 0, 12], (err, payment) => {
              ///////////////////////////////////////////////////////////////////////////////////////////////////
              //ส่วนของ เจ้าหนี้
              ///////////////////////////////////////////////////////////////////////////////////////////////////
              conn.query('select * from creditors where id = ?', [receipt[0].creditors_id], (err, creditors) => {
                conn.query('select sum(net_payout) as sumnet from payment where reference_id = ?;', [url.receipt], (err, paymentsave) => {
                  if (receipt[0].granttotal == paymentsave[0].sumnet) {
                    var sum1 = (+creditors[0].balance) - (+data.net_payout);
                    var sum2 = (+creditors[0].crbalance) + (+data.net_payout);
                    conn.query('update receipt set status_id = 5 , balance = ? where id = ?', [data.balance.replace(/,/g, ''), url.receipt], (err, savepayment1) => {
                      conn.query('update creditors set crbalance = ? , balance = ? where id = ?', [sum2, sum1, creditors[0].id], (err, creditor) => {
                      });
                    });
                  } else {
                    var sum1 = (+creditors[0].balance) - (+data.net_payout);
                    var sum2 = (+creditors[0].crbalance) + (+data.net_payout);
                    conn.query('update receipt set status_id = 6 , balance = ? where id = ?', [data.balance.replace(/,/g, ''), url.receipt], (err, savepayment1) => {
                      conn.query('update creditors set crbalance = ? , balance = ? where id = ?', [sum2, sum1, creditors[0].id], (err, creditor) => {
                      });
                    });
                  }
                  //
                  ///////////////////////////////////////////////////////////////////////////////////////////////////
                  //ส่วนของ GL
                  ///////////////////////////////////////////////////////////////////////////////////////////////////
                  if (receipt[0].status_pay == "" && data.balance == 0) {
                    console.log("เจ้าหนี้ GL1");
                    var papernum = receipt[0].receiptchar + receipt[0].receiptordernumber;
                    conn.query('select * from payment where reference_id = ? and papernumber = ?', [url.receipt, papernum], (err, select_payment) => {
                      var dataH = { book_id: 1, date: data.datepayment, doc_no: data.papernumber, detail: data.note, company_id: receipt[0].company_id, module_id: 12, reference_id: select_payment[0].id };
                      var dataD = [];
                      conn.query('INSERT INTO  glheads set ?', [dataH], (err, glheads) => {
                        if (err) {
                          res.json(err);
                        } else {
                          if (data.withholding_vate && data.withholding_vate != "") {
                            dataD.push({ code_id: 434, cheack: "", date_list: null, dr: 0, cr: data.withholding_vate.replace(/,/g, ''), glheads_id: glheads.insertId });
                          }
                          if (data.amount && data.amount != "") {
                            dataD.push({ code_id: creditors[0].chart_id, cheack: "", date_list: null, dr: data.amount, cr: 0, glheads_id: glheads.insertId });
                          }
                          if (data.type_payment_id == 1) {
                            dataD.push({ code_id: 406, cheack: "", date_list: null, dr: 0, cr: data.net_payout, glheads_id: glheads.insertId });
                          }
                          if (data.type_payment_id == 2) {
                            dataD.push({ code_id: 407, cheack: "", date_list: null, dr: 0, cr: data.net_payout, glheads_id: glheads.insertId });
                          }
                          if (data.type_payment_id == 3 && data.charge != "") {
                            dataD.push({ code_id: 408, cheack: "", date_list: null, dr: 0, cr: data.net_payout, glheads_id: glheads.insertId });
                            dataD.push({ code_id: 570, cheack: "", date_list: null, dr: data.charge, cr: 0, glheads_id: glheads.insertId });
                          }
                          if (data.type_payment_id == 4 && data.check_charge != "") {
                            dataD.push({ code_id: 551, cheack: "", date_list: null, dr: 0, cr: data.net_payout, glheads_id: glheads.insertId });
                            dataD.push({ code_id: 570, cheack: "", date_list: null, dr: data.check_charge, cr: 0, glheads_id: glheads.insertId });
                          }
                          if (data.type_payment_id == 5) {
                            dataD.push({ code_id: 496, cheack: "", date_list: null, dr: 0, cr: data.net_payout, glheads_id: glheads.insertId });
                          }
                          for (let i = 0; i < dataD.length; i++) {
                            conn.query('INSERT INTO gldetail set ?', [dataD[i]], (err, gldetail) => {
                              if (err) {
                                res.json(err);
                              }
                            });
                          }
                        }
                      });
                    });
                    //
                  } else {
                    console.log("เจ้าหนี้ GL2");
                    var papernum = receipt[0].receiptchar + receipt[0].receiptordernumber;
                    conn.query('select * from payment where reference_id = ? and papernumber = ?', [url.receipt, papernum], (err, select_payment) => {
                      var dataH = { book_id: 1, date: data.datepayment, doc_no: data.papernumber, detail: data.note, company_id: receipt[0].company_id, module_id: 12, reference_id: select_payment[0].id };
                      var dataD = [];
                      conn.query('INSERT INTO  glheads set ?', [dataH], (err, glheads) => {
                        if (err) {
                          res.json(err);
                        } else {
                          if (data.withholding_vate && data.withholding_vate != "") {
                            dataD.push({ code_id: 434, cheack: "", date_list: null, dr: 0, cr: data.withholding_vate.replace(/,/g, ''), glheads_id: glheads.insertId });
                          }
                          if (data.amount && data.amount != "") {
                            dataD.push({ code_id: creditors[0].chart_id, cheack: "", date_list: null, dr: data.amount, cr: 0, glheads_id: glheads.insertId });
                            if (data.balance == 0) {
                            } else {
                              dataD.push({ code_id: creditors[0].chart_id, cheack: "", date_list: null, dr: 0, cr: data.balance, glheads_id: glheads.insertId });
                            }
                          }
                          if (data.type_payment_id == 1) {
                            dataD.push({ code_id: 406, cheack: "", date_list: null, dr: 0, cr: data.net_payout, glheads_id: glheads.insertId });
                          }
                          if (data.type_payment_id == 2) {
                            dataD.push({ code_id: 407, cheack: "", date_list: null, dr: 0, cr: data.net_payout, glheads_id: glheads.insertId });
                          }
                          if (data.type_payment_id == 3 && data.charge != "") {
                            dataD.push({ code_id: 408, cheack: "", date_list: null, dr: 0, cr: data.net_payout, glheads_id: glheads.insertId });
                            dataD.push({ code_id: 570, cheack: "", date_list: null, dr: data.charge, cr: 0, glheads_id: glheads.insertId });
                          }
                          if (data.type_payment_id == 4 && data.check_charge != "") {
                            dataD.push({ code_id: 551, cheack: "", date_list: null, dr: 0, cr: data.net_payout, glheads_id: glheads.insertId });
                            dataD.push({ code_id: 570, cheack: "", date_list: null, dr: data.check_charge, cr: 0, glheads_id: glheads.insertId });
                          }
                          if (data.type_payment_id == 5) {
                            dataD.push({ code_id: 496, cheack: "", date_list: null, dr: 0, cr: data.net_payout, glheads_id: glheads.insertId });
                          }
                          for (let i = 0; i < dataD.length; i++) {
                            conn.query('INSERT INTO gldetail set ?', [dataD[i]], (err, gldetail) => {
                              if (err) {
                                res.json(err);
                              }
                            });
                          }
                        }
                      });
                    });
                  }
                  res.redirect('/product_receipt')
                });
              });
            })
        }
        if (receipt[0].status_pay != "" && receipt[0].balance == "") {
          console.log("เงินสด");
          conn.query('INSERT INTO payment (reference_id,papernumber,amount,acc_money,datepayment,net_payout,withholding,withholding_id,withholding_vate,type_payment_id,Identify,petty_cash,transfer_account,charge,check_account,check_date,check_number,check_charge,mastercard,typemachin_id,chanal_pay,note,balance,check_acc_cut,overdue,module_id) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
            [url.receipt, data.papernumber, data.amount, data.acc_money, data.datepayment, data.net_payout, data.withholding, data.withholding_id, data.withholding_vate, data.type_payment_id, data.Identify, data.petty_cash, data.transfer_account, data.charge, data.check_account, data.check_date, data.check_number, data.check_charge, data.mastercard, data.typemachin_id, data.chanal_pay, data.note, data.balance, data.check_acc_cut, 0, 12], (err, payment) => {
              ///////////////////////////////////////////////////////////////////////////////////////////////////
              //ส่วนของ เจ้าหนี้
              ///////////////////////////////////////////////////////////////////////////////////////////////////
              conn.query('select * from creditors where id = ?', [receipt[0].creditors_id], (err, creditors) => {
                conn.query('select sum(net_payout) as sumnet from payment where reference_id = ?;', [url.receipt], (err, paymentsave) => {
                  if (receipt[0].granttotal == paymentsave[0].sumnet) {
                    var sum1 = (+creditors[0].balance) - (+data.net_payout);
                    var sum2 = (+creditors[0].crbalance) + (+data.net_payout);
                    conn.query('update receipt set status_id = 5 , balance = ? where id = ?', [data.balance.replace(/,/g, ''), url.receipt], (err, savepayment1) => {
                      conn.query('update creditors set crbalance = ? , balance = ? where id = ?', [sum2, sum1, creditors[0].id], (err, creditor) => {
                      });
                    });
                  } else {
                    var sum1 = (+creditors[0].balance) - (+data.net_payout);
                    var sum2 = (+creditors[0].crbalance) + (+data.net_payout);
                    conn.query('update receipt set status_id = 6 , balance = ? where id = ?', [data.balance.replace(/,/g, ''), url.receipt], (err, savepayment1) => {
                      conn.query('update creditors set crbalance = ? , balance = ? where id = ?', [sum2, sum1, creditors[0].id], (err, creditor) => {
                      });
                    });
                  }
                  //
                  var papernum = receipt[0].receiptchar + receipt[0].receiptordernumber;
                  conn.query('select * from payment where reference_id = ? and papernumber = ?', [url.receipt, papernum], (err, select_payment) => {
                    var dataH = { book_id: 1, date: data.datepayment, doc_no: data.papernumber, detail: data.note, company_id: receipt[0].company_id, module_id: 12, reference_id: receipt[0].id };
                    var dataD = [];
                    conn.query('INSERT INTO  glheads set ?', [dataH], (err, glheads) => {

                      if (receipt[0].total && receipt[0].total != "") {
                        dataD.push({ code_id: receipt[0].acc_buy, cheack: "", date_list: null, dr: receipt[0].total, cr: 0, glheads_id: glheads.insertId });
                      }
                      if (receipt[0].vat && receipt[0].vat != "") {
                        dataD.push({ code_id: 433, cheack: "", date_list: null, dr: receipt[0].vatamount, cr: 0, glheads_id: glheads.insertId });
                      }
                      if (receipt[0].withholding > 1) {
                        dataD.push({ code_id: 434, cheack: "", date_list: null, dr: receipt[0].withholding, cr: 0, glheads_id: glheads.insertId });
                      }
                      if (receipt[0].creditors_id && receipt[0].status_pay != "") {
                        console.log("sss");
                        dataD.push({ code_id: 406, cheack: "", date_list: null, dr: 0, cr: receipt[0].granttotal, glheads_id: glheads.insertId });
                      }
                      console.log(dataD);
                      for (let i = 0; i < dataD.length; i++) {
                        conn.query('INSERT INTO gldetail set ?', [dataD[i]], (err, gldetail) => {
                          if (err) {
                            res.json(err);
                          }
                        });
                      }
                    });
                  });
                  //
                  res.redirect('/product_receipt')
                });
              });
            })
        } else if (receipt[0].status_pay != "" && receipt[0].balance != "") {
          console.log("เงิดสดและ เชื่อ");
          conn.query('INSERT INTO payment (reference_id,papernumber,amount,acc_money,datepayment,net_payout,withholding,withholding_id,withholding_vate,type_payment_id,Identify,petty_cash,transfer_account,charge,check_account,check_date,check_number,check_charge,mastercard,typemachin_id,chanal_pay,note,balance,check_acc_cut,overdue,module_id,boolean_id) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,1)',
            [url.receipt, data.papernumber, data.amount, data.acc_money, data.datepayment, data.net_payout, data.withholding, data.withholding_id, data.withholding_vate, data.type_payment_id, data.Identify, data.petty_cash, data.transfer_account, data.charge, data.check_account, data.check_date, data.check_number, data.check_charge, data.mastercard, data.typemachin_id, data.chanal_pay, data.note, data.balance, data.check_acc_cut, 0, 12], (err, payment) => {
              ///////////////////////////////////////////////////////////////////////////////////////////////////
              //ส่วนของ เจ้าหนี้
              ///////////////////////////////////////////////////////////////////////////////////////////////////
              conn.query('select * from creditors where id = ?', [receipt[0].creditors_id], (err, creditors) => {
                conn.query('select sum(net_payout) as sumnet from payment where reference_id = ?;', [url.receipt], (err, paymentsave) => {
                  if (receipt[0].granttotal == paymentsave[0].sumnet) {
                    var sum1 = (+creditors[0].balance) - (+data.net_payout);
                    var sum2 = (+creditors[0].crbalance) + (+data.net_payout);
                    conn.query('update receipt set status_id = 5 , balance = ? where id = ?', [data.balance.replace(/,/g, ''), url.receipt], (err, savepayment1) => {
                      conn.query('update creditors set crbalance = ? , balance = ? where id = ?', [sum2, sum1, creditors[0].id], (err, creditor) => {
                      });
                    });
                  } else {
                    var sum1 = (+creditors[0].balance) - (+data.net_payout);
                    var sum2 = (+creditors[0].crbalance) + (+data.net_payout);
                    conn.query('update receipt set status_id = 6 , balance = ? where id = ?', [data.balance.replace(/,/g, ''), url.receipt], (err, savepayment1) => {
                      conn.query('update creditors set crbalance = ? , balance = ? where id = ?', [sum2, sum1, creditors[0].id], (err, creditor) => {
                      });
                    });
                  }
                  //

                  var papernum = receipt[0].receiptchar + receipt[0].receiptordernumber;
                  conn.query('select * from payment where reference_id = ? and papernumber = ?', [url.receipt, papernum], (err, select_payment) => {
                    var dataH = { book_id: 1, date: data.datepayment, doc_no: data.papernumber, detail: data.note, company_id: receipt[0].company_id, module_id: 12, reference_id: select_payment[0].id };
                    var dataD = [];
                    conn.query('INSERT INTO  glheads set ?', [dataH], (err, glheads) => {

                      if (data.withholding_vate && data.withholding_vate != "") {
                        dataD.push({ code_id: 434, cheack: "", date_list: null, dr: 0, cr: data.withholding_vate.replace(/,/g, ''), glheads_id: glheads.insertId });
                      }
                      if (data.amount && data.amount != "") {
                        dataD.push({ code_id: creditors[0].chart_id, cheack: "", date_list: null, dr: data.amount, cr: 0, glheads_id: glheads.insertId });
                      }
                      if (data.type_payment_id == 1) {
                        dataD.push({ code_id: 406, cheack: "", date_list: null, dr: 0, cr: data.net_payout, glheads_id: glheads.insertId });
                      }
                      if (data.type_payment_id == 2) {
                        dataD.push({ code_id: 407, cheack: "", date_list: null, dr: 0, cr: data.net_payout, glheads_id: glheads.insertId });
                      }
                      if (data.type_payment_id == 3 && data.charge != "") {
                        dataD.push({ code_id: 408, cheack: "", date_list: null, dr: 0, cr: data.net_payout, glheads_id: glheads.insertId });
                        dataD.push({ code_id: 570, cheack: "", date_list: null, dr: data.charge, cr: 0, glheads_id: glheads.insertId });
                      }
                      if (data.type_payment_id == 4 && data.check_charge != "") {
                        dataD.push({ code_id: 551, cheack: "", date_list: null, dr: 0, cr: data.net_payout, glheads_id: glheads.insertId });
                        dataD.push({ code_id: 570, cheack: "", date_list: null, dr: data.check_charge, cr: 0, glheads_id: glheads.insertId });
                      }
                      if (data.type_payment_id == 5) {
                        dataD.push({ code_id: 496, cheack: "", date_list: null, dr: 0, cr: data.net_payout, glheads_id: glheads.insertId });
                      }

                      console.log(dataD);
                      for (let i = 0; i < dataD.length; i++) {
                        conn.query('INSERT INTO gldetail set ?', [dataD[i]], (err, gldetail) => {
                          if (err) {
                            res.json(err);
                          }
                        });
                      }
                    });
                  });
                  //
                  res.redirect('/product_receipt')
                });
              });
            })
        }
      });
    } else {
      conn.query('select expenses. *,expenses_list.acc_buy as acc_buy,expenses_list.detail as detail,expenses_list.expenses_detail as expenses_detail from expenses join expenses_list on expenses_list.expenses_id = expenses.id where expenses.id = ?;', [url.expenses], (err, expenses) => {
        if (expenses[0].status_pay == "" && expenses[0].balance == 0 || expenses[0].status_pay == "" && expenses[0].balance != "") {
          console.log("เจ้าหนี้");
          conn.query('INSERT INTO payment (reference_id,papernumber,amount,acc_money,datepayment,net_payout,withholding,withholding_id,withholding_vate,type_payment_id,Identify,petty_cash,transfer_account,charge,check_account,check_date,check_number,check_charge,mastercard,typemachin_id,chanal_pay,note,balance,check_acc_cut,overdue,module_id) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
            [url.expenses, data.papernumber, data.amount, data.acc_money, data.datepayment, data.net_payout, data.withholding, data.withholding_id, data.withholding_vate, data.type_payment_id, data.Identify, data.petty_cash, data.transfer_account, data.charge, data.check_account, data.check_date, data.check_number, data.check_charge, data.mastercard, data.typemachin_id, data.chanal_pay, data.note, data.balance, data.check_acc_cut, 0, 20], (err, payment) => {
              console.log(err);
              ///////////////////////////////////////////////////////////////////////////////////////////////////
              //ส่วนของ เจ้าหนี้
              ///////////////////////////////////////////////////////////////////////////////////////////////////
              conn.query('select * from expenses where id = ?', [url.expenses], (err, expenses) => {
                conn.query('select * from creditors where id = ?', [expenses[0].creditors_id], (err, creditors) => {
                  conn.query('select sum(net_payout) as sumnet from payment where reference_id = ?;', [url.expenses], (err, paymentsave) => {
                    if (expenses[0].granttotal == paymentsave[0].sumnet) {
                      var sum1 = (+creditors[0].balance) - (+data.net_payout);
                      var sum2 = (+creditors[0].crbalance) + (+data.net_payout);
                      conn.query('update expenses set status_id = 5 , balance = ? where id = ?', [data.balance.replace(/,/g, ''), url.expenses], (err, savepayment1) => {
                        conn.query('update creditors set crbalance = ? , balance = ? where id = ?', [sum2, sum1, creditors[0].id], (err, creditor) => {
                        });
                      });
                    } else {
                      var sum1 = (+creditors[0].balance) - (+data.net_payout);
                      var sum2 = (+creditors[0].crbalance) + (+data.net_payout);
                      conn.query('update payment set overdue = ? where id = ?', [1, url.expenses], (err, overdue) => {
                        console.log(err, overdue);
                        conn.query('update expenses set status_id = 6 , balance = ? where id = ?', [data.balance.replace(/,/g, ''), url.expenses], (err, savepayment1) => {
                          conn.query('update creditors set crbalance = ? , balance = ? where id = ?', [sum2, sum1, creditors[0].id], (err, creditor) => {
                          });
                        });
                      });
                    }
                    //
                    ///////////////////////////////////////////////////////////////////////////////////////////////////
                    //ส่วนของ GL
                    ///////////////////////////////////////////////////////////////////////////////////////////////////
                    if (expenses[0].status_pay == "" && data.balance == 0) {
                      console.log("เจ้าหนี้ GL1");
                      var papernum = expenses[0].expenseschar + expenses[0].expensesnumber;
                      conn.query('select * from payment where reference_id = ? and papernumber = ?', [url.expenses, papernum], (err, select_payment) => {
                        var dataH = { book_id: 1, date: data.datepayment, doc_no: data.papernumber, detail: "", company_id: expenses[0].company_id, module_id: 20, reference_id: select_payment[0].id };
                        var dataD = [];
                        conn.query('INSERT INTO  glheads set ?', [dataH], (err, glheads) => {
                          if (err) {
                            res.json(err);
                          } else {
                            if (data.withholding_vate && data.withholding_vate != "") {
                              dataD.push({ code_id: 434, cheack: "", date_list: null, dr: 0, cr: data.withholding_vate.replace(/,/g, ''), glheads_id: glheads.insertId });
                            }
                            if (data.amount && data.amount != "") {
                              dataD.push({ code_id: creditors[0].chart_id, cheack: "", date_list: null, dr: data.amount, cr: 0, glheads_id: glheads.insertId });
                            }
                            if (data.type_payment_id == 1) {
                              dataD.push({ code_id: 406, cheack: "", date_list: null, dr: 0, cr: data.net_payout, glheads_id: glheads.insertId });
                            }
                            if (data.type_payment_id == 2) {
                              dataD.push({ code_id: 407, cheack: "", date_list: null, dr: 0, cr: data.net_payout, glheads_id: glheads.insertId });
                            }
                            if (data.type_payment_id == 3 && data.charge != "") {
                              dataD.push({ code_id: 408, cheack: "", date_list: null, dr: 0, cr: data.net_payout, glheads_id: glheads.insertId });
                              dataD.push({ code_id: 570, cheack: "", date_list: null, dr: data.charge, cr: 0, glheads_id: glheads.insertId });
                            }
                            if (data.type_payment_id == 4 && data.check_charge != "") {
                              dataD.push({ code_id: 551, cheack: "", date_list: null, dr: 0, cr: data.net_payout, glheads_id: glheads.insertId });
                              dataD.push({ code_id: 570, cheack: "", date_list: null, dr: data.check_charge, cr: 0, glheads_id: glheads.insertId });
                            }
                            if (data.type_payment_id == 5) {
                              dataD.push({ code_id: 496, cheack: "", date_list: null, dr: 0, cr: data.net_payout, glheads_id: glheads.insertId });
                            }
                            for (let i = 0; i < dataD.length; i++) {
                              conn.query('INSERT INTO gldetail set ?', [dataD[i]], (err, gldetail) => {
                                if (err) {
                                  res.json(err);
                                }
                              });
                            }
                          }
                        });
                      });
                      //
                    } else {
                      console.log("เจ้าหนี้ GL2");
                      var papernum = expenses[0].expenseschar + expenses[0].expensesnumber;
                      conn.query('select * from payment where reference_id = ? and papernumber = ?', [url.expenses, papernum], (err, select_payment) => {
                        var dataH = { book_id: 1, date: data.datepayment, doc_no: data.papernumber, detail: data.note, company_id: expenses[0].company_id, module_id: 20, reference_id: select_payment[0].id };
                        var dataD = [];
                        conn.query('INSERT INTO  glheads set ?', [dataH], (err, glheads) => {
                          if (err) {
                            res.json(err);
                          } else {
                            if (data.withholding_vate && data.withholding_vate != "") {
                              dataD.push({ code_id: 434, cheack: "", date_list: null, dr: 0, cr: data.withholding_vate.replace(/,/g, ''), glheads_id: glheads.insertId });
                            }
                            if (data.amount && data.amount != "") {
                              dataD.push({ code_id: creditors[0].chart_id, cheack: "", date_list: null, dr: data.amount, cr: 0, glheads_id: glheads.insertId });
                              if (data.balance == 0) {
                              } else {
                                dataD.push({ code_id: creditors[0].chart_id, cheack: "", date_list: null, dr: 0, cr: data.balance, glheads_id: glheads.insertId });
                              }
                            }
                            if (data.type_payment_id == 1) {
                              dataD.push({ code_id: 406, cheack: "", date_list: null, dr: 0, cr: data.net_payout, glheads_id: glheads.insertId });
                            }
                            if (data.type_payment_id == 2) {
                              dataD.push({ code_id: 407, cheack: "", date_list: null, dr: 0, cr: data.net_payout, glheads_id: glheads.insertId });
                            }
                            if (data.type_payment_id == 3 && data.charge != "") {
                              dataD.push({ code_id: 408, cheack: "", date_list: null, dr: 0, cr: data.net_payout, glheads_id: glheads.insertId });
                              dataD.push({ code_id: 570, cheack: "", date_list: null, dr: data.charge, cr: 0, glheads_id: glheads.insertId });
                            }
                            if (data.type_payment_id == 4 && data.check_charge != "") {
                              dataD.push({ code_id: 551, cheack: "", date_list: null, dr: 0, cr: data.net_payout, glheads_id: glheads.insertId });
                              dataD.push({ code_id: 570, cheack: "", date_list: null, dr: data.check_charge, cr: 0, glheads_id: glheads.insertId });
                            }
                            if (data.type_payment_id == 5) {
                              dataD.push({ code_id: 496, cheack: "", date_list: null, dr: 0, cr: data.net_payout, glheads_id: glheads.insertId });
                            }
                            for (let i = 0; i < dataD.length; i++) {
                              conn.query('INSERT INTO gldetail set ?', [dataD[i]], (err, gldetail) => {
                                if (err) {
                                  res.json(err);
                                }
                              });
                            }
                          }
                        });
                      });
                    }
                    res.redirect('/expenses')
                  });
                });
              })
            });
        }
        if (expenses[0].status_pay != "" && expenses[0].balance == "") {
          console.log("เงินสด");
          conn.query('INSERT INTO payment (reference_id,papernumber,amount,acc_money,datepayment,net_payout,withholding,withholding_id,withholding_vate,type_payment_id,Identify,petty_cash,transfer_account,charge,check_account,check_date,check_number,check_charge,mastercard,typemachin_id,chanal_pay,note,balance,check_acc_cut,overdue,module_id) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
            [url.expenses, data.papernumber, data.amount, data.acc_money, data.datepayment, data.net_payout, data.withholding, data.withholding_id, data.withholding_vate, data.type_payment_id, data.Identify, data.petty_cash, data.transfer_account, data.charge, data.check_account, data.check_date, data.check_number, data.check_charge, data.mastercard, data.typemachin_id, data.chanal_pay, data.note, data.balance, data.check_acc_cut, 0, 20], (err, payment) => {
              ///////////////////////////////////////////////////////////////////////////////////////////////////
              //ส่วนของ เจ้าหนี้
              ///////////////////////////////////////////////////////////////////////////////////////////////////
              conn.query('select * from creditors where id = ?', [expenses[0].creditors_id], (err, creditors) => {
                conn.query('select sum(net_payout) as sumnet from payment where reference_id = ?;', [url.expenses], (err, paymentsave) => {
                  if (expenses[0].granttotal == paymentsave[0].sumnet) {
                    var sum1 = (+creditors[0].balance) - (+data.net_payout);
                    var sum2 = (+creditors[0].crbalance) + (+data.net_payout);
                    conn.query('update expenses set status_id = 5 , balance = ? where id = ?', [data.balance.replace(/,/g, ''), url.expenses], (err, savepayment1) => {
                      conn.query('update creditors set crbalance = ? , balance = ? where id = ?', [sum2, sum1, creditors[0].id], (err, creditor) => {
                      });
                    });
                  } else {
                    var sum1 = (+creditors[0].balance) - (+data.net_payout);
                    var sum2 = (+creditors[0].crbalance) + (+data.net_payout);
                    conn.query('update expenses set status_id = 6 , balance = ? where id = ?', [data.balance.replace(/,/g, ''), url.expenses], (err, savepayment1) => {
                      conn.query('update creditors set crbalance = ? , balance = ? where id = ?', [sum2, sum1, creditors[0].id], (err, creditor) => {
                      });
                    });
                  }
                  //
                  var papernum = expenses[0].expenseschar + expenses[0].expensesnumber;
                  conn.query('select * from payment where reference_id = ? and papernumber = ?', [url.expenses, papernum], (err, select_payment) => {
                    var dataH = { book_id: 1, date: data.datepayment, doc_no: data.papernumber, detail: data.note, company_id: expenses[0].company_id, module_id: 20, reference_id: expenses[0].id };
                    var dataD = [];
                    conn.query('INSERT INTO  glheads set ?', [dataH], (err, glheads) => {
                      if (expenses[0].detail > 1) {
                        console.log("GL111111");
                        for (var i = 0; i < expenses.detail.length; i++) {
                          for (var j = 0; j < expenses_detail.length; i++) {
                            if (expenses[i].expenses_detail == expenses_detail[j].id) {
                              dataD.push({ code_id: expenses[i].acc_buy, cheack: "", date_list: null, dr: expenses[i].total, cr: 0, glheads_id: glheads.insertId });
                            }
                            if (expenses[0].vat && data.vat != "") {
                              dataD.push({ code_id: 433, cheack: "", date_list: null, dr: expenses[0].vatamount, cr: 0, glheads_id: glheads.insertId });
                            }
                            if (expenses[0].creditors_id && expenses[0].status_pay != "") {
                              dataD.push({ code_id: 406, cheack: "", date_list: null, dr: 0, cr: expenses[0].granttotal, glheads_id: glheads.insertId });
                            }
                          }
                        }
                      } else {
                        console.log("GL2222222");
                        if (expenses[0].expenses_detail) {
                          dataD.push({ code_id: expenses[0].acc_buy, cheack: "", date_list: null, dr: expenses[0].total, cr: 0, glheads_id: glheads.insertId });
                        }
                        if (expenses[0].vat && expenses[0].vat != "") {
                          dataD.push({ code_id: 433, cheack: "", date_list: null, dr: expenses[0].vatamount, cr: 0, glheads_id: glheads.insertId });
                        }
                        if (expenses[0].creditors_id && expenses[0].status_pay != "") {
                          dataD.push({ code_id: 406, cheack: "", date_list: null, dr: 0, cr: expenses[0].granttotal, glheads_id: glheads.insertId });
                        }
                      }
                      console.log(dataD);
                      for (let i = 0; i < dataD.length; i++) {
                        conn.query('INSERT INTO gldetail set ?', [dataD[i]], (err, gldetail) => {
                          if (err) {
                            res.json(err);
                          }
                        });
                      }
                    });
                  });
                  //
                  res.redirect('/expenses')
                });
              });
            })
        } else if (expenses[0].status_pay != "" && expenses[0].balance != "") {
          console.log("เงิดสดและ เชื่อ");
          conn.query('INSERT INTO payment (reference_id,papernumber,amount,acc_money,datepayment,net_payout,withholding,withholding_id,withholding_vate,type_payment_id,Identify,petty_cash,transfer_account,charge,check_account,check_date,check_number,check_charge,mastercard,typemachin_id,chanal_pay,note,balance,check_acc_cut,overdue,module_id,boolean_id) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,1)',
            [url.expenses, data.papernumber, data.amount, data.acc_money, data.datepayment, data.net_payout, data.withholding, data.withholding_id, data.withholding_vate, data.type_payment_id, data.Identify, data.petty_cash, data.transfer_account, data.charge, data.check_account, data.check_date, data.check_number, data.check_charge, data.mastercard, data.typemachin_id, data.chanal_pay, data.note, data.balance, data.check_acc_cut, 0, 20], (err, payment) => {
              ///////////////////////////////////////////////////////////////////////////////////////////////////
              //ส่วนของ เจ้าหนี้
              ///////////////////////////////////////////////////////////////////////////////////////////////////
              conn.query('select * from creditors where id = ?', [expenses[0].creditors_id], (err, creditors) => {
                conn.query('select sum(net_payout) as sumnet from payment where reference_id = ?;', [url.expenses], (err, paymentsave) => {
                  if (expenses[0].granttotal == paymentsave[0].sumnet) {
                    var sum1 = (+creditors[0].balance) - (+data.net_payout);
                    var sum2 = (+creditors[0].crbalance) + (+data.net_payout);
                    conn.query('update expenses set status_id = 5 , balance = ? where id = ?', [data.balance.replace(/,/g, ''), url.expenses], (err, savepayment1) => {
                      conn.query('update creditors set crbalance = ? , balance = ? where id = ?', [sum2, sum1, creditors[0].id], (err, creditor) => {
                      });
                    });
                  } else {
                    var sum1 = (+creditors[0].balance) - (+data.net_payout);
                    var sum2 = (+creditors[0].crbalance) + (+data.net_payout);
                    conn.query('update expenses set status_id = 6 , balance = ? where id = ?', [data.balance.replace(/,/g, ''), url.expenses], (err, savepayment1) => {
                      conn.query('update creditors set crbalance = ? , balance = ? where id = ?', [sum2, sum1, creditors[0].id], (err, creditor) => {
                      });
                    });
                  }
                  //
                  var papernum = expenses[0].expenseschar + expenses[0].expensesnumber;
                  conn.query('select * from payment where reference_id = ? and papernumber = ?', [url.expenses, papernum], (err, select_payment) => {
                    var dataH = { book_id: 1, date: data.datepayment, doc_no: data.papernumber, detail: data.note, company_id: expenses[0].company_id, module_id: 20, reference_id: select_payment[0].id };
                    var dataD = [];
                    conn.query('INSERT INTO  glheads set ?', [dataH], (err, glheads) => {

                      if (data.withholding_vate && data.withholding_vate != "") {
                        dataD.push({ code_id: 434, cheack: "", date_list: null, dr: 0, cr: data.withholding_vate.replace(/,/g, ''), glheads_id: glheads.insertId });
                      }
                      if (data.amount && data.amount != "") {
                        dataD.push({ code_id: creditors[0].chart_id, cheack: "", date_list: null, dr: data.amount, cr: 0, glheads_id: glheads.insertId });
                      }
                      if (data.type_payment_id == 1) {
                        dataD.push({ code_id: 406, cheack: "", date_list: null, dr: 0, cr: data.net_payout, glheads_id: glheads.insertId });
                      }
                      if (data.type_payment_id == 2) {
                        dataD.push({ code_id: 407, cheack: "", date_list: null, dr: 0, cr: data.net_payout, glheads_id: glheads.insertId });
                      }
                      if (data.type_payment_id == 3 && data.charge != "") {
                        dataD.push({ code_id: 408, cheack: "", date_list: null, dr: 0, cr: data.net_payout, glheads_id: glheads.insertId });
                        dataD.push({ code_id: 570, cheack: "", date_list: null, dr: data.charge, cr: 0, glheads_id: glheads.insertId });
                      }
                      if (data.type_payment_id == 4 && data.check_charge != "") {
                        dataD.push({ code_id: 551, cheack: "", date_list: null, dr: 0, cr: data.net_payout, glheads_id: glheads.insertId });
                        dataD.push({ code_id: 570, cheack: "", date_list: null, dr: data.check_charge, cr: 0, glheads_id: glheads.insertId });
                      }
                      if (data.type_payment_id == 5) {
                        dataD.push({ code_id: 496, cheack: "", date_list: null, dr: 0, cr: data.net_payout, glheads_id: glheads.insertId });
                      }

                      console.log(dataD);
                      for (let i = 0; i < dataD.length; i++) {
                        conn.query('INSERT INTO gldetail set ?', [dataD[i]], (err, gldetail) => {
                          if (err) {
                            res.json(err);
                          }
                        });
                      }
                    });
                  });
                  //
                  res.redirect('/expenses')
                });
              });
            })
        }
      });
    }
  });
}
///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_can't savepay
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.payment_delete = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    if (url.receipt) {
      conn.query('select * from receipt where id = ?', [url.receipt], (err, select_receipt) => {
        var paper = select_receipt[0].receiptchar + select_receipt[0].receiptordernumber;
        conn.query('select * from payment where reference_id = ? and papernumber = ?', [url.receipt, paper], (err, select_payment) => {
          conn.query('update receipt set balance = null, status_id = 2 where id = ?', [url.receipt], (err, receipt) => {
            conn.query('Delete from payment where reference_id  = ? and papernumber = ?', [url.receipt, paper], (err, payment) => {
              ///////////////////////////////////////////////////////////////////////////////////////////////////
              // ส่วนของ เจ้าหนี้
              ///////////////////////////////////////////////////////////////////////////////////////////////////
              conn.query('select * from creditors where id = ? ', [select_receipt[0].creditors_id], (err, creditors) => {
                var sum1 = select_receipt[0].granttotal + creditors[0].balance;
                var sum2 = creditors[0].crbalance - select_receipt[0].granttotal;
                conn.query('update creditors set crbalance = ? , balance = ? where id = ?', [sum2, sum1, creditors[0].id], (err, creditor) => {
                  ///////////////////////////////////////////////////////////////////////////////////////////////////
                  //ส่วนของ GL
                  ///////////////////////////////////////////////////////////////////////////////////////////////////
                  conn.query('select * from glheads where company_id = ?  and doc_no = ? and module_id = ? and reference_id = ?', [select_receipt[0].company_id, paper, 12, select_payment[0].id], (err, glheads) => {
                    conn.query('Delete from gldetail  where glheads_id = ?', [glheads[0].id], (err, gldetail) => {
                      conn.query('Delete from glheads  where id = ?', [glheads[0].id], (err, del_glheads) => {
                      });
                    });
                  });
                  //
                  res.redirect('/product_receipt');
                });
              });
            });
          });
        });
      });
    } else {
      console.log("pppp");
      conn.query('select * from expenses where id = ?', [url.expenses], (err, select_expenses) => {
        var paper = select_expenses[0].expenseschar + select_expenses[0].expensesnumber;
        conn.query('select * from payment where reference_id = ? and papernumber = ?', [url.expenses, paper], (err, select_payment) => {
          conn.query('update expenses set balance = null, status_id = 2 where id = ?', [url.expenses], (err, expenses) => {
            conn.query('Delete from payment where reference_id  = ? and papernumber = ? ', [url.expenses, paper], (err, payment) => {
              ///////////////////////////////////////////////////////////////////////////////////////////////////
              // ส่วนของ เจ้าหนี้
              ///////////////////////////////////////////////////////////////////////////////////////////////////
              conn.query('select * from creditors where id = ? ', [select_expenses[0].creditors_id], (err, creditors) => {
                var sum1 = select_expenses[0].granttotal + creditors[0].balance;
                var sum2 = creditors[0].crbalance - select_expenses[0].granttotal;
                conn.query('update creditors set crbalance = ? , balance = ? where id = ?', [sum2, sum1, creditors[0].id], (err, creditor) => {
                  ///////////////////////////////////////////////////////////////////////////////////////////////////
                  //ส่วนของ GL
                  ///////////////////////////////////////////////////////////////////////////////////////////////////
                  conn.query('select * from glheads where company_id = ?  and doc_no = ? and module_id = ? and reference_id = ?', [select_expenses[0].company_id, paper, 20, select_payment[0].id], (err, glheads) => {
                    console.log(err);
                    conn.query('Delete from gldetail  where glheads_id = ?', [glheads[0].id], (err, gldetail) => {
                      conn.query('Delete from glheads  where id = ?', [glheads[0].id], (err, del_glheads) => {
                      });
                    });
                  });
                  //
                  res.redirect('/expenses');
                });
              });
            });
          });
        });
      });
    }
  });
};
controller.payment_moneydelete = (req, res) => {
  var url = req.params;
  console.log("ยกเลิก status_pay");
  req.getConnection((err, conn) => {
    if (url.receipt) {
      conn.query('select * from receipt where id = ?', [url.receipt], (err, select_receipt) => {
        if (select_receipt[0].status_pay < select_receipt[0].granttotal) {
          console.log("เงินสดและเชื่อ555555");
          // conn.query('select * from receipt where id = ?', [url.receipt], (err, select_receipt) => {
          var paper = select_receipt[0].receiptchar + select_receipt[0].receiptordernumber;
          var sum1 = select_receipt[0].granttotal - select_receipt[0].status_pay;
          conn.query('select * from payment where reference_id = ? and papernumber = ?', [url.receipt, paper], (err, select_payment) => {
            conn.query('update receipt set balance = ?, status_id = 6 where id = ?', [sum1, url.receipt], (err, receipt) => {
              conn.query('Delete from payment where reference_id  = ? and papernumber = ? and boolean_id = 1', [url.receipt, paper], (err, payment) => {
                ///////////////////////////////////////////////////////////////////////////////////////////////////
                // ส่วนของ เจ้าหนี้
                ///////////////////////////////////////////////////////////////////////////////////////////////////
                conn.query('select * from creditors where id = ? ', [select_receipt[0].creditors_id], (err, creditors) => {

                  var sum2 = sum1 + creditors[0].balance;
                  var sum3 = creditors[0].crbalance - sum1;
                  conn.query('update creditors set crbalance = ? , balance = ? where id = ?', [sum3, sum2, creditors[0].id], (err, creditor) => {
                    ///////////////////////////////////////////////////////////////////////////////////////////////////
                    //ส่วนของ GL
                    ///////////////////////////////////////////////////////////////////////////////////////////////////
                    conn.query('select * from glheads where company_id = ?  and doc_no = ? and module_id = ? and reference_id = ?', [select_receipt[0].company_id, paper, 12, select_payment[0].id], (err, glheads) => {
                      console.log(err);
                      conn.query('Delete from gldetail  where glheads_id = ?', [glheads[0].id], (err, gldetail) => {
                        console.log(err);
                        conn.query('Delete from glheads  where id = ?', [glheads[0].id], (err, del_glheads) => {
                          console.log(err);
                        });
                      });
                    });
                    //
                    res.redirect('/product_receipt');
                  });
                });
              });
            });
          });
          // });
        } else {
          console.log("เงินสด11");
          var paper = select_receipt[0].receiptchar + select_receipt[0].receiptordernumber;
          conn.query('select * from payment where reference_id = ? and papernumber = ?', [url.receipt, paper], (err, select_payment) => {
            conn.query('update receipt set balance = "", status_id = 6 where id = ?', [url.receipt], (err, receipt) => {
              conn.query('Delete from payment where reference_id  = ? and papernumber = ? ', [url.receipt, paper], (err, payment) => {
                ///////////////////////////////////////////////////////////////////////////////////////////////////
                // ส่วนของ เจ้าหนี้
                ///////////////////////////////////////////////////////////////////////////////////////////////////
                conn.query('select * from creditors where id = ? ', [select_receipt[0].creditors_id], (err, creditors) => {
                  console.log(select_receipt[0].granttotal);
                  var sum1 = select_receipt[0].granttotal + creditors[0].balance;
                  console.log(sum1);
                  var sum2 = creditors[0].crbalance - select_receipt[0].granttotal;
                  console.log(sum2);
                  conn.query('update creditors set crbalance = ? , balance = ? where id = ?', [sum2, sum1, creditors[0].id], (err, creditor) => {
                    console.log(err, creditor);
                    ///////////////////////////////////////////////////////////////////////////////////////////////////
                    //ส่วนของ GL
                    ///////////////////////////////////////////////////////////////////////////////////////////////////
                    conn.query('select * from glheads where company_id = ?  and doc_no = ? and module_id = ? and reference_id = ?', [select_receipt[0].company_id, paper, 12, select_receipt[0].id], (err, glheads) => {
                      conn.query('Delete from gldetail  where glheads_id = ?', [glheads[0].id], (err, gldetail) => {
                        conn.query('Delete from glheads  where id = ?', [glheads[0].id], (err, del_glheads) => {
                        });
                      });
                    });
                    //
                    res.redirect('/product_receipt');
                  });
                });
              });
            });
          });
        }
      });
    } else {
      conn.query('select expenses. *,expenses_list.acc_buy as acc_buy,expenses_list.detail as detail from expenses join expenses_list on expenses_list.expenses_id = expenses.id where expenses.id = ?;', [url.expenses], (err, select_expenses) => {
        if (select_expenses[0].status_pay < select_expenses[0].granttotal) {
          console.log("เงินสดและเชื่อ555555");
          var paper = select_expenses[0].expenseschar + select_expenses[0].expensesnumber;
          var sum1 = select_expenses[0].granttotal - select_expenses[0].status_pay;
          conn.query('select * from payment where reference_id = ? and papernumber = ?', [url.expenses, paper], (err, select_payment) => {
            conn.query('update expenses set balance = ?, status_id = 6 where id = ?', [sum1, url.expenses], (err, expenses) => {
              conn.query('Delete from payment where reference_id  = ? and papernumber = ? and boolean_id = 1', [url.expenses, paper], (err, payment) => {
                ///////////////////////////////////////////////////////////////////////////////////////////////////
                // ส่วนของ เจ้าหนี้
                ///////////////////////////////////////////////////////////////////////////////////////////////////
                conn.query('select * from creditors where id = ? ', [select_expenses[0].creditors_id], (err, creditors) => {

                  var sum2 = sum1 + creditors[0].balance;
                  var sum3 = creditors[0].crbalance - sum1;
                  conn.query('update creditors set crbalance = ? , balance = ? where id = ?', [sum3, sum2, creditors[0].id], (err, creditor) => {
                    ///////////////////////////////////////////////////////////////////////////////////////////////////
                    //ส่วนของ GL
                    ///////////////////////////////////////////////////////////////////////////////////////////////////
                    conn.query('select * from glheads where company_id = ?  and doc_no = ? and module_id = ? and reference_id = ?', [select_expenses[0].company_id, paper, 20, select_payment[0].id], (err, glheads) => {
                      console.log(err);
                      conn.query('Delete from gldetail  where glheads_id = ?', [glheads[0].id], (err, gldetail) => {
                        console.log(err);
                        conn.query('Delete from glheads  where id = ?', [glheads[0].id], (err, del_glheads) => {
                          console.log(err);
                        });
                      });
                    });
                    //
                    res.redirect('/expenses');
                  });
                });
              });
            });
          });
        } else {
          console.log("เงินสด11");
          var paper = select_expenses[0].expenseschar + select_expenses[0].expensesnumber;
          conn.query('select * from payment where reference_id = ? and papernumber = ?', [url.expenses, paper], (err, select_payment) => {
            conn.query('update expenses set balance = "", status_id = 6 where id = ?', [url.expenses], (err, expenses) => {
              conn.query('Delete from payment where reference_id  = ? and papernumber = ? ', [url.expenses, paper], (err, payment) => {
                ///////////////////////////////////////////////////////////////////////////////////////////////////
                // ส่วนของ เจ้าหนี้
                ///////////////////////////////////////////////////////////////////////////////////////////////////
                conn.query('select * from creditors where id = ? ', [select_expenses[0].creditors_id], (err, creditors) => {
                  console.log(select_expenses[0].granttotal);
                  var sum1 = select_expenses[0].granttotal + creditors[0].balance;
                  console.log(sum1);
                  var sum2 = creditors[0].crbalance - select_expenses[0].granttotal;
                  console.log(sum2);
                  conn.query('update creditors set crbalance = ? , balance = ? where id = ?', [sum2, sum1, creditors[0].id], (err, creditor) => {
                    console.log(err, creditor);
                    ///////////////////////////////////////////////////////////////////////////////////////////////////
                    //ส่วนของ GL
                    ///////////////////////////////////////////////////////////////////////////////////////////////////
                    conn.query('select * from glheads where company_id = ?  and doc_no = ? and module_id = ? and reference_id = ?', [select_expenses[0].company_id, paper, 20, select_expenses[0].id], (err, glheads) => {
                      conn.query('Delete from gldetail  where glheads_id = ?', [glheads[0].id], (err, gldetail) => {
                        conn.query('Delete from glheads  where id = ?', [glheads[0].id], (err, del_glheads) => {
                        });
                      });
                    });
                    //
                    res.redirect('/expenses');
                  });
                });
              });
            });
          });
        }
      });
    }
  });
}

controller.petty_cash = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    res.render('buy/product_receipt/product_receipt_pettycash', {
      session: req.session, url
    });
  });
};
controller.petty_cash_save = (req, res) => {
  var url = req.params;
  const data = req.body;
  req.getConnection((err, conn) => {
    conn.query('INSERT INTO petty_cash  (name_cash,petty_money,user,detail) values(?,?,?,?)', [data.name_cash, data.petty_money, data.user, data.detail], (err, petty_cash) => {
      res.redirect('/product_receipt/pay/' + url.receipt)
    });
  });
}


controller.acc_transfer = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('select * from bank;', (err, bank) => {
      conn.query('select * from type_account;', (err, typeaccount) => {
        res.render('buy/product_receipt/product_receipt_transfer', {
          session: req.session, url, bank, typeaccount
        });
      });
    });
  });
};
controller.acc_transfer_save = (req, res) => {
  var url = req.params;
  const data = req.body;
  console.log(data);
  req.getConnection((err, conn) => {
    conn.query('INSERT INTO account_transfer  (bank_name,bank_number,branchbank,account_name,typeacc_id) values(?,?,?,?,?)', [data.bank_name, data.bank_number, data.branchbank, data.account_name, data.typeacc_id], (err, acc_transfer) => {
      console.log(acc_transfer, err);
      res.redirect('/product_receipt/pay/' + url.receipt)
    });
  });
}

controller.mastercard = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('select * from bank;', (err, bank) => {
      conn.query('select * from type_card;', (err, typecard) => {
        res.render('buy/product_receipt/product_receipt_mastercard', {
          session: req.session, url, bank, typecard
        });
      });
    });
  });
};

controller.mastercard_save = (req, res) => {
  var url = req.params;
  const data = req.body;
  req.getConnection((err, conn) => {
    conn.query('INSERT INTO card (typecard_id,bank_id,numbercard,username,cardname,money,sumarydate,duedate) values(?,?,?,?,?,?,?,?)',
      [data.typecard_id, data.bank_id, data.numbercard, data.username, data.cardname, data.money, data.sumarydate, data.duedate], (err, card) => {
        res.redirect('/product_receipt/pay/' + url.receipt)
      });
  });
}


controller.chanal_other = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('select * from typepayment_card where id = ?;', [url.typeid], (err, typepayment_card) => {
      conn.query('select * from bank;', (err, bank) => {
        conn.query('select * from typepayment_card;', (err, select_typepaymentcard) => {
          res.render('buy/product_receipt/product_receipt_other', {
            session: req.session, url, bank, typepayment_card, select_typepaymentcard
          });
        });
      });
    });
  });
};
controller.chanal_other_save = (req, res) => {
  var url = req.params;
  const data = req.body;
  req.getConnection((err, conn) => {
    conn.query('INSERT INTO chanal_other  (machine_payment,bank_name,serial_number,service_name,name_market,detail) values(?,?,?,?,?,?)', [data.machine_payment, data.bank_name, data.serial_number, data.service_name, data.name_market, data.detail], (err, chanal) => {
      res.redirect('/product_receipt/pay/' + url.receipt)
    });
  });
}


///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_ajax
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.getinventory = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('select * from inventory join product_list on inventory.product_list_id = product_list.id where inventory_list_id = ?;', [url.inventory], (err, inventory) => {
      res.send(inventory);
    });
  });
};
controller.gettypemachin = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('select * from chanal_other  where machine_payment = ?;', [url.typemachin], (err, typemachin) => {
      res.send(typemachin);
    });
  });
};
module.exports = controller;
