const session = require("express-session");
const controller = {};
const { v4: uuidv4 } = require('uuid');
const fs = require('fs');
const e = require("express");


///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_list
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.purchase_list = (req, res) => {
  req.getConnection((err, conn) => {
    conn.query('select purchase.*, status.name as status ,DATE_FORMAT(DATE_ADD(purchase.date, INTERVAL 543 YEAR), " %d/%m/%Y" )dateday,DATE_FORMAT(DATE_ADD(purchase.due, INTERVAL 543 YEAR), " %d/%m/%Y" )dueday from purchase join status on status_id = status.id where YEAR(date) = YEAR(CURDATE())', (err, purchase) => {
      conn.query('select * from status', (err, status) => {
        res.render('buy/purchase_order/purchase_order_list.ejs', {
          purchase, status, session: req.session
        });
      });
    });
  });
};
///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_add
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.purchase_add = (req, res) => {
  req.getConnection((err, conn) => {
    //runnumber purchase
    conn.query('select * from module where id = 11', (err, purchasemodule) => {
      conn.query('select date_format(date(now()),"%Y%m") as number', (err, runnumber) => {
        conn.query('select count(purchaseordernumber) as countnumber,max(purchaseordernumber) as maxnumber from purchase where date_format(date,"%Y%m") = date_format(date(now()),"%Y%m");', (err, purchasenumber) => {
          //
          conn.query('select creditors.*,  provinces.name_th as provinces, amphures.name_th as amphures, districts.name_th as districts, zip_code from creditors join provinces on creditors.provinces_id = provinces.id join amphures on creditors.amphures_id = amphures.id join districts on creditors.districts_id = districts.id', (err, creditors) => {
            conn.query('select * from indicate', (err, indicate) => {
              conn.query('select * from inventory_list;', (err, inventory) => {
                conn.query('select * from company', (err, company) => {
                  conn.query('select * from category_price', (err, category_price) => {
                    conn.query('select * from witholding', (err, witholding) => {
                      conn.query('select * from discount', (err, discount) => {
                        conn.query('select * from vat', (err, vat) => {
                          conn.query('select * from compute', (err, compute) => {
                            conn.query('select * from projectgl', (err, projectgl) => {
                              //creditors
                              conn.query('select * from chart where typeAcc_id = 5;', (err, chart) => {
                                conn.query('select * from bank', (err, bank) => {
                                  conn.query('select * from provinces', (err, provinces) => {
                                    conn.query('select * from human', (err, human) => {
                                      //product
                                      conn.query('select * from chart where typeAcc_id = 9;', (err, acc_sell) => {
                                        conn.query('select * from chart where typeAcc_id = 11;', (err, acc_buy) => {
                                          conn.query('select * from product_list where enable = 1', (err, product_list) => {
                                            conn.query('select * from product_type', (err, product_type) => {
                                              conn.query('select * from product_type_list', (err, product_type_list) => {
                                                conn.query('select * from product_tax', (err, product_tax) => {
                                                  conn.query('select * from product_group', (err, product_group) => {
                                                    //runnumber creditors
                                                    conn.query('select * from module where id = 4', (err, creditormodule) => {
                                                      conn.query('select max(creditorID) as maxnumber from creditors where creditorID like ?', [creditormodule[0].char + "%"], (err, maxnumber) => {
                                                        res.render('buy/purchase_order/purchase_order_add.ejs', {
                                                          runnumber, purchasenumber, creditors, indicate, witholding, discount, vat, compute, projectgl, bank, provinces, product_list, category_price, purchasemodule, creditormodule,
                                                          product_type, product_type_list, product_tax, product_group, human, module, maxnumber, acc_sell, acc_buy, chart, inventory, company, session: req.session
                                                        });
                                                      });
                                                    });
                                                  });
                                                });
                                              });
                                            });
                                          });
                                        });
                                      });
                                    });
                                  });
                                });
                              });
                            });
                          });
                        });
                      });
                    });
                  });
                });
              });
            });
          });
        });
      });
    });
  });
};
///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_save
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.purchase_save = (req, res) => {
  const data = req.body;
  console.log(data);
  req.getConnection((err, conn) => {
    conn.query('INSERT INTO purchase (purchasechar,purchaseordernumber,creditors_id,creditors_name,address,post,tax,branch,date,credit,indicate_id,due,employee_saler,employee_delivery,project_id,number,compute_id,signature,note,notein,total,afterdiscount,except,vatcalculator,vatamount,notvatamount,alldiscount,discount_id,vat,witholding_id,witholding,granttotal,payment,status_id,itemizedvat,itemizeddiscount,inventory_id,company_id)  values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
      [data.purchasechar, data.purchaseordernumber, data.creditors_id, data.creditors_name, data.address, data.post, data.tax, data.branch, data.date, data.credit, data.indicate_id, data.due, data.employee_saler, data.employee_delivery, data.project_id, data.number, data.compute_id, data.signature, data.note, data.notein, data.total.replace(/,/g, ''), data.afterdiscount.replace(/,/g, ''), data.except.replace(/,/g, ''), data.vatcalculator.replace(/,/g, ''), data.vatamount.replace(/,/g, ''), data.notvatamount.replace(/,/g, ''), data.alldiscount.replace(/,/g, ''), data.discount_id, data.vat, data.witholding_id, data.witholding.replace(/,/g, ''), data.granttotal.replace(/,/g, ''), data.payment.replace(/,/g, ''), 2, data.itemizedvat, data.itemizeddiscount, data.inventory_id, data.company_id], (err, purchase) => {
        if (Array.isArray(data.product_id)) {
          for (var i = 0; i < data.product_id.length; i++) {
            conn.query('INSERT INTO purchase_list (product_id,product_code,detail,quantity,categoryprice_id,price,listdiscount,vat_id,totallist,purchase_id)  values(?,?,?,?,?,?,?,?,?,?)', [data.product_id[i], data.product_code[i], data.detail[i], data.quantity[i], data.categoryprice_id[i], data.price[i], data.listdiscount[i], data.vat_id[i], data.totallist[i], purchase.insertId], (err, purchase_list) => {
            });
          }
        } else {
          conn.query('INSERT INTO purchase_list (product_id,product_code,detail,quantity,categoryprice_id,price,listdiscount,vat_id,totallist,purchase_id)  values(?,?,?,?,?,?,?,?,?,?)', [data.product_id, data.product_code, data.detail, data.quantity, data.categoryprice_id, data.price, data.listdiscount, data.vat_id, data.totallist, purchase.insertId], (err, purchase_list) => {
          });
        }

        if (data.filename) {
          var file = data.filename;
          if (!Array.isArray(file)) {
            var type = file.split(".");
            var filename = uuidv4() + "." + type[type.length - 1];
            fs.copyFile("public/purchase/" + data.filename, "public/purchase/" + filename, (err) => {
              if (err) {
                console.log("Error Found:", err);
              }
            });
            data.filename = filename;
            conn.query('INSERT INTO purchase_file (filename,purchase_id) values(?,?)', [data.filename, purchase.insertId], (err, purchase_file) => {
            });
          } else {
            for (var i = 0; i < file.length; i++) {
              var type = file[i].split(".");
              var filename = uuidv4() + "." + type[type.length - 1];
              fs.copyFile("public/purchase/" + file[i], "public/purchase/" + filename, (err) => {
                if (err) {
                  console.log("Error Found:", err);
                }
              });
              data.filename = filename;
              conn.query('INSERT INTO purchase_file (filename,purchase_id) values(?,?)', [data.filename, purchase.insertId], (err, purchase_file) => {
              });
            }
          }
        }
      });
    res.redirect('/purchase_order')
  });
}
///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_edit
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.purchase_edit = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('select purchase.* ,projectgl.name as projectgl,compute.name as compute,product_list.name as name,category_price.name as category,purchase_list.detail as detail,purchase_list.quantity as quantity,purchase_list.price as price ,discount.name as discount,listdiscount,vat_id,totallist,product_code,product_id,categoryprice_id,filename,DATE_FORMAT(purchase.date,"%Y-%m-%d") as dateday ,DATE_FORMAT(purchase.due,"%Y-%m-%d") as dueday  from purchase  join purchase_list on purchase_list.purchase_id = purchase.id left join purchase_file on purchase_file.purchase_id = purchase.id left join projectgl on purchase.project_id = projectgl.id join compute on purchase.compute_id  = compute.id  join product_list on purchase_list.product_id = product_list.id join category_price  on purchase_list.categoryprice_id = category_price.id left join discount on purchase.discount_id = discount.id where  purchase_list.purchase_id = ?;', [url.purchase], (err, purchase) => {
      //runnumber purchase
      conn.query('select * from module where id = 11', (err, purchasemodule) => {
        conn.query('select date_format(date(now()),"%Y%m") as number', (err, runnumber) => {
          conn.query('select count(purchaseordernumber) as countnumber,max(purchaseordernumber) as maxnumber from purchase where date_format(date,"%Y%m") = date_format(date(now()),"%Y%m")', (err, purchasenumber) => {
            //
            conn.query('select creditors.*,  provinces.name_th as provinces, amphures.name_th as amphures, districts.name_th as districts, zip_code from creditors join provinces on creditors.provinces_id = provinces.id join amphures on creditors.amphures_id = amphures.id join districts on creditors.districts_id = districts.id', (err, creditors) => {
              conn.query('select * from human_resource', (err, human_resource) => {
                conn.query('select * from company', (err, company) => {
                  conn.query('select * from inventory_list;', (err, inventory) => {
                    conn.query('select * from indicate', (err, indicate) => {
                      conn.query('select * from witholding', (err, witholding) => {
                        conn.query('select * from discount', (err, discount) => {
                          conn.query('select * from category_price', (err, category_price) => {
                            conn.query('select * from vat', (err, vat) => {
                              conn.query('select * from compute', (err, compute) => {
                                conn.query('select * from projectgl', (err, projectgl) => {
                                  //creditors
                                  conn.query('select * from chart where typeAcc_id = 5;', (err, chart) => {
                                    conn.query('select * from bank', (err, bank) => {
                                      conn.query('select * from provinces', (err, provinces) => {
                                        conn.query('select * from human', (err, human) => {
                                          //product
                                          conn.query('select * from chart where typeAcc_id = 9;', (err, acc_sell) => {
                                            conn.query('select * from chart where typeAcc_id = 11;', (err, acc_buy) => {
                                              conn.query('select product_list.* from product_list join inventory on inventory.product_list_id = product_list.id where product_list.enable = 1 and inventory.inventory_list_id = ?', [purchase[0].inventory_id], (err, product_list) => {
                                                conn.query('select * from product_type', (err, product_type) => {
                                                  conn.query('select * from product_type_list', (err, product_type_list) => {
                                                    conn.query('select * from product_tax', (err, product_tax) => {
                                                      conn.query('select * from product_group', (err, product_group) => {
                                                        //runnumber creditors
                                                        conn.query('select * from module where id = 4', (err, creditormodule) => {
                                                          conn.query('select max(creditorID) as maxnumber from creditors where creditorID like ?', [creditormodule[0].char + "%"], (err, maxnumber) => {
                                                            res.render('buy/purchase_order/purchase_order_edit', {
                                                              runnumber, purchasenumber, creditors, indicate, witholding, discount, vat, compute, projectgl, bank, provinces, product_list, purchase, category_price, human_resource, purchasemodule,
                                                              product_type, product_type_list, product_tax, product_group, human, module, maxnumber, acc_buy, acc_sell, creditormodule, chart, inventory, company, session: req.session
                                                            });
                                                          });
                                                        });
                                                      });
                                                    });
                                                  });
                                                });
                                              });
                                            });
                                          });
                                        });
                                      });
                                    });
                                  });
                                });
                              });
                            });
                          });
                        });
                      });
                    });
                  });
                });
              });
            });
          });
        });
      });
    });
  });
};
///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_update
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.purchase_update = (req, res) => {
  var url = req.params;
  const data = req.body;
  console.log(data);
  req.getConnection((err, conn) => {
    conn.query('update purchase set creditors_id = ? ,creditors_name = ? ,address = ? ,post = ?,tax = ?,branch = ?,date = ?,credit = ?,indicate_id = ?,due = ?,employee_saler = ?,employee_delivery = ?,project_id = ?,number = ?,compute_id = ?,signature = ?,note = ?,notein = ?,total = ?,afterdiscount = ?,except = ?,vatcalculator = ?,vatamount = ?,notvatamount = ?,alldiscount = ?,discount_id = ?,vat = ?,witholding_id = ?,witholding = ?,granttotal = ?,payment = ?,itemizedvat = ?,itemizeddiscount = ?,inventory_id = ? , company_id = ?  where  id = ?'
      , [data.creditors_id, data.creditors_name, data.address, data.post, data.tax, data.branch, data.date, data.credit, data.indicate_id, data.due, data.employee_saler, data.employee_delivery, data.project_id, data.number, data.compute_id, data.signature, data.note, data.notein, data.total.replace(/,/g, ''), data.afterdiscount.replace(/,/g, ''), data.except.replace(/,/g, ''), data.vatcalculator.replace(/,/g, ''), data.vatamount.replace(/,/g, ''), data.notvatamount.replace(/,/g, ''), data.alldiscount.replace(/,/g, ''), data.discount_id, data.vat, data.witholding_id, data.witholding.replace(/,/g, ''), data.granttotal.replace(/,/g, ''), data.payment.replace(/,/g, ''), data.itemizedvat, data.itemizeddiscount, data.inventory_id, data.company_id, url.purchase], (err, purchase) => {
        conn.query('delete from purchase_list where purchase_id = ?', [url.purchase], (err, purchase_product) => {
          if (Array.isArray(data.product_id)) {
            for (var i = 0; i < data.product_id.length; i++) {
              conn.query('INSERT INTO purchase_list (product_id,product_code,detail,quantity,categoryprice_id,price,listdiscount,vat_id,totallist,purchase_id)  values(?,?,?,?,?,?,?,?,?,?)', [data.product_id[i], data.product_code[i], data.detail[i], data.quantity[i], data.categoryprice_id[i], data.price[i].replace(/,/g, ''), data.listdiscount[i].replace(/,/g, ''), data.vat_id[i], data.totallist[i].replace(/,/g, ''), url.purchase], (err, purchase_list) => {
              });
            }
          } else {
            conn.query('INSERT INTO purchase_list (product_id,product_code,detail,quantity,categoryprice_id,price,listdiscount,vat_id,totallist,purchase_id)  values(?,?,?,?,?,?,?,?,?,?)', [data.product_id, data.product_code, data.detail, data.quantity, data.categoryprice_id, data.price.replace(/,/g, ''), data.listdiscount.replace(/,/g, ''), data.vat_id, data.totallist.replace(/,/g, ''), url.purchase], (err, purchase_list) => {
            });
          }
        })
        if (req.files) {
          var file = req.files.filename;
          if (!Array.isArray(file)) {
            var type = file.name.split(".");
            var filename = uuidv4() + "." + type[type.length - 1];
            file.mv("./public/purchase/" + filename, function (err) {
              if (err) { console.log(err) }
              data.filename = filename;
              console.log(filename);
              conn.query('update purchase_file set filename = ? where purchase_id = ?', [data.filename, url.purchase], (err, purchase_file) => {
                console.log(err, purchase_file);
              });
            })
          } else {
            for (var i = 0; i < file.length; i++) {
              var type = file[i].name.split(".");
              var filename = uuidv4() + "." + type[type.length - 1];
              file[i].mv("./public/purchase/" + filename, function (err) {
                if (err) { console.log(err) }
              })
              data.filename = filename;
              conn.query('update purchase_file set filename = ? where purchase_id = ?', [filename, url.purchase], (err, purchase_file) => {
                console.log(err, purchase_file);
              });
            }
          }
        }
        res.redirect('/purchase_order')
      });
  });
};
///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_delete
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.purchase_delete = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('Delete from purchase_file where purchase_id = ?', [url.purchase], (err, purchase_file) => {
      conn.query('Delete from purchase_list where purchase_id = ?', [url.purchase], (err, purchase_list) => {
        conn.query('Delete from purchase where id = ?', [url.purchase], (err, purchase) => {
          res.redirect('/purchase_order');
        });
      })
    })
  });
};
///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_detail
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.purchase_detail = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('select purchase.*, inventory_list.name as inventory_id,company.branchNo as company_id,  purchase_list.product_id as product_id,purchase_list.detail as detail,purchase_list.quantity as quantity,purchase_list.categoryprice_id as categoryprice_id,purchase_list.price as price,purchase_list.listdiscount as listdiscount,purchase_list.vat_id as vat_id,purchase_list.totallist as totallist,purchase_list.product_code as product_code,filename,category_price.name as category,projectgl.name as projectgl,indicate.name as indicate,compute.name as compute,discount.name as discount,product_list.name as name,DATE_FORMAT(purchase.date,"%Y-%m-%d") as dateday ,DATE_FORMAT(purchase.due,"%Y-%m-%d") as dueday   from purchase  join purchase_list on purchase_list.purchase_id = purchase.id left join purchase_file on purchase_file.purchase_id = purchase.id left join projectgl on purchase.project_id = projectgl.id join compute on purchase.compute_id  = compute.id  join product_list on purchase_list.product_id = product_list.id join category_price  on purchase_list.categoryprice_id = category_price.id left join discount on purchase.discount_id = discount.id join indicate on purchase.indicate_id = indicate.id join inventory_list on purchase.inventory_id = inventory_list.id join company on purchase.company_id = company.id  where  purchase_list.purchase_id = ?;', [url.purchase], (err, purchase) => {
      conn.query('select * from indicate', (err, indicate) => {
        conn.query('select count(purchaseordernumber) as countnumber,max(purchaseordernumber) as maxnumber from purchase where date_format(date,"%Y%m") = date_format(date(now()),"%Y%m")', (err, purchasenumber) => {
          //////////////////////////////////////////////////////////
          var yearen_1 = purchase[0].dateday.split('-')
          var yearth_1 = +(yearen_1[0]) + 543
          var dateth = yearen_1[2] + "/" + yearen_1[1] + "/" + yearth_1;
          //////////////////////////////////////////////////////////
          if (purchase[0].indicate_id == 1) {
            var yearen_2 = purchase[0].dueday.split('-');
            var yearth_2 = +(yearen_2[0]) + 543
            var dueth = yearen_2[2] + "/" + yearen_2[1] + "/" + yearth_2;
          }
          //////////////////////////////////////////////////////////
          conn.query('select * from witholding', (err, witholding) => {
            res.render('buy/purchase_order/purchase_order_detail', {
              purchase, dateth, dueth, witholding, indicate, purchasenumber,
              session: req.session
            });
          });
        });
      });
    });
  });
};
///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_readd
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.purchase_readd = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('select purchase.* ,projectgl.name as projectgl,compute.name as compute,product_list.name as name,category_price.name as category,purchase_list.detail as detail,purchase_list.quantity as quantity,purchase_list.price as price ,discount.name as discount,listdiscount,vat_id,totallist,product_code,product_id,categoryprice_id,filename,DATE_FORMAT(purchase.date,"%Y-%m-%d") as dateday ,DATE_FORMAT(purchase.due,"%Y-%m-%d") as dueday  from purchase  join purchase_list on purchase_list.purchase_id = purchase.id left join purchase_file on purchase_file.purchase_id = purchase.id left join projectgl on purchase.project_id = projectgl.id join compute on purchase.compute_id  = compute.id  join product_list on purchase_list.product_id = product_list.id join category_price  on purchase_list.categoryprice_id = category_price.id left join discount on purchase.discount_id = discount.id where  purchase_list.purchase_id = ?;', [url.purchase], (err, purchase) => {
      //runnumber purchase
      conn.query('select * from module where id = 11', (err, purchasemodule) => {
        conn.query('select date_format(date(now()),"%Y%m") as number', (err, runnumber) => {
          conn.query('select count(purchaseordernumber) as countnumber,max(purchaseordernumber) as maxnumber from purchase where date_format(date,"%Y%m") = date_format(date(now()),"%Y%m")', (err, purchasenumber) => {
            //
            conn.query('select creditors.*,  provinces.name_th as provinces, amphures.name_th as amphures, districts.name_th as districts, zip_code from creditors join provinces on creditors.provinces_id = provinces.id join amphures on creditors.amphures_id = amphures.id join districts on creditors.districts_id = districts.id', (err, creditors) => {
              conn.query('select * from human_resource', (err, human_resource) => {
                conn.query('select * from company', (err, company) => {
                  conn.query('select * from inventory_list;', (err, inventory) => {
                    conn.query('select * from indicate', (err, indicate) => {
                      conn.query('select * from witholding', (err, witholding) => {
                        conn.query('select * from discount', (err, discount) => {
                          conn.query('select * from category_price', (err, category_price) => {
                            conn.query('select * from vat', (err, vat) => {
                              conn.query('select * from compute', (err, compute) => {
                                conn.query('select * from projectgl', (err, projectgl) => {
                                  //creditors
                                  conn.query('select * from chart where typeAcc_id = 5;', (err, chart) => {
                                    conn.query('select * from bank', (err, bank) => {
                                      conn.query('select * from provinces', (err, provinces) => {
                                        conn.query('select * from human', (err, human) => {
                                          //product
                                          conn.query('select * from chart where typeAcc_id = 9;', (err, acc_sell) => {
                                            conn.query('select * from chart where typeAcc_id = 11;', (err, acc_buy) => {
                                              conn.query('select product_list.* from product_list join inventory on inventory.product_list_id = product_list.id where product_list.enable = 1 and inventory.inventory_list_id = ?', [purchase[0].inventory_id], (err, product_list) => {
                                                conn.query('select * from product_type', (err, product_type) => {
                                                  conn.query('select * from product_type_list', (err, product_type_list) => {
                                                    conn.query('select * from product_tax', (err, product_tax) => {
                                                      conn.query('select * from product_group', (err, product_group) => {
                                                        //runnumber creditors
                                                        conn.query('select * from module where id = 4', (err, creditormodule) => {
                                                          conn.query('select max(creditorID) as maxnumber from creditors where creditorID like ?', [creditormodule[0].char + "%"], (err, maxnumber) => {
                                                            res.render('buy/purchase_order/purchase_order_readd', {
                                                              runnumber, purchasenumber, creditors, indicate, witholding, discount, vat, compute, projectgl, bank, provinces, product_list, purchase, category_price, human_resource, purchasemodule,
                                                              product_type, product_type_list, product_tax, product_group, human, module, maxnumber, acc_buy, acc_sell, creditormodule, chart, inventory, company, session: req.session
                                                            });
                                                          });
                                                        });
                                                      });
                                                    });
                                                  });
                                                });
                                              });
                                            });
                                          });
                                        });
                                      });
                                    });
                                  });
                                });
                              });
                            });
                          });
                        });
                      });
                    });
                  });
                });
              });
            });
          });
        });
      });
    });
  });
};
///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_purchase_receipt
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.purchase_to_receipt = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('select purchase.* ,projectgl.name as projectgl,compute.name as compute,product_list.name as name,category_price.name as category,purchase_list.detail as detail,purchase_list.quantity as quantity,purchase_list.price as price ,discount.name as discount,listdiscount,vat_id,totallist,product_code,product_id,categoryprice_id,filename,DATE_FORMAT(purchase.date,"%Y-%m-%d") as dateday ,DATE_FORMAT(purchase.due,"%Y-%m-%d") as dueday  from purchase  join purchase_list on purchase_list.purchase_id = purchase.id left join purchase_file on purchase_file.purchase_id = purchase.id left join projectgl on purchase.project_id = projectgl.id join compute on purchase.compute_id  = compute.id  join product_list on purchase_list.product_id = product_list.id join category_price  on purchase_list.categoryprice_id = category_price.id left join discount on purchase.discount_id = discount.id where  purchase_list.purchase_id = ?;', [url.purchase], (err, purchase) => {

      //runnumber receipt
      conn.query('select * from module where id = 12', (err, receiptmodule) => {
        conn.query('select date_format(date(now()),"%Y%m") as number', (err, runnumber) => {
          conn.query('select count(receiptordernumber) as countnumber,max(receiptordernumber) as maxnumber from receipt where date_format(date,"%Y%m") = date_format(date(now()),"%Y%m");', (err, receiptnumber) => {
            //
            conn.query('select creditors.*,  provinces.name_th as provinces, amphures.name_th as amphures, districts.name_th as districts, zip_code from creditors join provinces on creditors.provinces_id = provinces.id join amphures on creditors.amphures_id = amphures.id join districts on creditors.districts_id = districts.id', (err, creditors) => {
              conn.query('select * from human_resource', (err, human_resource) => {
                conn.query('select * from inventory_list;', (err, inventory) => {
                  conn.query('select * from indicate', (err, indicate) => {
                    conn.query('select * from category_price where product_list_id = ?', [purchase[0].product_id], (err, category_price) => {
                      conn.query('select * from witholding', (err, witholding) => {
                        conn.query('select * from discount', (err, discount) => {
                          conn.query('select * from vat', (err, vat) => {
                            conn.query('select * from company', (err, company) => {
                              conn.query('select * from compute', (err, compute) => {
                                conn.query('select * from projectgl', (err, projectgl) => {
                                  //creditors
                                  conn.query('select * from chart where typeAcc_id = 5;', (err, chart) => {
                                    conn.query('select * from bank', (err, bank) => {
                                      conn.query('select * from provinces', (err, provinces) => {
                                        conn.query('select * from human', (err, human) => {
                                          //product
                                          conn.query('select * from chart where typeAcc_id = 9;', (err, acc_sell) => {
                                            conn.query('select * from chart where typeAcc_id = 11;', (err, acc_buy) => {
                                              conn.query('select product_list.* from product_list join inventory on inventory.product_list_id = product_list.id where product_list.enable = 1 and inventory.inventory_list_id = ?', [purchase[0].inventory_id], (err, product_list) => {
                                                conn.query('select * from product_type', (err, product_type) => {
                                                  conn.query('select * from product_type_list', (err, product_type_list) => {
                                                    conn.query('select * from product_tax', (err, product_tax) => {
                                                      conn.query('select * from product_group', (err, product_group) => {
                                                        //runnumber creditors
                                                        conn.query('select * from module where id = 4', (err, creditormodule) => {
                                                          conn.query('select max(creditorID) as maxnumber from creditors where creditorID like ?', [creditormodule[0].char + "%"], (err, maxnumber) => {
                                                            res.render('buy/purchase_order/purchase_to_receipt', {
                                                              purchase, receiptmodule, runnumber, receiptnumber, creditors, human_resource, inventory, indicate, category_price, witholding, company, discount, creditormodule,
                                                              vat, compute, projectgl, chart, bank, provinces, human, acc_sell, acc_buy, product_list, product_type, product_type_list, product_tax, product_group, maxnumber,
                                                              session: req.session
                                                            });
                                                          });
                                                        });
                                                      });
                                                    });
                                                  });
                                                });
                                              });
                                            });
                                          });
                                        });
                                      });
                                    });
                                  });
                                });
                              });
                            });
                          });
                        });
                      });
                    });
                  });
                });
              });
            });
          });
        });
      });
    });
  });
};
///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_purchase_costpayment
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.purchase_to_expenses = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('select purchase.* ,projectgl.name as projectgl,compute.name as compute,product_list.name as name,category_price.name as category,purchase_list.detail as detail,purchase_list.quantity as quantity,purchase_list.price as price ,discount.name as discount,listdiscount,vat_id,totallist,product_code,product_id,categoryprice_id,filename,DATE_FORMAT(purchase.date,"%Y-%m-%d") as dateday ,DATE_FORMAT(purchase.due,"%Y-%m-%d") as dueday from purchase  join purchase_list on purchase_list.purchase_id = purchase.id left join purchase_file on purchase_file.purchase_id = purchase.id left join projectgl on purchase.project_id = projectgl.id join compute on purchase.compute_id  = compute.id  join product_list on purchase_list.product_id = product_list.id join category_price  on purchase_list.categoryprice_id = category_price.id left join discount on purchase.discount_id = discount.id where  purchase_list.purchase_id = ?;', [url.purchase], (err, purchase) => {
      //runnumber costpayment
      conn.query('select * from module where id = 20', (err, expensesmodule) => {
        conn.query('select date_format(date(now()),"%Y%m") as number', (err, runnumber) => {
          conn.query('select count(expensesnumber) as countnumber,max(expensesnumber) as maxnumber from expenses where date_format(date,"%Y%m") = date_format(date(now()),"%Y%m");', (err, expensesnumber) => {
            //
            conn.query('select creditors.*,  provinces.name_th as provinces, amphures.name_th as amphures, districts.name_th as districts, zip_code from creditors join provinces on creditors.provinces_id = provinces.id join amphures on creditors.amphures_id = amphures.id join districts on creditors.districts_id = districts.id', (err, creditors) => {
              conn.query('select * from human_resource', (err, human_resource) => {
                conn.query('select * from company', (err, company) => {
                  conn.query('select * from category_price', (err, category_price) => {
                    conn.query('select * from expenses_head', (err, expenses_head) => {
                      conn.query('select * from indicate', (err, indicate) => {
                        conn.query('select * from witholding', (err, witholding) => {
                          conn.query('select * from discount', (err, discount) => {
                            conn.query('select * from vat', (err, vat) => {
                              conn.query('select * from compute', (err, compute) => {
                                conn.query('select * from projectgl', (err, projectgl) => {
                                  //creditors
                                  conn.query('select * from chart where typeAcc_id = 5;', (err, chart) => {
                                    conn.query('select * from bank', (err, bank) => {
                                      conn.query('select * from provinces', (err, provinces) => {
                                        conn.query('select * from human', (err, human) => {
                                          //product
                                          conn.query('select * from product_list', (err, product_list) => {
                                            conn.query('select * from product_type', (err, product_type) => {
                                              conn.query('select * from product_type_list', (err, product_type_list) => {
                                                conn.query('select * from product_tax', (err, product_tax) => {
                                                  conn.query('select * from product_group', (err, product_group) => {
                                                    //runnumber creditors
                                                    conn.query('select * from module where id = 4', (err, creditormodule) => {
                                                      conn.query('select max(creditorID) as maxnumber from creditors where creditorID like ?', [creditormodule[0].char + "%"], (err, maxnumber) => {
                                                        res.render('buy/purchase_order/purchase_to_expenses.ejs', {
                                                          runnumber, expensesnumber, creditors, indicate, witholding, discount, vat, compute, projectgl, bank, provinces, product_list, purchase, human_resource, category_price, expensesmodule, creditormodule,
                                                          product_type, product_type_list, product_tax, product_group, human, module, maxnumber, chart, company, expenses_head, session: req.session
                                                        });
                                                      });
                                                    });
                                                  });
                                                });
                                              });
                                            });
                                          });
                                        });
                                      });
                                    });
                                  });
                                });
                              });
                            });
                          });
                        });
                      });
                    });
                  });
                });
              });
            });
          });
        });
      });
    });
  });
};
///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_ajax
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.getoptionunit = (req, res) => {
  var { pid } = req.params;
  req.getConnection((err, conn) => {
    conn.query('select * from category_price where product_list_id = ?', [pid], (err, category_price) => {
      console.log(category_price);
      res.send(category_price);
    });
  });
};

module.exports = controller;
