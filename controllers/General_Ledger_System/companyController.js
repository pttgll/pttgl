
const controller = {};

controller.list = (req, res) => {
  req.getConnection((err, conn) => {
    conn.query('SELECT c.*, t.name as tname,tem.name as temname,g.name as gname ,DATE_FORMAT(DATE_ADD(periodAcc, INTERVAL 543 YEAR), " %d/%m/%Y") as periodAccw, pr.name_th as provinces, am.name_th as amphures, di.name_th as districts, di.zip_code as post  from company as c join typebusiness as t on c.typeBusiness_id = t.id join generalbook as g on c.GeneralBook_id = g.id join template as tem on c.template_id = tem.id left join provinces as pr on c.provinces_id = pr.id left join amphures as am on c.amphures_id = am.id left join districts as di on c.districts_id = di.id order by c.id;', (err, company) => {
      if (err) {
        res.json(err);
      } else {
        if (company.length > 0) {
          res.render('General_Ledger_System/company/companylist', {
            data: company,
            session:req.session
          });
        }else {
          res.redirect('/');
        }
      }
    });
  })
}

//--------------------------------------------------------------------------------------------------

controller.add = (req, res) => {
  var data4;
  req.getConnection((err, conn) => {
    conn.query('select * from typebusiness;', (err, typebusiness) => {
      conn.query('select * from generalbook;', (err, generalbook) => {
        conn.query('select * from template;', (err, template) => {
          conn.query('select * from company where com_id is null;', (err, company1) => {
            conn.query('select * from company',(err,company2)=>{
              if (company2.length > 0) {
                data4 = company2;
              }else {
                data4 = company1;
              }
              conn.query('select * from provinces order by name_th ;', (err, provinces) => {
                res.render('General_Ledger_System/company/companyadd', {
                  provinces: provinces,
                  data1: typebusiness,
                  data2: generalbook,
                  data3: template,
                  data4,
                  session: req.session
                });
              });
            });
          });
        });
      });
    });
  });
};
//---------------------------------------------------------------------------------------------------
controller.save = (req, res) => {
  const data = req.body;
  if (!data.edit) {
    data.edit = 0;
  }
  req.getConnection((err, conn) => {
    conn.query(' SELECT * from company where com_id is null;', (err, company) => {
      if (company.length > 0) { //มีบริษัทใหญ่
        conn.query('INSERT INTO  company set ?,GeneralBook_id = ?,template_id = ?;', [data, company[0].GeneralBook_id, company[0].template_id], (err, x) => {
          console.log(err);
          res.redirect('/company');
        });
      } else {
        conn.query('INSERT INTO  company set ?', [data], (err, x) => {
          console.log(err);
          res.redirect('/selectchart');
        });
      }
    });
  });
};
//--------------------------------------------------------------------------------------------------------
controller.edit = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('select * from typebusiness ;', (err, typebusiness) => {
      conn.query('select * from generalbook;', (err, generalbook) => {
        conn.query('select * from template;', (err, template) => {
          conn.query('SELECT * from company where id = ?;', [url.comid], (err, company) => {
            conn.query('SELECT * from provinces order by name_th;', (err, provinces) => {
              conn.query('SELECT * from amphures where provinces_id = ?; ', [company[0].provinces_id], (err, amphures) => {
                conn.query('select * from districts where amphures_id = ?;', [company[0].amphures_id],(err, districts) => {
                  res.render('General_Ledger_System/company/companyupdate', {
                    provinces: provinces,
                    amphures: amphures,
                    districts:districts,
                    data1: typebusiness,
                    data2: generalbook,
                    data3: template,
                    data4: company,
                    id: url,
                    session:req.session
                  });
                });
              });
            });
          });
        });
      });
    });
  });
};
//----------------------------------------------------------------------------------------------------------------
controller.update = (req, res) => {
  var url = req.params;
  var data = req.body;
  var allow = req.body.allow;
  if (data.com_id == '') {
    data.com_id = null;
  };
  if (data.allow) {
    var data = {com_id:data.com_id,name:data.name,branchNo:data.branchNo,address:data.address,provinces_id:data.provinces_id,amphures_id:data.amphures_id,districts_id:data.districts_id,phone:data.phone,tradeNumber:data.tradeNumber,taxID:data.taxID,periodAcc:data.periodAcc,typeBusiness_id:data.typeBusiness_id,GeneralBook_id:data.GeneralBook_id,template_id:data.template_id};
  }
  console.log(data);
  req.getConnection((err, conn) => {
    conn.query('UPDATE company set ? WHERE id = ?', [data, url.comid], (err, company) => {
      if (err) {
        res.json(err);
      }
    });
    if (allow == 1) {
      conn.query('UPDATE company set tradeNumber = ?,taxID = ?,periodAcc = ?,typeBusiness_id = ?,GeneralBook_id = ?,template_id = ? WHERE com_id = ?', [data.tradeNumber,data.taxID,data.periodAcc,data.typeBusiness_id,data.GeneralBook_id,data.template_id, url.comid], (err, company) => {
        if (err) {
          res.json(err);
        }
      });
    }
    res.redirect('/company');
  });
};
//-----------------------------------------------------------------------------------------------------------
controller.delete = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query("Delete from company where id = ? ", [url.comid], (err, compamy) => {
      if (err) {
        req.session.check=false;
        req.session.warning="ไม่สามารถลบได้";
        res.redirect('/company');
      }else{
        res.redirect('/company');
      }

    });
  });
};
module.exports = controller;
