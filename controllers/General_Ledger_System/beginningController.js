const controller = {};

controller.list = (req, res) => {
  req.getConnection((err, conn) => {
    conn.query('select ch.code as code, ch.name as chart,tb.name as tname ,be.*,DATE_FORMAT(DATE_ADD(closedate, INTERVAL 543 YEAR), " %d/%m/%Y") as bclosedate from beginning as be join chart as ch on be.chart_id = ch.id join beginning_type as tb on be.type_id = tb.id', (err, beginning) => {
      conn.query('select * from beginning_type', (err, beginning_type) => {
        res.render('General_Ledger_System/beginning/beginninglist', {
          beginning: beginning, typebegin: beginning_type,
          session: req.session
        })
      });
    });
  });
};

//----------------------------------------------------------------------------------
controller.add = (req, res) => {
  req.getConnection((err, conn) => {
    conn.query('select * from beginning_type', (err, beginning_type) => {
      conn.query('select * from chart ', (err, chart) => {
        res.render('General_Ledger_System/beginning/beginningadd', {
          beginning_type: beginning_type, chart: chart,
          session: req.session
        });
      });
    });
  });
};
//------------------------------------------------------------------------------------
controller.save = (req, res) => {
  var data = req.body;
  req.getConnection((err, conn) => {
    conn.query('select * from beginning where closedate = ?',[data.closedate],(err,beginning) =>{
      if (beginning.length == 0) {
        for (var i = 0; i < data.chart_id.length; i++) {
          conn.query('INSERT INTO beginning (dr, cr, closedate, chart_id, type_id) values (?, ?, ?, ?, ?)', [data.dr[i], data.cr[i], data.closedate, data.chart_id[i], data.type_id], (err, beginning) => {
            if (err) {
              res.json(err);
            }
          });
        }
      }
    });
    res.redirect('/beginning');
  });
};
//-------------------------------------------------------------------------------------
controller.edit = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('select * from beginning where closedate like ?', [url.year+'%'], (err, beginning) => {
      conn.query('select * from beginning_type', (err, beginning_type) => {
        conn.query('select * from chart', (err, chart) => {
          res.render('General_Ledger_System/beginning/beginningupdate', {
            beginning:beginning, session:req.session, chart:chart, beginning_type:beginning_type, year:url.year
          });
        });
      });
    });
  });
};
//---------------------------------------------------------------------------------------------------
controller.update = (req, res) => {
  var url = req.params;
  const data = req.body;
  req.getConnection((err, conn) => {
    for (var i = 0; i < data.chart_id.length; i++) {
      conn.query('UPDATE beginning set dr = ?, cr = ?, closedate = ?, chart_id = ?, type_id = ? where chart_id = ?', [data.dr[i],data.cr[i],data.closedate,data.chart_id[i],data.type_id, data.chart_id[i]], (err, beginning) => {
        if (err) {
          res.json(err);
        }
      });
    }
    res.redirect('/beginning')
  })
};
//-------------------------------------------------------------------------------------------
controller.delete = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query("Delete from beginning where closedate like ? ", [url.year+'%'], (err, beginning) => {
      if (err) {
        req.session.check = false;
        req.session.warning = "ไม่สามารถลบได้";
        res.redirect('/beginning');
      } else {
        res.redirect('/beginning');
      };
    });
  });
};

module.exports = controller;
