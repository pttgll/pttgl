const controller = {};

controller.selectchart = (req, res) => {
  req.getConnection((err, conn) => {
    conn.query('select nameacc.id as nameaccid,nameacc.name as name,nameacc.code as code,typeacc.name as typename from nameacc join typeacc on nameacc.typeacc_id = typeacc.id   where nameacc.category_id =1;', (err, x1) => {
      conn.query('select category.name as categoryname from nameacc  join category on nameacc.category_id = category.id where category.id = 1;', (err, cate1) => {
        conn.query('select nameacc.id as nameaccid,nameacc.name as name,nameacc.code as code,typeacc.name as typename from nameacc join typeacc on nameacc.typeacc_id = typeacc.id where nameacc.category_id =2;', (err, x2) => {
          conn.query('select category.name as categoryname from nameacc  join category on nameacc.category_id = category.id where category.id = 2;', (err, cate2) => {
            conn.query('select nameacc.id as nameaccid,nameacc.name as name,nameacc.code as code,typeacc.name as typename from nameacc join typeacc on nameacc.typeacc_id = typeacc.id where nameacc.category_id =3;', (err, x3) => {
              conn.query('select category.name as categoryname from nameacc  join category on nameacc.category_id = category.id where category.id = 3;', (err, cate3) => {
                conn.query('select nameacc.id as nameaccid,nameacc.name as name,nameacc.code as code,typeacc.name as typename from nameacc join typeacc on nameacc.typeacc_id = typeacc.id where nameacc.category_id =4;', (err, x4) => {
                  conn.query('select category.name as categoryname from nameacc  join category on nameacc.category_id = category.id where category.id = 4;', (err, cate4) => {
                    conn.query('select nameacc.id as nameaccid,nameacc.name as name,nameacc.code as code,typeacc.name as typename from nameacc join typeacc on nameacc.typeacc_id = typeacc.id where nameacc.category_id =5;', (err, x5) => {
                      conn.query('select category.name as categoryname from nameacc  join category on nameacc.category_id = category.id where category.id = 5;', (err, cate5) => {
                        conn.query('select * from company where com_id is null;', (err, company) => {
                          conn.query('select * from nameacc;', (err, nameacc) => {
                            conn.query('select * from charttemplate where template_id = ? ;', [company[0].template_id], (err, charttemplate) => {
                              var x = []
                              for (let i = 0; i < nameacc.length; i++) {
                                x.push(0);
                              }
                              for (let j = 0; j < charttemplate.length; j++) {
                                x[charttemplate[j].nameAcc_id] = 1;
                              }
                              var check_box_no = 1
                              res.render('General_Ledger_System/chart/selectchart.ejs', {
                                x: x,
                                check_box_no: check_box_no,
                                nameacc1: x1,
                                nameacc2: x2,
                                nameacc3: x3,
                                nameacc4: x4,
                                nameacc5: x5,
                                nameacc6: cate1,
                                nameacc7: cate2,
                                nameacc8: cate3,
                                nameacc9: cate4,
                                nameacc10: cate5,
                                session: req.session
                              });
                            });
                          });
                        });
                      });
                    });
                  });
                });
              });
            });
          });
        });
      });
    });
  });
};

controller.addchart = (req, res) => {
  var sum = req.body
  req.getConnection((err, conn) => {
    conn.query('select *from nameacc', (err, nameacc) => {
      conn.query('select * from company order by id desc limit 1;', (err, company) => {
        for (var i = 0; i < nameacc.length; i++) {
          for (var j = 0; j < sum.xxx.length; j++) {
            if (nameacc[i].id == sum.xxx[j]) {
              // console.log(i + 1);
              // console.log(nameacc[i].name);
              conn.query('insert into chart set company_id = ? ,category_id = ?,typeAcc_id = ?,name = ?,code = ?;', [company[0].id, nameacc[i].category_id, nameacc[i].typeacc_id, nameacc[i].name, nameacc[i].code], (err, xx) => {
              });
            }
          }
        }
      })
    })
    res.redirect('/company');
  });
};
//------------------------------------------------------------------------------------------------------------------------------------------
controller.chart = (req, res) => {
  req.getConnection((err, conn) => {
    conn.query('SELECT chart.* , typeacc.name as typename ,typeacc.id as typecode   FROM chart join typeacc on chart.typeAcc_id = typeacc.id order by chart.code asc ;', (err, chart) => {
      conn.query('SELECT * from typeacc order by typeacc.id asc;', (err, typeacc) => {
        res.render('General_Ledger_System/chart/chart.ejs', {
          chart: chart,
          typeacc: typeacc,
          session: req.session


        });
      });
    });
  });
};

controller.update = (req, res) => {
  const data = req.body;
  var category_id = data.code.substring(0);
  req.getConnection((err, conn) => {
    conn.query('select * from company where com_id is null ;', (err, comid) => {
      conn.query('UPDATE chart set company_id = ? ,category_id = ?,code = ? ,name = ?,typeAcc_id= ? where id = ?', [comid[0].id, category_id[0], data.code, data.name, data.typeAcc_id, data.chartid], (err, xx) => {
        if (err) {
          res.json(err);
        }
        res.redirect('/chart')
      })
    })
  })
};
//-----------------------------------------------------------------------------------------------------------------------------------------
controller.save = (req, res) => {
  const data = req.body;
  var category_id = data.code.substring(0);
  req.getConnection((err, conn) => {
    conn.query('select * from company where com_id is null ;', (err, comid) => {
      conn.query('INSERT INTO chart (company_id,category_id,typeAcc_id,name,code) VALUES (?,?,?,?,?);', [comid[0].id, category_id[0], data.typeAcc_id, data.name, data.code], (err, xx) => {
        if (err) {
          res.json(err);
        }
        res.redirect('/chart')
      })
    })
  })
};
//-----------------------------------------------------------------------------------------------------------------------------------------
controller.delete = (req, res) => {
  const data = req.body;
  req.getConnection((err, conn) => {
    conn.query('DELETE FROM chart WHERE (id = ?);', [data.chartid], (err, xx) => {
      if (err) {
        res.json(err);
      }
      res.redirect('/chart')
    })
  })
};
//-----------------------------------------------------------------------------------------------------------------------------------------
controller.edit_balance = (req, res) => {
  var url = req.params;
  req.getConnection((err,conn)=>{
    conn.query('select * from chart order by code',(err,chart)=>{
      res.render('General_Ledger_System/chart/beginning_balanceEdit.ejs',{
        chart,session:req.session
      });
    });
  });
}

controller.save_balance = (req, res) => {
  var data = req.body;
  req.getConnection((err,conn)=>{
    for (var i = 0; i < data.id.length; i++) {
      conn.query('update chart set dr = ?, cr = ?, date_start = ? where id = ?',[data.dr[i],data.cr[i],data.date_start,data.id[i]],(err,chart)=>{
        if (err) {
          res.json(err);
        }
      });
    }
    res.redirect('/chart');
  });
}

module.exports = controller;
