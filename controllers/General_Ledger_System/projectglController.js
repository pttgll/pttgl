const controller = {};

controller.list = (req, res) => {
    req.getConnection((err, conn) => {
        conn.query('select * from projectgl', (err, projectgl) => {
            res.render('General_Ledger_System/projectgl/projectgllist', {
                projectgl: projectgl,
                session: req.session
            })
        });
    });
};
//----------------------------------------------------------------------------------------
controller.add = (req, res) => {
    req.getConnection((err, conn) => {
        conn.query('select * from projectgl;', (err, projectgl) => {
            if (err) {
                res.json(err);
            } else {
                res.render('General_Ledger_System/projectgl/projectgladd', {
                    session: req.session, data: projectgl
                });
            };
        });
    });
};
//---------------------------------------------------------------------------------------
controller.save = (req, res) => {
    const data = req.body;
    req.getConnection((err, conn) => {
        conn.query('INSERT INTO projectgl set ?', [data], (err, projectgl) => {
            res.redirect('/projectgl');
        });
    });
};
//----------------------------------------------------------------------------------------
controller.edit = (req, res) => {
    var url = req.params;
    req.getConnection((err,conn) => {
        conn.query('select * from projectgl where id = ?',[url.proid],(err,projectgl) => {
            res.render('General_Ledger_System/projectgl/projectglupdate',{
                 projectgl : projectgl, session:req.session
            });
        });
    });
};
//-------------------------------------------------------------------------------------------
controller.update = (req,res) => {
    var url = req.params;
  const data = req.body;
  req.getConnection((err,conn) => {
      conn.query('UPDATE projectgl set ? where id = ?',[data,url.proid] ,(err,projectgl) => {
          res.redirect('/projectgl')
      })
  })
};
//-------------------------------------------------------------------------------------------
controller.delete = (req, res) => {
    var url = req.params;
    req.getConnection((err, conn) => {
      conn.query("Delete from projectgl where id = ? ", [url.proid], (err, projectgl) => {
        if (err) {
          req.session.check=false;
          req.session.warning="ไม่สามารถลบได้";
          res.redirect('/projectgl');
        }else{
          res.redirect('/projectgl');
        };
      });
    });
  };
module.exports = controller;
