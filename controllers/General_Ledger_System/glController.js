const controller = {};

controller.list = (req, res) => {
  var url = req.params;
  var data = req.query;
  var d = new Date();
  var date = d.getDate();
  if (date < 10) {
    date = "0"+date;
  }
  var m = d.getMonth()+1;
  if (m < 10) {
    m = "0"+m;
  }
  var y = d.getFullYear()
  var f_date1 = y+"-"+m+"-01";
  var l_date1 = d.toISOString().split('T')[0];
  var branch = {com_id:0,branchNo:"ทั้งหมด"};
  if (data.f_date) {
    f_date1 = data.f_date;
  }
  if (data.l_date) {
    l_date1 = data.l_date;
  }
  if (data.com_id) {
    branch.com_id = data.com_id;
  }
  req.getConnection((err, conn) => {
    conn.query('select * from company',(err,company)=>{
      if (data.com_id && data.com_id != 0) {
        conn.query("select gh.*,sum(gd.cr) as sum,book.name as book ,CONCAT(date_format(gh.date, '%d-%m-'),(date_format(gh.date, '%Y')+543)) as dates, branchNo from glheads as gh join gldetail as gd on gd.glheads_id = gh.id join book on gh.book_id = book.id join company as com on gh.company_id = com.id where gh.company_id = ? and gh.date >= ? and gh.date <= ? group by gd.glheads_id order by gh.date, gh.doc_no asc;",[data.com_id,f_date1,l_date1],(err, glheads) => {
          conn.query('select * from company where id = ?',[data.com_id],(err,com)=>{
            branch.com_id = com[0].id;
            branch.branchNo = com[0].branchNo;
            res.render('General_Ledger_System/gl/gllist', {
              f_date1,
              l_date1,
              branch,
              company,
              glheads: glheads,
              session: req.session
            });
          });
        });
      } else {
        conn.query("select gh.*,sum(gd.cr) as sum,book.name as book ,CONCAT(date_format(gh.date, '%d-%m-'),(date_format(gh.date, '%Y')+543)) as dates, branchNo from glheads as gh join gldetail as gd on gd.glheads_id = gh.id join book on gh.book_id = book.id join company as com on gh.company_id = com.id where gh.date >= ? and gh.date <= ? group by gd.glheads_id order by gh.date asc, gh.doc_no asc;",[f_date1,l_date1],(err, glheads) => {
          res.render('General_Ledger_System/gl/gllist', {
            f_date1,
            l_date1,
            branch,
            company,
            glheads: glheads,
            session: req.session
          });
        });
      }
    });
  });
}

//---------------------------------------------------------------------------------------------------------------------

controller.detail = (req, res) => {
  var url = req.params
  req.getConnection((err, conn) => {
    conn.query('select * from glheads where id = ?',[url.glhid],(err,glheads)=>{
      req.session.companyid = glheads[0].company_id;
      conn.query('select gldetail.*, name, code from gldetail join chart on code_id = chart.id where glheads_id = ?',[glheads[0].id],(err,gldetail)=>{
        conn.query('select * from book',(err,book)=>{
          conn.query('select * from chart;', (err, chart) => {
            conn.query('select * from company;', (err, company) => {
              conn.query('select * from projectgl ',(err,projectgl) => {
                if (err) {
                  res.json(err);
                } else {
                  res.render('General_Ledger_System/gl/gldetail', {
                    book,glheads,chart,gldetail,company,projectgl,
                    session: req.session
                  });
                }
              });
            });
          });
        });
      })
    })
  })
}
//---------------------------------------------------------------------------------------------------------------------
controller.add = (req, res) => {
  req.getConnection((err, conn) => {
    conn.query('select * from book',(err,book)=>{
      conn.query('select * from chart;', (err, chart) => {
        conn.query('select * from company ;', (err, company) => {
          conn.query('select * from projectgl where hidden = 0 ',(err,projectgl) => {
            conn.query("select gh.*,sum(gd.cr) as sum,book.name as book ,CONCAT(date_format(gh.date, '%d-%m-'),(date_format(gh.date, '%Y')+543)) as dates, branchNo from glheads as gh join gldetail as gd on gd.glheads_id = gh.id join book on gh.book_id = book.id join company as com on gh.company_id = com.id group by gd.glheads_id order by gh.id desc limit 10;",(err, glheads) => {
              conn.query("select * from comment where part = 1",(err,comment)=>{
                res.render('General_Ledger_System/gl/gladd', {
                  glheads:glheads,
                  book:book,
                  chart: chart,
                  company: company,
                  projectgl : projectgl,
                  comment: comment,
                  session: req.session
                });
              });
            });
          });
        });
      });
    });
  });
};
//---------------------------------------------------------------------------------------------------
controller.save = (req, res) => {
  const data = req.body;
  req.session.companyid = data.company_id;
  var dataH = { book_id: data.book_id, date: data.date, doc_no: data.doc_no, detail: data.detail, company_id: data.company_id, module_id: 1 };
  var dataD = [];
  req.getConnection((err, conn) => {
    conn.query('select * from comment where comment = ?',[data.detail],(err,comment)=>{
      if (comment.length == 0) {
        conn.query('INSERT INTO comment (comment,part) values (?,?)', [data.detail,1], (err, comment) => {
          if (err) {
            res.json(err);
          }
        });
      }
    });
    conn.query('INSERT INTO  glheads set ?', [dataH], (err, glheads) => {
      if (err) {
        res.json(err);
      }
    });
    conn.query('select * from glheads order by id desc limit 1;', (err, idH) => {
      for (let j = 0; j < data.code_id.length; j++) {
        if (data.code_id[j] != '') {
          dataD.push({ code_id: data.code_id[j], cheack: data.cheack[j], date_list: data.date_list[j], dr: (data.dr[j]).replace(/,/g, ''), cr: (data.cr[j]).replace(/,/g, ''), glheads_id: idH[0].id });
        }
      }
      for (let i = 0; i < dataD.length; i++) {
        conn.query('INSERT INTO gldetail set ?', [dataD[i]], (err, gldetail) => {
          if (err) {
            res.json(err);
          }
        });
      }
      res.redirect('/gl/add');
    });
  });
};
//--------------------------------------------------------------------------------------------------------
controller.update = (req, res) => {
  var data = req.body;
  console.log(data);
  req.session.companyid = data.company_id;
  var url = req.params;
  var dataH = { book_id: data.book_id, date: data.date, doc_no: data.doc_no, detail: data.detail, company_id: data.company_id, module_id: 1 };
  var dataD = [];
  req.getConnection((err, conn) => {
    conn.query('update glheads set ? where id = ?', [dataH,data.glhid], (err, glheads) => {
      if (err) {
        res.json(err);
      }
    });
    for (let j = 0; j < data.code_id.length; j++) {
      if (data.code_id[j] != '') {
        dataD.push({ code_id: data.code_id[j], cheack: data.cheack[j], date_list: data.date_list[j], dr: data.dr[j].replace(/,/g, ''), cr: data.cr[j].replace(/,/g, ''), glheads_id: data.glhid });
      }
    }
    console.log(dataD);
    for (let i = 0; i < dataD.length; i++) {
      if (data.gldid[i] == undefined) {
        conn.query('insert into gldetail set ?', [dataD[i]], (err, gldetail) => {
          if (err) {
            res.json(err);
          }
        });
      }else {
        conn.query('update gldetail set ? where id = ?', [dataD[i],data.gldid[i]], (err, gldetail) => {
          if (err) {
            res.json(err);
          }
        });
      }
    }
    res.redirect('/gl');
  });
};

controller.delete = (req,res)=>{
var url = req.params;
  req.getConnection((err,conn)=>{
    conn.query('delete from gldetail where glheads_id = ?',[url.glhid],(err,gldetail)=>{
      conn.query('delete from glheads where id = ?',[url.glhid],(err,glheads)=>{
        if (err) {
          res.json(err);
        }
        res.redirect('/gl');
      })
    })
  })
}

module.exports = controller;
