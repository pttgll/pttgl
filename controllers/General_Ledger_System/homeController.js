const controller = {};

controller.List = (req, res) => {
  req.session.companyid = null;
  req.getConnection((err, conn) => {
    conn.query('select * from company where com_id is null', (err, company) => {
      if (company.length > 0) {
        conn.query('select * from module where module_id is null', (err, module) => {
          res.render('General_Ledger_System/home/homeList.ejs', {
            module, session: req.session
          });
        });
      } else {
        res.render('General_Ledger_System/company/companylist.ejs', {
          data: company, session: req.session
        });
      }
    });
  });
};

controller.homeInventoryList = (req, res) => {
  req.getConnection((err, conn) => {
    res.render('General_Ledger_System/home/homeInventoryList.ejs', {
      module, session: req.session
    });
  });
};

module.exports = controller;
