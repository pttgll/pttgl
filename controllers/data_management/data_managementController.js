const session = require("express-session");

const controller = {};

controller.list = (req, res) => {
  req.getConnection((err, conn) => {
    res.render('data_management/data_managementlist.ejs', {
      session: req.session
    });
  });
};

controller.add = (req, res) => {
  req.getConnection((err, conn) => {
    res.render('data_management/data_managementadd.ejs', {
      session: req.session
    });
  });
};
//----------------------------------------------------------------------------------------
controller.save = (req, res) => {
  const data = req.body;
  req.getConnection((err, conn) => {
    res.redirect('/data_management');
  });
};

module.exports = controller;
