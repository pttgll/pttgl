const session = require("express-session");
const controller = {};
const { v4: uuidv4 } = require('uuid');
const fs = require('fs');
const { Cookie } = require("express-session");


///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_list
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.bill_list = (req, res) => {
  req.getConnection((err, conn) => {
    conn.query('select bill.*, status.name as status ,DATE_FORMAT(DATE_ADD(bill.date, INTERVAL 543 YEAR), " %d/%m/%Y" )dateday ,DATE_FORMAT(DATE_ADD(bill.due, INTERVAL 543 YEAR), " %d/%m/%Y" )dueday from bill join status on status_id = status.id where YEAR(date) = YEAR(CURDATE())', (err, bill) => {
      conn.query('select * from status  ', (err, status) => {
        res.render('sell/bill/bill_list.ejs', {
          session: req.session, bill, status
        });
      });
    });
  });
};
///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_add
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.bill_add = (req, res) => {
  req.getConnection((err, conn) => {
    // runnumber bill
    conn.query('select * from module where id = 14', (err, billmodule) => {
      conn.query('select date_format(date(now()),"%Y%m") as number', (err, runnumber) => {
        conn.query('select count(billnumber) as countnumber,max(billnumber) as maxnumber from bill where date_format(date,"%Y%m") = date_format(date(now()),"%Y%m");', (err, billnumber) => {
          //
          conn.query('select debtors.*,  provinces.name_th as provinces, amphures.name_th as amphures, districts.name_th as districts, zip_code from debtors join provinces on debtors.provinces_id = provinces.id join amphures on debtors.amphures_id = amphures.id join districts on debtors.districts_id = districts.id', (err, debtors) => {
            conn.query('select * from human_resource', (err, human_resource) => {
              conn.query('select * from indicate', (err, indicate) => {
                conn.query('select * from category_price', (err, category_price) => {
                  conn.query('select * from witholding', (err, witholding) => {
                    conn.query('select * from discount', (err, discount) => {
                      conn.query('select * from vat', (err, vat) => {
                        conn.query('select * from compute', (err, compute) => {
                          conn.query('select * from projectgl', (err, projectgl) => {
                            //debtors
                            conn.query('select * from bank', (err, bank) => {
                              conn.query('select * from provinces', (err, provinces) => {
                                conn.query('select * from human', (err, human) => {
                                  //product
                                  conn.query('select * from product_list where enable = 1', (err, product_list) => {
                                    conn.query('select * from product_type', (err, product_type) => {
                                      conn.query('select * from product_type_list', (err, product_type_list) => {
                                        conn.query('select * from product_tax', (err, product_tax) => {
                                          conn.query('select * from product_group', (err, product_group) => {
                                            // runnumber debtors
                                            conn.query('select * from module where id = 3', (err, debtormodule) => {
                                              conn.query('select max(debtorID) as maxnumber from debtors where debtorID like ?', [debtormodule[0].char + "%"], (err, maxnumber) => {
                                                res.render('sell/bill/bill_add.ejs', {
                                                  session: req.session, bank, provinces, runnumber, billnumber, debtors, human_resource, indicate, category_price, witholding, discount, vat, compute, billmodule,
                                                  projectgl, product_list, product_type, product_type_list, product_tax, product_group, human, maxnumber
                                                })
                                              });
                                            });
                                          });
                                        });
                                      });
                                    });
                                  });
                                });
                              });
                            });
                          });
                        });
                      });
                    });
                  });
                });
              });
            });
          });
        });
      });
    });
  });
};
///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_save
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.bill_save = (req, res) => {
  const data = req.body;
  req.getConnection((err, conn) => {
    conn.query('INSERT INTO bill (billchar,billnumber,debtors_id,debtors_name,address,post,tax,branch,date,credit,indicate_id,due,employee_saler,employee_delivery,project_id,number,compute_id,signature,note,notein,total,afterdiscount,except,vatcalculator,vatamount,notvatamount,alldiscount,vat,witholding_id,witholding,granttotal,payment,status_id,itemizedvat,itemizeddiscount)  values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
      [data.billchar, data.billnumber, data.debtors_id, data.debtors_name, data.address, data.post, data.tax, data.branch, data.date, data.credit, data.indicate_id, data.due, data.employee_saler, data.employee_delivery, data.project_id, data.number, data.compute_id, data.signature, data.note, data.notein, data.total.replace(/,/g, ''), data.afterdiscount.replace(/,/g, ''), data.except.replace(/,/g, ''), data.vatcalculator.replace(/,/g, ''), data.vatamount.replace(/,/g, ''), data.notvatamount.replace(/,/g, ''), data.alldiscount.replace(/,/g, ''), data.vat, data.witholding_id, data.witholding.replace(/,/g, ''), data.granttotal.replace(/,/g, ''), data.payment.replace(/,/g, ''), 7, data.itemizedvat, data.itemizeddiscount], (err, bill) => {
        if (err) {
          res.json(err);
        }
        if (Array.isArray(data.product_id)) {
          for (var i = 0; i < data.product_id.length; i++) {
            conn.query('INSERT INTO bill_list (product_id,product_code,detail,quantity,categoryprice_id,price,listdiscount,vat_id,totallist,bill_id)  values(?,?,?,?,?,?,?,?,?,?)', [data.product_id[i], data.product_code[i], data.detail[i], data.quantity[i], data.categoryprice_id[i], data.price[i], data.listdiscount[i], data.vat_id[i], data.totallist[i], bill.insertId], (err, bill_list) => {
            });
          }
        } else {
          conn.query('INSERT INTO bill_list (product_id,product_code,detail,quantity,categoryprice_id,price,listdiscount,vat_id,totallist,bill_id)  values(?,?,?,?,?,?,?,?,?,?)', [data.product_id, data.product_code, data.detail, data.quantity, data.categoryprice_id, data.price, data.listdiscount, data.vat_id, data.totallist, bill.insertId], (err, bill_list) => {
          });
        }
        if (req.files) {
          var file = req.files.filename;
          if (!Array.isArray(file)) {
            var type = file.name.split(".");
            var filename = uuidv4() + "." + type[type.length - 1];
            file.mv("./public/bill/" + filename, function (err) {
              if (err) { console.log(err) }
              data.filename = filename;
              conn.query('INSERT INTO bill_file (filename,bill_id) values(?,?)', [data.filename, bill.insertId], (err, bill_file) => {
                if (err) {
                  res.json(err);
                }
              });
            })
          } else {
            for (var i = 0; i < file.length; i++) {
              var type = file[i].name.split(".");
              var filename = uuidv4() + "." + type[type.length - 1];
              file[i].mv("./public/bill/" + filename, function (err) {
                if (err) { console.log(err) }
              })
              data.filename = filename;
              conn.query('INSERT INTO bill_file (filename,bill_id) values(?,?)', [data.filename, bill.insertId], (err, bill_file) => {
                if (err) {
                  res.json(err);
                }
              });
            }
          }
        }
        res.redirect('/bill');
      });
  });
};
///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_edit
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.bill_edit = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('select bill.* ,projectgl.name as projectgl,compute.name as compute,product_list.name as name,category_price.name as category,bill_list.detail as detail,bill_list.quantity as quantity,bill_list.price as price ,discount.name as discount,listdiscount,vat_id,totallist,product_code,product_id,categoryprice_id,filename from bill  join bill_list on bill_list.bill_id = bill.id left join bill_file on bill_file.bill_id = bill.id left join projectgl on bill.project_id = projectgl.id join compute on bill.compute_id  = compute.id  join product_list on bill_list.product_id = product_list.id join category_price  on bill_list.categoryprice_id = category_price.id left join discount on bill_list.discount_id = discount.id where  bill_list.bill_id = ?;', [url.bill], (err, bill) => {
      // runnumber bill
      conn.query('select * from module where id = 14', (err, billmodule) => {
        conn.query('select date_format(date(now()),"%Y%m") as number', (err, runnumber) => {
          conn.query('select count(billnumber) as countnumber,max(billnumber) as maxnumber from bill where date_format(date,"%Y%m") = date_format(date(now()),"%Y%m");', (err, billnumber) => {
            //
            conn.query('select debtors.*,  provinces.name_th as provinces, amphures.name_th as amphures, districts.name_th as districts, zip_code from debtors join provinces on debtors.provinces_id = provinces.id join amphures on debtors.amphures_id = amphures.id join districts on debtors.districts_id = districts.id', (err, debtors) => {
              conn.query('select * from human_resource', (err, human_resource) => {
                conn.query('select * from indicate', (err, indicate) => {
                  conn.query('select * from witholding', (err, witholding) => {
                    conn.query('select * from discount', (err, discount) => {
                      conn.query('select * from category_price', (err, category_price) => {
                        conn.query('select * from vat', (err, vat) => {
                          conn.query('select * from compute', (err, compute) => {
                            conn.query('select * from projectgl', (err, projectgl) => {
                              // debtors
                              conn.query('select * from bank', (err, bank) => {
                                conn.query('select * from provinces', (err, provinces) => {
                                  conn.query('select * from human', (err, human) => {
                                    //product
                                    conn.query('select * from product_list where enable = 1', (err, product_list) => {
                                      conn.query('select * from product_type', (err, product_type) => {
                                        conn.query('select * from product_type_list', (err, product_type_list) => {
                                          conn.query('select * from product_tax', (err, product_tax) => {
                                            conn.query('select * from product_group', (err, product_group) => {
                                              //runnumber debtors
                                              conn.query('select * from module where id = 3', (err, debtormodule) => {
                                                conn.query('select max(debtorID) as maxnumber from debtors where debtorID like ?', [debtormodule[0].char + "%"], (err, maxnumber) => {
                                                  res.render('sell/bill/bill_edit.ejs', {
                                                    session: req.session, bill, debtors, human_resource, indicate, witholding, discount, category_price, vat, compute, billmodule, billnumber, runnumber,
                                                    projectgl, bank, provinces, product_list, product_type, product_type_list, product_tax, product_group, human, maxnumber
                                                  });
                                                });
                                              });
                                            });
                                          });
                                        });
                                      });
                                    });
                                  });
                                });
                              });
                            });
                          });
                        });
                      });
                    });
                  });
                });
              });
            });
          });
        });
      });
    });
  });
};
///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_update
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.bill_update = (req, res) => {
  const data = req.body;
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('UPDATE  bill  set debtors_id = ?,debtors_name = ?,address = ?,post = ?,tax = ?,branch = ?,date = ?,credit = ?,indicate_id = ?,due = ?,employee_saler = ?,employee_delivery = ?,project_id = ?,number = ?,compute_id = ?,signature = ?,note = ?,notein = ?,total = ?,afterdiscount = ?,except = ?,vatcalculator = ?,vatamount = ?,notvatamount = ?,alldiscount = ?,vat = ?,witholding_id = ?,witholding = ?,granttotal = ?,payment = ?,itemizedvat = ?,itemizeddiscount = ? where id = ?',
      [data.debtors_id, data.debtors_name, data.address, data.post, data.tax, data.branch, data.date, data.credit, data.indicate_id, data.due, data.employee_saler, data.employee_delivery, data.project_id, data.number, data.compute_id, data.signature, data.note, data.notein, data.total.replace(/,/g, ''), data.afterdiscount.replace(/,/g, ''), data.except.replace(/,/g, ''), data.vatcalculator.replace(/,/g, ''), data.vatamount.replace(/,/g, ''), data.notvatamount.replace(/,/g, ''), data.alldiscount.replace(/,/g, ''), data.vat, data.witholding_id, data.witholding.replace(/,/g, ''), data.granttotal.replace(/,/g, ''), data.payment.replace(/,/g, ''), data.itemizedvat, data.itemizeddiscount, url.bill], (err, bill) => {
        if (err) {
          res.json(err);
        }
        conn.query('delete from bill_list where bill_id = ?', [url.bill], (err, bill_product) => {
          if (Array.isArray(data.product_id)) {
            for (var i = 0; i < data.product_id.length; i++) {
              conn.query('INSERT INTO bill_list (product_id,product_code,detail,quantity,categoryprice_id,price,listdiscount,vat_id,totallist,bill_id)  values(?,?,?,?,?,?,?,?,?,?)', [data.product_id[i], data.product_code[i], data.detail[i], data.quantity[i], data.categoryprice_id[i], data.price[i], data.listdiscount[i], data.vat_id[i], data.totallist[i], url.bill], (err, bill_list) => {
              });
            }
          } else {
            conn.query('INSERT INTO bill_list (product_id,product_code,detail,quantity,categoryprice_id,price,listdiscount,vat_id,totallist,bill_id)  values(?,?,?,?,?,?,?,?,?,?)', [data.product_id, data.product_code, data.detail, data.quantity, data.categoryprice_id, data.price, data.listdiscount, data.vat_id, data.totallist, url.bill], (err, bill_list) => {
            });
          }
        });
        if (req.files) {
          var file = req.files.filename;
          if (!Array.isArray(file)) {
            var type = file.name.split(".");
            var filename = uuidv4() + "." + type[type.length - 1];
            file.mv("./public/bill/" + filename, function (err) {
              if (err) { console.log(err) }
              data.filename = filename;
              conn.query('UPDATE bill_file set filename = ?,bill_id = ?', [data.filename, url.bill], (err, bill_file) => {
                if (err) {
                  res.json(err);
                }
              });
            })
          } else {
            for (var i = 0; i < file.length; i++) {
              var type = file[i].name.split(".");
              var filename = uuidv4() + "." + type[type.length - 1];
              file[i].mv("./public/bill/" + filename, function (err) {
                if (err) { console.log(err) }
              })
              data.filename = filename;
              conn.query('UPDATE set bill_file set filename = ?,bill_id = ?', [data.filename, url.bill], (err, bill_file) => {
                if (err) {
                  res.json(err);
                }
              });
            }
          }
        }
        res.redirect('/bill');
      });
  });
};
///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_delete
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.bill_delete = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('Delete from bill_file where bill_id = ?', [url.bill], (err, bill_file) => {
      conn.query('Delete from bill_list where bill_id = ?', [url.bill], (err, bill_list) => {
        conn.query('Delete from bill where id = ?', [url.bill], (err, bill) => {
          res.redirect('/bill');
        });
      })
    })
  });
};
///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_detail
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.bill_detail = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('select * from discount', (err, discount) => {
      conn.query(' select bill.*,  bill_list.product_id as product_id,bill_list.detail as detail,bill_list.quantity as quantity,bill_list.categoryprice_id as categoryprice_id,bill_list.price as price,bill_list.listdiscount as listdiscount,bill_list.vat_id as vat_id,bill_list.totallist as totallist,bill_list.product_code as product_code,filename,category_price.name as category,projectgl.name as projectgl,indicate.name as indicate,compute.name as compute,discount.name as discount,product_list.name as name  from bill  join bill_list on bill_list.bill_id = bill.id left join bill_file on bill_file.bill_id = bill.id left join projectgl on bill.project_id = projectgl.id join compute on bill.compute_id  = compute.id  join product_list on bill_list.product_id = product_list.id join category_price  on bill_list.categoryprice_id = category_price.id left join discount on bill_list.discount_id = discount.id join indicate on bill.indicate_id = indicate.id  where  bill_list.bill_id = ?;', [url.bill], (err, bill) => {
        //////////////////////////////////////////////////////////
        var da = bill[0].date.toISOString().split('T')[0]
        var dat = da.split('-')
        var date = parseInt(dat[0]) + 543;
        var date_now = dat[2] + "/" + dat[1] + "/" + date;
        //////////////////////////////////////////////////////////
        var du = bill[0].due.toISOString().split('T')[0]
        var due = du.split('-')
        var due_d = parseInt(due[0]) + 543;
        var due_now = due[2] + "/" + due[1] + "/" + due_d;
        //////////////////////////////////////////////////////////
        conn.query('select * from witholding', (err, witholding) => {
          res.render('sell/bill/bill_detail.ejs', {
            bill, date_now, due_now, witholding, discount,
            session: req.session
          });
        });
      });
    });
  });
};
///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_readd
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.bill_readd = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('select bill.* ,projectgl.name as projectgl,compute.name as compute,product_list.name as name,category_price.name as category,bill_list.detail as detail,bill_list.quantity as quantity,bill_list.price as price ,discount.name as discount,listdiscount,vat_id,totallist,product_code,product_id,categoryprice_id,filename from bill  join bill_list on bill_list.bill_id = bill.id left join bill_file on bill_file.bill_id = bill.id left join projectgl on bill.project_id = projectgl.id join compute on bill.compute_id  = compute.id  join product_list on bill_list.product_id = product_list.id join category_price  on bill_list.categoryprice_id = category_price.id left join discount on bill_list.discount_id = discount.id where  bill_list.bill_id = ?;', [url.bill], (err, bill) => {
      // runnumber bill
      conn.query('select * from module where id = 14', (err, billmodule) => {
        conn.query('select date_format(date(now()),"%Y%m") as number', (err, runnumber) => {
          conn.query('select count(billnumber) as countnumber,max(billnumber) as maxnumber from bill where date_format(date,"%Y%m") = date_format(date(now()),"%Y%m");', (err, billnumber) => {
            //
            conn.query('select debtors.*,  provinces.name_th as provinces, amphures.name_th as amphures, districts.name_th as districts, zip_code from debtors join provinces on debtors.provinces_id = provinces.id join amphures on debtors.amphures_id = amphures.id join districts on debtors.districts_id = districts.id', (err, debtors) => {
              conn.query('select * from human_resource', (err, human_resource) => {
                conn.query('select * from indicate', (err, indicate) => {
                  conn.query('select * from witholding', (err, witholding) => {
                    conn.query('select * from discount', (err, discount) => {
                      conn.query('select * from category_price', (err, category_price) => {
                        conn.query('select * from vat', (err, vat) => {
                          conn.query('select * from compute', (err, compute) => {
                            conn.query('select * from projectgl', (err, projectgl) => {
                              // debtors
                              conn.query('select * from bank', (err, bank) => {
                                conn.query('select * from provinces', (err, provinces) => {
                                  conn.query('select * from human', (err, human) => {
                                    //product
                                    conn.query('select * from product_list where enable = 1', (err, product_list) => {
                                      conn.query('select * from product_type', (err, product_type) => {
                                        conn.query('select * from product_type_list', (err, product_type_list) => {
                                          conn.query('select * from product_tax', (err, product_tax) => {
                                            conn.query('select * from product_group', (err, product_group) => {
                                              //runnumber debtors
                                              conn.query('select * from module where id = 3', (err, debtormodule) => {
                                                conn.query('select max(debtorID) as maxnumber from debtors where debtorID like ?', [debtormodule[0].char + "%"], (err, maxnumber) => {
                                                  res.render('sell/bill/bill_readd.ejs', {
                                                    session: req.session, bill, runnumber, billnumber, debtors, human_resource, indicate, witholding, discount, category_price, vat, compute,
                                                    projectgl, bank, provinces, product_list, product_type, product_type_list, product_tax, product_group, human, maxnumber, billmodule
                                                  });
                                                });
                                              });
                                            });
                                          });
                                        });
                                      });
                                    });
                                  });
                                });
                              });
                            });
                          });
                        });
                      });
                    });
                  });
                });
              });
            });
          });
        });
      });
    });
  });
};
///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_bill to taxinvoice
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.bill_taxinvoice = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('select bill.* ,projectgl.name as projectgl,compute.name as compute,product_list.name as name,category_price.name as category,bill_list.detail as detail,bill_list.quantity as quantity,bill_list.price as price ,discount.name as discount,listdiscount,vat_id,totallist,product_code,product_id,categoryprice_id,filename from bill  join bill_list on bill_list.bill_id = bill.id left join bill_file on bill_file.bill_id = bill.id left join projectgl on bill.project_id = projectgl.id join compute on bill.compute_id  = compute.id  join product_list on bill_list.product_id = product_list.id join category_price  on bill_list.categoryprice_id = category_price.id left join discount on bill_list.discount_id = discount.id where  bill_list.bill_id = ?;', [url.bill], (err, bill) => {
      // runnumber bill
      conn.query('select * from module where id = 15', (err, taxinvoicemodule) => {
        conn.query('select date_format(date(now()),"%Y%m") as number', (err, runnumber) => {
          conn.query('select count(taxinvoicenumber) as countnumber,max(taxinvoicenumber) as maxnumber from taxinvoice where date_format(date,"%Y%m") = date_format(date(now()),"%Y%m");', (err, taxinvoicenumber) => {
            //
            conn.query('select debtors.*,  provinces.name_th as provinces, amphures.name_th as amphures, districts.name_th as districts, zip_code from debtors join provinces on debtors.provinces_id = provinces.id join amphures on debtors.amphures_id = amphures.id join districts on debtors.districts_id = districts.id', (err, debtors) => {
              conn.query('select * from human_resource', (err, human_resource) => {
                conn.query('select * from indicate', (err, indicate) => {
                  conn.query('select * from witholding', (err, witholding) => {
                    conn.query('select * from discount', (err, discount) => {
                      conn.query('select * from category_price', (err, category_price) => {
                        conn.query('select * from vat', (err, vat) => {
                          conn.query('select * from compute', (err, compute) => {
                            conn.query('select * from projectgl', (err, projectgl) => {
                              //debtors
                              conn.query('select * from bank', (err, bank) => {
                                conn.query('select * from provinces', (err, provinces) => {
                                  conn.query('select * from human', (err, human) => {
                                    //product
                                    conn.query('select * from product_list where enable = 1', (err, product_list) => {
                                      conn.query('select * from product_type', (err, product_type) => {
                                        conn.query('select * from product_type_list', (err, product_type_list) => {
                                          conn.query('select * from product_tax', (err, product_tax) => {
                                            conn.query('select * from product_group', (err, product_group) => {
                                              // runnumber debtors
                                              conn.query('select * from module where id = 3', (err, debtormodule) => {
                                                conn.query('select max(debtorID) as maxnumber from debtors where debtorID like ?', [debtormodule[0].char + "%"], (err, maxnumber) => {
                                                  res.render('sell/bill/bill_taxinvoice.ejs', {
                                                    session: req.session, bill, runnumber, taxinvoicenumber, debtors, human_resource, indicate, witholding, discount, category_price, vat, compute,
                                                    projectgl, bank, provinces, product_list, product_type, product_type_list, product_tax, product_group, human, maxnumber, taxinvoicemodule
                                                  });
                                                });
                                              });
                                            });
                                          });
                                        });
                                      });
                                    });
                                  });
                                });
                              });
                            });
                          });
                        });
                      });
                    });
                  });
                });
              });
            });
          });
        });
      });
    });
  });
};
module.exports = controller;
