const session = require("express-session");
const controller = {};
const { v4: uuidv4 } = require('uuid');
const fs = require('fs');
const { Cookie } = require("express-session");



controller.creditnote_list = (req, res) => {
  req.getConnection((err, conn) => {
    conn.query('select credit_note.*, status.name as status ,DATE_FORMAT(DATE_ADD(credit_note.date, INTERVAL 543 YEAR), " %d/%m/%Y" )dateday ,DATE_FORMAT(DATE_ADD(credit_note.due, INTERVAL 543 YEAR), " %d/%m/%Y" )dueday from credit_note join status on status_id = status.id where YEAR(date) = YEAR(CURDATE())', (err, credit_note) => {
      conn.query('select * from status where id = 1 or id = 2  ', (err, status) => {
        res.render('sell/credit_note/credit_note_list.ejs', {
          session: req.session,status,credit_note
        });
      });
    });
  });
};

controller.creditnote_add = (req, res) => {
  req.getConnection((err, conn) => {
    conn.query('select date_format(date(now()),"%Y%m") as number', (err, runnumber) => {
      conn.query('select count(credit_notenumber) as countnumber,max(credit_notenumber) as maxnumber from credit_note where date_format(date,"%Y%m") = date_format(date(now()),"%Y%m");', (err, credit_notenumber) => {
        conn.query('select debtors.*,  provinces.name_th as provinces, amphures.name_th as amphures, districts.name_th as districts, zip_code from debtors join provinces on debtors.provinces_id = provinces.id join amphures on debtors.amphures_id = amphures.id join districts on debtors.districts_id = districts.id', (err, debtors) => {
          conn.query('select * from human_resource', (err, human_resource) => {
            conn.query('select * from indicate', (err, indicate) => {
              conn.query('select * from category_price', (err, category_price) => {
                conn.query('select * from witholding', (err, witholding) => {
                  conn.query('select * from discount', (err, discount) => {
                    conn.query('select * from vat', (err, vat) => {
                      conn.query('select * from compute', (err, compute) => {
                        conn.query('select * from projectgl', (err, projectgl) => {
                          conn.query('select * from bank', (err, bank) => {
                            conn.query('select * from provinces', (err, provinces) => {
                              conn.query('select * from product_list where enable = 1', (err, product_list) => {
                                conn.query('select * from product_type', (err, product_type) => {
                                  conn.query('select * from product_type_list', (err, product_type_list) => {
                                    conn.query('select * from product_tax', (err, product_tax) => {
                                      conn.query('select * from product_group', (err, product_group) => {
                                        conn.query('select * from human', (err, human) => {
                                          conn.query('select * from module where id = 3', (err, module) => {
                                            conn.query('select max(debtorID) as maxnumber from debtors where debtorID like ?', [module[0].char + "%"], (err, maxnumber) => {
                                              res.render('sell/credit_note/credit_note_add.ejs', {
                                                session: req.session, bank, provinces, runnumber, credit_notenumber, debtors, human_resource, indicate, category_price, witholding, discount, vat, compute,
                                                projectgl, product_list, product_type, product_type_list, product_tax, product_group, human, maxnumber
                                              })
                                            });
                                          });
                                        });
                                      });
                                    });
                                  });
                                });
                              });
                            });
                          });
                        });
                      });
                    });
                  });
                });
              });
            });
          });
        });
      });
    });
  });
};


controller.creditnote_save = (req, res) => {
  const data = req.body;
  console.log(data);
  req.getConnection((err, conn) => {
    conn.query('INSERT INTO credit_note (credit_notenumber,debtors_id,debtors_name,address,post,tax,branch,date,credit,indicate_id,due,employee_saler,employee_delivery,project_id,number,compute_id,signature,note,notein,total,afterdiscount,except,vatcalculator,vatamount,notvatamount,alldiscount,vat,witholding_id,witholding,granttotal,payment,status_id,itemizedvat,itemizeddiscount)  values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
      [data.credit_notenumber, data.debtors_id, data.debtors_name, data.address, data.post, data.tax, data.branch, data.date, data.credit, data.indicate_id, data.due, data.employee_saler, data.employee_delivery, data.project_id, data.number, data.compute_id, data.signature, data.note, data.notein, data.total.replace(/,/g, ''), data.afterdiscount.replace(/,/g, ''), data.except.replace(/,/g, ''), data.vatcalculator.replace(/,/g, ''), data.vatamount.replace(/,/g, ''), data.notvatamount.replace(/,/g, ''), data.alldiscount.replace(/,/g, ''), data.vat, data.witholding_id, data.witholding.replace(/,/g, ''), data.granttotal.replace(/,/g, ''), data.payment.replace(/,/g, ''), 2, data.itemizedvat, data.itemizeddiscount], (err, credit_note) => {
        console.log(err);
        if (err) {
          res.json(err);
        }
        if (Array.isArray(data.product_id)) {
          for (var i = 0; i < data.product_id.length; i++) {
            conn.query('INSERT INTO credit_note_list (product_id,product_code,detail,quantity,categoryprice_id,price,listdiscount,vat_id,totallist,credit_note_id)  values(?,?,?,?,?,?,?,?,?,?)', [data.product_id[i], data.product_code[i], data.detail[i], data.quantity[i], data.categoryprice_id[i], data.price[i].replace(/,/g, ''), data.listdiscount[i].replace(/,/g, ''), data.vat_id[i], data.totallist.replace(/,/g, ''), credit_note.insertId], (err, credit_note_list) => {
              console.log(err);
            });
          }
        } else {
          conn.query('INSERT INTO credit_note_list (product_id,product_code,detail,quantity,categoryprice_id,price,listdiscount,vat_id,totallist,credit_note_id)  values(?,?,?,?,?,?,?,?,?,?)', [data.product_id, data.product_code, data.detail, data.quantity, data.categoryprice_id, data.price.replace(/,/g, ''), data.listdiscount.replace(/,/g, ''), data.vat_id, data.totallist.replace(/,/g, ''), credit_note.insertId], (err, credit_note_list) => {
            console.log(err);
          });
        }
        if (req.files) {
          var file = req.files.filename;
          if (!Array.isArray(file)) {
            var type = file.name.split(".");
            var filename = uuidv4() + "." + type[type.length - 1];
            file.mv("./public/credit_note/" + filename, function (err) {
              if (err) { console.log(err) }
              data.filename = filename;
              conn.query('INSERT INTO credit_note_file (filename,credit_note_id) values(?,?)', [data.filename, credit_note.insertId], (err, credit_note_file) => {
                if (err) {
                  res.json(err);
                }
              });
            })
          } else {
            for (var i = 0; i < file.length; i++) {
              var type = file[i].name.split(".");
              var filename = uuidv4() + "." + type[type.length - 1];
              file[i].mv("./public/credit_note/" + filename, function (err) {
                if (err) { console.log(err) }
              })
              data.filename = filename;
              conn.query('INSERT INTO credit_note_file (filename,credit_note_id) values(?,?)', [data.filename, credit_note.insertId], (err, credit_note_file) => {
                if (err) {
                  res.json(err);
                }
              });
            }
          }
        }
        res.redirect('/credit_note');
      });
  });
};

controller.creditnote_edit = (req, res) => {
  var url = req.params;
  console.log(url);
  req.getConnection((err, conn) => {
    conn.query('select credit_note.* ,projectgl.name as projectgl,compute.name as compute,product_list.name as name,category_price.name as category,credit_note_list.detail as detail,credit_note_list.quantity as quantity,credit_note_list.price as price ,discount.name as discount,listdiscount,vat_id,totallist,product_code,product_id,categoryprice_id,filename from credit_note  join credit_note_list on credit_note_list.credit_note_id = credit_note.id left join credit_note_file on credit_note_file.credit_note_id = credit_note.id left join projectgl on credit_note.project_id = projectgl.id join compute on credit_note.compute_id  = compute.id  join credit_note_list on credit_note_list.product_id = product_list.id join category_price  on credit_note_list.categoryprice_id = category_price.id left join discount on credit_note_list.discount_id = discount.id where  credit_note_list.credit_note_id = ?;', [url.credit_note], (err, credit_note) => {
      conn.query('select date_format(date(now()),"%Y%m") as number', (err, runnumber) => {
        conn.query('select count(credit_notenumber) as countnumber,max(credit_notenumber) as maxnumber from credit_note where date_format(date,"%Y%m") = date_format(date(now()),"%Y%m");', (err, credit_notenimber) => {
          conn.query('select debtors.*,  provinces.name_th as provinces, amphures.name_th as amphures, districts.name_th as districts, zip_code from debtors join provinces on debtors.provinces_id = provinces.id join amphures on debtors.amphures_id = amphures.id join districts on debtors.districts_id = districts.id', (err, debtors) => {
            conn.query('select * from human_resource', (err, human_resource) => {
              conn.query('select * from indicate', (err, indicate) => {
                conn.query('select * from witholding', (err, witholding) => {
                  conn.query('select * from discount', (err, discount) => {
                    conn.query('select * from category_price', (err, category_price) => {
                      conn.query('select * from vat', (err, vat) => {
                        conn.query('select * from compute', (err, compute) => {
                          conn.query('select * from projectgl', (err, projectgl) => {
                            conn.query('select * from bank', (err, bank) => {
                              conn.query('select * from provinces', (err, provinces) => {
                                conn.query('select * from product_list where enable = 1', (err, product_list) => {
                                  conn.query('select * from product_type', (err, product_type) => {
                                    conn.query('select * from product_type_list', (err, product_type_list) => {
                                      conn.query('select * from product_tax', (err, product_tax) => {
                                        conn.query('select * from product_group', (err, product_group) => {
                                          conn.query('select * from human', (err, human) => {
                                            conn.query('select * from module where id = 4', (err, module) => {
                                              conn.query('select max(debtorID) as maxnumber from debtors where debtorID like ?', [module[0].char + "%"], (err, maxnumber) => {
                                                res.render('sell/credit_note/credit_note_edit.ejs', {
                                                  session: req.session, credit_note, runnumber, credit_notenimber, debtors, human_resource, indicate, witholding, discount, category_price, vat, compute,
                                                  projectgl, bank, provinces, product_list, product_type, product_type_list, product_tax, product_group, human, maxnumber
                                                });
                                              });
                                            });
                                          });
                                        });
                                      });
                                    });
                                  });
                                });
                              });
                            });
                          });
                        });
                      });
                    });
                  });
                });
              });
            });
          });
        });
      });
    });
  });
};


controller.creditnote_update = (req, res) => {
  const data = req.body;
  var url = req.params;
  console.log(url);
  console.log(data);
  req.getConnection((err, conn) => {
    conn.query('UPDATE  credit_note  set debtors_id = ?,debtors_name = ?,address = ?,post = ?,tax = ?,branch = ?,date = ?,credit = ?,indicate_id = ?,due = ?,employee_saler = ?,employee_delivery = ?,project_id = ?,number = ?,compute_id = ?,signature = ?,note = ?,notein = ?,total = ?,afterdiscount = ?,except = ?,vatcalculator = ?,vatamount = ?,notvatamount = ?,alldiscount = ?,vat = ?,witholding_id = ?,witholding = ?,granttotal = ?,payment = ?,status_id = ?,itemizedvat = ?,itemizeddiscount = ? where id = ?',
      [data.debtors_id, data.debtors_name, data.address, data.post, data.tax, data.branch, data.date, data.credit, data.indicate_id, data.due, data.employee_saler, data.employee_delivery, data.project_id, data.number, data.compute_id, data.signature, data.note, data.notein, data.total.replace(/,/g, ''), data.afterdiscount.replace(/,/g, ''), data.except.replace(/,/g, ''), data.vatcalculator.replace(/,/g, ''), data.vatamount.replace(/,/g, ''), data.notvatamount.replace(/,/g, ''), data.alldiscount.replace(/,/g, ''), data.vat, data.witholding_id, data.witholding.replace(/,/g, ''), data.granttotal.replace(/,/g, ''), data.payment.replace(/,/g, ''), 2, data.itemizedvat, data.itemizeddiscount, url.credit_note], (err, credit_note) => {
        console.log(err);
        if (err) {
          res.json(err);
        }
        conn.query('delete from credit_note_list where credit_note_id = ?', [url.credit_note], (err, credit_note_product) => {
          if (Array.isArray(data.product_id)) {
            for (var i = 0; i < data.product_id.length; i++) {
              conn.query('INSERT INTO credit_note_list (product_id,product_code,detail,quantity,categoryprice_id,price,listdiscount,vat_id,totallist,credit_note_id)  values(?,?,?,?,?,?,?,?,?,?)', [data.product_id[i], data.product_code[i], data.detail[i], data.quantity[i], data.categoryprice_id[i], data.price[i].replace(/,/g, ''), data.listdiscount[i].replace(/,/g, ''), data.vat_id[i], data.totallist.replace(/,/g, ''), url.credit_note], (err, credit_note_list) => {
                console.log(err);
              });
            }
          } else {
            conn.query('INSERT INTO credit_note_list (product_id,product_code,detail,quantity,categoryprice_id,price,listdiscount,vat_id,totallist,credit_note_id)  values(?,?,?,?,?,?,?,?,?,?)', [data.product_id, data.product_code, data.detail, data.quantity, data.categoryprice_id, data.price.replace(/,/g, ''), data.listdiscount.replace(/,/g, ''), data.vat_id, data.totallist.replace(/,/g, ''), url.credit_note], (err, credit_note_list) => {
              console.log(err);
            });
          }
        });
        if (req.files) {
          var file = req.files.filename;
          if (!Array.isArray(file)) {
            var type = file.name.split(".");
            var filename = uuidv4() + "." + type[type.length - 1];
            file.mv("./public/credit_note/" + filename, function (err) {
              if (err) { console.log(err) }
              data.filename = filename;
              conn.query('UPDATE credit_note_file set filename = ?,credit_note_id = ?', [data.filename, url.credit_note], (err, credit_note_file) => {
                if (err) {
                  res.json(err);
                }
              });
            })
          } else {
            for (var i = 0; i < file.length; i++) {
              var type = file[i].name.split(".");
              var filename = uuidv4() + "." + type[type.length - 1];
              file[i].mv("./public/credit_note/" + filename, function (err) {
                if (err) { console.log(err) }
              })
              data.filename = filename;
              conn.query('UPDATE set credit_note_file set filename = ?,credit_note_id = ?', [data.filename, url.credit_note], (err, credit_note_file) => {
                if (err) {
                  res.json(err);
                }
              });
            }
          }
        }
        res.redirect('/credit_note');
      });
  });
};


controller.creditnote_delete = (req, res) => {
  var url = req.params;
  console.log(url);
  req.getConnection((err, conn) => {
    conn.query('Delete from credit_note_file where credit_note_id = ?', [url.credit_note], (err, credit_note_file) => {
      console.log(err);
      conn.query('Delete from credit_note_list where credit_note_id = ?', [url.credit_note], (err, credit_note_list) => {
        console.log(err);
        conn.query('Delete from credit_note where id = ?', [url.credit_note], (err, credit_note) => {
          console.log(err);
          res.redirect('/credit_note');
        });
      })
    })
  });
};

controller.creditnote_detail = (req, res) => {
  var url = req.params;
  console.log(url);
  req.getConnection((err, conn) => {
    conn.query(' select credit_note.*,  credit_note_list.product_id as product_id,credit_note_list.detail as detail,credit_note_list.quantity as quantity,credit_note_list.categoryprice_id as categoryprice_id,credit_note_list.price as price,credit_note_list.listdiscount as listdiscount,credit_note_list.vat_id as vat_id,credit_note_list.totallist as totallist,credit_note_list.product_code as product_code,filename,category_price.name as category,projectgl.name as projectgl,indicate.name as indicate,compute.name as compute,discount.name as discount,product_list.name as name  from credit_note  join credit_note_list on credit_note_list.credit_note_id = credit_note.id left join credit_note_file on credit_note_file.credit_note_id = credit_note.id left join projectgl on credit_note.project_id = projectgl.id join compute on credit_note.compute_id  = compute.id  join product_list on credit_note_list.product_id = product_list.id join category_price  on credit_note_list.categoryprice_id = category_price.id left join discount on credit_note_list.discount_id = discount.id join indicate on credit_note.indicate_id = indicate.id  where  credit_note_list.credit_note_id = ?;', [url.credit_note], (err, credit_note) => {
      console.log(credit_note);
      //////////////////////////////////////////////////////////
      var da = credit_note[0].date.toISOString().split('T')[0]
      var dat = da.split('-')
      var date = parseInt(dat[0]) + 543;
      var date_now = dat[2] + "/" + dat[1] + "/" + date;
      //////////////////////////////////////////////////////////
      var du = credit_note[0].due.toISOString().split('T')[0]
      var due = du.split('-')
      var due_d = parseInt(due[0]) + 543;
      var due_now = due[2] + "/" + due[1] + "/" + due_d;
      //////////////////////////////////////////////////////////
      conn.query('select * from witholding', (err, witholding) => {
        res.render('sell/credit_note/credit_note_detail.ejs', {
          credit_note, date_now, due_now, witholding,
          session: req.session
        });
      });
    });
  });
};
module.exports = controller;
