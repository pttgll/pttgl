const session = require("express-session");
const controller = {};
const { v4: uuidv4 } = require('uuid');
const fs = require('fs');
const { Cookie } = require("express-session");


///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_list
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.quotation_list = (req, res) => {
  req.getConnection((err, conn) => {
    conn.query('select quotation.*, status.name as status ,DATE_FORMAT(DATE_ADD(quotation.date, INTERVAL 543 YEAR), " %d/%m/%Y" )dateday ,DATE_FORMAT(DATE_ADD(quotation.due, INTERVAL 543 YEAR), " %d/%m/%Y" )dueday from quotation join status on status_id = status.id where YEAR(date) = YEAR(CURDATE())', (err, quotation) => {
      conn.query('select * from status ', (err, status) => {
        res.render('sell/quotation/quotation_list.ejs', {
          session: req.session, status, quotation
        });
      });
    });
  });
};
///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_add
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.quotation_add = (req, res) => {
  req.getConnection((err, conn) => {
    //runnumber quotation
    conn.query('select * from module where id = 13', (err, quotationmodule) => {
      conn.query('select date_format(date(now()),"%Y%m") as number', (err, runnumber) => {
        conn.query('select count(quotationnumber) as countnumber,max(quotationnumber) as maxnumber from quotation where date_format(date,"%Y%m") = date_format(date(now()),"%Y%m");', (err, quotationnumber) => {
          //
          conn.query('select debtors.*,  provinces.name_th as provinces, amphures.name_th as amphures, districts.name_th as districts, zip_code from debtors join provinces on debtors.provinces_id = provinces.id join amphures on debtors.amphures_id = amphures.id join districts on debtors.districts_id = districts.id', (err, debtors) => {
            conn.query('select * from human_resource', (err, human_resource) => {
              conn.query('select * from indicate', (err, indicate) => {
                conn.query('select * from category_price', (err, category_price) => {
                  conn.query('select * from witholding', (err, witholding) => {
                    conn.query('select * from discount', (err, discount) => {
                      conn.query('select * from vat', (err, vat) => {
                        conn.query('select * from compute', (err, compute) => {
                          conn.query('select * from projectgl', (err, projectgl) => {
                            //debtors
                            conn.query('select * from bank', (err, bank) => {
                              conn.query('select * from provinces', (err, provinces) => {
                                conn.query('select * from human', (err, human) => {
                                  //product
                                  conn.query('select * from chart where typeAcc_id = 9;', (err, acc_sell) => {
                                    conn.query('select * from chart where typeAcc_id = 11;', (err, acc_buy) => {
                                      conn.query('select * from product_list where enable = 1', (err, product_list) => {
                                        conn.query('select * from product_type', (err, product_type) => {
                                          conn.query('select * from product_type_list', (err, product_type_list) => {
                                            conn.query('select * from product_tax', (err, product_tax) => {
                                              conn.query('select * from product_group', (err, product_group) => {
                                                //runnumber debtors
                                                conn.query('select * from module where id = 3', (err, debtormodule) => {
                                                  conn.query('select max(debtorID) as maxnumber from debtors where debtorID like ?', [debtormodule[0].char + "%"], (err, maxnumber) => {
                                                    res.render('sell/quotation/quotation_add.ejs', {
                                                      session: req.session, bank, provinces, runnumber, quotationnumber, debtors, human_resource, indicate, category_price, witholding, discount, vat, compute,
                                                      projectgl, product_list, product_type, product_type_list, product_tax, product_group, human, maxnumber, quotationmodule,acc_sell,acc_buy
                                                    });
                                                  })
                                                });
                                              });
                                            });
                                          });
                                        });
                                      });
                                    });
                                  });
                                });
                              });
                            });
                          });
                        });
                      });
                    });
                  });
                });
              });
            });
          });
        });
      });
    });
  });
};
///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_save
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.quotation_save = (req, res) => {
  const data = req.body;
  req.getConnection((err, conn) => {
    conn.query('INSERT INTO quotation (quotationchar,quotationnumber,debtors_id,debtors_name,address,post,tax,branch,date,credit,indicate_id,due,employee_saler,employee_delivery,project_id,number,compute_id,signature,note,notein,total,afterdiscount,except,vatcalculator,vatamount,notvatamount,alldiscount,vat,witholding_id,witholding,granttotal,payment,status_id,itemizedvat,itemizeddiscount)  values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
      [data.quotationchar, data.quotationnumber, data.debtors_id, data.debtors_name, data.address, data.post, data.tax, data.branch, data.date, data.credit, data.indicate_id, data.due, data.employee_saler, data.employee_delivery, data.project_id, data.number, data.compute_id, data.signature, data.note, data.notein, data.total.replace(/,/g, ''), data.afterdiscount.replace(/,/g, ''), data.except.replace(/,/g, ''), data.vatcalculator.replace(/,/g, ''), data.vatamount.replace(/,/g, ''), data.notvatamount.replace(/,/g, ''), data.alldiscount.replace(/,/g, ''), data.vat, data.witholding_id, data.witholding.replace(/,/g, ''), data.granttotal.replace(/,/g, ''), data.payment.replace(/,/g, ''), 2, data.itemizedvat, data.itemizeddiscount], (err, quotation) => {
        if (err) {
          res.json(err);
        }
        if (Array.isArray(data.product_id)) {
          for (var i = 0; i < data.product_id.length; i++) {
            conn.query('INSERT INTO quotation_list (product_id,product_code,detail,quantity,categoryprice_id,price,listdiscount,vat_id,totallist,quotation_id)  values(?,?,?,?,?,?,?,?,?,?)', [data.product_id[i], data.product_code[i], data.detail[i], data.quantity[i], data.categoryprice_id[i], data.price[i], data.listdiscount[i], data.vat_id[i], data.totallist[i], quotation.insertId], (err, quotation_list) => {
            });
          }
        } else {
          conn.query('INSERT INTO quotation_list (product_id,product_code,detail,quantity,categoryprice_id,price,listdiscount,vat_id,totallist,quotation_id)  values(?,?,?,?,?,?,?,?,?,?)', [data.product_id, data.product_code, data.detail, data.quantity, data.categoryprice_id, data.price, data.listdiscount, data.vat_id, data.totallist, quotation.insertId], (err, quotation_list) => {
          });
        }
        if (req.files) {
          var file = req.files.filename;
          if (!Array.isArray(file)) {
            var type = file.name.split(".");
            var filename = uuidv4() + "." + type[type.length - 1];
            file.mv("./public/quotation/" + filename, function (err) {
              if (err) { console.log(err) }
              data.filename = filename;
              conn.query('INSERT INTO quotation_file (filename,quotation_id) values(?,?)', [data.filename, quotation.insertId], (err, quotation_file) => {
                if (err) {
                  res.json(err);
                }
              });
            })
          } else {
            for (var i = 0; i < file.length; i++) {
              var type = file[i].name.split(".");
              var filename = uuidv4() + "." + type[type.length - 1];
              file[i].mv("./public/quotation/" + filename, function (err) {
                if (err) { console.log(err) }
              })
              data.filename = filename;
              conn.query('INSERT INTO quotation_file (filename,quotation_id) values(?,?)', [data.filename, quotation.insertId], (err, quotation_file) => {
                if (err) {
                  res.json(err);
                }
              });
            }
          }
        }
        res.redirect('/quotation');
      });
  });
};
///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_edit
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.quotation_edit = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('select quotation.* ,projectgl.name as projectgl,compute.name as compute,product_list.name as name,category_price.name as category,quotation_list.detail as detail,quotation_list.quantity as quantity,quotation_list.price as price ,discount.name as discount,listdiscount,vat_id,totallist,product_code,product_id,categoryprice_id,filename from quotation  join quotation_list on quotation_list.quotation_id = quotation.id left join quotation_file on quotation_file.quotation_id = quotation.id left join projectgl on quotation.project_id = projectgl.id join compute on quotation.compute_id  = compute.id  join product_list on quotation_list.product_id = product_list.id join category_price  on quotation_list.categoryprice_id = category_price.id left join discount on quotation_list.discount_id = discount.id where  quotation_list.quotation_id = ?;', [url.quotation], (err, quotation) => {
      //runnumber quotation
      conn.query('select * from module where id = 13', (err, quotationmodule) => {
        conn.query('select date_format(date(now()),"%Y%m") as number', (err, runnumber) => {
          conn.query('select count(quotationnumber) as countnumber,max(quotationnumber) as maxnumber from quotation where date_format(date,"%Y%m") = date_format(date(now()),"%Y%m");', (err, quotationnumber) => {
            //
            conn.query('select debtors.*,  provinces.name_th as provinces, amphures.name_th as amphures, districts.name_th as districts, zip_code from debtors join provinces on debtors.provinces_id = provinces.id join amphures on debtors.amphures_id = amphures.id join districts on debtors.districts_id = districts.id', (err, debtors) => {
              conn.query('select * from human_resource', (err, human_resource) => {
                conn.query('select * from indicate', (err, indicate) => {
                  conn.query('select * from witholding', (err, witholding) => {
                    conn.query('select * from discount', (err, discount) => {
                      conn.query('select * from category_price', (err, category_price) => {
                        conn.query('select * from vat', (err, vat) => {
                          conn.query('select * from compute', (err, compute) => {
                            conn.query('select * from projectgl', (err, projectgl) => {
                              //debtors
                              conn.query('select * from bank', (err, bank) => {
                                conn.query('select * from provinces', (err, provinces) => {
                                  conn.query('select * from human', (err, human) => {
                                    //product
                                    conn.query('select * from product_list where enable = 1', (err, product_list) => {
                                      conn.query('select * from product_type', (err, product_type) => {
                                        conn.query('select * from product_type_list', (err, product_type_list) => {
                                          conn.query('select * from product_tax', (err, product_tax) => {
                                            conn.query('select * from product_group', (err, product_group) => {
                                              //runnumber debtors
                                              conn.query('select * from module where id = 3', (err, debtormodule) => {
                                                conn.query('select max(debtorID) as maxnumber from debtors where debtorID like ?', [debtormodule[0].char + "%"], (err, maxnumber) => {
                                                  res.render('sell/quotation/quotation_edit.ejs', {
                                                    session: req.session, quotation, runnumber, quotationnumber, debtors, human_resource, indicate, witholding, discount, category_price, vat, compute,
                                                    projectgl, bank, provinces, product_list, product_type, product_type_list, product_tax, product_group, human, maxnumber, quotationmodule
                                                  });
                                                });
                                              });
                                            });
                                          });
                                        });
                                      });
                                    });
                                  });
                                });
                              });
                            });
                          });
                        });
                      });
                    });
                  });
                });
              });
            });
          });
        });
      });
    });
  });
};
///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_update
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.quotation_update = (req, res) => {
  const data = req.body;
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('UPDATE  quotation  set debtors_id = ?,debtors_name = ?,address = ?,post = ?,tax = ?,branch = ?,date = ?,credit = ?,indicate_id = ?,due = ?,employee_saler = ?,employee_delivery = ?,project_id = ?,number = ?,compute_id = ?,signature = ?,note = ?,notein = ?,total = ?,afterdiscount = ?,except = ?,vatcalculator = ?,vatamount = ?,notvatamount = ?,alldiscount = ?,vat = ?,witholding_id = ?,witholding = ?,granttotal = ?,payment = ?,itemizedvat = ?,itemizeddiscount = ? where id = ?',
      [data.debtors_id, data.debtors_name, data.address, data.post, data.tax, data.branch, data.date, data.credit, data.indicate_id, data.due, data.employee_saler, data.employee_delivery, data.project_id, data.number, data.compute_id, data.signature, data.note, data.notein, data.total.replace(/,/g, ''), data.afterdiscount.replace(/,/g, ''), data.except.replace(/,/g, ''), data.vatcalculator.replace(/,/g, ''), data.vatamount.replace(/,/g, ''), data.notvatamount.replace(/,/g, ''), data.alldiscount.replace(/,/g, ''), data.vat, data.witholding_id, data.witholding.replace(/,/g, ''), data.granttotal.replace(/,/g, ''), data.payment.replace(/,/g, ''), data.itemizedvat, data.itemizeddiscount, url.quotation], (err, quotation) => {
        if (err) {
          res.json(err);
        }
        conn.query('delete from quotation_list where quotation_id = ?', [url.quotation], (err, quotation_product) => {
          if (Array.isArray(data.product_id)) {
            for (var i = 0; i < data.product_id.length; i++) {
              conn.query('INSERT INTO quotation_list (product_id,product_code,detail,quantity,categoryprice_id,price,listdiscount,vat_id,totallist,quotation_id)  values(?,?,?,?,?,?,?,?,?,?)', [data.product_id[i], data.product_code[i], data.detail[i], data.quantity[i], data.categoryprice_id[i], data.price[i], data.listdiscount[i], data.vat_id[i], data.totallist[i], url.quotation], (err, quotation_list) => {
              });
            }
          } else {
            conn.query('INSERT INTO quotation_list (product_id,product_code,detail,quantity,categoryprice_id,price,listdiscount,vat_id,totallist,quotation_id)  values(?,?,?,?,?,?,?,?,?,?)', [data.product_id, data.product_code, data.detail, data.quantity, data.categoryprice_id, data.price, data.listdiscount, data.vat_id, data.totallist, url.quotation], (err, quotation_list) => {
            });
          }
        });
        if (req.files) {
          var file = req.files.filename;
          if (!Array.isArray(file)) {
            var type = file.name.split(".");
            var filename = uuidv4() + "." + type[type.length - 1];
            file.mv("./public/quotation/" + filename, function (err) {
              if (err) { console.log(err) }
              data.filename = filename;
              conn.query('UPDATE quotation_file set filename = ?,quotation_id = ?', [data.filename, url.quotation], (err, quotation_file) => {
                if (err) {
                  res.json(err);
                }
              });
            })
          } else {
            for (var i = 0; i < file.length; i++) {
              var type = file[i].name.split(".");
              var filename = uuidv4() + "." + type[type.length - 1];
              file[i].mv("./public/quotation/" + filename, function (err) {
                if (err) { console.log(err) }
              })
              data.filename = filename;
              conn.query('UPDATE set quotation_file set filename = ?,quotation_id = ?', [data.filename, url.quotation], (err, quotation_file) => {
                if (err) {
                  res.json(err);
                }
              });
            }
          }
        }
        res.redirect('/quotation');
      });
  });
};
///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_delete
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.quotation_delete = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('Delete from quotation_file where quotation_id = ?', [url.quotation], (err, quotation_file) => {
      conn.query('Delete from quotation_list where quotation_id = ?', [url.quotation], (err, quotation_list) => {
        conn.query('Delete from quotation where id = ?', [url.quotation], (err, quotation) => {
          res.redirect('/quotation');
        });
      })
    })
  });
};
///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_detail
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.quotation_detail = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('select * from discount', (err, discount) => {
      conn.query(' select quotation.*,  quotation_list.product_id as product_id,quotation_list.detail as detail,quotation_list.quantity as quantity,quotation_list.categoryprice_id as categoryprice_id,quotation_list.price as price,quotation_list.listdiscount as listdiscount,quotation_list.vat_id as vat_id,quotation_list.totallist as totallist,quotation_list.product_code as product_code,filename,category_price.name as category,projectgl.name as projectgl,indicate.name as indicate,compute.name as compute,discount.name as discount,product_list.name as name  from quotation  join quotation_list on quotation_list.quotation_id = quotation.id left join quotation_file on quotation_file.quotation_id = quotation.id left join projectgl on quotation.project_id = projectgl.id join compute on quotation.compute_id  = compute.id  join product_list on quotation_list.product_id = product_list.id join category_price  on quotation_list.categoryprice_id = category_price.id left join discount on quotation_list.discount_id = discount.id join indicate on quotation.indicate_id = indicate.id  where  quotation_list.quotation_id = ?;', [url.quotation], (err, quotation) => {
        //////////////////////////////////////////////////////////
        var da = quotation[0].date.toISOString().split('T')[0]
        var dat = da.split('-')
        var date = parseInt(dat[0]) + 543;
        var date_now = dat[2] + "/" + dat[1] + "/" + date;
        //////////////////////////////////////////////////////////
        var du = quotation[0].due.toISOString().split('T')[0]
        var due = du.split('-')
        var due_d = parseInt(due[0]) + 543;
        var due_now = due[2] + "/" + due[1] + "/" + due_d;
        //////////////////////////////////////////////////////////
        conn.query('select * from witholding', (err, witholding) => {
          res.render('sell/quotation/quotation_detail.ejs', {
            quotation, date_now, due_now, witholding, discount,
            session: req.session
          });
        });
      });
    });
  });
};
///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_readd
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.quotation_readd = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('select quotation.* ,projectgl.name as projectgl,compute.name as compute,product_list.name as name,category_price.name as category,quotation_list.detail as detail,quotation_list.quantity as quantity,quotation_list.price as price ,discount.name as discount,listdiscount,vat_id,totallist,product_code,product_id,categoryprice_id,filename from quotation  join quotation_list on quotation_list.quotation_id = quotation.id left join quotation_file on quotation_file.quotation_id = quotation.id left join projectgl on quotation.project_id = projectgl.id join compute on quotation.compute_id  = compute.id  join product_list on quotation_list.product_id = product_list.id join category_price  on quotation_list.categoryprice_id = category_price.id left join discount on quotation_list.discount_id = discount.id where  quotation_list.quotation_id = ?;', [url.quotation], (err, quotation) => {
      //runnumber quotation
      conn.query('select * from module where id = 13', (err, quotationmodule) => {
        conn.query('select date_format(date(now()),"%Y%m") as number', (err, runnumber) => {
          conn.query('select count(quotationnumber) as countnumber,max(quotationnumber) as maxnumber from quotation where date_format(date,"%Y%m") = date_format(date(now()),"%Y%m");', (err, quotationnumber) => {
            //
            conn.query('select debtors.*,  provinces.name_th as provinces, amphures.name_th as amphures, districts.name_th as districts, zip_code from debtors join provinces on debtors.provinces_id = provinces.id join amphures on debtors.amphures_id = amphures.id join districts on debtors.districts_id = districts.id', (err, debtors) => {
              conn.query('select * from human_resource', (err, human_resource) => {
                conn.query('select * from indicate', (err, indicate) => {
                  conn.query('select * from witholding', (err, witholding) => {
                    conn.query('select * from discount', (err, discount) => {
                      conn.query('select * from category_price', (err, category_price) => {
                        conn.query('select * from vat', (err, vat) => {
                          conn.query('select * from compute', (err, compute) => {
                            conn.query('select * from projectgl', (err, projectgl) => {
                              //debtors
                              conn.query('select * from bank', (err, bank) => {
                                conn.query('select * from human', (err, human) => {
                                  conn.query('select * from provinces', (err, provinces) => {
                                    //product
                                    conn.query('select * from product_list where enable = 1', (err, product_list) => {
                                      conn.query('select * from product_type', (err, product_type) => {
                                        conn.query('select * from product_type_list', (err, product_type_list) => {
                                          conn.query('select * from product_tax', (err, product_tax) => {
                                            conn.query('select * from product_group', (err, product_group) => {
                                              //runnumber debtors
                                              conn.query('select * from module where id = 3', (err, debtormodule) => {
                                                conn.query('select max(debtorID) as maxnumber from debtors where debtorID like ?', [debtormodule[0].char + "%"], (err, maxnumber) => {
                                                  res.render('sell/quotation/quotation_readd.ejs', {
                                                    session: req.session, quotation, runnumber, quotationnumber, debtors, human_resource, indicate, witholding, discount, category_price, vat, compute,
                                                    projectgl, bank, provinces, product_list, product_type, product_type_list, product_tax, product_group, human, maxnumber, quotationmodule
                                                  });
                                                });
                                              });
                                            });
                                          });
                                        });
                                      });
                                    });
                                  });
                                });
                              });
                            });
                          });
                        });
                      });
                    });
                  });
                });
              });
            });
          });
        });
      });
    });
  });
};
///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_quotation to purchase
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.quotation_purchase = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('select quotation.* ,projectgl.name as projectgl,compute.name as compute,product_list.name as name,category_price.name as category,quotation_list.detail as detail,quotation_list.quantity as quantity,quotation_list.price as price ,discount.name as discount,listdiscount,vat_id,totallist,product_code,product_id,categoryprice_id,filename from quotation  join quotation_list on quotation_list.quotation_id = quotation.id left join quotation_file on quotation_file.quotation_id = quotation.id left join projectgl on quotation.project_id = projectgl.id join compute on quotation.compute_id  = compute.id  join product_list on quotation_list.product_id = product_list.id join category_price  on quotation_list.categoryprice_id = category_price.id left join discount on quotation_list.discount_id = discount.id where  quotation_list.quotation_id = ?;', [url.quotation], (err, quotation) => {
      //runnumber purchase
      conn.query('select * from module where id = 11', (err, purchasemodule) => {
        conn.query('select date_format(date(now()),"%Y%m") as number', (err, runnumber) => {
          conn.query('select count(purchaseordernumber) as countnumber,max(purchaseordernumber) as maxnumber from purchase where date_format(date,"%Y%m") = date_format(date(now()),"%Y%m")', (err, purchasenumber) => {
            //
            conn.query('select creditors.*,  provinces.name_th as provinces, amphures.name_th as amphures, districts.name_th as districts, zip_code from creditors join provinces on creditors.provinces_id = provinces.id join amphures on creditors.amphures_id = amphures.id join districts on creditors.districts_id = districts.id', (err, creditors) => {
              conn.query('select * from human_resource', (err, human_resource) => {
                conn.query('select * from indicate', (err, indicate) => {
                  conn.query('select * from witholding', (err, witholding) => {
                    conn.query('select * from discount', (err, discount) => {
                      conn.query('select * from category_price', (err, category_price) => {
                        conn.query('select * from vat', (err, vat) => {
                          conn.query('select * from compute', (err, compute) => {
                            conn.query('select * from projectgl', (err, projectgl) => {
                              //creditors
                              conn.query('select * from bank', (err, bank) => {
                                conn.query('select * from provinces', (err, provinces) => {
                                  conn.query('select * from human', (err, human) => {
                                    //product
                                    conn.query('select * from product_list where enable = 1', (err, product_list) => {
                                      conn.query('select * from product_type', (err, product_type) => {
                                        conn.query('select * from product_type_list', (err, product_type_list) => {
                                          conn.query('select * from product_tax', (err, product_tax) => {
                                            conn.query('select * from product_group', (err, product_group) => {
                                              //runnumber creditors
                                              conn.query('select * from module where id = 4', (err, creditormodule) => {
                                                conn.query('select max(creditorID) as maxnumber from creditors where creditorID like ?', [creditormodule[0].char + "%"], (err, maxnumber) => {
                                                  res.render('sell/quotation/quotation_purchase.ejs', {
                                                    session: req.session, quotation, runnumber, purchasenumber, creditors, human_resource, indicate, witholding, discount, category_price, vat, compute,
                                                    projectgl, bank, provinces, product_list, product_type, product_type_list, product_tax, product_group, human, maxnumber, purchasemodule
                                                  });
                                                });
                                              });
                                            });
                                          });
                                        });
                                      });
                                    });
                                  });
                                });
                              });
                            });
                          });
                        });
                      });
                    });
                  });
                });
              });
            });
          });
        });
      });
    });
  });
};
///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_quotaion to bill
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.quotation_bill = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('select quotation.* ,projectgl.name as projectgl,compute.name as compute,product_list.name as name,category_price.name as category,quotation_list.detail as detail,quotation_list.quantity as quantity,quotation_list.price as price ,discount.name as discount,listdiscount,vat_id,totallist,product_code,product_id,categoryprice_id,filename from quotation  join quotation_list on quotation_list.quotation_id = quotation.id left join quotation_file on quotation_file.quotation_id = quotation.id left join projectgl on quotation.project_id = projectgl.id join compute on quotation.compute_id  = compute.id  join product_list on quotation_list.product_id = product_list.id join category_price  on quotation_list.categoryprice_id = category_price.id left join discount on quotation_list.discount_id = discount.id where  quotation_list.quotation_id = ?;', [url.quotation], (err, quotation) => {
      //runnumber bill
      conn.query('select * from module where id = 14', (err, billmodule) => {
        conn.query('select date_format(date(now()),"%Y%m") as number', (err, runnumber) => {
          conn.query('select count(billnumber) as countnumber,max(billnumber) as maxnumber from bill where date_format(date,"%Y%m") = date_format(date(now()),"%Y%m");', (err, billnumber) => {
            //
            conn.query('select debtors.*,  provinces.name_th as provinces, amphures.name_th as amphures, districts.name_th as districts, zip_code from debtors join provinces on debtors.provinces_id = provinces.id join amphures on debtors.amphures_id = amphures.id join districts on debtors.districts_id = districts.id', (err, debtors) => {
              conn.query('select * from human_resource', (err, human_resource) => {
                conn.query('select * from indicate', (err, indicate) => {
                  conn.query('select * from witholding', (err, witholding) => {
                    conn.query('select * from discount', (err, discount) => {
                      conn.query('select * from category_price', (err, category_price) => {
                        conn.query('select * from vat', (err, vat) => {
                          conn.query('select * from compute', (err, compute) => {
                            conn.query('select * from projectgl', (err, projectgl) => {
                              //debtors
                              conn.query('select * from bank', (err, bank) => {
                                conn.query('select * from provinces', (err, provinces) => {
                                  conn.query('select * from human', (err, human) => {
                                    //product
                                    conn.query('select * from product_list where enable = 1', (err, product_list) => {
                                      conn.query('select * from product_type', (err, product_type) => {
                                        conn.query('select * from product_type_list', (err, product_type_list) => {
                                          conn.query('select * from product_tax', (err, product_tax) => {
                                            conn.query('select * from product_group', (err, product_group) => {
                                              //runnumber debtors
                                              conn.query('select * from module where id = 3', (err, debtormodule) => {
                                                conn.query('select max(debtorID) as maxnumber from debtors where debtorID like ?', [debtormodule[0].char + "%"], (err, maxnumber) => {
                                                  res.render('sell/quotation/quotation_bill.ejs', {
                                                    session: req.session, quotation, runnumber, billnumber, debtors, human_resource, indicate, witholding, discount, category_price, vat, compute,
                                                    projectgl, bank, provinces, product_list, product_type, product_type_list, product_tax, product_group, human, maxnumber, billmodule
                                                  });
                                                });
                                              });
                                            });
                                          });
                                        });
                                      });
                                    });
                                  });
                                });
                              });
                            });
                          });
                        });
                      });
                    });
                  });
                });
              });
            });
          });
        });
      });
    });
  });
};
///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_quotaion to taxinvoice
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.quotation_taxinvoice = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('select quotation.* ,projectgl.name as projectgl,compute.name as compute,product_list.name as name,category_price.name as category,quotation_list.detail as detail,quotation_list.quantity as quantity,quotation_list.price as price ,discount.name as discount,listdiscount,vat_id,totallist,product_code,product_id,categoryprice_id,filename from quotation  join quotation_list on quotation_list.quotation_id = quotation.id left join quotation_file on quotation_file.quotation_id = quotation.id left join projectgl on quotation.project_id = projectgl.id join compute on quotation.compute_id  = compute.id  join product_list on quotation_list.product_id = product_list.id join category_price  on quotation_list.categoryprice_id = category_price.id left join discount on quotation_list.discount_id = discount.id where  quotation_list.quotation_id = ?;', [url.quotation], (err, quotation) => {
      //runnumber taxinvoice
      conn.query('select * from module where id = 15', (err, taxinvoicemodule) => {
        conn.query('select date_format(date(now()),"%Y%m") as number', (err, runnumber) => {
          conn.query('select count(taxinvoicenumber) as countnumber,max(taxinvoicenumber) as maxnumber from taxinvoice where date_format(date,"%Y%m") = date_format(date(now()),"%Y%m");', (err, taxinvoicenumber) => {
            //
            conn.query('select debtors.*,  provinces.name_th as provinces, amphures.name_th as amphures, districts.name_th as districts, zip_code from debtors join provinces on debtors.provinces_id = provinces.id join amphures on debtors.amphures_id = amphures.id join districts on debtors.districts_id = districts.id', (err, debtors) => {
              conn.query('select * from human_resource', (err, human_resource) => {
                conn.query('select * from indicate', (err, indicate) => {
                  conn.query('select * from witholding', (err, witholding) => {
                    conn.query('select * from discount', (err, discount) => {
                      conn.query('select * from category_price', (err, category_price) => {
                        conn.query('select * from vat', (err, vat) => {
                          conn.query('select * from compute', (err, compute) => {
                            conn.query('select * from projectgl', (err, projectgl) => {
                              //debtors
                              conn.query('select * from bank', (err, bank) => {
                                conn.query('select * from provinces', (err, provinces) => {
                                  conn.query('select * from human', (err, human) => {
                                    //prodcut
                                    conn.query('select * from product_list where enable = 1', (err, product_list) => {
                                      conn.query('select * from product_type', (err, product_type) => {
                                        conn.query('select * from product_type_list', (err, product_type_list) => {
                                          conn.query('select * from product_tax', (err, product_tax) => {
                                            conn.query('select * from product_group', (err, product_group) => {
                                              //runnumber taxinvoice
                                              conn.query('select * from module where id = 3', (err, debtormodule) => {
                                                conn.query('select max(debtorID) as maxnumber from debtors where debtorID like ?', [debtormodule[0].char + "%"], (err, maxnumber) => {
                                                  res.render('sell/quotation/quotation_taxinvoice.ejs', {
                                                    session: req.session, quotation, runnumber, taxinvoicenumber, debtors, human_resource, indicate, witholding, discount, category_price, vat, compute,
                                                    projectgl, bank, provinces, product_list, product_type, product_type_list, product_tax, product_group, human, maxnumber, taxinvoicemodule
                                                  });
                                                });
                                              });
                                            });
                                          });
                                        });
                                      });
                                    });
                                  });
                                });
                              });
                            });
                          });
                        });
                      });
                    });
                  });
                });
              });
            });
          });
        });
      });
    });
  });
};
///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_quotaion to cashsale
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.quotation_cashsale = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('select quotation.* ,projectgl.name as projectgl,compute.name as compute,product_list.name as name,category_price.name as category,quotation_list.detail as detail,quotation_list.quantity as quantity,quotation_list.price as price ,discount.name as discount,listdiscount,vat_id,totallist,product_code,product_id,categoryprice_id,filename from quotation  join quotation_list on quotation_list.quotation_id = quotation.id left join quotation_file on quotation_file.quotation_id = quotation.id left join projectgl on quotation.project_id = projectgl.id join compute on quotation.compute_id  = compute.id  join product_list on quotation_list.product_id = product_list.id join category_price  on quotation_list.categoryprice_id = category_price.id left join discount on quotation_list.discount_id = discount.id where  quotation_list.quotation_id = ?;', [url.quotation], (err, quotation) => {
      //runnumber cash_sale
      conn.query('select * from module where id = 17', (err, cashsalemodule) => {
        conn.query('select date_format(date(now()),"%Y%m") as number', (err, runnumber) => {
          conn.query('select count(cash_salenumber) as countnumber,max(cash_salenumber) as maxnumber from cash_sale where date_format(date,"%Y%m") = date_format(date(now()),"%Y%m");', (err, cash_salenumber) => {
            //
            conn.query('select debtors.*,  provinces.name_th as provinces, amphures.name_th as amphures, districts.name_th as districts, zip_code from debtors join provinces on debtors.provinces_id = provinces.id join amphures on debtors.amphures_id = amphures.id join districts on debtors.districts_id = districts.id', (err, debtors) => {
              conn.query('select * from human_resource', (err, human_resource) => {
                conn.query('select * from indicate', (err, indicate) => {
                  conn.query('select * from witholding', (err, witholding) => {
                    conn.query('select * from discount', (err, discount) => {
                      conn.query('select * from category_price', (err, category_price) => {
                        conn.query('select * from vat', (err, vat) => {
                          conn.query('select * from compute', (err, compute) => {
                            conn.query('select * from projectgl', (err, projectgl) => {
                              //debtors
                              conn.query('select * from bank', (err, bank) => {
                                conn.query('select * from provinces', (err, provinces) => {
                                  conn.query('select * from human', (err, human) => {
                                    //product
                                    conn.query('select * from chart where typeAcc_id = 9;', (err, acc_sell) => {
                                      conn.query('select * from chart where typeAcc_id = 11;', (err, acc_buy) => {
                                        conn.query('select * from product_list where enable = 1', (err, product_list) => {
                                          conn.query('select * from product_type', (err, product_type) => {
                                            conn.query('select * from product_type_list', (err, product_type_list) => {
                                              conn.query('select * from product_tax', (err, product_tax) => {
                                                conn.query('select * from product_group', (err, product_group) => {
                                                  //runnumber debtors
                                                  conn.query('select * from module where id = 3', (err, debtormodule) => {
                                                    conn.query('select max(debtorID) as maxnumber from debtors where debtorID like ?', [debtormodule[0].char + "%"], (err, maxnumber) => {
                                                      res.render('sell/quotation/quotation_cashsale.ejs', {
                                                        session: req.session, quotation, runnumber, cash_salenumber, debtors, human_resource, indicate, witholding, discount, category_price, vat, compute,
                                                        projectgl, bank, provinces, product_list, product_type, product_type_list, product_tax, product_group, human, maxnumber, cashsalemodule, acc_sell, acc_buy
                                                      });
                                                    });
                                                  });
                                                });
                                              });
                                            });
                                          });
                                        });
                                      });
                                    });
                                  });
                                });
                              });
                            });
                          });
                        });
                      });
                    });
                  });
                });
              });
            });
          });
        });
      });
    });
  });
};
module.exports = controller;
