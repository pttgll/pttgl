const session = require("express-session");
const controller = {};
const { v4: uuidv4 } = require('uuid');
const fs = require('fs');
const { Cookie } = require("express-session");


///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_list
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.cashsale_list = (req, res) => {
  req.getConnection((err, conn) => {
    conn.query('select cash_sale.*, status.name as status ,DATE_FORMAT(DATE_ADD(cash_sale.date, INTERVAL 543 YEAR), " %d/%m/%Y" )dateday ,DATE_FORMAT(DATE_ADD(cash_sale.due, INTERVAL 543 YEAR), " %d/%m/%Y" )dueday from cash_sale join status on status_id = status.id where YEAR(date) = YEAR(CURDATE())', (err, cash_sale) => {
      conn.query('select * from status  ', (err, status) => {
        res.render('sell/cash_sale/cash_sale_list.ejs', {
          session: req.session, status, cash_sale
        });
      });
    });
  });
};
///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_add
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.cashsale_add = (req, res) => {
  req.getConnection((err, conn) => {
    //runnumber cashsale
    conn.query('select * from module where id = 17', (err, cashsalemodule) => {
      conn.query('select date_format(date(now()),"%Y%m") as number', (err, runnumber) => {
        conn.query('select count(cash_salenumber) as countnumber,max(cash_salenumber) as maxnumber from cash_sale where date_format(date,"%Y%m") = date_format(date(now()),"%Y%m");', (err, cash_salenumber) => {
          //
          conn.query('select debtors.*,  provinces.name_th as provinces, amphures.name_th as amphures, districts.name_th as districts, zip_code from debtors join provinces on debtors.provinces_id = provinces.id join amphures on debtors.amphures_id = amphures.id join districts on debtors.districts_id = districts.id', (err, debtors) => {
            conn.query('select * from human_resource', (err, human_resource) => {
              conn.query('select * from indicate', (err, indicate) => {
                conn.query('select * from category_price', (err, category_price) => {
                  conn.query('select * from witholding', (err, witholding) => {
                    conn.query('select * from discount', (err, discount) => {
                      conn.query('select * from vat', (err, vat) => {
                        conn.query('select * from compute', (err, compute) => {
                          conn.query('select * from projectgl', (err, projectgl) => {
                            //debtors
                            conn.query('select * from bank', (err, bank) => {
                              conn.query('select * from provinces', (err, provinces) => {
                                conn.query('select * from human', (err, human) => {
                                  //product
                                  conn.query('select * from product_list where enable = 1', (err, product_list) => {
                                    conn.query('select * from product_type', (err, product_type) => {
                                      conn.query('select * from product_type_list', (err, product_type_list) => {
                                        conn.query('select * from product_tax', (err, product_tax) => {
                                          conn.query('select * from product_group', (err, product_group) => {
                                            //runnumber debtors
                                            conn.query('select * from module where id = 3', (err, debtormodule) => {
                                              conn.query('select max(debtorID) as maxnumber from debtors where debtorID like ?', [debtormodule[0].char + "%"], (err, maxnumber) => {
                                                res.render('sell/cash_sale/cash_sale_add.ejs', {
                                                  session: req.session, bank, provinces, runnumber, cash_salenumber, debtors, human_resource, indicate, category_price, witholding, discount, vat, compute,
                                                  projectgl, product_list, product_type, product_type_list, product_tax, product_group, human, maxnumber, cashsalemodule
                                                });
                                              })
                                            });
                                          });
                                        });
                                      });
                                    });
                                  });
                                });
                              });
                            });
                          });
                        });
                      });
                    });
                  });
                });
              });
            });
          });
        });
      });
    });
  });
};
///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_save
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.cashsale_save = (req, res) => {
  const data = req.body;
  req.getConnection((err, conn) => {
    conn.query('INSERT INTO cash_sale (cash_salechar,cash_salenumber,debtors_id,debtors_name,address,post,tax,branch,date,credit,indicate_id,due,employee_saler,employee_delivery,project_id,number,compute_id,signature,note,notein,total,afterdiscount,except,vatcalculator,vatamount,notvatamount,alldiscount,vat,witholding_id,witholding,granttotal,payment,status_id,itemizedvat,itemizeddiscount)  values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
      [data.cash_salechar, data.cash_salenumber, data.debtors_id, data.debtors_name, data.address, data.post, data.tax, data.branch, data.date, data.credit, data.indicate_id, data.due, data.employee_saler, data.employee_delivery, data.project_id, data.number, data.compute_id, data.signature, data.note, data.notein, data.total.replace(/,/g, ''), data.afterdiscount.replace(/,/g, ''), data.except.replace(/,/g, ''), data.vatcalculator.replace(/,/g, ''), data.vatamount.replace(/,/g, ''), data.notvatamount.replace(/,/g, ''), data.alldiscount.replace(/,/g, ''), data.vat, data.witholding_id, data.witholding.replace(/,/g, ''), data.granttotal.replace(/,/g, ''), data.payment.replace(/,/g, ''), 2, data.itemizedvat, data.itemizeddiscount], (err, cashsale) => {
        if (err) {
          res.json(err);
        }
        if (Array.isArray(data.product_id)) {
          for (var i = 0; i < data.product_id.length; i++) {
            conn.query('INSERT INTO cash_sale_list (product_id,product_code,detail,quantity,categoryprice_id,price,listdiscount,vat_id,totallist,cashsale_id)  values(?,?,?,?,?,?,?,?,?,?)', [data.product_id[i], data.product_code[i], data.detail[i], data.quantity[i], data.categoryprice_id[i], data.price[i], data.listdiscount[i], data.vat_id[i], data.totallist[i], cashsale.insertId], (err, cash_sale_list) => {
            });
          }
        } else {
          conn.query('INSERT INTO cash_sale_list (product_id,product_code,detail,quantity,categoryprice_id,price,listdiscount,vat_id,totallist,cashsale_id)  values(?,?,?,?,?,?,?,?,?,?)', [data.product_id, data.product_code, data.detail, data.quantity, data.categoryprice_id, data.price, data.listdiscount, data.vat_id, data.totallist, cashsale.insertId], (err, cashsale_list) => {
          });
        }
        if (req.files) {
          var file = req.files.filename;
          if (!Array.isArray(file)) {
            var type = file.name.split(".");
            var filename = uuidv4() + "." + type[type.length - 1];
            file.mv("./public/cash_sale/" + filename, function (err) {
              if (err) { console.log(err) }
              data.filename = filename;
              conn.query('INSERT INTO cash_sale_file (filename,cashsale_id) values(?,?)', [data.filename, cashsale.insertId], (err, cash_sale_file) => {
                if (err) {
                  res.json(err);
                }
              });
            })
          } else {
            for (var i = 0; i < file.length; i++) {
              var type = file[i].name.split(".");
              var filename = uuidv4() + "." + type[type.length - 1];
              file[i].mv("./public/cash_sale/" + filename, function (err) {
                if (err) { console.log(err) }
              })
              data.filename = filename;
              conn.query('INSERT INTO cash_sale_file (filename,cashsale_id) values(?,?)', [data.filename, cashsale.insertId], (err, cash_sale_file) => {
                if (err) {
                  res.json(err);
                }
              });
            }
          }
        }
        res.redirect('/cash_sale');
      });
  });
};
///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_edit
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.cashsale_edit = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('select cash_sale.* ,projectgl.name as projectgl,compute.name as compute,product_list.name as name,category_price.name as category,cash_sale_list.detail as detail,cash_sale_list.quantity as quantity,cash_sale_list.price as price ,discount.name as discount,listdiscount,vat_id,totallist,product_code,product_id,categoryprice_id,filename from cash_sale  join cash_sale_list on cash_sale_list.cashsale_id = cash_sale.id left join cash_sale_file on cash_sale_file.cashsale_id = cash_sale.id left join projectgl on cash_sale.project_id = projectgl.id join compute on cash_sale.compute_id  = compute.id  join product_list on cash_sale_list.product_id = product_list.id join category_price  on cash_sale_list.categoryprice_id = category_price.id left join discount on cash_sale_list.discount_id = discount.id where  cash_sale_list.cashsale_id = ?;', [url.cashsale], (err, cash_sale) => {
      //runnumber cashsale
      conn.query('select * from module where id = 17', (err, cashsalemodule) => {
        conn.query('select date_format(date(now()),"%Y%m") as number', (err, runnumber) => {
          conn.query('select count(cash_salenumber) as countnumber,max(cash_salenumber) as maxnumber from cash_sale where date_format(date,"%Y%m") = date_format(date(now()),"%Y%m");', (err, cash_salenumber) => {
            //
            conn.query('select debtors.*,  provinces.name_th as provinces, amphures.name_th as amphures, districts.name_th as districts, zip_code from debtors join provinces on debtors.provinces_id = provinces.id join amphures on debtors.amphures_id = amphures.id join districts on debtors.districts_id = districts.id', (err, debtors) => {
              conn.query('select * from human_resource', (err, human_resource) => {
                conn.query('select * from indicate', (err, indicate) => {
                  conn.query('select * from witholding', (err, witholding) => {
                    conn.query('select * from discount', (err, discount) => {
                      conn.query('select * from category_price', (err, category_price) => {
                        conn.query('select * from vat', (err, vat) => {
                          conn.query('select * from compute', (err, compute) => {
                            conn.query('select * from projectgl', (err, projectgl) => {
                              //debtors
                              conn.query('select * from bank', (err, bank) => {
                                conn.query('select * from provinces', (err, provinces) => {
                                  conn.query('select * from human', (err, human) => {
                                    //product
                                    conn.query('select * from product_list where enable = 1', (err, product_list) => {
                                      conn.query('select * from product_type', (err, product_type) => {
                                        conn.query('select * from product_type_list', (err, product_type_list) => {
                                          conn.query('select * from product_tax', (err, product_tax) => {
                                            conn.query('select * from product_group', (err, product_group) => {
                                              //runnumber debtors
                                              conn.query('select * from module where id = 3', (err, debtormodule) => {
                                                conn.query('select max(debtorID) as maxnumber from debtors where debtorID like ?', [debtormodule[0].char + "%"], (err, maxnumber) => {
                                                  res.render('sell/cash_sale/cash_sale_edit.ejs', {
                                                    session: req.session, cash_sale, runnumber, cash_salenumber, debtors, human_resource, indicate, witholding, discount, category_price, vat, compute,
                                                    projectgl, bank, provinces, product_list, product_type, product_type_list, product_tax, product_group, human, maxnumber, cashsalemodule
                                                  });
                                                });
                                              });
                                            });
                                          });
                                        });
                                      });
                                    });
                                  });
                                });
                              });
                            });
                          });
                        });
                      });
                    });
                  });
                });
              });
            });
          });
        });
      });
    });
  });
};
///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_update
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.cashsale_update = (req, res) => {
  const data = req.body;
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('UPDATE  cash_sale  set debtors_id = ?,debtors_name = ?,address = ?,post = ?,tax = ?,branch = ?,date = ?,credit = ?,indicate_id = ?,due = ?,employee_saler = ?,employee_delivery = ?,project_id = ?,number = ?,compute_id = ?,signature = ?,note = ?,notein = ?,total = ?,afterdiscount = ?,except = ?,vatcalculator = ?,vatamount = ?,notvatamount = ?,alldiscount = ?,vat = ?,witholding_id = ?,witholding = ?,granttotal = ?,payment = ?,itemizedvat = ?,itemizeddiscount = ? where id = ?',
      [data.debtors_id, data.debtors_name, data.address, data.post, data.tax, data.branch, data.date, data.credit, data.indicate_id, data.due, data.employee_saler, data.employee_delivery, data.project_id, data.number, data.compute_id, data.signature, data.note, data.notein, data.total.replace(/,/g, ''), data.afterdiscount.replace(/,/g, ''), data.except.replace(/,/g, ''), data.vatcalculator.replace(/,/g, ''), data.vatamount.replace(/,/g, ''), data.notvatamount.replace(/,/g, ''), data.alldiscount.replace(/,/g, ''), data.vat, data.witholding_id, data.witholding.replace(/,/g, ''), data.granttotal.replace(/,/g, ''), data.payment.replace(/,/g, ''), data.itemizedvat, data.itemizeddiscount, url.cashsale], (err, cash_sale) => {
        conn.query('delete from cash_sale_list where cashsale_id = ?', [url.cashsale], (err, cash_sale_product) => {
          if (Array.isArray(data.product_id)) {
            for (var i = 0; i < data.product_id.length; i++) {
              conn.query('INSERT INTO cash_sale_list (product_id,product_code,detail,quantity,categoryprice_id,price,listdiscount,vat_id,totallist,cashsale_id)  values(?,?,?,?,?,?,?,?,?,?)', [data.product_id[i], data.product_code[i], data.detail[i], data.quantity[i], data.categoryprice_id[i], data.price[i], data.listdiscount[i], data.vat_id[i], data.totallist[i], url.cashsale], (err, cash_sale_list) => {
              });
            }
          } else {
            conn.query('INSERT INTO cash_sale_list (product_id,product_code,detail,quantity,categoryprice_id,price,listdiscount,vat_id,totallist,cashsale_id)  values(?,?,?,?,?,?,?,?,?,?)', [data.product_id, data.product_code, data.detail, data.quantity, data.categoryprice_id, data.price, data.listdiscount, data.vat_id, data.totallist, url.cashsale], (err, cash_sale_list) => {
            });
          }
        });
        if (req.files) {
          var file = req.files.filename;
          if (!Array.isArray(file)) {
            var type = file.name.split(".");
            var filename = uuidv4() + "." + type[type.length - 1];
            file.mv("./public/cash_sale/" + filename, function (err) {
              if (err) { console.log(err) }
              data.filename = filename;
              conn.query('UPDATE cash_sale_file set filename = ?,cashsale_id = ?', [data.filename, url.cashsale], (err, cash_sale_file) => {
                if (err) {
                  res.json(err);
                }
              });
            })
          } else {
            for (var i = 0; i < file.length; i++) {
              var type = file[i].name.split(".");
              var filename = uuidv4() + "." + type[type.length - 1];
              file[i].mv("./public/cash_sale/" + filename, function (err) {
                if (err) { console.log(err) }
              })
              data.filename = filename;
              conn.query('UPDATE set cash_sale_file set filename = ?,cashsale_id = ?', [data.filename, url.cashsale], (err, cash_sale_file) => {
                if (err) {
                  res.json(err);
                }
              });
            }
          }
        }
        res.redirect('/cash_sale');
      });
  });
};
///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_delete
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.cashsale_delete = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('Delete from cash_sale_file where cashsale_id = ?', [url.cashsale], (err, cash_sale_file) => {
      conn.query('Delete from cash_sale_list where cashsale_id = ?', [url.cashsale], (err, cash_sale_list) => {
        conn.query('Delete from cash_sale where id = ?', [url.cashsale], (err, cash_sale) => {
          res.redirect('/cash_sale');
        });
      })
    })
  });
};
///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_detail
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.cashsale_detail = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('select * from discount', (err, discount) => {
      conn.query(' select cash_sale.*,  cash_sale_list.product_id as product_id,cash_sale_list.detail as detail,cash_sale_list.quantity as quantity,cash_sale_list.categoryprice_id as categoryprice_id,cash_sale_list.price as price,cash_sale_list.listdiscount as listdiscount,cash_sale_list.vat_id as vat_id,cash_sale_list.totallist as totallist,cash_sale_list.product_code as product_code,filename,category_price.name as category,projectgl.name as projectgl,indicate.name as indicate,compute.name as compute,discount.name as discount,product_list.name as name  from cash_sale  join cash_sale_list on cash_sale_list.cashsale_id = cash_sale.id left join cash_sale_file on cash_sale_file.cashsale_id = cash_sale.id left join projectgl on cash_sale.project_id = projectgl.id join compute on cash_sale.compute_id  = compute.id  join product_list on cash_sale_list.product_id = product_list.id join category_price  on cash_sale_list.categoryprice_id = category_price.id left join discount on cash_sale_list.discount_id = discount.id join indicate on cash_sale.indicate_id = indicate.id  where  cash_sale_list.cashsale_id = ?;', [url.cashsale], (err, cash_sale) => {
        //////////////////////////////////////////////////////////
        var da = cash_sale[0].date.toISOString().split('T')[0]
        var dat = da.split('-')
        var date = parseInt(dat[0]) + 543;
        var date_now = dat[2] + "/" + dat[1] + "/" + date;
        //////////////////////////////////////////////////////////
        var du = cash_sale[0].due.toISOString().split('T')[0]
        var due = du.split('-')
        var due_d = parseInt(due[0]) + 543;
        var due_now = due[2] + "/" + due[1] + "/" + due_d;
        //////////////////////////////////////////////////////////
        conn.query('select * from witholding', (err, witholding) => {
          res.render('sell/cash_sale/cash_sale_detail.ejs', {
            cash_sale, date_now, due_now, witholding, discount,
            session: req.session
          });
        });
      });
    });
  });
};
///////////////////////////////////////////////////////////////////////////////////////////////////
//Controller_readd
///////////////////////////////////////////////////////////////////////////////////////////////////
controller.cashsale_readd = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('select cash_sale.* ,projectgl.name as projectgl,compute.name as compute,product_list.name as name,category_price.name as category,cash_sale_list.detail as detail,cash_sale_list.quantity as quantity,cash_sale_list.price as price ,discount.name as discount,listdiscount,vat_id,totallist,product_code,product_id,categoryprice_id,filename from cash_sale  join cash_sale_list on cash_sale_list.cashsale_id = cash_sale.id left join cash_sale_file on cash_sale_file.cashsale_id = cash_sale.id left join projectgl on cash_sale.project_id = projectgl.id join compute on cash_sale.compute_id  = compute.id  join product_list on cash_sale_list.product_id = product_list.id join category_price  on cash_sale_list.categoryprice_id = category_price.id left join discount on cash_sale_list.discount_id = discount.id where  cash_sale_list.cashsale_id = ?;', [url.cashsale], (err, cash_sale) => {
      //runnumber cashsale
      conn.query('select * from module where id = 17', (err, cashsalemodule) => {
        conn.query('select date_format(date(now()),"%Y%m") as number', (err, runnumber) => {
          conn.query('select count(cash_salenumber) as countnumber,max(cash_salenumber) as maxnumber from cash_sale where date_format(date,"%Y%m") = date_format(date(now()),"%Y%m");', (err, cash_salenumber) => {
            //
            conn.query('select debtors.*,  provinces.name_th as provinces, amphures.name_th as amphures, districts.name_th as districts, zip_code from debtors join provinces on debtors.provinces_id = provinces.id join amphures on debtors.amphures_id = amphures.id join districts on debtors.districts_id = districts.id', (err, debtors) => {
              conn.query('select * from human_resource', (err, human_resource) => {
                conn.query('select * from indicate', (err, indicate) => {
                  conn.query('select * from witholding', (err, witholding) => {
                    conn.query('select * from discount', (err, discount) => {
                      conn.query('select * from category_price', (err, category_price) => {
                        conn.query('select * from vat', (err, vat) => {
                          conn.query('select * from compute', (err, compute) => {
                            conn.query('select * from projectgl', (err, projectgl) => {
                              //debtors
                              conn.query('select * from bank', (err, bank) => {
                                conn.query('select * from human', (err, human) => {
                                  conn.query('select * from provinces', (err, provinces) => {
                                    //product
                                    conn.query('select * from product_list where enable = 1', (err, product_list) => {
                                      conn.query('select * from product_type', (err, product_type) => {
                                        conn.query('select * from product_type_list', (err, product_type_list) => {
                                          conn.query('select * from product_tax', (err, product_tax) => {
                                            conn.query('select * from product_group', (err, product_group) => {
                                              //runnumber debtors
                                              conn.query('select * from module where id = 3', (err, debtormodule) => {
                                                conn.query('select max(debtorID) as maxnumber from debtors where debtorID like ?', [debtormodule[0].char + "%"], (err, maxnumber) => {
                                                  res.render('sell/cash_sale/cash_sale_readd.ejs', {
                                                    session: req.session, cash_sale, runnumber, cash_salenumber, debtors, human_resource, indicate, witholding, discount, category_price, vat, compute,
                                                    projectgl, bank, provinces, product_list, product_type, product_type_list, product_tax, product_group, human, maxnumber, cashsalemodule
                                                  });
                                                });
                                              });
                                            });
                                          });
                                        });
                                      });
                                    });
                                  });
                                });
                              });
                            });
                          });
                        });
                      });
                    });
                  });
                });
              });
            });
          });
        });
      });
    });
  });
};
module.exports = controller;
