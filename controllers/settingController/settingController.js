const controller = {};
const session = require("express-session");

controller.setting_expenses_head = (req, res) => {
    req.getConnection((err, conn) => {
        conn.query('select * from expenses_head', (err, expenseshead) => {
            res.render('setting_all/setting_expenses/setting_expenses_head', {
                session: req.session, expenseshead
            });
        });
    });
};

controller.setting_expenses_headsave = (req, res) => {
    const data = req.body;
    req.getConnection((err, conn) => {
        conn.query('INSERT INTO  expenses_head (name)  values(?)',[data.name], (err, expenseshead) => {
            res.redirect('/setting/expenses_head')
        });
    });
};

controller.setting_expenses_headupdate = (req, res) => {
    const data = req.body;
    var url = req.params;
    req.getConnection((err, conn) => {
        conn.query('update expenses_head  set name = ? where id = ?',[data.name,url.expenseshead], (err, expenseshead) => {
            res.redirect('/setting/expenses_head')
        });
    });
};

controller.setting_expenses_headdelete = (req, res) => {
    var url = req.params;
    console.log(url);
    req.getConnection((err, conn) => {
        conn.query('delete from  expenses_head  where id = ? ',[url.expenseshead],(err,expensesdetail) =>{
            res.redirect('/setting/expenses_head')
        });
    });
}

controller.setting_expenses_detail = (req, res) => {
    var url = req.params;
    req.getConnection((err, conn) => {
        conn.query('select expenses_detail.*,chart.name as chartname from expenses_detail join chart on expenses_detail.chart_id = chart.id where  expenses_detail.expenses_id = ?', [url.expensesdetail], (err, expensesdetail) => {
            conn.query('select * from chart where category_id = 5', (err, chart) => {
                res.render('setting_all/setting_expenses/setting_expenses_detail', {
                    session: req.session, expensesdetail, chart, url
                });
            });
        });
    });
};

controller.setting_expenses_save = (req, res) => {
    var url = req.params;
    const data = req.body;
    req.getConnection((err, conn) => {
        conn.query('INSERT INTO  expenses_detail (expenses_id,name,chart_id)  values(?,?,?)', [url.expensesdetail, data.name, data.chart_id], (err, expensesdetail) => {
            res.redirect('/setting/expenses_detail/' + url.expensesdetail);
        })
    });
};

controller.setting_expenses_update = (req, res) => {
    var url = req.params;
    const data = req.body;
    req.getConnection((err, conn) => {
        conn.query('update expenses_detail set expenses_id = ? , name = ? , chart_id = ? where id = ?',[url.expensesdetail,data.name,data.chart_id,data.name_id],(err,expensesdetail) =>{
            res.redirect('/setting/expenses_detail/' + url.expensesdetail);
        })
    });
}

controller.setting_expenses_delete = (req, res) => {
    var url = req.params;
    req.getConnection((err, conn) => {
        conn.query('delete from  expenses_detail  where id = ? ',[url.expensesdetail],(err,expensesdetail) =>{
            res.redirect('/setting/expenses_detail/' + url.id);
        });
    });
}



module.exports = controller;