const session = require("express-session");

const controller = {};

controller.list = (req, res) => {
  req.getConnection((err, conn) => {
        conn.query('SELECT * FROM general_ledger.payment_method;', (err, payment_method) => { 
        res.render('salary_management/payment_method/payment_method_list.ejs', {
          payment_method: payment_method,
          session: req.session
        })
      });
    });
};
controller.add = (req, res) => {
  const data = req.body;
  console.log(data);
  req.getConnection((err, conn) => {
          conn.query('INSERT INTO payment_method SET ?',[data], (err, payment_method) => { 
            res.redirect('/payment_method')
      });
});
};
controller.ajax = (req, res) => {
  const {id} = req.params;
  console.log(id);
  req.getConnection((err, conn) => {
    conn.query('SELECT * FROM general_ledger.payment_method where id = ?;',[id], (err, payment_method) => {
      conn.query('SELECT * FROM general_ledger.payment_method as pm inner join general_ledger.payment_list as pl on pl.payment_method_id = pm.id inner join general_ledger.payment_topic as pt on pt.payment_method_id = pm.id inner join general_ledger.employee as em on em.payment_method_id = pm.id where pm.id = ?;',[id], (err, payment_method_check) => {

      res.send({payment_method: payment_method,payment_method_check:payment_method_check,session: req.session})
    });
  });

});
};
controller.delcon = (req, res) => {
  const {id} = req.params;
  req.getConnection((err, conn) => {
          conn.query('delete from payment_method where id = ?',[id], (err, payment_method) => { 
            res.redirect('/payment_method')
      });
});
};

controller.edit = (req, res) => {
  const {id} = req.params;
  const data = req.body;
  console.log(data);
  req.getConnection((err, conn) => {
    conn.query('update payment_method set ? where payment_method.id=?',[data,id], (err, payment_method) => {
      res.redirect('/payment_method')
    });
});
};
module.exports = controller;
