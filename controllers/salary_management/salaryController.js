const session = require("express-session");

const controller = {};

controller.list = (req, res) => {
  req.getConnection((err, conn) => {
      conn.query('SELECT b.name,bs.*,bt.name as nametype FROM bank as b inner join bank_salary as bs on bs.bank_id=b.id inner join bank_type as bt on bt.id=bs.bank_type_id order by bs.id asc;', (err, salary) => {
        conn.query('SELECT * FROM bank;', (err, bank) => { 
          conn.query('SELECT * FROM bank_type;', (err, banktype) => { 
        res.render('salary_management/salary/salary_list.ejs', {
          salary: salary,
          bank: bank,
          banktype: banktype,
          session: req.session
        })
      });
    });
  });
});
};
controller.add = (req, res) => {
  const data = req.body;
  console.log(data);
  req.getConnection((err, conn) => {
          conn.query('INSERT INTO bank_salary SET ?',[data], (err, banktype) => { 
            res.redirect('/salary')
      });
});
};
controller.ajax = (req, res) => {
  const {id} = req.params;
  console.log(id);
  req.getConnection((err, conn) => {
    conn.query('SELECT b.name,bs.*,bt.name as nametype FROM bank as b inner join bank_salary as bs on bs.bank_id=b.id inner join bank_type as bt on bt.id=bs.bank_type_id where bs.id=?;',[id], (err, salary) => {
      conn.query('SELECT concat(b.name," ",bt.name," ",bs.bank_number) as bsname,bset.*,bs.id as bsid FROM bank as b inner join bank_salary as bs on bs.bank_id=b.id inner join bank_type as bt on bt.id=bs.bank_type_id inner join bank_settting as bset on bset.bank_salary_id=bs.id where bs.id=?;',[id], (err, salary_check) => {
      res.send({salary: salary,salary_check: salary_check,session: req.session})
    });
});
});
};
controller.delcon = (req, res) => {
  const {id} = req.params;
  req.getConnection((err, conn) => {
          conn.query('delete from bank_salary where id=?',[id], (err, banktype) => { 
            res.redirect('/salary')
      });
});
};

controller.edit = (req, res) => {
  const {id} = req.params;
  const data = req.body;
  console.log("pppppppp",data);
  req.getConnection((err, conn) => {
    conn.query('update bank_salary set ? where bank_salary.id=?',[data,id], (err, salary) => {
      res.redirect('/salary')
    });
});
};
module.exports = controller;
