const session = require("express-session");

const controller = {};

controller.list = (req, res) => {
  req.getConnection((err, conn) => {
        conn.query('SELECT * FROM general_ledger.employee_type;', (err, employee_type) => { 
        res.render('salary_management/employee_type/employee_type_list.ejs', {
          employee_type: employee_type,
          session: req.session
        })
      });
    });
};
controller.add = (req, res) => {
  const data = req.body;
  console.log(data);
  req.getConnection((err, conn) => {
          conn.query('INSERT INTO employee_type SET ?',[data], (err, employee_type) => { 
            res.redirect('/employee_type')
      });
});
};
controller.ajax = (req, res) => {
  const {id} = req.params;
  console.log(id);
  req.getConnection((err, conn) => {
    conn.query('SELECT * FROM general_ledger.employee_type where id = ?;',[id], (err, employee_type) => {
      conn.query('SELECT * FROM general_ledger.employee_type as emt inner join employee as em on em.employee_type_id = emt.id where emt.id = ?;',[id], (err, employee_type_check) => {

      res.send({employee_type: employee_type,employee_type_check:employee_type_check,session: req.session})
    });
  });


});
};
controller.delcon = (req, res) => {
  const {id} = req.params;
  req.getConnection((err, conn) => {
          conn.query('delete from employee_type where id = ?',[id], (err, employee_type) => { 
            res.redirect('/employee_type')
      });
});
};

controller.edit = (req, res) => {
  const {id} = req.params;
  const data = req.body;
  console.log(data);
  req.getConnection((err, conn) => {
    conn.query('update employee_type set ? where employee_type.id=?',[data,id], (err, employee_type) => {
      res.redirect('/employee_type')
    });
});
};
module.exports = controller;
