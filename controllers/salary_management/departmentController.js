const session = require("express-session");

const controller = {};

controller.list = (req, res) => {
  req.getConnection((err, conn) => {
        conn.query('SELECT * FROM general_ledger.department;', (err, department) => { 
        res.render('salary_management/department/department_list.ejs', {
          department: department,
          session: req.session
        })
      });
    });
};
controller.add = (req, res) => {
  const data = req.body;
  console.log(data);
  req.getConnection((err, conn) => {
          conn.query('INSERT INTO department SET ?',[data], (err, department) => { 
            res.redirect('/department')
      });
});
};
controller.ajax = (req, res) => {
  const {id} = req.params;
  console.log(id);
  req.getConnection((err, conn) => {
    conn.query('SELECT * FROM general_ledger.department where id = ?;',[id], (err, department) => {
      conn.query('SELECT * FROM general_ledger.department as dp inner join general_ledger.employee as em on em.department_id = dp.id where em.id = ?;',[id], (err, department_check) => {
      res.send({department: department,department_check: department_check,session: req.session})
    });
  });
});
};
controller.delcon = (req, res) => {
  const {id} = req.params;
  req.getConnection((err, conn) => {
          conn.query('delete from department where id = ?',[id], (err, department) => { 
            res.redirect('/department')
      });
});
};

controller.edit = (req, res) => {
  const {id} = req.params;
  const data = req.body;
  console.log(data);
  req.getConnection((err, conn) => {
    conn.query('update department set ? where department.id=?',[data,id], (err, department) => {
      res.redirect('/department')
    });
});
};
module.exports = controller;
