const session = require("express-session");

const controller = {};

controller.list = (req, res) => {
  req.getConnection((err, conn) => {
        conn.query('SELECT * FROM general_ledger.down_list;', (err, down_list) => { 
        res.render('salary_management/down_list/down_list_list.ejs', {
          down_list: down_list,
          session: req.session
        })
      });
    });
};
controller.add = (req, res) => {
  const data = req.body;
  console.log(data);
  req.getConnection((err, conn) => {
          conn.query('INSERT INTO down_list SET ?',[data], (err, down_list) => { 
            res.redirect('/down_list')
      });
});
};
controller.ajax = (req, res) => {
  const {id} = req.params;
  console.log(id);
  req.getConnection((err, conn) => {
    conn.query('SELECT * FROM general_ledger.down_list where id = ?;',[id], (err, down_list) => {
      conn.query('SELECT * FROM down_list as ul inner join pay_down_list as pul on pul.down_list_id = ul.id where ul.id = ?;',[id], (err, down_list_check) => {
      res.send({down_list: down_list,down_list_check: down_list_check,session: req.session})
    });
  });
});
};
controller.delcon = (req, res) => {
  const {id} = req.params;
  console.log(id);
  req.getConnection((err, conn) => {
          conn.query('delete from down_list where id = ?',[id], (err, down_list) => { 
            res.redirect('/down_list')
      });
});
};

controller.edit = (req, res) => {
  const {id} = req.params;
  const data = req.body;
  console.log(data);
  req.getConnection((err, conn) => {
    conn.query('update down_list set ? where down_list.id=?',[data,id], (err, down_list) => {
      res.redirect('/down_list')
    });
});
};
module.exports = controller;
