const session = require("express-session");

const controller = {};

controller.list = (req, res) => {
  req.getConnection((err, conn) => {
      conn.query('select * from social_security', (err, social) => {
        res.render('salary_management/social_security/social_security_list.ejs', {
          social: social,
          session: req.session
        })
  });
});
};
controller.add = (req, res) => {
  const data = req.body;
  console.log(data);
  req.getConnection((err, conn) => {
          conn.query('INSERT INTO social_security SET ?',[data], (err, banktype) => { 
            res.redirect('/social')
      });
});
};
controller.ajax = (req, res) => {
  const {id} = req.params;
  console.log(id);
  req.getConnection((err, conn) => {
    conn.query('select * from social_security where id=?;',[id], (err, so) => {
      conn.query('select * from social_security where id=?;',[id], (err, so_check) => {
      res.send({so: so,so_check: so_check,session: req.session})
    });
});
});
};
controller.delcon = (req, res) => {
  const {id} = req.params;
  req.getConnection((err, conn) => {
          conn.query('delete from social_security where id=?',[id], (err, banktype) => { 
            res.redirect('/social')
      });
});
};

controller.edit = (req, res) => {
  const {id} = req.params;
  const data = req.body;
  console.log("pppppppp",data);
  req.getConnection((err, conn) => {
    conn.query('update social_security set ? where id=?',[data,id], (err, salary) => {
      res.redirect('/social')
    });
});
};
module.exports = controller;
