const session = require("express-session");

const controller = {};

controller.list = (req, res) => {
  req.getConnection((err, conn) => {
    // conn.query('delete FROM payment_topic WHERE social_security IS NULL;', (err, em) => {
    conn.query('SELECT ps.payment_topic_id as topic,DATE_FORMAT(DATE_ADD(pt.date_start,INTERVAL 543 YEAR),"%d/%m/%Y") as date_startn,DATE_FORMAT(DATE_ADD(pt.date_end,INTERVAL 543 YEAR),"%d/%m/%Y") as date_endn,DATE_FORMAT(DATE_ADD(pt.date_pay,INTERVAL 543 YEAR),"%d/%m/%Y") as date_payn,pt.* FROM payment_topic as pt left join pay_save as ps on ps.payment_topic_id=pt.id;', (err, payment_topic) => {

      res.render('salary_management/payment_topic/payment_topic_list.ejs', {
        payment_topic: payment_topic,
        session: req.session
      })
      // });
    });
  });
};
controller.addpaylist = (req, res) => {
  const data = req.body;
  const {id} = req.params;
  data["payment_topic_id"]=id;
  data["employee_id"]=data["employee_id_check"]
  delete data["employee_id_check"]
  req.getConnection((err, conn) => {
    conn.query('INSERT INTO payment_list SET ?;',[data], (err, pl) => {
      var link = "/paymentedit/"+id
      res.redirect(link);
    });
  });
};

controller.add = (req, res) => {
  const data = req.body;
  req.getConnection((err, conn) => {
    conn.query('SELECT em.*,b.name as bname FROM employee as em left join bank as b on b.id=em.bank_id where em.employee_type_id = ? and em.payment_method_id = ?;',[data.selectemployee,data.selecttype], (err, payment_topic) => {
      conn.query('SELECT * FROM social_security;', (err, social) => {
        conn.query('SELECT * FROM employee_type as em where em.id = ?;',[data.selectemployee], (err, em) => {
          conn.query('SELECT * FROM up_list;', (err, uplist) => {
            conn.query('SELECT * FROM down_list;',[data.selectemployee], (err, downlist) => {
              conn.query('SELECT * FROM payment_method as pm where pm.id = ?;',[data.selecttype], (err, pm) => {
                res.render('salary_management/payment_topic/payment_topic_addshow.ejs',{
                  payment_topic:payment_topic,social:social,id:data,uplist:uplist,downlist:downlist,em:em,pm:pm,session: req.session
                });
              });
            });
          });
        });
      });
    });
  });
};

controller.addform = (req, res) => {
  const data = req.body;
  var topic = data.topic[0];
  var paylist = data.paylist;
  var uplist = data.uplist;
  var downlsit = data.downlist;
  // console.log(data);
  req.getConnection((err, conn) => {
    conn.query('INSERT INTO payment_topic SET ?;',[topic], (err, em) => {
      var idpaytop = em.insertId
      for (const property in paylist) {
        paylist[property]["payment_topic_id"] = idpaytop
        conn.query('INSERT INTO payment_list SET ?;',[paylist[property]], (err, pl) => {
        });
      }
      for (const property in uplist) {
        uplist[property]["payment_topic_id"] = idpaytop
        conn.query('INSERT INTO pay_up_list SET ?;',[uplist[property]], (err, ul) => {
        });
      }
      for (const property in downlsit) {
        downlsit[property]["payment_topic_id"] = idpaytop
        conn.query('INSERT INTO pay_down_list SET ?;',[downlsit[property]], (err, dl) => {
        });
      }
      res.send({id:"pppppppp"})
    });
  });
};

controller.editform = (req, res) => {
  const {id} = req.params;
  req.getConnection((err, conn) => {
    conn.query('SELECT pt.*,DATE_FORMAT(pt.date_create,"%Y-%m-%d") as date_cre,DATE_FORMAT(pt.date_start,"%Y-%m-%d") as date_st,DATE_FORMAT(pt.date_end,"%Y-%m-%d") as date_en,DATE_FORMAT(pt.date_pay,"%Y-%m-%d") as date_p FROM payment_topic as pt where pt.id=?;',[id], (err, topic) => {
      conn.query('SELECT pml.*,b.name as bname,em.firstname_th as firstname_th,em.lastname_th as lastname_th,em.employee_code,em.bank_number FROM payment_list as pml inner join employee as em on em.id = pml.employee_id left join bank as b on b.id = em.bank_id where pml.payment_topic_id = ? order by pml.employee_id ASC;',[id], (err, payment_topic) => {
        conn.query('SELECT * FROM social_security;', (err, social) => {
          conn.query('SELECT em.name as emname,pm.name as pmname,pm.id as pmid,em.id as emid FROM employee_type as em inner join payment_topic as pt on pt.employee_type_id = em.id inner join payment_method as pm on pm.id = pt.payment_method_id where pt.id = ?',[id], (err, pmemname) => {
            conn.query('SELECT * FROM up_list;', (err, uplist) => {
              conn.query('SELECT * FROM down_list;', (err, downlist) => {
                conn.query('SELECT * FROM up_list as ul inner join pay_up_list as pul on pul.up_list_id = ul.id where pul.payment_topic_id=?;',[id], (err, uplistselect) => {
                  conn.query('SELECT * FROM down_list as dl inner join pay_down_list as pdl on pdl.down_list_id = dl.id where pdl.payment_topic_id=?;',[id], (err, downlistselect) => {
                    conn.query('SELECT em.*,b.name as bname FROM employee as em left join bank as b on b.id = em.bank_id where em.employee_type_id = ? and em.payment_method_id = ?;',[pmemname[0].emid,pmemname[0].pmid], (err, employee) => {
                      res.render('salary_management/payment_topic/payment_topic_editform.ejs',{
                        topic:topic,payment_topic:payment_topic,social:social,id:id,uplist:uplist,downlist:downlist,uplistselect:uplistselect,downlistselect:downlistselect,pmemname:pmemname,employee:employee,session: req.session
                      });
                    });
                  });
                });
              });
            });
          });
        });
      });
    });
  });
};

controller.delpaytop = (req, res) => {
  const {id} = req.params;
  req.getConnection((err, conn) => {
    conn.query('DELETE FROM pay_down_list WHERE (payment_topic_id = ?);',[id], (err, payment_topic) => {
      conn.query('DELETE FROM pay_up_list WHERE (payment_topic_id = ?);',[id], (err, payment_topic) => {
        conn.query('DELETE FROM payment_list WHERE (payment_topic_id = ?);',[id], (err, payment_topic) => {
          conn.query('DELETE FROM payment_topic WHERE (id = ?);',[id], (err, payment_topic) => {
            res.redirect("/paymenttopic")
          });
        });
      });
    });
  });
};

controller.update = (req, res) => {
  const {id} = req.params;
  const data = req.body;
  var topic = data.topic[0];
  var paylist = data.paylist;
  var uplist = data.uplist;
  var downlsit = data.downlist;
  req.getConnection((err, conn) => {
    conn.query('update payment_topic set ? where payment_topic.id=?',[topic,id], (err, payment_topic) => {
      conn.query('DELETE FROM pay_down_list WHERE (payment_topic_id = ?);',[id], (err, test) => {
        conn.query('DELETE FROM pay_up_list WHERE (payment_topic_id = ?);',[id], (err, payment_topic) => {
          conn.query('DELETE FROM payment_list WHERE (payment_topic_id = ?);',[id], (err, payment_topic) => {
            for (const property in paylist) {
              paylist[property]["payment_topic_id"] = id
              conn.query('INSERT INTO payment_list SET ?;',[paylist[property]], (err, pl) => {
              });
            }
            for (const property in uplist) {
              uplist[property]["payment_topic_id"] = id
              conn.query('INSERT INTO pay_up_list SET ?;',[uplist[property]], (err, ul) => {
              });
            }
            for (const property in downlsit) {
              downlsit[property]["payment_topic_id"] = id
              conn.query('INSERT INTO pay_down_list SET ?;',[downlsit[property]], (err, dl) => {
              });
            }
            res.send({id:"pppppppp"})
          });
        });
      });
    });
  });
};

module.exports = controller;
