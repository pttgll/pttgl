const session = require("express-session");

const controller = {};

controller.list = (req, res) => {
  req.getConnection((err, conn) => {
      conn.query('SELECT concat(b.name," ",bt.name," ",bs.bank_number) as bsname,bset.* FROM bank as b inner join bank_salary as bs on bs.bank_id=b.id inner join bank_type as bt on bt.id=bs.bank_type_id inner join bank_settting as bset on bset.bank_salary_id=bs.id order by bset.id ASC;', (err, salary_setting) => {
        conn.query('SELECT concat(b.name," ",bt.name," ",bs.bank_number) as bsname,bs.id as id FROM bank as b inner join bank_salary as bs on bs.bank_id=b.id inner join bank_type as bt on bt.id=bs.bank_type_id order by bs.id asc;', (err, salary) => {
        res.render('salary_management/salary_setting/salary_setting_list.ejs', {
          salary: salary,
          salary_setting: salary_setting,
          session: req.session
        })
      });
    });
  });
};
controller.add = (req, res) => {
  const data = req.body;
  console.log(data);
  req.getConnection((err, conn) => {
          conn.query('INSERT INTO bank_settting SET ?',[data], (err, banktype) => { 
            res.redirect('/salarysetting')
      });
});
};
controller.ajax = (req, res) => {
  const {id} = req.params;
  console.log(id);
  req.getConnection((err, conn) => {
    conn.query('SELECT concat(b.name," ",bt.name," ",bs.bank_number) as bsname,bset.*,bs.id as bsid FROM bank as b inner join bank_salary as bs on bs.bank_id=b.id inner join bank_type as bt on bt.id=bs.bank_type_id inner join bank_settting as bset on bset.bank_salary_id=bs.id where bset.id=? order by bset.id ASC;',[id], (err, bank_setting) => {
      console.log(bank_setting);
      res.send({bank_setting: bank_setting,session: req.session})
    });
});
};
controller.delcon = (req, res) => {
  const {id} = req.params;
  req.getConnection((err, conn) => {
          conn.query('delete from bank_settting where id=?',[id], (err, banktype) => { 
            res.redirect('/salarysetting')
      });
});
};

controller.edit = (req, res) => {
  const {id} = req.params;
  const data = req.body;
  console.log("pppppppp",data,id);
  req.getConnection((err, conn) => {
    conn.query('update bank_settting set ? where bank_settting.id=?',[data,id], (err, salary) => {
      res.redirect('/salarysetting')
    });
});
};
module.exports = controller;
