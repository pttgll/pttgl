const session = require("express-session");

const controller = {};

controller.list = (req, res) => {
  req.getConnection((err, conn) => {
    conn.query('SELECT e.id,e.employee_code,n.noun_th,e.firstname_th,e.lastname_th,d.name,e.email,e.phone,e.subdepartment FROM general_ledger.employee as e join general_ledger.noun as n on e.noun_id = n.id join general_ledger.department as d on e.department_id = d.id;', (err, employee) => { 
      conn.query('SELECT * FROM general_ledger.noun;', (err, noun) => { 
        conn.query('SELECT * FROM general_ledger.department;', (err, department) => { 
          conn.query('SELECT * FROM general_ledger.employee_type;', (err, employee_type) => { 
            conn.query('SELECT * FROM general_ledger.social_security;', (err, social_security) => { 
              conn.query('SELECT * FROM general_ledger.payment_method;', (err, payment_method) => { 
                conn.query('SELECT * FROM general_ledger.bank_type;', (err, bank_type) => { 
                  conn.query('SELECT * FROM general_ledger.bank;', (err, bank) => { 
        res.render('salary_management/employee/employee_list.ejs', {
          employee: employee,
          noun:noun,
          department:department,
          employee_type:employee_type,
          social_security,
          payment_method,
          bank_type,
          bank:bank,
          session: req.session
        })
      });
    });
  });
});
});
    });
  });
  });
    });
};
controller.add = (req, res) => {
  const data = req.body;
  
  console.log(data);
  req.getConnection((err, conn) => {
          conn.query('INSERT INTO employee SET ?',[data], (err, employee) => { 
            res.redirect('/employee')
      });
});
};
controller.ajax = (req, res) => {
  const {id} = req.params;
  console.log(id);
  req.getConnection((err, conn) => {
    conn.query('SELECT e.*,DATE_FORMAT(e.date_start,"%Y-%m-%d") as date_st,DATE_FORMAT(e.birthday,"%Y-%m-%d") as date_bd FROM general_ledger.employee as e join general_ledger.noun as n on e.noun_id = n.id join general_ledger.department as d on e.department_id = d.id where e.id = ?;', [id], (err, employee) => { 
      conn.query('select * from general_ledger.employee as e join general_ledger.payment_list as pl on pl.employee_id = e.id join general_ledger.pay_up_list as up on up.employee_id = e.id join general_ledger.pay_down_list as down on down.employee_id = e.id where e.id = ?;',[id], (err, employee_check) => {
        conn.query('SELECT e.id,e.employee_code,n.noun_th,e.firstname_th,e.lastname_th,d.name as dname,e.email,e.phone,e.subdepartment FROM general_ledger.employee as e join general_ledger.noun as n on e.noun_id = n.id join general_ledger.department as d on e.department_id = d.id where e.id = ?;',[id], (err, employee_del) => { 
        res.send({employee: employee,employee_del:employee_del,employee_check: employee_check,session: req.session})
    });
});
});
});
};
controller.delcon = (req, res) => {
  const {id} = req.params;
  req.getConnection((err, conn) => {
          conn.query('delete from employee where id=?',[id], (err, employee) => { 
            res.redirect('/employee')
      });
});
};

controller.edit = (req, res) => {
  const {id} = req.params;
  const data = req.body;

  console.log(data);
  req.getConnection((err, conn) => {
    conn.query('update employee set ? where employee.id = ?',[data,id], (err, employee) => {
      res.redirect('/employee')
    });
});
};
module.exports = controller;
