const session = require("express-session");

const controller = {};

controller.list = (req, res) => {
  req.getConnection((err, conn) => {
    conn.query('SELECT b.name, bt.name as btn, ey.bank_number, ey.firstname_th as fname,ey.lastname_th as lname,pl.* FROM payment_list as pl join employee as ey on pl.employee_id = ey.id join payment_topic as pt on pl.payment_topic_id = pt.id join payment_method as pm on pl.payment_method_id = pm.id left join bank_type as bt on ey.bank_type_id = bt.id left join bank as b on ey.bank_id = b.id;', (err, payment_list)=>{
      conn.query('SELECT ps.id as psid,pt.id as ptid, DATE_FORMAT(DATE_ADD(ps.date_pay,INTERVAL 543 YEAR),"%d/%m/%Y") as date,ps.* FROM pay_save as ps inner join payment_topic as pt on ps.payment_topic_id = pt.id;', (err, pay_save) => {
        conn.query('SELECT b.name,bs.*,bt.name as nametype FROM bank as b inner join bank_salary as bs on bs.bank_id=b.id inner join bank_type as bt on bt.id=bs.bank_type_id order by bs.id asc;', (err, bank) => {
          conn.query('SELECT pm.name as pmname, DATE_FORMAT(DATE_ADD(pt.date_start,INTERVAL 543 YEAR),"%d/%m/%Y") as date_startn,DATE_FORMAT(DATE_ADD(pt.date_end,INTERVAL 543 YEAR),"%d/%m/%Y") as date_endn,DATE_FORMAT(DATE_ADD(pt.date_pay,INTERVAL 543 YEAR),"%d/%m/%Y") as date_payn,DATE_FORMAT(pt.date_pay,"%Y-%m-%d") as date_pa,pt.* FROM payment_topic as pt join payment_method as pm on pt.payment_method_id = pm.id;', (err, payment_topic) => {
            res.render('salary_management/pay_save/pay_save_list.ejs', {
              pay_save,payment_list,bank,payment_topic,session: req.session
            })
          });
        });
      });
    });
  });
};

controller.add = (req, res) => {
  const data = req.body;
  req.getConnection((err, conn) => {
    conn.query('INSERT INTO pay_save SET ?',[data], (err, pay_save) => {
      res.redirect('/pay_save')
    });
  });
};

controller.ajax = (req, res) => {
  const {id} = req.params;
  req.getConnection((err, conn) => {
    conn.query('SELECT pm.name as pname, DATE_FORMAT(DATE_ADD(pt.date_start,INTERVAL 543 YEAR),"%d/%m/%Y") as date_startn,DATE_FORMAT(DATE_ADD(pt.date_end,INTERVAL 543 YEAR),"%d/%m/%Y") as date_endn,DATE_FORMAT(pt.date_pay,"%Y-%m-%d") as date_payn,pt.* FROM payment_topic as pt join payment_method as pm on pt.payment_method_id = pm.id where pt.id = ?;',[id], (err, pay_save) => {
      conn.query('SELECT * FROM pay_save as ul inner join pay_pay_save as pul on pul.pay_save_id = ul.id where ul.id = ?;',[id], (err, pay_save_check) => {
        conn.query('SELECT b.name, bt.name as btname, ey.bank_number, ey.firstname_th as fname,ey.lastname_th as lname,pl.* FROM payment_list as pl join employee as ey on pl.employee_id = ey.id join payment_topic as pt on pl.payment_topic_id = pt.id join payment_method as pm on pl.payment_method_id = pm.id join bank_type as bt on ey.bank_type_id = bt.id join bank as b on ey.bank_id = b.id where pt.id = ?;', (err, paysave_topic) => {
          res.send({
            pay_save: pay_save,paysave_topic:paysave_topic,pay_save_check: pay_save_check,session: req.session
          });
        });
      });
    });
  });
};

controller.delcon = (req, res) => {
  const {id} = req.params;
  req.getConnection((err, conn) => {
    conn.query('delete from pay_save where pay_save.payment_topic_id = ?',[id], (err, pay_save) => {
      res.send('dew')
    });
  });
};

controller.edit = (req, res) => {
  const {id} = req.params;
  const data = req.body;
  req.getConnection((err, conn) => {
    conn.query('update pay_save set ? where pay_save.id=?',[data,id], (err, pay_save) => {
      res.redirect('/pay_save')
    });
  });
};

module.exports = controller;
