const session = require("express-session");

const controller = {};

controller.list = (req, res) => {
  req.getConnection((err, conn) => {
        conn.query('SELECT * FROM general_ledger.up_list;', (err, up_list) => { 
        res.render('salary_management/up_list/up_list_list.ejs', {
          up_list: up_list,
          session: req.session
        })
      });
    });
};
controller.add = (req, res) => {
  const data = req.body;
  console.log(data);
  req.getConnection((err, conn) => {
          conn.query('INSERT INTO up_list SET ?',[data], (err, up_list) => { 
            res.redirect('/up_list')
      });
});
};
controller.ajax = (req, res) => {
  const {id} = req.params;
  console.log(id);
  req.getConnection((err, conn) => {
    conn.query('SELECT * FROM general_ledger.up_list where id = ?;',[id], (err, up_list) => {
      conn.query('SELECT * FROM up_list as ul inner join pay_up_list as pul on pul.up_list_id = ul.id where ul.id = ?;',[id], (err, up_list_check) => {
      res.send({up_list: up_list,up_list_check: up_list_check,session: req.session})
    });
  });
});
};
controller.delcon = (req, res) => {
  const {id} = req.params;
  req.getConnection((err, conn) => {
          conn.query('delete from up_list where id = ?',[id], (err, up_list) => { 
            res.redirect('/up_list')
      });
});
};

controller.edit = (req, res) => {
  const {id} = req.params;
  const data = req.body;
  console.log(data);
  req.getConnection((err, conn) => {
    conn.query('update up_list set ? where up_list.id=?',[data,id], (err, up_list) => {
      res.redirect('/up_list')
    });
});
};
module.exports = controller;
