const session = require("express-session");

const controller = {};

controller.list = (req, res) => {
  req.getConnection((err, conn) => {
    res.render('salary_management/salary_managementlist.ejs', {
      session: req.session
    });
  });
};

module.exports = controller;
