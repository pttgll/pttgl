const session = require("express-session");
const controller = {};
const { v4: uuidv4 } = require('uuid');
const fs = require('fs');
const { Cookie } = require("express-session");


controller.human_list = (req, res) => {
    req.getConnection((err, conn) => {
        conn.query('select human_resource.*,permission.name as permission from human_resource join permission on permission.id=human_resource.permission_id ', (err, human) => {
            res.render('human_resource/human_resource_list.ejs', {
                session: req.session, human
            });
        });
    });
};

controller.human_add = (req, res) => {
    req.getConnection((err, conn) => {
        conn.query('select * from provinces', (err, provinces) => {
            conn.query('select * from permission', (err, permission) => {
                conn.query('select * from company', (err, company) => {
                    res.render('human_resource/human_resource_add.ejs', {
                        session: req.session, provinces, permission, company
                    });
                });
            });
        });
    });
};

controller.human_save = (req, res) => {
    const data = req.body;
    console.log(data);
    if (data.provinces_id == '') {
        data.provinces_id = null
    }
    if (data.amphures_id == '') {
        data.amphures_id = null
    }
    if (data.districts_id == '') {
        data.districts_id = null
    }
    if (data.pos == '') {
        data.pos = null
    }
    req.getConnection((err, conn) => {
        var token = uuidv4();
        var password2 = uuidv4();
        var password = data.password + password2;
        conn.query('INSERT INTO human_resource set email = ? , token = ?, username = ? , password = sha1(?) ,password2 = ?,permission_id = ? ,position = ?,firstname = ? ,lastname = ? ,nickname = ?,cardpeople = ? ,phone = ? ,birthday = ? ,address= ? ,provinces_id = ? , amphures_id = ? ,districts_id = ?,pos = ? ,company_id = ?;',
            [data.email, token, data.username, password, password2, data.permission_id, data.position, data.firstname, data.lastname, data.nickname, data.cardpeople, data.phone, data.birthday, data.address, data.provinces_id, data.amphures_id, data.districts_id, data.pos, data.company], (err, human) => {
                console.log(err);
                res.redirect('/human/list')
            });
    });
};


controller.human_edit = (req, res) => {
    var url = req.params;
    req.getConnection((err, conn) => {
        conn.query('select * from human_resource where id = ?', [url.id], (err, human_resource) => {
            conn.query('select * from provinces', (err, provinces) => {
                conn.query('select * from amphures where provinces_id = ?', [human_resource[0].provinces_id], (err, amphures) => {
                    console.log(err);
                    conn.query('select * from districts where amphures_id = ?', [human_resource[0].amphures_id], (err, districts) => {
                        console.log(err);

                        conn.query('select * from permission', (err, permission) => {
                            console.log(err);
                            conn.query('select * from company', (err, company) => {

                                res.render('human_resource/human_resource_edit.ejs', {
                                    session: req.session, provinces, amphures, districts, permission, human_resource, company
                                });
                            });
                        });
                    });
                });
            });
        });
    });
};




controller.human_update = (req, res) => {
    const data = req.body;
    var url = req.params;
    console.log(url);
    console.log(data);
    if (data.provinces_id == '') {
        data.provinces_id = null
    }
    if (data.amphures_id == '') {
        data.amphures_id = null
    }
    if (data.districts_id == '') {
        data.districts_id = null
    }
    if (data.pos == '') {
        data.pos = null
    }
    req.getConnection((err, conn) => {
        conn.query('select * from human_resource where id = ?', [url.id], (err, human_resource) => {
            if (data.password.length > 0) {
                var password2 = uuidv4();
                var password = data.password + password2;
                conn.query('UPDATE human_resource set email = ? , password = sha1(?) ,password2 = ?,permission_id = ?,firstname = ? , lastname = ?  , cardpeople = ? , birthday = ? , phone = ? ,address = ? ,provinces_id = ? ,amphures_id = ? ,districts_id = ? ,pos = ?,position = ? ,nickname = ? , company_id =? where id = ? ;',
                    [data.email,password, password2, data.permission_id, data.firstname, data.lastname, data.cardpeople, data.birthday, data.phone, data.address, data.provinces_id, data.amphures_id, data.districts_id, data.pos, data.position, data.nickname, data.company, url.id], (err, human) => {
                        console.log(err);
                        res.redirect('/human/list')
                    });
            } else {
                conn.query('UPDATE human_resource set email = ? , permission_id = ?,firstname = ? , lastname = ?  , cardpeople = ? , birthday = ? , phone = ? ,address = ? ,provinces_id = ? ,amphures_id = ? ,districts_id = ? ,pos = ?,position = ? ,nickname = ? ,company_id = ? where id = ? ;',
                    [data.email,data.permission_id, data.firstname, data.lastname, data.cardpeople, data.birthday, data.phone, data.address, data.provinces_id, data.amphures_id, data.districts_id, data.pos, data.position, data.nickname, data.company, url.id], (err, human) => {
                        console.log(err);
                        res.redirect('/human/list')
                    });
            }

        });
    });
};



















// ajax
controller.checkemail = (req, res) => {
    var url = req.params;
    req.getConnection((err, conn) => {
        conn.query('select * from human_resource where email= ? ;', [url.email], (err, human_resource) => {
            if (human_resource.length > 0) {
                res.send('no');
            } else {
                res.send('yes');
            }

        });
    });
}
controller.checkusername = (req, res) => {
    var url = req.params;
    req.getConnection((err, conn) => {
        conn.query('select * from human_resource where username = ? ;', [url.username], (err, human_resource) => {
            if (human_resource.length > 0) {
                res.send('no');
            } else {
                res.send('yes');
            }

        });
    });
}
module.exports = controller;