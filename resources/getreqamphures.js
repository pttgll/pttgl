$( document ).ready(function() {

	// GET REQUEST
	var amphuresValue = $('#getAmphuresOption option:selected').val();
	if (amphuresValue != undefined) {
		var amphuresChecked = amphuresValue.length > 0;
		if (amphuresChecked == false) {
			// $("#getAmphuresOption").prop('disabled', true);
		}
	}else {
		// $("#getAmphuresOption").prop('disabled', true);
	}
	$("#getResultOption").change(function(event){
		event.preventDefault();
		ajaxGet();
	});

	// DO GET
	function ajaxGet(){
		var pvid = $("#getResultOption").val();
		$.ajax({
			type : "GET",
			url : "/api/amphures/list/"+pvid,
			success: function(result){
				$('#getAmphuresOption').empty();
				$("#getDistrictsOption").empty();
				$("#changPost").val("");
				var amphuresList = "";
				$('#getAmphuresOption').append("<option value=''></option>");
				$.each(result, function(i, amphures){
					$('#getAmphuresOption').append("<option value="+amphures.id +">"+ amphures.name_th + "</option>")
				});
				// console.log("Success: ", result);
			},
			error : function(e) {
				$("#getAmphuresOption").html("<strong>Error</strong>");
				// console.log("ERROR: ", e);
			}
		});
	}

	var amphuresValue2 = $('#getAmphuresOption2 option:selected').val();
	if (amphuresValue2 != undefined) {
		var amphuresChecked2 = amphuresValue2.length > 0;
		if (amphuresChecked2 == false) {
			// $("#getAmphuresOption2").prop('disabled', true);
		}
	}else {
		// $("#getAmphuresOption2").prop('disabled', true);
	}
	$("#getResultOption2").change(function(event){
		event.preventDefault();
		ajaxGet2();
	});

	// DO GET
	function ajaxGet2(){
		var pvid = $("#getResultOption2").val();
		$.ajax({
			type : "GET",
			url : "/api/amphures/list/"+pvid,
			success: function(result){
				$('#getAmphuresOption2').empty();
				$("#getDistrictsOption2").empty();
				$("#changPost2").val("");
				var amphuresList = "";
				$('#getAmphuresOption2').append("<option value=''></option>");
				$.each(result, function(i, amphures){
					$('#getAmphuresOption2').append("<option value="+amphures.id +">"+ amphures.name_th + "</option>");
				});
				// console.log("Success: ", result);
			},
			error : function(e) {
				$("#getAmphuresOption2").html("<strong>Error</strong>");
				// console.log("ERROR: ", e);
			}
		});
	}
})
