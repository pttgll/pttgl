$( document ).ready(function() {

	// GET REQUEST
	$("#getDistrictsOption").change(function(event){
		// $("#getPost").find('input#changPost').remove().end();
		$("#getPost").find('strong').remove().end();
		event.preventDefault();
		ajaxGet();
	});

	// DO GET
	function ajaxGet(){
		var dtid = $("#getDistrictsOption").val();
		$.ajax({
			type : "GET",
			url : "/api/districts/post/"+dtid,
			success: function(result){
				$('#changPost').empty();
				var postList = "";
				$.each(result, function(i, post){
					$('input#changPost').val(post.zip_code);
				});
				// console.log("Success: ", result);
			},
			error : function(e) {
				$("input#changPost").val("Error");
				// console.log("ERROR: ", e);
			}
		});
	}

	$("#getDistrictsOption2").change(function(event){
		// $("#getPost2").find('input#changPost2').remove().end();
		$("#getPost2").find('strong').remove().end();
		event.preventDefault();
		ajaxGet2();
	});

	// DO GET
	function ajaxGet2(){
		var dtid = $("#getDistrictsOption2").val();
		$.ajax({
			type : "GET",
			url : "/api/districts/post/"+dtid,
			success: function(result){
				$('#changPost2').empty();
				var postList = "";
				$.each(result, function(i, post){
					$('input#changPost2').val(post.zip_code);
				});
				// console.log("Success: ", result);
			},
			error : function(e) {
				$("#changPost2").val("Error");
				// console.log("ERROR: ", e);
			}
		});
	}
})
