$( document ).ready(function() {

	// GET REQUEST
	var districtsValue = $('#getDistrictsOption option:selected').val();
	if (districtsValue != undefined) {
		var districtsChecked = districtsValue.length > 0;
		if (districtsChecked == false) {
			// $("#getDistrictsOption").prop('disabled', true);
		}
	}else {
		// $("#getDistrictsOption").prop('disabled', true);
	}
	$("#getAmphuresOption").change(function(event){
		event.preventDefault();
		ajaxGet();
	});

	// DO GET
	function ajaxGet(){
		var apid = $("#getAmphuresOption").val();
		$.ajax({
			type : "GET",
			url : "/api/districts/list/"+apid,
			success: function(result){
				$('#getDistrictsOption').empty();
				$("#changPost").val("");
				var districtsList = "";
				$('#getDistrictsOption').append("<option value=''></option>");
				$.each(result, function(i, districts){
					$('#getDistrictsOption').append("<option value="+districts.id +">"+ districts.name_th + "</option>");
				});
				// console.log("Success: ", result);
			},
			error : function(e) {
				$("#getDistrictsOption").html("<strong>Error</strong>");
				// console.log("ERROR: ", e);
			}
		});
	}

	var districtsValue2 = $('#getDistrictsOption2 option:selected').val();
	if (districtsValue2 != undefined) {
		var districtsChecked2 = districtsValue2.length > 0;
		if (districtsChecked2 == false) {
			// $("#getDistrictsOption2").prop('disabled', true);
		}
	}else {
		// $("#getDistrictsOption2").prop('disabled', true);
	}
	$("#getAmphuresOption2").change(function(event){
		event.preventDefault();
		ajaxGet2();
	});

	// DO GET
	function ajaxGet2(){
		var apid = $("#getAmphuresOption2").val();
		$.ajax({
			type : "GET",
			url : "/api/districts/list/"+apid,
			success: function(result){
				$('#getDistrictsOption2').empty();
				$("#changPost2").val("");
				var districtsList = "";
				$('#getDistrictsOption2').append("<option value=''></option>");
				$.each(result, function(i, districts){
					$('#getDistrictsOption2').append("<option value="+districts.id +">"+ districts.name_th + "</option>");
				});
				// console.log("Success: ", result);
			},
			error : function(e) {
				$("#getDistrictsOption2").html("<strong>Error</strong>");
				// console.log("ERROR: ", e);
			}
		});
	}
});
