$(document).ready(function () {

	$("input#glheadsDate").click(function (event) {
		$('input#shooseNo').val('');
		ajaxGetDocNo();
	});

	$("input#glheadsDate").keyup(function (event) {
		$('input#shooseNo').val('');
		ajaxGetDocNo();
	});

	// DO GET
	function ajaxGetDocNo() {
		var d = $("input#glheadsDate").val();
		$.ajax({
			type: "GET",
			url: "/api/glheads/search/" + d,
			success: function (result) {
				$('datalist.getDocNo').empty();
				var glheads = "";
				if (result.length > 0) {
					document.querySelector('input#shooseNo').setAttribute("placeholder", "กรุณาเลือก");
					$.each(result, function (i, doc) {
						$('datalist.getDocNo').append("<option data-value="+doc.id+" value=" + doc.doc_no +'::('+doc.money.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')+')'+ "></option>");
					});
				} else {
					document.querySelector('input#shooseNo').setAttribute("placeholder", "ไม่พบข้อมูล");
				}
				// console.log("Success: ", result);
			},
			error: function (e) {
				$('datalist.getDocNo').append("<option value='เกิดข้อผิดพลาด' selected></option>")
				// console.log("ERROR: ", e);
			}
		});
	}
});

//--------------------------------  gledit(this)
function gledit(gltr) {
	$('#myTbodyGetDetail').find('tr').remove().end();
	var id = gltr.id;
	$.ajax({
		type: "GET",
		url: "/api/glheads/gledit/" + id,
		success: function (result) {
			$('datalist.getDocNo option').empty();
			var glheads = "";
			$('#exampleModal').modal();
			$.each(result.glheads, function (i, glheads) {
				$('#getCompanyEdit option[value="' + glheads.company_id + '"]').prop('selected', true);
				$('#getProjectglEdit option[value="' + glheads.projectgl_id + '"]').prop('selected', true);
				var d = new Date(glheads.date);
				var d_new = d.getDate();
				if (d_new<10) {
					d_new = '0'+d_new;
				}
				var m = d.getMonth() + 1 ;
				if (m<10) {
					m = '0'+m;
				}
				var date = d.getFullYear() + '-' + m + '-' + d_new;
				$('input#getGlgeadDateEdite').val(date);
				$('#getbookEdit option[value="' + glheads.book_id + '"]').prop('selected', true);
				$('input#getDocNoEdit').val(glheads.doc_no);
				$('input#getDetailEdit').val(glheads.detail);
				$('input#idH').val(id);
				// $('#getAmphuresOption').append("<option value="+amphures.id +">"+ amphures.name_th + "</option>")
			});
			var nn = 0;
			$.each(result.gldetail, function (i, gldetail) {
				var n = i + 1;
				$('#myTbodyGetDetail').append("<tr id='etr" + n + ">' class='etrrow" + n + "'>"
					+ "<td><input list='datalistC' name='code' value='" + gldetail.code + "' id='epassf" + n + "' class='form-control inputchart' onclick=eId('" + n + "'),selecttext(this) onkeyup=eId('" + n + "')></td>"
					+ "<td><input type='text' name='name_id' value='" + gldetail.name + "' class='form-control' id='epasst" + n + "' disabled></td>"
					+ "<td><input type='text' name='cheack' value='" + gldetail.cheack + "' class='form-control'></td>"
					+ "<td><input type='date' name='date_list' value='" + gldetail.date_list + "' class='form-control'></td>"
					+ "<td align='center'><input type='text' name='dr' value='" + (gldetail.dr).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') + "' class='form-control text-right eddr exdc" + n + "' id='edr" + n + "' onchange=valueChang2('#edr" + n + "'), checkSum2() onclick=selecttext(this) onkeyup=drSum2(),checkSum2(),inputChangeddr2(this)></td>"
					+ "<td align='center'><input type='text' name='cr' value='" + (gldetail.cr).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') + "' class='form-control text-right eccr exdc" + n + "' id='ecr" + n + "' onchange=valueChang2('#edr" + n + "'), checkSum2() onclick=selecttext(this) onkeyup=crSum2(),checkSum2(),inputChangeccr2(this)><input type='hidden' name='code_id' value='" + gldetail.code_id + "' id='eqqq" + n + "'></td>"
					+ "<td align='center'><button type='button' onclick='edeleterow(this)' class='btn btn-outline-danger etrrow" + n + "'><i class=' fa fa-trash'></i></button></td></tr>");
			});
			drSum2();
			crSum2();
			checkSum2();
			// console.log("Success: ", result);
		},
		error: function (e) {
			$('#myTbodyGetDetail').append("<strong>เกิดข้อผิดพลาด</strong>");
			// console.log("ERROR: ", e);
		}
	});
}

// ---------------------------------
const opts = document.getElementById('glheadDocNo').childNodes;
const dinput = document.getElementById('shooseNo');
let eventSource = null;
let value1 = '';

dinput.addEventListener('keydown', (e) => {
  eventSource = e.key ? 'input' : 'list';
});
dinput.addEventListener('input', (e) => {
  value1 = e.target.value1;
  if (eventSource === 'list') {
		glSearchEdit(value1)
  }
});

function glSearchEdit() {
	$('#myTbodyGetDetail').find('tr').remove().end();
	var sda = $('input#glheadsDate').val();
	var doc = $('input#shooseNo').val();
	var sdo = $('#glheadDocNo option').filter(function() {
        return this.value == doc;
    }).data('value');
	$.ajax({
		type: "GET",
		url: "/api/glheads/glsearchedit/" + sda + "/" + sdo,
		success: function (result) {
			$('datalist.getDocNo option').empty();
			var glheads = "";
			$('#exampleModal').modal();
			$.each(result.glheads, function (i, glheads) {
				$('#getCompanyEdit option[value="' + glheads.company_id + '"]').prop('selected', true);
				$('#getProjectglEdit option[value="' + glheads.projectgl_id + '"]').prop('selected', true);
				var d = new Date(glheads.date);
				var d_new = d.getDate();
				if (d_new<10) {
					d_new = '0'+d_new;
				}
				var m = d.getMonth() + 1 ;
				if (m<10) {
					m = '0'+m;
				}
				var date = d.getFullYear() + '-' + m + '-' + d_new;

				$('input#getGlgeadDateEdite').val(date);
				$('#getbookEdit option[value="' + glheads.book_id + '"]').prop('selected', true);
				$('input#getDocNoEdit').val(glheads.doc_no);
				$('input#getDetailEdit').val(glheads.detail);
			});
			$.each(result.gldetail, function (i, gldetail) {
				var n = i + 1;
				$('#myTbodyGetDetail').append("<tr id='etr" + n + ">' class='etrrow" + n + "'>"
					+ "<td><input list='datalistC' name='code' value='" + gldetail.code + "' id='epassf" + n + "' class='form-control inputchart' onclick=eId('" + n + "') onkeyup=eId('" + n + "')></td>"
					+ "<td><input type='text' name='name_id' value='" + gldetail.name + "' class='form-control' id='epasst" + n + "' disabled></td>"
					+ "<td><input type='text' name='cheack' value='" + gldetail.cheack + "' class='form-control'></td>"
					+ "<td><input type='date' name='date_list' value='" + gldetail.date_list + "' class='form-control'></td>"
					+ "<td align='center'><input type='text' name='dr' value='" + (gldetail.dr).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') + "' class='form-control text-right eddr exdc" + n + "' id='edr" + n + "' onclick='drChang2(), checkSum2(),selectFocusddr2(this),select()' onkeyup='drSum2(), checkSum2(),inputChangeddr2(this)'></td>"
					+ "<td align='center'><input type='text' name='cr' value='" + (gldetail.cr).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') + "' class='form-control text-right eccr exdc" + n + "' id='ecr" + n + "' onclick='crChang2(), checkSum2(),selectFocusccr2(this),select()' onkeyup='crSum2(), checkSum2(),inputChangeccr2(this)'><input type='hidden' name='code_id' value='" + gldetail.code_id + "' id='eqqq" + n + "'></td>"
					+ "<td><button type='button' onclick='edeleterow(this)' class='btn btn-outline-danger etrrow" + n + "'><i class=' fa fa-trash'></i></button></td></tr>");
			});
			drSum2();
			crSum2();
			checkSum2();
		},
		error: function (e) {
			$('#myTbodyGetDetail').append("<strong>เกิดข้อผิดพลาด</strong>");
			// console.log("ERROR: ", e);
		}
	});
}

$("form#gleditseve").submit(function (event) {
	var datax = $(this).serializeArray();
	$.ajax({
		type: "Post",
		contentType: "application/json",
		url: "/api/glheads/gleditsave/",
		data: JSON.stringify(datax),
		dataType: "json",
		success: function (result) {
			$('#exampleModal').modal('hide');
		},
		error: function (e) {
			$('datalist.getDocNo').append("<option value='เกิดข้อผิดพลาด' selected></option>")
			console.log("ERROR: ", e);
		}

	});
	event.preventDefault();
});
