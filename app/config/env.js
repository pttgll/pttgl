const env = {
  database: 'general_ledger',
  username: 'gl2021admin',
  password: 'gl2021Passw0rd',
  host: 'database-ptt.ce9imirwsreh.ap-southeast-1.rds.amazonaws.com',
  dialect: 'mysql',
  pool: {
	  max: 5,
	  min: 0,
	  acquire: 30000,
	  idle: 10000
  }
};

module.exports = env;
