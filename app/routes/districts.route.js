module.exports = function(app) {

	var express = require("express");
	var router = express.Router();

    const districts = require('../controllers/districts.controller.js');

	var path = __basedir + '/views/';

	// router.use(function (req,res,next) {
	// 	console.log("/" + req.method);
	// 	next();
	// });

	// app.get('/province', (req,res) => {
	// 	res.sendFile(path + "province.html");
	// });

    // Save a User to MySQL
    // app.post('/api/users/save', users.save);

    // Retrieve all Users
    app.get('/api/districts/list/:apid', districts.List);
		app.get('/api/districts/post/:dtid', districts.GetPost);

	// app.use("/",router);

	// app.use("*", (req,res) => {
	// 	res.sendFile(path + "404.html");
	// });
}
