module.exports = function (app) {

	var express = require("express");
	var router = express.Router();

	const glheads = require('../controllers/glsearch.controller.js');
	const gldetail = require('../controllers/gidetail.controller.js');
	var path = __basedir + '/views/';
	// app.get('/api/glheads/search/:d', glheads.getDocNo);
	app.get('/api/glheads/search/:d', gldetail.getDocNo);
	app.get('/api/glheads/repeatedly/:dn', glheads.repeatedly);
	app.get('/api/glheads/gledit/:id', gldetail.list);
	app.post('/api/glheads/gleditsave', gldetail.editsave);
	app.get('/api/glheads/glsearchedit/:sda/:sdo', gldetail.searchByDoc);
}
