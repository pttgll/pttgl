module.exports = function(app) {

	var express = require("express");
	var router = express.Router();

    const amphures = require('../controllers/amphures.controller.js');

	var path = __basedir + '/views/';

	// router.use(function (req,res,next) {
	// 	console.log("/" + req.method);
	// 	next();
	// });

	// app.get('/province', (req,res) => {
	// 	res.sendFile(path + "province.html");
	// });

    // Save a User to MySQL
    // app.post('/api/users/save', users.save);

    // Retrieve all Users
    app.get('/api/amphures/list/:pvid', amphures.List);

	// app.use("/",router);

	// app.use("*", (req,res) => {
	// 	res.sendFile(path + "404.html");
	// });
}
