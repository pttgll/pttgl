module.exports = (sequelize, Sequelize) => {
	const Districts = sequelize.define('districts', {
	  zip_code: {
		  type: Sequelize.STRING
	  },
	  name_th: {
		  type: Sequelize.STRING
	  },
		name_en: {
		  type: Sequelize.STRING
	  },
		amphures_id: {
		  type: Sequelize.INTEGER,
			foreignKey: 'amphures.id'
	  }
	});

	return Districts;
}
