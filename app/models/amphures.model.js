module.exports = (sequelize, Sequelize) => {
	const Amphures = sequelize.define('amphures', {
	  code: {
		  type: Sequelize.INTEGER
	  },
	  name_th: {
		  type: Sequelize.STRING
	  },
		name_en: {
		  type: Sequelize.STRING
	  },
		provinces_id: {
		  type: Sequelize.INTEGER,
			foreignKey: 'provinces.id'
	  }
	});

	return Amphures;
}
