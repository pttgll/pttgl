module.exports = (sequelize, Sequelize) => {
	const Glheads = sequelize.define('glheads', {
	  id: {
		  type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
	  },
    book_id: {
		  type: Sequelize.INTEGER,
      foreignKey: 'book.id'
	  },
	  date: {
		  type: Sequelize.STRING
	  },
		doc_no: {
		  type: Sequelize.STRING
	  },
		detail: {
		  type: Sequelize.STRING,
	  },
    company_id: {
		  type: Sequelize.INTEGER,
      foreignKey: 'company.id'
	  },
    module_id: {
		  type: Sequelize.INTEGER,
      foreignKey: 'module.id'
	  },
    projectgl_id: {
		  type: Sequelize.INTEGER,
      foreignKey: 'projectgl.id'
	  }
	});

	return Glheads;
}
