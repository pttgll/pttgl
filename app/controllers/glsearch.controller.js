const db = require('../config/db.config.js');
const Glheads = db.glheads;
// const Gldetail = db.gldetail;

exports.getDocNo = (req, res) => {
	Glheads.findAll({
		attributes: ['doc_no'],
		where:{date:req.params.d},
		order: [['date','DESC']]
  }).then(glheads => {
	   res.send(glheads);
	});
};

exports.repeatedly = (req, res) => {
	Glheads.findAll({
		attributes: ['doc_no'],
		where:{doc_no:req.params.dn}
  }).then(glheads => {
	   res.send(glheads);
	});
};


// exports.gledit = (req, res) => {
// 	Gldetail.findAll({
// 		attributes: ['doc_no'],
// 		where:{doc_no:req.params.id}
//   }).then(glheads => {
// 	   res.send(glheads);
// 	});
// };
