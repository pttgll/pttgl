const controller = {};

controller.list = (req, res) => {
  const { id } = req.params;
  req.getConnection((err, conn) => {
    conn.query('select * from glheads where id = ?', [id], (err, glheads) => {
      conn.query('select gldetail.*, name, code from gldetail join chart on gldetail.code_id = chart.id where glheads_id =? ', [id], (err, gldetail) => {
        res.send({ gldetail: gldetail, glheads: glheads });
      });
    });
  });
}

controller.searchByDoc = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('select * from glheads where date = ? and id = ?', [url.sda, url.sdo], (err, glheads) => {
      conn.query('select gldetail.*, name, code from gldetail join chart on gldetail.code_id = chart.id join glheads on glheads_id = glheads.id where glheads.date = ? and glheads.id = ? ', [url.sda, url.sdo], (err, gldetail) => {
        res.send({ gldetail: gldetail, glheads: glheads });
      });
    });
  });
}

controller.getDocNo = (req,res) =>{
  var url = req.params;
  req.getConnection((err,conn)=>{
    conn.query('select glheads.*, sum(gldetail.dr) as money from glheads join gldetail on glheads_id = glheads.id where glheads.date = ? group by glheads_id order by glheads.date desc',[url.d],(err,glheads)=>{
      res.send(glheads);
    });
  });
}

controller.editsave = (req, res) => {
  const data = req.body;
  var glH = [{ book_id: '', date: '', doc_no: '', detail: '', company_id: '', module_id: '', projectgl_id: '' }]
  var idglH = 0;
  var glD = [{ code_id: [], cheack: [], date_list: [], dr: [], cr: [] }]

  for (let i = 0; i < data.length; i++) {
    if (data[i].name == 'idH') {
      idglH = data[i].value;
    } else if (data[i].name == 'company_id') {
      glH[0].company_id = data[i].value;
    } else if (data[i].name == 'prijectgl_id') {
      glH[0].prijectgl_id = data[i].value;
    } else if (data[i].name == 'date') {
      glH[0].date = data[i].value;
    } else if (data[i].name == 'book_id') {
      glH[0].book_id = data[i].value;
    } else if (data[i].name == 'doc_no') {
      glH[0].doc_no = data[i].value;
    } else if (data[i].name == 'detail') {
      glH[0].detail = data[i].value;
    } else if (data[i].name == 'code_id') {//+++++++++++++++++++++++++++++++++++++  detail  +++++++++++
      glD[0].code_id.push(data[i].value);
    } else if (data[i].name == 'cheack') {
      glD[0].cheack.push(data[i].value);
    } else if (data[i].name == 'date_list') {
      glD[0].date_list.push(data[i].value);
    } else if (data[i].name == 'dr') {
      glD[0].dr.push(data[i].value);
    } else if (data[i].name == 'cr') {
      glD[0].cr.push(data[i].value);
    }
  }

  req.getConnection((err, conn) => {
    // res.send("hello2");
    conn.query("UPDATE glheads SET book_id = ? ,date = ? ,doc_no = ? ,detail = ? ,company_id = ? ,module_id = ? ,projectgl_id = ? WHERE id = ?", [glH[0].book_id, glH[0].date, glH[0].doc_no, glH[0].detail, glH[0].company_id, 1, glH[0].prijectgl_id, idglH], (err, glheads) => {
      conn.query("DELETE FROM gldetail WHERE glheads_id = ? ;", [idglH], (err, delgldetail) => {});
      for (let i = 0; i < glD[0].code_id.length; i++) {
        conn.query("INSERT INTO gldetail (code_id ,cheack ,date_list ,dr ,cr ,glheads_id) values (?,?,?,?,?,?)", [glD[0].code_id[i], glD[0].cheack[i], glD[0].date_list[i], (glD[0].dr[i]).replace(/,/g, ''), (glD[0].cr[i]).replace(/,/g, ''), idglH], (err, gldetail) => {
          console.log(err);
        });
      }
      conn.query("select gh.*,sum(gd.cr) as sum,book.name as book ,CONCAT(date_format(gh.date, '%d-%m-'),(date_format(gh.date, '%Y')+543)) as dates, branchNo from glheads as gh join gldetail as gd on gd.glheads_id = gh.id join book on gh.book_id = book.id join company as com on gh.company_id = com.id where gh.id = ?", [idglH], (err, gldetail) => {
        res.send(gldetail);
      });
    });
  });
}


module.exports = controller;
