const db = require('../config/db.config.js');
const Amphures = db.amphures;

// Save FormData - User to MySQL
// exports.save = (req, res) => {
// 	console.log('Post a Customer: ' + JSON.stringify(req.body));
//
// 	User.create({
// 		firstname: req.body.firstname,
// 		lastname: req.body.lastname,
// 	},{
// 		attributes: {include: ['firstname', 'lastname']}
// 	}).then(users => {
// 		res.send(users);
// 	})
// };

// Fetch all Users
exports.List = (req, res) => {
	// console.log("Get some Amphures",req.params.pvid);
	Amphures.findAll({
		attributes: ['id', 'code', 'name_th'],
		where:{provinces_id:req.params.pvid},
		order: [['name_th','ASC']]
  }).then(amphures => {
	   res.send(amphures);
	});
};
