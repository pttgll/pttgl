const db = require('../config/db.config.js');
const Districts = db.districts;

// Save FormData - User to MySQL
// exports.save = (req, res) => {
// 	console.log('Post a Customer: ' + JSON.stringify(req.body));
//
// 	User.create({
// 		firstname: req.body.firstname,
// 		lastname: req.body.lastname,
// 	},{
// 		attributes: {include: ['firstname', 'lastname']}
// 	}).then(users => {
// 		res.send(users);
// 	})
// };

// Fetch all Users
exports.List = (req, res) => {
	// console.log("Get some Districts",req.params.apid);
	Districts.findAll({
		attributes: ['id', 'zip_code', 'name_th'],
		where:{amphures_id:req.params.apid},
		order: [['name_th','ASC']]
  }).then(districts => {
	   res.send(districts);
	});
};

exports.GetPost = (req, res) => {
	// console.log("Get some Post",req.params.dtid);
	Districts.findAll({
		attributes: ['id', 'zip_code'],
		where:{id:req.params.dtid}
  }).then(post => {
	   res.send(post);
	});
};
