$('#sidenavOpen').hide();
$('#sidenavClose').show();
function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
  document.getElementById("main").style.marginLeft= "0";
  $('#sidenavOpen').show();
  $('#sidenavClose').hide();
}

function openNav() {
  document.getElementById("mySidenav").style.width = "250px";
  document.getElementById("main").style.marginLeft = "250px";
  $('#sidenavOpen').hide();
  $('#sidenavClose').show();
}
