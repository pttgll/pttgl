
$(document).ready(function () {
  if ($('#product_type_list_id').val() == 3) {
    productType3();
  } else if ($('#product_type_list_id').val() == 2) {
    productType2();
  } else {
    productType1();
  }

  $('#product_type_list_id').change(function () {

    if ($('#product_type_list_id').val() == 1) {
      productType1();
    } else if ($('#product_type_list_id').val() == 2) {
      productType2();
    } else {
      productType3();
    }
  })
  //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
})

function productType1() {
  // alert(1);
  //---------------- show -------------
  //---------------- hide -------------
  $('#product_type_id').fadeOut(150);
  $('#detailBuy').fadeOut(150);
  $('#setting').fadeOut(150);



}
function productType2() {
  // alert(2);
  //---------------- show -------------
  $('#product_type_id').fadeIn(150);
  $('#detailBuy').fadeIn(150);
  //---------------- hide -------------
  $('#setting').fadeOut(150);


}
function productType3() {
  // alert(3);
  //---------------- show -------------
  $('#setting').fadeIn(150);
  $('#detailBuy').fadeIn(150);
  //---------------- hide -------------
  $('#product_type_id').fadeOut(150);

}


// //---------------edit photo---------

// $('#divuploadphoto').hide();
// $(document).ready(function () {

// })
// //---------------edit photo---------


if ($('#divUp img').attr('src') == 'http://100dayscss.com/codepen/upload.svg') {
  $('#divUp div').hide();
  $('#close-item').hide();
} else {
  $('#divUp span:eq(1)').hide();
}


function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function (e) {
      $('#divUp img').attr('src', e.target.result);
      $('#divUp img').attr('style', 'width: 145px;height: 95px;');
      $('#divUp img').attr('class', '');
      $('#divUp input').attr('class', '');
      $('#divUp input').attr('hidden', true);
      $('#divUp span:eq(1)').hide();
    }
    reader.readAsDataURL(input.files[0]); // convert to base64 string
  }
  var numi = 1;
  if (input.files.length > 1) {
    for (i = 1; i < input.files.length; i++) {
      if (input.files[i]) {
        var reader = new FileReader();
        reader.onload = function (e) {
          $('#photos').append('	<div class="col-md-6 new">' +
            '<div class="dropzone divUpCope" id="divUp' + numi + '" >' +
            '<img src="' + e.target.result + '" style="width: 145px;height: 95px;"><br>' +
            '</div>' +
            '</div>');
          numi++;
          console.log(numi);

        }
      }

      reader.readAsDataURL(input.files[i]);
    }
  }
}

$(document).ready(function () {
  $("#imgInp").change(function () {
    if ($("#imgInp").val()) {
      $('#spanphoto').hide();
    }
    console.log($("#imgInp").val());
    $('#divUp div').show();
    $('#close-item').show();
    $('.divUpCope').remove();
    // readURL(this);



    readURL(this);




  });

  $(".close-pic").on('click', function () {
    // blankPic();
    $('.divUpCope').remove();
    $('.new').remove().end();
    $('#divUp img').attr('src', 'http://100dayscss.com/codepen/upload.svg');
    $('#divUp img').attr('style', '');
    $('#divUp img').attr('class', 'upload-icon');
    $('#divUp input').attr('type', 'file');
    $('#divUp input').attr('class', 'upload-input');
    $('#divUp input').attr('hidden', false);
    $('#divUp input').attr('value', '');
    $('#divUp span:eq(1)').show();
    $('#divUp div').hide();
    $('#close-item').hide();
    $('#imgInp').val('');
  });
});

var i, tabcontent

function activeTabp(evt, tabPd) {
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
    $("table#tablesearch").attr('id', '');
  }
  document.getElementById(tabPd).style.display = "block";
  $('#search').val('');
  $("#" + tabPd + " table:first").attr('id', 'tablesearch');
  if ($('div#' + tabPd + ' > div:first').attr('id') != 'tablesearch_wrapper') {
    dataTable();
  }
}

// end js eletric

$('#resvenueInput').hide();
function resvenue() {
  $('#resvenueInput').show();
  $('#resvenueEdit').hide();
}

$('#paymentInput').hide();
function payment() {
  $('#paymentInput').show();
  $('#paymentEdit').hide();
}


// --- datatable ----///
$(document).ready(function () {
  $('#tablesearch').DataTable({
    "order": [[0, "asc"]]
  });
});

function dataTable() {
  // $('#tablesearch_wrapper .row').remove().end()
  // $(document).ready(function() {
  $('#tablesearch').DataTable({
    "order": [[0, "asc"]]
  });
  // });
}

// inventory delete

function inventoryDelete(trId) {
  var inid = trId.split('-')[1];
  $('#delete').modal();
  $('#delete .modal-body p:eq(1) span').text($('#' + trId + ' td:eq(3)').text());
  $('#delete .modal-body p:eq(2) span').text($('#' + trId + ' td:eq(4)').text());
  $('#delete .modal-footer a').attr('href', '/product/delete/' + inid);
}

// qrcode and barcode generater
$('#codeText').keyup(function () {
  codeTextx();
});

function codeTextx() {
  $('#newbar').empty();
  $('#newbar').append('<img id="barcode"/>');
  $('#newqr').empty();
  $('#newqr').append('<div id="qrcode"></div>');
  var data = $('#codeText').val();
  if (data != "") {
    JsBarcode("#barcode", data, {
      // format: "auto",
      lineColor: "#000000",
      width: 1.5,
      height: 50,
      displayValue: true,
      background: "#ffffff"
    });
    new QRCode(document.getElementById("qrcode"), {
      text: data,
      width: 100,
      height: 100,
      colorDark: "#000000",
      colorLight: "#ffffff",
      correctLevel: QRCode.CorrectLevel.H
    });
  }
}

// $('#Bdel').hide();
var numberAddTrUnit = $('.trUnit').length;
$(document).ready(function () {
  $('#addUnitType').click(function () {

    // $('#Bdel').show();
    if (numberAddTrUnit < 1) {
      numberAddTrUnit = 1;
    }
    //var tableUnitType = (document.getElementById('tableUnitType').getElementsByTagName('tr').length + 1);
    var u1;
    if ($('#unitType' + numberAddTrUnit).val() != '') {
      u1 = $('#unitType' + numberAddTrUnit).val();
    } else {
      u1 = 'หน่วยสินค้าที่ ';
    }
    numberAddTrUnit++;
    var numberAddTrUnitmax = numberAddTrUnit + 1;
    var addUnit = "<tr  class='trBdel-" + numberAddTrUnit + "'><td><input type='text' id='unitType" + numberAddTrUnit + "' class='form-control numberAddTrUnit-" + numberAddTrUnit + "' name='nameUnit' onkeyup='unitSet(this,this.value)'" + " placeholder='หน่วยสินค้าระดับที่ " + numberAddTrUnit + "'></td><td><t>จำนวน</t></td><td><input type='number' class='form-control' mask='Number' min='-10000' max='10000' thousandsSeparator=' ' name='Unit' placeholder='จำนวน' required></td>"+
    "<td><t class='textUnitType" + numberAddTrUnit + "'>" + u1 + "</t></td></tr>" +
    "<tr class='trBdel-" + numberAddTrUnit + "'><td> <t class='textUnitType" + numberAddTrUnitmax + "'></t></td><td><t>ราคาขายA:</t></td><td><input type='text' class='form-control' name='sellA' placeholder='ราคาขายA' > </td>" +
    "<tr class='trBdel-" + numberAddTrUnit + "'><td> </td><td><t>ราคาขายB:</t></td><td><input type='text' class='form-control' name='sellB' placeholder='ราคาขายB' ></td>" +
    "<tr class='trBdel-" + numberAddTrUnit + "'><td> </td><td><t>ราคาขายC:</t></td><td> <input type='text' class='form-control' name='sellC' placeholder='ราคาขายC' ></td>";

    // var addUnit = "<tr  class='trBdel-" + numberAddTrUnit + "'><td><input type='text' id='unitType" + numberAddTrUnit + "' class='form-control numberAddTrUnit-" + numberAddTrUnit + "' name='nameUnit' onkeyup='unitSet(this,this.value)'" + " placeholder='หน่วยสินค้าระดับที่ " + numberAddTrUnit + "'></td><td><input type='number' class='form-control' mask='Number' min='-10000' max='10000' thousandsSeparator=' ' name='Unit' placeholder='จำนวน' required></td><td><t class='textUnitType" + numberAddTrUnit + "'>" + u1 + "</t></td></tr>" +
    //   "<tr class='trBdel-" + numberAddTrUnit + "'><td> <t class='textUnitType" + numberAddTrUnitmax + "'></t></td><td><input type='number' class='form-control' name='sellA' placeholder='ราคาขายA' > </td><td> <input type='number' class='form-control' name='buyA' placeholder='ราคาซื้อA' ></td></tr>" +
    //   "<tr class='trBdel-" + numberAddTrUnit + "'><td> </td><td><input type='number' class='form-control' name='sellB' placeholder='ราคาขายB' ></td><td><input type='number' class='form-control' name='buyB' placeholder='ราคาซื้อB' ></td> </tr>" +
    //   "<tr class='trBdel-" + numberAddTrUnit + "'><td></td><td> <input type='number' class='form-control' name='sellC' placeholder='ราคาขายC' ></td><td><input type='number' class='form-control' name='buyC'  placeholder='ราคาซื้อC' ></td></tr>";

    $('#tableUnitType').append(addUnit);
  });


});
function deletetr() {
  $('.trBdel-' + numberAddTrUnit).remove();
  numberAddTrUnit--;

}

function unitSet(t, g) {
  var tt = parseInt($(t).attr('class').split("-")[2]) + 1;
  if (g != '') {
    $('.textUnitType' + tt).text(g);
  } else {
    // for (var i = 0; i < $('.textUnitType'+tt).length; i++) {

    // }
    $('.textUnitType' + tt + ':eq(' + i + ')').text('หน่วยสินค้าที่ ' + tt);
  }
}

var updatetable = 0;
$(document).ready(function () {
  $('.hideproduct_group').hide();

  $('#product_group').change(function () {
    if ($('#product_group').val() == -1) {
      if ($('#productAddForm')) {
        $('#productAddForm').modal('hide');
      }
      setTimeout(
        function () {
          $('#categoryAddForm').modal('show')
        }, 500);
      getgroup();
    }
  });

  $("#submidProductgroup").click(function () {
    var data = $('#inputProductgroup').serializeArray();
    $('#categoryAddForm').modal('hide');
    $.ajax({
      type: "Post",
      contentType: "application/json",
      url: "/api/addgroup/add",
      data: JSON.stringify(data),
      dataType: "json",
      success: function (result) {
        if ($('#productAddForm')) {
          setTimeout(
            function () {
              $('#productAddForm').modal('show');
            }, 500);
        }
        updatetable = 1;
        getgroup();
      },
      error: function (e) {
        console.log(err);
      }
    });
    // event.preventDefault();
  });
});

function getgroup() {
  // alert(prid);
  $.ajax({
    type: 'GET',
    url: '/api/getgroup',
    success: function (result) {
      console.log(result);
      $('#product_group').empty();
      // $('#product_group').append('<option>กรุณาเลือก</option>');
      $.each(result, function (i, product_group) {
        $('#product_group').append('<option value=' + product_group.id + '>' + product_group.name + '</option>');
      });
      $('#product_group').append('<option value="-1">เพิ่มหมวดสินค้า</option>');
      // console.log(result);
      if (updatetable == 1) {
        var numberiii = document.getElementById('tbodyaddproduct_group').getElementsByTagName('tr').length + 1;
        console.log(numberiii);
        var textaddtr = '<tr id="trproduct_group-' + result[numberiii - 1].id + '"><td>' + numberiii + '</td><td>' +
          '<input type="text" value="' + result[numberiii - 1].name + '" onkeyup="$(' + "'" + '#showproduct_group-' + result[numberiii - 1].id + "'" + ').show()" ' +
          'id="inputupdate-' + result[numberiii - 1].id + '"></td>' +
          '<td><button type="button" onclick="updateproduct_groupAjex(this)" id="showproduct_group-' + result[numberiii - 1].id + '" class="hideproduct_group">แก้ไข</button></td>' +
          '<td><button type="button" onclick="delproduct_group(this)" id="delproduct_group-' + result[numberiii - 1].id + '">ลบ</button></td></tr>'
        $('#tbodyaddproduct_group').append(textaddtr)
        $('#showproduct_group-' + result[numberiii - 1].id).hide();
        updatetable = 0;
      }
    },
    error: function (e) {
      console.log(e);
    }
  });
}

$('#categoryAddForm .close').click(function () {
  $('#categoryAddForm').modal('hide');
  if ($('#productAddForm')) {
    setTimeout(
      function () {
        $('#productAddForm').modal('show');
      }, 500);
  }
});


function updateproduct_groupAjex(button) {
  var id = button.id.split('-')[1]
  var va = $('#inputupdate-' + id).val();
  $.ajax({
    type: 'GET',
    url: '/api/updategroup/' + id + '/' + va,
    success: function (result) {
      $('#showproduct_group-' + id).hide();
      getgroup();
    },
    error: function (e) {
      console.log(e);
    }
  });
}


function delproduct_group(button) {
  var id = button.id.split('-')[1]
  $.ajax({
    type: 'GET',
    url: '/api/delgroup/' + id,
    success: function (result) {
      if (result == 'success') {
        $('#trproduct_group-' + id).hide();
      } else {
        alert("xx")
      }
      getgroup();
    },
    error: function (e) {
    }
  });
}

$(document).ready(function () {
  $('#chbar').click(function () {
    $('#chbar2').toggle();
  })
  $('#chqr').click(function () {
    $('#chqr2').toggle();
  })
})


$(document).ready(function () {
  $('.showphotomain').click(function () {
    var idpdmain =this.id.split('-')[1];
    var idphotomain =this.id.split('-')[2];
    $.ajax({
      type: 'GET',
      url: '/api/product/changephotomain/'+idpdmain+'/' + idphotomain,
      success: function (result) {
        $('.showphotomain').prop('checked',false)
        $('#photomain-'+idpdmain+'-'+idphotomain).prop('checked',true)
      },
      error: function (e) {
      }
    });
  })
})

function deletephoto(params) {
  var idpd =params.id.split('-')[1];
    var idphoto =params.id.split('-')[2];
    console.log(idpd);
    console.log(idphoto);
  $.ajax({
    type: 'GET',
    url: '/api/product/deletephoto/'+idpd+'/' + idphoto,
    success: function (result) {
      $('#divshow_photo-'+idphoto).hide();
    },
    error: function (e) {
    }
  });
}