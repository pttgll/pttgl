
/**
 *
 * @param {number} num
 *
 * @returns {string}
 */
 const getId = (num) => {
    return num.toString().padStart(4, "0")
  };
var newnumber = getId(pn + 1); //ตัวเลขวันที่
var module  = module[0].char;
var maxnumber = taxinvoicenumber[0].maxnumber;
if (newnumber > maxnumber) {
  $('#taxinvoicechar').val(module+runnumber[0].number)
  $('#taxinvoicenumber').text(module +runnumber[0].number+ newnumber);
  $('#taxinvoicenumberin').val(newnumber);
}
  
  
  
  //show contact popup
  function contact(id) {
    $('#' + id).modal();
  }
  $('#inputpopup').change(function () {
    var val = $('#inputpopup').val();
    var id = $('#debtorslist option').filter(function () {
      return this.value == val;
    }).data('id');
    $('#debtorsditpopuplink').empty();
    for (var i = 0; i < debtors.length; i++) {
      if (debtors[i].id == id) {
        $('#debtorsaddress').text(debtors[i].districts + ' ' + debtors[i].amphures + ' ' + debtors[i].provinces);
        $('#debtorszipcode').val(debtors[i].zip_code);
        $('#debtorstax').val(debtors[i].tax);
        $('#debtorsbranch').val(debtors[i].branch);
        $('#debtorsbranch').val(debtors[i].branchID + " สาขา" + debtors[i].branchname);
        $('#debtorsdata').val(debtors[i].crday);
        $('#debid').val(id);
  
      }
  
    }
    setDue();
    if (val != "") {
      $('#debtorsditpopuplink').append('<a href="#" data-toggle="modal" data-target="#popup" onclick="debtorsPopup(' + "edit_" + id + "'" + ')"><i class="fa fa-edit"></i>แก้ไขข้อมูลลูกหนี้</a>');
    }
  });
  
  function indicate(r) {
    var id = r.split('_')[1];
    $('#indicatetext').text($('#' + r).text() + ':');
    $('#indicateid').val(id);
    if (id != 1) {
      $('#duegroup').hide();
      $('#duedata').attr('disabled', true);
    } else {
      $('#duegroup').show();
      $('#duedata').attr('disabled', false);
    }
  }
  
  function setDue() {
    if ($('#indicateid').val() == 1) {
      var d = new Date($('#datedata').val());
      var date = new Date($('#datedata').val());
      date.setDate(date.getDate() + (+$('#debtorsdata').val()));
      $('#duedata').val(date.toISOString().split('T')[0]);
    }
  }
  
  function setDay() {
    if ($('#indicateid').val() == 1) {
      var date = new Date($('#datedata').val());
      var due = new Date($('#duedata').val());
      if (date < due) {
        $('#debtorsdata').val((due - date) / (60000 * 60 * 24));
      } else {
        $('#datedata').val(due.toISOString().split('T')[0]);
        $('#duedata').val(due.toISOString().split('T')[0]);
        $('#debtorsdata').val(0);
      }
    }
  }
  
  
  
  // setting compute
  function settingcompute() {
    if ($('#computevatlist').prop('checked') == true) {
      $('#computediscountlist').prop('checked', true);
      $('#computediscountlist').prop('checked', true);
      $('.vatlist').prop('hidden', false);
      $('.discountlist').prop('hidden', false);
      $('#sumdiscount b:eq(0)').text('ส่วนลดรวม');
      $('#sumdiscount b:eq(1)').prop('hidden', true);
      $('#sumdiscount input').prop('hidden', true);
      $('#sumdiscount input:eq(1)').prop('hidden', true);
      $('#sumdiscount b:eq(2)').prop('hidden', false);
      $('#computevat1').empty();
      $('#computevat1').append('<div class="input-group">' +
        '<div class="col-8">' +
        '<div class="input-group">' +
        '<input class="form-check" type="checkbox" name="vat" value="7" id="ordervat" onclick="computeList()">' +
        '<b>ภาษีมูลค่าเพิ่ม</b>' +
        '<input type="hidden" name="vat" value="">' +
        '</div>' +
        '</div>' +
        '<div class="col-4 text-right">' +
        '<b id="computevat">0.00</b>' +
        '<input type="hidden" name="vatamount" value="" id="computevatin">' +
        '</div>' +
        '</div>');
      $('#computevat2').empty();
      $('#ordervat').prop('hidden', true);
      $('.allvattr').prop('hidden', false);
      $('.vatlistset').prop('hidden', false);
      $('.withholding').prop('hidden', true);
    } else if ($('#computevatlist').prop('checked') == false && $('#computediscountlist').prop('checked') == true) {
      $('#computediscountlist').prop('disabled', false);
      $('.vatlist').prop('hidden', true);
      $('.discountlist').prop('hidden', false);
      $('#sumdiscount b:eq(0)').text('ส่วนลดรวม');
      $('#sumdiscount b:eq(1)').prop('hidden', true);
      $('#sumdiscount input').prop('hidden', true);
      $('#sumdiscount input:eq(1)').prop('hidden', true);
      $('#sumdiscount b:eq(2)').prop('hidden', false);
      if ($('#compute').val() == 1) {
        $('#computevat1').empty();
        $('#computevat1').append('<div class="input-group">' +
          '<div class="col-8">' +
          '<div class="input-group">' +
          '<input class="form-check" type="checkbox" name="vat" value="7" id="ordervat" checked onclick="computeList()">' +
          '<b>ภาษีมูลค่าเพิ่ม</b>' +
          '</div>' +
          '</div>' +
          '<div class="col-4 text-right">' +
          '<b id="computevat">0.00</b>' +
          '<input type="hidden" name="vatamount" value="" id="computevatin">' +
          '</div>' +
          '</div>');
        $('#computevat2').empty();
        $('#notplusvat').prop('hidden', true);
      } else if ($('#compute').val() == 2) {
        $('#computevat1').empty();
        $('#computevat2').empty();
        $('#computevat2').append('<hr><div class="input-group">' +
          '<div class="col-8">' +
          '<div class="input-group">' +
          '<input class="form-check" type="checkbox" name="vat" value="7" id="ordervat" onclick="computeList()">' +
          '<b>ภาษีมูลค่าเพิ่ม</b>' +
          '</div>' +
          '</div>' +
          '<div class="col-4 text-right">' +
          '<b id="computevat">0.00</b>' +
          '<input type="hidden" name="vatamount" value="" id="computevatin">' +
          '</div>' +
          '</div>');
        $('#notplusvat').prop('hidden', false);
        $('#ordervat').prop('hidden', true);
      }
      $('.vatlistset').prop('hidden', true);
      $('.withholding').prop('hidden', false);
    } else {
      $('.vatlist').prop('hidden', true);
      $('.discountlist').prop('hidden', true);
      $('#sumdiscount b:eq(0)').text('ส่วนลด');
      $('#sumdiscount b:eq(1)').prop('hidden', false);
      $('#sumdiscount input').prop('hidden', false);
      $('#sumdiscount input:eq(1)').prop('hidden', false);
      $('#sumdiscount b:eq(2)').prop('hidden', true);
      if ($('#compute').val() == 1) {
        $('#computevat1').empty();
        $('#computevat1').append('<div class="input-group">' +
          '<div class="col-8">' +
          '<div class="input-group">' +
          '<input class="form-check" type="checkbox" name="vat" value="7" id="ordervat" onclick="computeList()">' +
          '<b>ภาษีมูลค่าเพิ่ม</b>' +
          '</div>' +
          '</div>' +
          '<div class="col-4 text-right">' +
          '<b id="computevat">0.00</b>' +
          '<input type="hidden" name="vatamount" value="" id="computevatin">' +
          '</div>' +
          '</div>');
        $('#computevat2').empty();
        $('#notplusvat').prop('hidden', true);
      } else if ($('#compute').val() == 2) {
        $('#computevat1').empty();
        $('#computevat2').empty();
        $('#computevat2').append('<hr><div class="input-group">' +
          '<div class="col-8">' +
          '<div class="input-group">' +
          '<input class="form-check" type="checkbox" name="vat" value="7" id="ordervat" checked disabled onclick="computeList()">' +
          '<b>ภาษีมูลค่าเพิ่ม</b>' +
          '</div>' +
          '</div>' +
          '<div class="col-4 text-right">' +
          '<b id="computevat">0.00</b>' +
          '<input type="hidden" name="vatamount" value="" id="computevatin">' +
          '</div>' +
          '</div>');
        $('#notplusvat').prop('hidden', false);
      }
      $('.vatlistset').prop('hidden', true);
      $('.withholding').prop('hidden', false);
    }
  }
  
  
  // คำนวนราคาสินค้าแต่ละรายการ
  // calculator function for compute 
  
  function computeList() {
    var setdiscount, quantity, price, discount = 0, vat = 0, computeprice = 0,
      total = 0, sumdiscount = 0, afterdiscount = 0, granttotal = 0,
      devat = 0, ordervat = 0, computevat = 0, sumvat = 0, withholdingC = 0,
      computewitholding = 0, notplusvat = 0, vatexamption = 0, vatcalculator = 0;
    setdiscount = $('#setdiscount').val();
    $.each(tr, function (i, el) {
      var getId = $(tr[i]).attr('id');
      quantity = (+$('tr#' + getId + ' td:eq(2) input').val());
      price = (+$('tr#' + getId + ' td:eq(4) input').val());
      total += (quantity * price);
  
      if ($('#computevatlist').prop('checked') == true) {
        discount = (+$('tr#' + getId + ' td:eq(5) input').val());
        vat = (+$('tr#' + getId + ' td:eq(6) select').val());
        if ($('#compute').val() == 1) {
          if ($('#setdiscount').val() == 1) {
            computeprice = ((quantity * price) - (((quantity * price) / 100) * discount));
            sumdiscount += ((quantity * price) / 100) * discount;
          } else if ($('#setdiscount').val() == 2) {
            computeprice = (quantity * price - discount);
            sumdiscount += (+discount);
          }
          if (vat == 0 || vat == 'null') {
            vatexamption += computeprice;
          } else if (vat > 0) {
            vatcalculator += computeprice;
            computevat += computeprice / 100 * vat;
          }
        } else if ($('#compute').val() == 2) {
          if ($('#setdiscount').val() == 1) {
            computeprice = ((quantity * price) - (((quantity * price) / 100) * discount));
            sumdiscount += ((quantity * price) / 100) * discount;
          } else if ($('#setdiscount').val() == 2) {
            computeprice = (quantity * price - discount);
            sumdiscount += discount;
          }
          if (vat == 0 || vat == 'null') {
            vatexamption += computeprice;
          } else if (vat > 0) {
            computevat += (computeprice * vat) / (100 + vat);
            vatcalculator += (computeprice - ((computeprice * vat) / (100 + vat)));
          }
        }
        $('tr#' + getId + ' td:eq(7) input').val(computeprice.toFixed(2));
      } else if ($('#computevatlist').prop('checked') == false && $('#computediscountlist').prop('checked') == true) {
        discount = (+$('tr#' + getId + ' td:eq(5) input').val());
        $('tr#' + getId + ' td:eq(6) select').val(0);
        if ($('#compute').val() == 1) {
          if ($('#setdiscount').val() == 1) {
            computeprice = ((quantity * price) - (((quantity * price) / 100) * discount));
            sumdiscount += ((quantity * price) / 100) * discount;
          } else if ($('#setdiscount').val() == 2) {
            computeprice = (quantity * price - discount);
            sumdiscount += discount;
          }
        } else if ($('#compute').val() == 2) {
          if ($('#setdiscount').val() == 1) {
            computeprice = ((quantity * price) - (((quantity * price) / 100) * discount));
            sumdiscount += ((quantity * price) / 100) * discount;
          } else if ($('#setdiscount').val() == 2) {
            computeprice = (quantity * price - discount);
            sumdiscount += discount;
          }
        }
        $('tr#' + getId + ' td:eq(7) input').val(computeprice.toFixed(2));
      } else {
        $('tr#' + getId + ' td:eq(5) input').val(0);
        $('tr#' + getId + ' td:eq(6) select').val(0);
        computeprice = (quantity * price);
        $('tr#' + getId + ' td:eq(7) input').val(computeprice.toFixed(2));
      }
    });
  
    if ($('#computevatlist').prop('checked') == true) {
  
      if ($('#compute').val() == 1) {
        granttotal = (vatexamption + vatcalculator + computevat);
      } else if ($('#compute').val() == 2) {
        granttotal = (vatexamption + vatcalculator + computevat);
      }
  
    } else if ($('#computevatlist').prop('checked') == false && $('#computediscountlist').prop('checked') == true) {
      if ($('#compute').val() == 1) {
        if ($('#ordervat').prop('checked') == true) {
          vat = (+$('#ordervat').val());
          computevat = (total - sumdiscount) / 100 * vat;
        } else if ($('#ordervat').prop('checked') == false) {
          computevat = 0;
        }
        granttotal = (total - sumdiscount) + computevat;
        if ($('#withholdingC').prop("checked") == true) {
          withholdingC = (+$('#witholdingoption').val());
          computewitholding = (total - sumdiscount) / 100 * withholdingC;
        } else if ($('#withholdingC').prop("checked") == false) {
          withholdingC = 0;
          computewitholding = 0;
        }
      } else if ($('#compute').val() == 2) {
        vat = (+$('#ordervat').val());
        computevat = ((total - sumdiscount) * vat) / (100 + vat);
        granttotal = total - sumdiscount;
        notplusvat = (total - sumdiscount) - computevat;
        if ($('#withholdingC').prop("checked") == true) {
          withholdingC = (+$('#witholdingoption').val());
          computewitholding = notplusvat / 100 * withholdingC;
        } else if ($('#withholdingC').prop("checked") == false) {
          withholdingC = 0;
          computewitholding = 0;
        }
      }
    } else {
      if ($('#compute').val() == 1) {
        if ($('#discountM').val() > 0) {
          discount = (+$('#discountM').val());
          sumdiscount = discount;
        } else if ($('#discountP').val() > 0) {
          discount = (+$('#discountP').val());
          sumdiscount = total / 100 * discount;
        }
        vat = (+$('#ordervat').val());
        $('#discountM').val(sumdiscount.toFixed(2));
        if ($('#ordervat').prop('checked') == true) {
          computevat = (total - sumdiscount) / 100 * vat;
        } else if ($('#ordervat').prop('checked') == false) {
          computevat = 0;
        }
        granttotal = (total - sumdiscount) + computevat;
        if ($('#withholdingC').prop("checked") == true) {
          withholdingC = (+$('#witholdingoption').val());
          computewitholding = (total - sumdiscount) / 100 * withholdingC;
        } else if ($('#withholdingC').prop("checked") == false) {
          withholdingC = 0;
          computewitholding = 0;
        }
      } else if ($('#compute').val() == 2) {
        if ($('#discountM').val() > 0) {
          discount = (+$('#discountM').val());
          sumdiscount = discount;
        } else if ($('#discountP').val() > 0) {
          discount = (+$('#discountP').val());
          sumdiscount = total / 100 * discount;
        }
        vat = (+$('#ordervat').val());
        $('#discountM').val(sumdiscount.toFixed(2));
        computevat = ((total - sumdiscount) * vat) / (100 + vat);
        granttotal = total - sumdiscount;
        notplusvat = (total - sumdiscount) - computevat;
        if ($('#withholdingC').prop("checked") == true) {
          withholdingC = (+$('#witholdingoption').val());
          computewitholding = notplusvat / 100 * withholdingC;
        } else if ($('#withholdingC').prop("checked") == false) {
          withholdingC = 0;
          computewitholding = 0;
        }
      }
    }
  
    afterdiscount = (total - sumdiscount);
    $('#total').text(total.toFixed(2).replace( /(\d)(?=(\d{3})+(?!\d))/g, '$1,' ));
    $('#totalin').val(total.toFixed(2).replace( /(\d)(?=(\d{3})+(?!\d))/g, '$1,' ));
  
    $('#sumdiscount b:eq(2)').text(sumdiscount.toFixed(2).replace( /(\d)(?=(\d{3})+(?!\d))/g, '$1,' ));
    $('#vatexamption b:eq(1)').text(vatexamption.toFixed(2).replace( /(\d)(?=(\d{3})+(?!\d))/g, '$1,' ));
    $('#vatexamption input').val(vatexamption.toFixed(2).replace( /(\d)(?=(\d{3})+(?!\d))/g, '$1,' ));
  
    $('#vatcalculator b:eq(1)').text(vatcalculator.toFixed(2).replace( /(\d)(?=(\d{3})+(?!\d))/g, '$1,' ));
    $('#vatcalculator input').val(vatcalculator.toFixed(2).replace( /(\d)(?=(\d{3})+(?!\d))/g, '$1,' ));
  
    $('#afterdiscount').text(afterdiscount.toFixed(2).replace( /(\d)(?=(\d{3})+(?!\d))/g, '$1,' ));
    $('#afterdiscountin').val(afterdiscount.toFixed(2).replace( /(\d)(?=(\d{3})+(?!\d))/g, '$1,' ));
  
    $('#computevat').text(computevat.toFixed(2).replace( /(\d)(?=(\d{3})+(?!\d))/g, '$1,' ));
    $('#computevatin').val(computevat.toFixed(2).replace( /(\d)(?=(\d{3})+(?!\d))/g, '$1,' ));
  
    $('#granttotal').text(granttotal.toFixed(2).replace( /(\d)(?=(\d{3})+(?!\d))/g, '$1,' ));
    $('#granttotalin').val(granttotal.toFixed(2).replace( /(\d)(?=(\d{3})+(?!\d))/g, '$1,' ));
  
    $('#topgranttotal').text(granttotal.toFixed(2).replace( /(\d)(?=(\d{3})+(?!\d))/g, '$1,' ));
    $('#notplusvat b:eq(1)').text(notplusvat.toFixed(2).replace( /(\d)(?=(\d{3})+(?!\d))/g, '$1,' ));
    $('#notplusvat input').val(notplusvat.toFixed(2).replace( /(\d)(?=(\d{3})+(?!\d))/g, '$1,' ));
  
    $('#computewitholding').text(computewitholding.toFixed(2).replace( /(\d)(?=(\d{3})+(?!\d))/g, '$1,' ));
    $('#computewitholdingin').val(computewitholding.toFixed(2).replace( /(\d)(?=(\d{3})+(?!\d))/g, '$1,' ));
  
    $('#paymoney').text((granttotal - computewitholding).toFixed(2).replace( /(\d)(?=(\d{3})+(?!\d))/g, '$1,' ));
    $('#paymoneyin').val((granttotal - computewitholding).toFixed(2).replace( /(\d)(?=(\d{3})+(?!\d))/g, '$1,' ));
  }
  
  
  function setDiscountValue(id) {
    $('#' + id).val('0.00');
    if (id == 'discountM') {
      $('#discountM').attr('type', 'hidden');
      $('#sumdiscount i').prop('hidden', false);
      $('#sumdiscount b:eq(2)').prop('hidden', false);
      $('#sumdiscount b:eq(2)').text('0.00');
    } else if (id == 'discountP') {
      $('#discountM').attr('type', 'hidden');
      $('#sumdiscount i').prop('hidden', false);
      $('#sumdiscount b:eq(2)').prop('hidden', false);
    }
  }
  
  
  function inputdiscount() {
    $('#discountM').attr('type', 'text');
    $('#sumdiscount i').prop('hidden', true);
    $('#sumdiscount b:eq(2)').prop('hidden', true);
  }
  
  // set id for product_id
  function productidset(evt, trid) {
    var val = $('#' + trid + ' .productidset').val();
    var id = $('#productName option').filter(function () {
      return this.value == val;
    }).data('value');
    var code = $('#productName option').filter(function () {
      return this.value == val;
    }).data('value2');
    $('#' + trid + ' .productidget').val(id);
    $('#' + trid + ' .productcode').val(code);
    $.ajax({
      type: 'GET',
      url: '/api/optionunit/'+id,
      success: function(result){
        $('#optionunit_'+trid).empty();
        // $('#optionunit_'+trid).append('<option>กรุณาเลือก</option>');
        $.each(result, function(i, category_price){
          $('#optionunit_'+trid).append('<option value='+category_price.id+'>'+category_price.name+'</option>');
        });
      },
      error: function(e){
        console.log(e);
      }
    })

  }



  var t = [ ];
  function product(evt,trid){
    var val = $('#' + trid + ' .codePD').val();
    // if(t.length == 0){
    //   t.push(val);
    //   console.log(t);
    // }else if(t.length > 0){
    //   var c = 0;
    //   for(var i = 0;i<t.length;i++ ){
    //     if(t[i] == val){
    //       c = 1;
          
         
    //     }
    //   }
    //   if(c == 1){
    //     $('#' + trid + ' td:eq(1) input').val('');
    //     console.log(c);
    //   }
    // }
    var chack = 0;
    var tbody2 = document.getElementById('bodyRowForAddData');
    var tr2 = tbody2.getElementsByTagName('tr');
    $.each(tr2, function (i, el) {
      var getId = $(tr2[i]).attr('id');
      if (val== $('tr#' + getId).find("td:eq(1) input.codePD").val()) {
        chack++;
      }
    });
    if (chack==2) {
      $('#' + trid + ' td:eq(1) input').val('');
    }
  
    
  }
  
  function productcode(evt, trid) {
    var val = $('#' + trid + ' .productcode').val();
    var id = $('#productcode option').filter(function () {
      return this.value == val;
    }).data('value');
    var name = $('#productcode option').filter(function () {
      return this.value == val;
    }).data('value2');
    $('#' + trid + ' .productidget').val(id);
    $('#' + trid + ' .productidset').val(name);
    $.ajax({
      type: 'GET',
      url: '/api/optionunit/'+id,
      success: function(result){
        $('#optionunit_'+trid).empty();
        // $('#optionunit_'+trid).append('<option>กรุณาเลือก</option>');
        $.each(result, function(i, category_price){
          $('#optionunit_'+trid).append('<option value='+category_price.id+'>'+category_price.name+'</option>');
        });
      },
      error: function(e){
        console.log(e);
      }
    })
    
  }
  
  
  
  
  //   $('#inputpopup').keyup(function () {
  //     if ($('#inputpopup').val() === "เพิ่มรายชื่อผู้ติดต่อ") {
  //       $('#popupadd').click();
  //       $('#inputpopup').val('');
  //     }
  //   });
  
  $(document).ready(function () {
    $('#input1').hide();
    $('#input2').hide();
    $('#input3').hide();
    $('#input4').hide();
    $('#input5').hide();
    $('#input6').hide();
  })
  
  var numberdrop = 0;
  function hidedorp() {
    if (numberdrop == 6) {
      $('#dropdown').hide();
    }
  }
  
  function dropworke1() {
    $('#dropwork1').hide();
    $('#input1').show();
    numberdrop++;
    hidedorp();
  
  }
  function dropworke2() {
    $('#dropwork2').hide();
    $('#input2').show();
    numberdrop++;
    hidedorp();
  }
  function dropworke3() {
    $('#dropwork3').hide();
    $('#input3').show();
    numberdrop++;
    hidedorp();
  }
  function dropworke4() {
    $('#dropwork4').hide();
    $('#input4').show();
    numberdrop++;
    hidedorp();
  }
  function dropworke5() {
    $('#dropwork5').hide();
    $('#input5').show();
    numberdrop++;
    hidedorp();
  }
  function dropworke6() {
    $('#dropwork6').hide();
    $('#input6').show();
    numberdrop++;
    hidedorp();
  }
  
  
  // add row and run row number
  
//   var n = 2
  function plusRow() {
    $('#rowForAddData tbody').append('<tr class="border" id="row_' + n + '">' +
      '<td align="center">' +
      '<span>' + n + '</span>' +
      '</td>' +
      '<td>' +
      '<div class="input-group">' +
      '<input type="hidden" class="productidget product" name="product_id" value="" onchange="replace()">' +
      '<input list="productName" type="text" class="form-control radia productidset" placeholder="ชื่อสินค้า" onclick="productidset(event,' + "'row_" + n + "'" + ')" onchange="productidset(event,' + "'row_" + n + "'" + '),product(event,' + "'row_" + n + "'" + ')">' +
      '<div class="input-group-append">' +
      '<span class="input-group-text" id="basic-addon2">' +
      '<div class="dropdown">' +
      '<a class="dropdown-toggle" type="button" id="dropdownMenuButton1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' +
      '<i class="fa fa-ellipsis-h"></i>' +
      '</a>' +
      '<div class="dropdown-menu" aria-labelledby="dropdownMenuButton1">' +
      '<a class="dropdown-item" href="#" data-toggle="modal" data-target="#productAddForm"><i class="fa fa-plus"></i> เพิ่มรายการสินค้า</a>' +
      '</div>' +
      '</div>' +
      '</span>' +
      '</div>' +
      '</div>' +
      '  <input type="text" name="product_code" class="form-control productcode"  placeholder="รหัสสินค้า">'+
      '<input type="text" name="detail" class="form-control raida" placeholder="รายละเอียด">' +
      '</td>' +
      '<td>' +
      '<input type="text" name="quantity" class="form-control text-right" value="1.00" step="1.00" onkeyup="computeList()">' +
      '</td>' +
      '<td>' +
      ' <select class="form-control createselect" name="categoryprice_id" id="optionunit_row_' + n + '">'+
  
      '</select>'+
      '</td>' +
      '<td>' +
      '<input type="text" name="price" class="form-control text-right" value="0.00" onkeyup="computeList()">' +
      '</td>' +
      '<td class="discountlist"  >' +
      '<input type="text" class="form-control text-right" name="listdiscount" value="0.00" onkeyup="computeList()">' +
      '</td>' +
      '<td class="vatlist"  >' +
      '<select class="form-control taxinvoicelistvat" name="vat_id" onchange="computeList()">' +
  
      '</select>' +
      '</td>' +
      '<td>' +
      '<input type="text" class="form-control text-right" value="0.00" name="totallist">' +
      '</td>' +
      '<td>' +
      '<button type="button" class="btn btn-outline-danger" onclick="removeRow(row_' + n + ')"><i class="fa fa-trash"></i></button>' +
      '</td>' +
      '</tr>');
    if ($('#computevatlist').prop('checked') == false && $('#computediscountlist').prop('checked') == false) {
      $('.discountlist').prop('hidden', true);
      $('.vatlist').prop('hidden', true);
    } else if ($('#computevatlist').prop('checked') == false && $('#computediscountlist').prop('checked') == true) {
      $('.discountlist').prop('hidden', false); 
      $('.vatlist').prop('hidden', true);
    }
    for (var i = 0; i < vat.length; i++) {
      $('#row_' + n +' .taxinvoicelistvat').append('<option value="' + vat[i].number + '">' + vat[i].name + '</option>');
    }
  
    // for (var i = 0; i < category_price.length; i++) {
    //   $('#row_' + n + ' .createselect').append('<option value="' + category_price[i].id + '">' + category_price[i].name + '</option>');
    // }
    n = n + 1;
    numRow();
  }
  
  var tbody = document.getElementById('bodyRowForAddData');
  var tr = tbody.getElementsByTagName('tr');
  
  function removeRow(r) {
    if (tr.length > 1) {
      $(r).remove();
      numRow();
    }
  }
  
  function numRow() {
    $.each(tr, function (i, el) {
      var getId = $(tr[i]).attr('id');
      $('tr#' + getId).find("td:eq(0) span").remove();
      $('tr#' + getId).find("td:eq(0)").append('<span>' + (i + 1) + '</span>')
    });
  }
  
  // writting % check----------
  
  $('#discountP').on('keyup', function () {
    var dp = $('#discountP').val();
    if (dp > 100) {
      // $('#discountP').val((100).toFixed(2));
      alert('ข้อมูลที่กรอก ' + dp + '% มากก่าว 100.00 %')
    } else if (dp < 0) {
      // $('#discountP').val((0).toFixed(2));
      alert('ข้อมูลที่กรอก ' + dp + '% น้อยกว่า 0.00 %')
    } else if (isNaN(+dp)) {
      alert('ข้อมูลที่กรอก ' + dp + '% ไม่ใช่ตัวเลข')
      $('#discountP').val('');
    } else {
      // -----
    }
  });
  
  ////- รายการปรับลด
  function corectCheck(id) {
    var withholding = document.getElementById(id);
    if (withholding.checked == true) {
      $('#discountSum').prop('hidden', false);
      $('#witholdingoption').prop('hidden', false);
      $('#witholdingoption').prop('disabled', false);
    } else {
      $('#discountSum').prop('hidden', true);
      $('#witholdingoption').prop('hidden', true);
      $('#witholdingoption').prop('disabled', true);
    }
  }
  
      // function corectCheck(id){
      //   var withholding = document.getElementById(id);
      //   if (withholding.checked == true) {
      //     $('#withholdingI span').remove().end();
      //     $('#withholdingI').append(
      //       '<input class="text-right float-right" type="text" name="" value="0.00" style="width: 100px;">'
      //     );
      //     $('#discountSum').show();
      //     $('#witholdingoption').attr('hidden', false);
      //     $('#witholdingoption').attr('disabled', false);
      //   }else {
      //     $('#withholdingI input').remove().end();
      //     $('#withholdingI span').remove().end();
      //     $('#withholdingI').append('<span>0.00</span>');
      //     $('#discountSum').hide();
      //     $('#witholdingoption').attr('hidden', true);
      //     $('#witholdingoption').attr('disabled', true);
      //   }
      // }
  
      // // new card
      //   var i, tabcontent, tablinks;
      //   tabcontent = document.getElementsByClassName("tabcontent");
      //   for (i = 0; i < tabcontent.length; i++) {
      //     if (i > 0) {
      //       tabcontent[i].style.display = "none";
      //     }
      //   }
      //   function openCity(evt, tabgl) {
      //     tabcontent = document.getElementsByClassName("tabcontent");
      //     for (i = 0; i < tabcontent.length; i++) {
      //       tabcontent[i].style.display = "none";
      //     }
      //     tablinks = document.getElementsByClassName("tablinks");
      //     for (i = 0; i < tablinks.length; i++) {
      //       tablinks[i].className = tablinks[i].className.replace("active", "");
      //     }
      //     document.getElementById(tabgl).style.display = "block";
      //     evt.currentTarget.className += " active";
      //   }