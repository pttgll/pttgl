// $(document).ready(function(){
  drSum();
  crSum();
  function drSum() {
    var drsum = 0;
    //iterate through each textboxes and add the values
    $("input.drS").each(function () {
      //add only if the value is number
      if (!isNaN((this.value).replace(/,/g, '')) && ((this.value).replace(/,/g, '')).length != 0) {
        drsum += parseFloat((this.value).replace(/,/g, ''));
      }else if (((this.value).replace(/,/g, '')).length != 0) {
      }
    });
    $("input#drR").val(drsum.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
    // keepDrSum = drsum.toFixed(2);
    checkSum()
  }

  function crSum() {
    var crsum = 0;
    //iterate through each textboxes and add the values
    $("input.crS").each(function () {
      //add only if the value is number
      if (!isNaN((this.value).replace(/,/g, '')) && ((this.value).replace(/,/g, '')).length != 0) {
        crsum += parseFloat((this.value).replace(/,/g, ''));
      }else if (((this.value).replace(/,/g, '')).length != 0) {
      }
    });
    $("input#crR").val(crsum.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
    // keepDrSum = drsum.toFixed(2);
    checkSum()
  }

  function checkSum() {
    drq = +($("input#drR").val().replace(/,/g, ''));
    crq = +($("input#crR").val().replace(/,/g, ''));
    if (drq > 0 && crq > 0 && drq == crq) {
      $('button#ok').removeClass('class').attr('class', 'btn btn-success');
      $('button#ok').removeAttr("type").attr("type", "submit");
    } else {
      $('button#ok').removeClass('class').attr('class', 'btn btn-warning');
      $('button#ok').removeAttr("type").attr("type", "button");
    }
  }
// });
