// auto run number
var l = $('#debtorID').val();
if (l != "" && l != undefined) {
    var c,t;
    var s = "";
    var a = l.split(/(\d+)/);
    for (var i = 0; i < a.length; i++) {
        if (isNaN(+a[i]) == false && i < a.length-1) {
          t = ((+a[i])+1);
        }else if (isNaN(+a[i]) == true) {
          c = a[i];
        }
      }
      var len = $("#debtorID").attr('maxlength');
      if (c.length+t.toString().length < (+len)) {
        for (var i = 0; i < (len-(c.length+t.toString().length)); i++) {
          s += '0'
        }
      }
      if (c) {
        $('#debtorID').val(c+''+s+''+t);
      }else {
        $('#debtorID').val(t);
      }
  }

$('#debbranch').hide();
function branchCheck(c){
if (checkb.checked == true) {
  $('#debbranch').show();
}else {
  $('#debbranch').hide();
}
}
//////////////////////////////////////////////
if ($('#clip').attr('type') == 'file') {
$('#imga1').hide();
$('#fileDelete').hide();
}



$(document).on("input", "input:file", function(e) {
var fileName = e.target.files[0].name;
var name = fileName.split('.');
var type = name[name.length-1];
var nameicon;
if (type == "pdf") {
 nameicon = "fa fa-file-pdf";
}else if (type == "docx") {
 nameicon = "fa fa-file-word";
}else if (type == "xlsx") {
 nameicon = "fa fa-file-excel";
}else if (type == "zip") {
 nameicon = "fa fa-file-archive";
}else if (type == "jpg" || type == "png") {
 nameicon = "fa fa-file-image";
}else {
 nameicon = "fa fa-file";
}
$('#imga1').attr('class',nameicon);
$("#imga1").text(" "+fileName);
$('#fileDelete').show();
$('#forClip').attr('hidden', true);
});

$('#clip').change(function() {
if($('clip').val()){
$('#imga1').hide();
}else{
$('#imga1').show();
}
})

$('#fileDelete').on('click',function(){
$('input#clip').attr('type','file');
$('input#clip').val('');
$('#addIClass i:eq(0)').attr('class','');
$('#addIClass i:eq(0)').text('');
$('#fileDelete').hide();
$('#forClip').attr('hidden', false);
});

// for debtor edit popup

function debtorPopup(data) {
    var formTo = data.split('_')[0];
    var id = data.split('_')[1];
    if (formTo == 'edit') {
      document.adddebtor.action = '/debtor/update/'+id;
      $('h5.modal-title').text('แก้ไขข้อมูล');
    }else if(formTo == 'debtorpopupedit'){
 
    }else {
      document.adddebtor.action = '/debtor/save';
      $('h5.modal-title').text('สร้างซ้ำ');
    }
    $.ajax({
        type: "GET",
        url: "/api/debtor/edit/"+id,
        success: function (result) {
          $('input').val('');
          $('#getResultOption option').prop('selected', false);
          $('select#getAmphuresOption').empty();
          $('select#getDistrictsOption').empty();
          $('input#changPost').val('');
          $('#getResultOption2 option').prop('selected', false);
          $('select#getAmphuresOption2').empty();
          $('select#getDistrictsOption2').empty();
          $('input#changPost2').val('');
          // var amphures = "";
          $.each(result.amphures, function (i, amphures) {
            $('select#getAmphuresOption').append("<option value=" + amphures.id +">"+amphures.name_th+"</option>");
          });
          $.each(result.districts, function (i, districts) {
            $('select#getDistrictsOption').append("<option value=" + districts.id +">"+districts.name_th+"</option>");
          });
          $.each(result.amphures1, function (i, amphures1) {
            $('select#getAmphuresOption2').append("<option value=" + amphures1.id +">"+amphures1.name_th+"</option>");
          });
          $.each(result.districts1, function (i, districts1) {
            $('select#getDistrictsOption2').append("<option value=" + districts1.id +">"+districts1.name_th+"</option>");
          });
          console.log(result.debtor);
          if (result.debtor.length > 0) {
            $('#idd').val(result.debtor[0].id);
            $('#editchart_id option[value="'+result.debtor[0].chart_id+'"]').prop('selected', true);
            $('#edithuman option[value="'+result.debtor[0].human_id+'"]').prop('selected', true);
            $('#editcrday').val(result.debtor[0].crday);
            $('#editbalance').val(result.debtor[0].balance);
            $('#editcrfull').val(result.debtor[0].crfull);
            $('#editcrbalance').val(result.debtor[0].crbalance);
            $('#editdebtorID').val(result.debtor[0].debtorID);
            $('#editcompany').val(result.debtor[0].company);
            $('#edittax').val(result.debtor[0].tax);
            if (result.debtor[0].branch == "สำนักงานใหญ่") {
              $('#checka').prop('checked', true);
              $('#crebranch').hide();
            }else {
              $('#checkb').prop('checked', true);
              $('#editbranchID').val(result.debtor[0].branchID);
              $('#editbranchname').val(result.debtor[0].branchname);
            }
            $('#editaddress').val(result.debtor[0].address);
            $('#getResultOption option[value="'+result.debtor[0].provinces_id+'"]').prop('selected', true);
            $('#getAmphuresOption option[value="'+result.debtor[0].amphures_id+'"]').prop('selected', true);
            $('#getDistrictsOption option[value="'+result.debtor[0].districts_id+'"]').prop('selected', true);
            if (result.districts.length > 0) {
              for (var i = 0; i < result.districts.length; i++) {
                if (result.districts[i].id == result.debtor[0].districts_id) {
                  $('input#changPost').val(result.districts[i].zip_code);
                }
              }
            }
            $('#editaddress1').val(result.debtor[0].address1);
            $('#getResultOption2 option[value="'+result.debtor[0].provinces_id1+'"]').prop('selected', true);
            $('#getAmphuresOption2 option[value="'+result.debtor[0].amphures_id1+'"]').prop('selected', true);
            $('#getDistrictsOption2 option[value="'+result.debtor[0].districts_id1+'"]').prop('selected', true);
            if (result.districts1.length > 0) {
              for (var i = 0; i < result.districts1.length; i++) {
                if (result.districts1[i].id == result.debtor[0].districts_id1) {
                  $('input#changPost2').val(result.districts1[i].zip_code);
                }
              }
            }
            $('#editofficephone').val(result.debtor[0].officephone);
            $('#editfaxnumber').val(result.debtor[0].faxnumber);
            $('#editwebsite').val(result.debtor[0].website);
            $('#editemployee').val(result.debtor[0].employee);
            $('#editemail').val(result.debtor[0].email);
            $('#editmobile').val(result.debtor[0].mobile);
            $('#editbank_id option[value="'+result.debtor[0].bank_id+'"]').prop('selected', true);
            $('#editbankID').val(result.debtor[0].bankID);
            $('#editbankbranch').val(result.debtor[0].bankbranch);
            if (result.debtor[0].accounttype == "บัญชีออมทรัพย์") {
              $('#checkp').prop('checked', true);
            }else {
              $('#checko').prop('checked', true);
            }
            $('#editnote').text(result.debtor[0].note);
          } else {
            alert('ไม่พบข้อมูล');
          }
        },
        error: function (e) {
          alert('เกิดข้อผิดพลาด');
        }
      });
    }


// test update
$('form#debtoreditform').submit(function(event){
  var data = $(this).serializeArray();
  console.log(data);
  event.preventDefault();
});
