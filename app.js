const express = require('express');
const body = require('body-parser');
const cookie = require('cookie-parser');
const session = require('express-session');
const mysql = require('mysql');
const connection = require('express-myconnection');
const upload = require('express-fileupload');
const { v4: uuidv4 } = require('uuid');
const app = express();
var nodemailer = require('nodemailer');


app.set('view engine','ejs');
app.set('views','views');
app.engine('html', require('ejs').renderFile);

app.use(body.urlencoded({ extended: true }));
app.use(cookie());
app.use(upload());
app.use(session({
    secret:'Passw0rd',
    resave: true,
    saveUninitialized: true
}));
app.use(connection(mysql,{
    host:'database-ptt.ce9imirwsreh.ap-southeast-1.rds.amazonaws.com',
    user:'gl2021admin',
    password:'gl2021Passw0rd',
    port:3306,
    database:'general_ledger'
},'single'));

var bodyParser = require('body-parser');
app.use(bodyParser.json());

app.use(express.static('resources'));

global.__basedir = __dirname;

const db = require('./app/config/db.config.js');
require('./app/routes/amphures.route.js')(app);
require('./app/routes/districts.route.js')(app);
require('./app/routes/glsearch.route.js')(app);

app.use(express.static('public'));
app.use('/dist', express.static('dist'));

//----------------- General_Ledger_System -------------//
const homeRoute = require('./routes/General_Ledger_System/homeRoute');
app.use('/',homeRoute);

const chartRoute = require('./routes/General_Ledger_System/chartRoute');
app.use('/',chartRoute);

const company = require('./routes/General_Ledger_System/companyRoute');
app.use('/', company);

const gl = require('./routes/General_Ledger_System/glRoute');
app.use('/', gl);

const projectgl = require('./routes/General_Ledger_System/projectglRoute');
app.use('/', projectgl);

const beginning = require('./routes/General_Ledger_System/beginningRoute');
app.use('/', beginning);
//----------------- Bank -------------//
const bankRoute = require('./routes/bank/bankRoute');
app.use('/', bankRoute);
//----------------- Inventory_Control -------------//

const inventoryRoute = require('./routes/Inventory_Control/inventoryRoute');
app.use('/', inventoryRoute);

const productRoute = require('./routes/Inventory_Control/productRoute');
app.use('/', productRoute);

//------------------debtor-----------------//
const debtor = require('./routes/debtor/debtorRoute');
app.use('/', debtor);

//------------------creditor-----------------//
const creditor = require('./routes/creditor/creditorRoute');
app.use('/', creditor);

//------------------pos-----------------//
const pos = require('./routes/pos/posRoute');
app.use('/', pos);

//------------------buy-----------------//
const expenses = require('./routes/buy/expensesRoute');
app.use('/', expenses);


const purchase_order = require('./routes/buy/purchase_orderRoute');
app.use('/', purchase_order);

const product_receipt = require('./routes/buy/product_receiptRoute');
app.use('/', product_receipt);

const human_resource = require('./routes/human_resource/human_resourceRoute');
app.use('/', human_resource);

//------------------sell-----------------//
const quotation = require('./routes/sell/quotationRoute');
app.use('/', quotation);

const bill = require('./routes/sell/billRoute');
app.use('/', bill);

const tax_invoice = require('./routes/sell/tax_invoiceRoute');
app.use('/', tax_invoice);

const receipt = require('./routes/sell/receiptRoute');
app.use('/', receipt);

const cash_sale = require('./routes/sell/cash_saleRoute');
app.use('/', cash_sale);

const credit_note = require('./routes/sell/credit_noteRoute');
app.use('/', credit_note);

const debit_note = require('./routes/sell/debit_noteRoute');
app.use('/', debit_note);

// ---------------- salary management -----------------//

const salary_management = require('./routes/salary_management/salary_managementRoute');
app.use('/', salary_management);

// ---------------- asset ------------------//

const property_list = require('./Routes/asset/property_listRoute');
app.use('/', property_list);

const record_depreciation = require('./Routes/asset/record_depreciationRoute');
app.use('/', record_depreciation);

const category_asset_setting = require('./Routes/asset/category_asset_settingRoute');
app.use('/', category_asset_setting);

// ----------------- data management -----------------//

const data_management = require('./routes/data_management/data_managementRoute');
app.use('/', data_management);

// --------------- report -----------------//

const report = require('./Routes/reportRoute');
app.use('/', report);
// --------------- login -----------------//

const setting = require('./Routes/setting/settingRoute');
app.use('/', setting);
// --------------- login -----------------//

const loginRoute = require('./routes/login/loginRoute');
app.use('/', loginRoute);

var server = app.listen('8088', function () {

  var host = server.address().address
  var port = server.address().port

  console.log("App listening at http://%s%s", host, port)

});
